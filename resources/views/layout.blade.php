<!DOCTYPE html>
<html lang="es" @yield('class_html')>
	<head>
		<meta charset="utf-8"/>
		<meta name="csrf-token" content="{!! csrf_token() !!}">

		<title> @yield('title_page', 'Bienvenido a la Aplicación') </title>
		@yield('css_files')
		@yield('meta')
		<style>	
			#map_container {
				width: 100% !important;
				height:400px !important;
			}

			#map {
				width: 100% !important;
				height:400px !important;
			}

			#mapSearchContainer{
				position:fixed;
				top:20px;
				right: 40px;
				height:30px;
				width:180px;
				z-index:110;
				font-size:10pt;
				color:#5d5d5d;
				border:solid 1px #bbb;
				background-color:#f8f8f8;
			}
			.pointer{
				position:absolute;
				top:86px;
				left:60px;
				z-index:99999;
			}
		</style>

		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-145410951-1"></script>
		<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-145410951-1');
		</script>
	</head>
	<body @yield('class_body') style="background: white !important">
		<div id="app_vue"></div>
		@yield('content_body')
		@yield('js_files')
	</body>

</html>
