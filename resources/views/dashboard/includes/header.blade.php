<header class="navbar navbar-inverse navbar-fixed-top" style="background: white; border-bottom: 1px solid #F3F3F3!important">
    <!-- Left Header Navigation -->
    
    <ul class="nav navbar-nav-custom">
        <!-- Main Sidebar Toggle Button -->
        <li style="display: flex; align-items:center">
            <a href="javascript:void(0)" onclick="App.sidebar('toggle-sidebar');" id="toggler">
                <i class="fa fa-ellipsis-v fa-fw animation-fadeInRight" id="sidebar-toggle-mini"></i>
                <i class="fa fa-bars fa-fw animation-fadeInRight" id="sidebar-toggle-full"></i>
            </a>
        </li>

        @if(Auth::user()->type->id != 5 && Auth::user()->type->id != 8)
                <!-- Search Form -->
            <li class="search-form">
                {!! Form::open(['route' => 'database.voters.search', 'method' => 'get', 'class' => 'navbar-form-custom']) !!}
                    {!! Form::text('text', null, ['class' => 'form-control', 'placeholder' => 'Buscar Votante..']) !!}
                {!! Form::close() !!}
            </li>
        @endif
        <!-- END Main Sidebar Toggle Button -->

        <!-- Header Link -->
        <!--<li class="hidden-xs animation-fadeInQuick">
            <a href=""><strong>Bienvenido</strong></a>
        </li>-->
        <!-- END Header Link -->
    </ul>
    <!-- END Left Header Navigation -->

    <!-- Right Header Navigation -->
    <ul class="nav navbar-nav-custom pull-right">
        <!-- END Search Form -->

        <!-- Alternative Sidebar Toggle Button -->
        <!--<li>
            <a href="javascript:void(0)" onclick="App.sidebar('toggle-sidebar-alt');">
                <i class="gi gi-settings"></i>
            </a>
        </li>-->
        <!-- END Alternative Sidebar Toggle Button -->

        <!-- User Dropdown -->
        <li class="dropdown">
            <a href="javascript:void(0)" class="dropdown-toggle profile-header" data-toggle="dropdown" style="display: flex; align-items: center; margin-right: 10xx1xpx">
                <!--<img src="https://www.rutanmedellin.org/images/1pruebas/foto-persona.jpg" alt="Menú de Usuario">-->
                <span class="text-profile" style="padding: 0px 8px 0px 8px; color: #00C2D9 !important">{{Auth::user()->name}}</span>
                <i class="fa fa-angle-down icon-profile" aria-hidden="true" style="font-size: 20px; color: #00C2D9"></i>
            </a>
            <!--<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                <img src="/images/placeholders/avatars/usuario64.png" alt="Menú de Usuario">
            </a>-->
            <ul class="dropdown-menu dropdown-menu-right">
                <li class="dropdown-header">
                    <strong>ADMINISTRADOR</strong>
                </li>
<!--
                <li>
                    <a href="page_app_email.html">
                        <i class="fa fa-inbox fa-fw pull-right"></i>
                        Mensajes
                    </a>
                </li>
                <li>
                    <a href="page_app_social.html">
                        <i class="fa fa-pencil-square fa-fw pull-right"></i>
                        Mi Perfil
                    </a>
                </li>
                <li class="divider"><li>
-->

                @if (Auth::user()->type->can_view_all == '1')
                <li>
                    <a href="{{route('system')}}" onclick="App.sidebar('toggle-sidebar-alt');">
                        <i class="gi gi-settings fa-fw pull-right"></i>
                        Configuración
                    </a>
                </li>
                @endif

                <li>
                    <a href="javascript:void(0)" onclick="App.sidebar('toggle-sidebar-alt');">
                        <i class="gi gi-user fa-fw pull-right"></i>
                        Mi Perfil
                    </a>
                </li>
<!--
                <li>
                    <a href="page_ready_lock_screen.html">
                        <i class="gi gi-lock fa-fw pull-right"></i>
                        Bloquear
                    </a>
                </li>
-->
                <li>
                    <a href="/auth/logout">
                        <i class="fa fa-power-off fa-fw pull-right"></i>
                        Cerrar sesión
                    </a>
                </li>
            </ul>
        </li>
        <!-- END User Dropdown -->
    </ul>

    <script>
        let toggler = document.getElementById("toggler")
        toggler.addEventListener('click', togglerSide)

        function togglerSide(){
            if(window.screen.width >= 992){
                let options = document.getElementsByClassName('item-option')

                for(let option of options){
                    let styles = window.getComputedStyle(option)
                    let paddingLeft = styles.getPropertyValue('padding-left')
                    
                    if(paddingLeft == "30px"){
                        option.style.paddingLeft = '10px'
                    }else{
                        option.style.paddingLeft = '30px'
                    }
                }
            }
        }
    </script>
    <!-- END Right Header Navigation -->
    </header>