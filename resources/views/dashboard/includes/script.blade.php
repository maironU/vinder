<!-- Include Jquery library from Google's CDN but if something goes wrong get Jquery from local file (Remove 'http:' if you have SSL) -->

{!! Html::script('assets/js/vendor/jquery-2.1.3.min.js'); !!}
<!-- Bootstrap.js, Jquery plugins and Custom JS code -->
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>


{!! Html::script('assets/js/vendor/bootstrap.min.js'); !!}
{!! Html::script('assets/js/app.min.js'); !!}
{!! Html::script('assets/js/plugins.js'); !!}
{!! Html::script('assets/js/jquery.timepicker.js'); !!}
{!! Html::script('assets/js/jquery.datepair.min.js'); !!}
{!! Html::script('assets/js/admin.js'); !!}

<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/toastify-js"></script>


<script>
  $(document).ready(function() {
    $('.js-example-basic-single').select2({
        placeholder: ""
    }).on('change', function (e) {
        if(e.target.value){
            let name = e.target.id
            $("#" + name + "-error").remove()
            $(".form-group-"  + name).removeClass("has-error")
        }
    }); 
});
</script>