<style>
    @media (max-width: 756px){
        .content-filter {
            width: 310px;
            margin: 0 auto;
            flex-direction: column;
            align-items: space-between !important;
        }

        .content-filter > .form-group {
            width: 100%;
            justify-content: center !important;
            padding-left: 10px !important;
            padding-right: 10px !important;
        }

        .content-filter > .form-group .input-group {
            min-width: 200px !important;
        }

        #text-filter {
            display: block;
            align-self: center;
            margin-bottom: 20px;
        }
    }
</style>


<div class="content-filter" style="display: flex; margin-bottom: 30px; justify-content: flex-end; align-items: flex-end">
        <h4 style="margin-right: 20px" id="text-filter">Filtrar Por:</h4>
        @if(!$from_colaborators)
                <?php 
                    $users = \App\Entities\User::select('user.id', 'user.name')
                    ->where('user.type_id', 6)
                    ->lists('name', 'id')
                    ->all();
                ?>
            @if(Auth::user()->type->id != 6) 
                <div class="form-group" style="display: flex; align-items: center; padding-right: 18px; margin-bottom: 0">
                    <!--<label class="control-label" style="margin-right: 10px" for="orientador">Orientador</label>-->
                    <div style="padding-right: 0">
                        <div class="input-group" style="min-width: 150px">
                            <select class="js-example-basic-single select-chosen change-select" style="width: 100%" required="required" name="orientador" id="orientador" aria-required="true" aria-invalid="false" data-placeholder="Orientador" onchange="filterVoter()">
                                <option value=""></option>
                                @foreach($users as  $key => $value)
                                    @if(isset($request->orientador))
                                        @if($key == $request->orientador)
                                            <option value="{{$key}}" selected>{{$value}}</option>
                                        @else
                                            <option value="{{$key}}">{{$value}}</option>
                                        @endif
                                    @else
                                        <option value="{{$key}}">{{$value}}</option>
                                    @endif
                                @endforeach
                            </select>
                            <span class="input-group-addon" style="width: 15px; cursor: pointer" onclick="emptyFilter('orientador')"><i class="fa fa-remove"></i></span>
                        </div>
                    </div>
                </div>
            @endif
        @else
            <?php 
                $coordinadores = \App\Entities\Voter::where('is_coordinator', 1)
                ->lists('name', 'id')
                ->all();
            ?>
            <div class="form-group" style="display: flex; align-items: center; padding-right: 18px; margin-bottom: 0">
                <!-- <label class="control-label" style="margin-right: 10px" for="coordinador">Coordinador</label> -->
                <div style="padding-right: 0">
                    <div class="input-group" style="min-width: 150px">
                        <select class="js-example-basic-single select-chosen change-select" style="width: 100%" required="required" name="coordinador" id="coordinador" aria-required="true" aria-invalid="false" data-placeholder="Coordinador" onchange="filterVoter()">
                            <option value=""></option>
                            @foreach($coordinadores as  $key => $value)
                                @if(isset($request->coordinador))
                                    @if($key == $request->coordinador)
                                        <option value="{{$key}}" selected>{{$value}}</option>
                                    @else
                                        <option value="{{$key}}">{{$value}}</option>
                                    @endif
                                @else
                                    <option value="{{$key}}">{{$value}}</option>
                                @endif
                            @endforeach
                        </select>
                        <span class="input-group-addon" style="width: 15px; cursor: pointer" onclick="emptyFilter('coordinador')"><i class="fa fa-remove"></i></span>
                    </div>
                </div>
            </div>
        @endif

        <?php 
            $municipalities = \App\Entities\Location::select('id', 'name')
            ->where('type_id', 2)
            ->lists('name', 'id')
            ->all();
        ?>
        <div class="form-group" style="display: flex; align-items: center; padding-right: 18px; margin-bottom: 0">
            <!-- <label class="control-label" style="margin-right: 10px" for="superior">Comuna</label> -->
            <div style="padding-right: 0">
                <div class="input-group" style="min-width: 150px">
                    <select class="js-example-basic-single select-chosen change-select" style="width: 100%" required="required" name="municipio" id="municipio" aria-required="true" aria-invalid="false" data-placeholder="Municipio" onchange="filterVoter()">
                        <option value=""></option>
                        @foreach($municipalities as  $key => $value)
                            @if(isset($request->municipio))
                                @if($key == $request->municipio)
                                    <option value="{{$key}}" selected>{{$value}}</option>
                                @else
                                    <option value="{{$key}}">{{$value}}</option>
                                @endif
                            @else
                                <option value="{{$key}}">{{$value}}</option>
                            @endif
                        @endforeach
                    </select>
                    <span class="input-group-addon" style="width: 15px; cursor: pointer" onclick="emptyFilter('municipio')"><i class="fa fa-remove"></i></span>
                </div>
            </div>
        </div>

        <?php 
            $locations = \App\Entities\Location::select('id', 'name')
            ->where('type_id', 3)
            ->lists('name', 'id')
            ->all();
        ?>
        <div class="form-group" style="display: flex; align-items: center; padding-right: 18px; margin-bottom: 0">
            <!-- <label class="control-label" style="margin-right: 10px" for="superior">Comuna</label> -->
            <div style="padding-right: 0">
                <div class="input-group" style="min-width: 150px">
                    <select class="js-example-basic-single select-chosen change-select" style="width: 100%" required="required" name="comuna" id="comuna" aria-required="true" aria-invalid="false" data-placeholder="Comuna" onchange="filterVoter()">
                        <option value=""></option>
                        @foreach($locations as  $key => $value)
                            @if(isset($request->comuna))
                                @if($key == $request->comuna)
                                    <option value="{{$key}}" selected>{{$value}}</option>
                                @else
                                    <option value="{{$key}}">{{$value}}</option>
                                @endif
                            @else
                                <option value="{{$key}}">{{$value}}</option>
                            @endif
                        @endforeach
                    </select>
                    <span class="input-group-addon" style="width: 15px; cursor: pointer" onclick="emptyFilter('comuna')"><i class="fa fa-remove"></i></span>
                </div>
            </div>
        </div>

        <?php 
            $locations = \App\Entities\Location::select('id', 'name')
            ->where('type_id', 4)
            ->lists('name', 'id')
            ->all();
        ?>
        <div class="form-group" style="display: flex; align-items: center; padding-right: 18px; margin-bottom: 0">
            <!-- <label class="control-label" style="margin-right: 10px" for="superior">Barrio</label> -->
            <div style="padding-right: 0">
                <div class="input-group" style="min-width: 150px">
                    <select class="js-example-basic-single select-chosen change-select" style="width: 100%" required="required" name="barrio" id="barrio" aria-required="true" aria-invalid="false" data-placeholder="Barrio" onchange="filterVoter()">
                        <option value=""></option>
                        @foreach($locations as  $key => $value)
                            @if(isset($request->barrio))
                                @if($key == $request->barrio)
                                    <option value="{{$key}}" selected>{{$value}}</option>
                                @else
                                    <option value="{{$key}}">{{$value}}</option>
                                @endif
                            @else
                                <option value="{{$key}}">{{$value}}</option>
                            @endif
                        @endforeach
                    </select>
                    <span class="input-group-addon" style="width: 15px; cursor: pointer" onclick="emptyFilter('barrio')"><i class="fa fa-remove"></i></span>
                </div>
            </div>
        </div>
    </div>

    
    {!! Form::open(['route' => $route, 'method' => 'POST', 'class' => 'form-inline form-cont']) !!}
    
    @if(Session::get('error'))
        <div class="has-error">
            <div id="name-error" class="help-block animation-pullUp">
                {{ Session::get('error') }}
            </div>
        </div>
    @endif    
    @if(Session::get('error_delete'))
        <div class="has-error">
            <div id="name-error" class="help-block animation-pullUp">
                <?php 
                    $data = json_decode(Session::get('error_delete'));
                ?>
                {{ $data->message}}
                <a href="/database/{{Session::get('type')}}/restore/{{$data->voter->id}}"><span>Volver Activar</span></a>
            </div>
        </div>
    @endif   

    @if(Session::get('restore'))
        <div>
            <div id="name-error" class="help-block animation-pullUp">
                {{ Session::get('restore') }}
            </div>
        </div>
    @endif   
    @if($from_colaborators ==false)
        @if(Auth::user()->type->id == 1 || Auth::user()->type->id == 2 || Auth::user()->type->id == 3)
            <?php 
                $users = \App\Entities\User::select('user.id', 'user.name')
                ->where('user.type_id', 6)
                ->lists('name', 'id')
                ->all();
            ?>
            
            <div class="form-group" style="min-width:230px;">
               {{-- {!! Form::select('orientator', $users, null , ['id' => 'orientator_select', 'class' => 'js-example-basic-single select-chosen change-select','required' => 'required', 'data-placeholder' => 'Persona Jefe', 'style' => 'width:250px;']) !!} --}}

                @include('dashboard.pages.form-select.select_without_label', ["type" => "orientator", "col_div" => "col-md-12", "placeholder" => "Selecciona Orientador", "icon" => "fa fa-bars", "options" => $users])
            </div>
        @else    
            @if(Auth::user()->type->id == 6)
                <div class="form-group" style="min-width:230px;">
                    {{-- {!! Form::select('orientator', [Auth::user()->name], Auth::user()->id, ['class' => 'js-example-basic-single select-chosen change-select','required' => 'required', 'data-placeholder' => 'Persona Jefe', 'style' => 'width:250px;']) !!} --}}
                    @include('dashboard.pages.form-select.select_without_label', ["type" => "orientator", "col_div" => "col-md-12", "placeholder" => "Selecciona Orientador", "icon" => "fa fa-bars", "options" => $users, "selected" => Auth::user()->id, "disabled" => true])
                </div>
            @else
                <div class="form-group" style="min-width:230px;">
                    {{-- {!! Form::select('orientator', [Auth::user()->name], Auth::user()->id, ['class' => 'js-example-basic-single select-chosen change-select','required' => 'required', 'data-placeholder' => 'Persona Jefe', 'style' => 'width:250px;']) !!} --}}
                    @include('dashboard.pages.form-select.select_without_label', ["type" => "orientator", "col_div" => "col-md-12", "placeholder" => "Selecciona Orientador", "icon" => "fa fa-bars", "options" => $users, "selected" => Auth::user()->id])
                </div>
            @endif
        @endif

        @if(Auth::user()->type->id == 6)
            <div class="form-group" style="min-width:230px;">
                {{-- {!! Form::select('colaborator', $team, $teamSession, ['class' => 'js-example-basic-single select-chosen change-select','required' => 'required', 'data-placeholder' => 'Persona Jefe', 'style' => 'width:250px;']) !!} --}}
                @include('dashboard.pages.form-select.select_without_label', ["type" => "colaborator", "col_div" => "col-md-12", "placeholder" => "Persona Jefe", "icon" => "fa fa-bars", "options" => $team, "required", "selected" => Auth::user()->coordinator_id, "disabled" => true])
            </div>
        @else
            <div class="form-group" style="min-width:230px;">
                {{-- {!! Form::select('colaborator', $team, $teamSession, ['class' => 'js-example-basic-single select-chosen change-select','required' => 'required', 'data-placeholder' => 'Persona Jefe', 'style' => 'width:250px;']) !!} --}}
                @include('dashboard.pages.form-select.select_without_label', ["type" => "colaborator", "col_div" => "col-md-12", "placeholder" => "Persona Jefe", "icon" => "fa fa-bars", "options" => $team, "required"])
            </div>
        @endif
        
    @endif

    <div class="form-group input-cedula">
        {!! Form::number('doc', null, ['class' => 'form-control padding-md', 'placeholder' => 'Número de Cédula', 'required' => 'required']) !!}
    </div>
    <div class="form-group">
        <input type="submit" class="btn btn-primary"  value="{{ $value_submit }}">
    </div>
{!! Form::close() !!}
@yield('extra_form')

<script>

    function filterVoter(){
        let orientador = $("#orientador").val()
        let coordinador = $("#coordinador").val()
        let municipio = $("#municipio").val()
        let comuna = $("#comuna").val()
        let barrio = $("#barrio").val()

        let url = `/database/<?php echo $type ?>/?`

        if(orientador){
            url += `orientador=${orientador}&`
        }

        if(coordinador){
            url += `coordinador=${coordinador}&`
        }

        if(municipio){
            url += `municipio=${municipio}&`
        }

        if(comuna){
            url += `comuna=${comuna}&`
        }

        if(barrio){
            url += `barrio=${barrio}`
        }

        location.href = url
    }

    function emptyFilter(type){
        if($("#" + type).val()){
            $("#" + type).val("")
            filterVoter()
        }
    }
</script>