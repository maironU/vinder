
<!-- Second Step -->
<div id="validation-second" class="step">
    <!-- Step Info -->
    <div class="form-group hidden-xs">
        <div class="col-xs-12">
            <ul class="nav nav-pills nav-justified clickable-steps">
                <li><a href="javascript:void(0)" data-gotostep="validation-first" class="text-muted"> <i class="fa fa-user"></i> <strong>Principales</strong></a></li>
                <li class="active"><a href="javascript:void(0)" data-gotostep="validation-second"><i class="fa fa-info-circle"></i> <strong>Adicionales</strong></a></li>
                <li><a href="javascript:void(0)" data-gotostep="validation-third"><i class="fa fa-info-circle"></i> <strong>Segmentación</strong></a></li>
                <li><a href="javascript:void(0)" data-gotostep="validation-fourt"><i class="fa fa-info-circle"></i> <strong>Notas</strong></a></li>
            </ul>
        </div>
    </div>
    <!-- END Step Info -->
    @include('dashboard.pages.form-select.select', ["type" => "location_id", "label" => "Ubicación", "placeholder" => "Seleccione una Ubicación", "icon" => "fa fa-bars", "options" => $locations, "selected" => $voter->location_id])

    {{--{!!  Field::select('location_id', $locations , $voter->location_id, ['template' => 'horizontal-large', 'data-placeholder' => 'Seleccione una ubicación', 'required' => 'required']) !!} --}}

    {!!  Field::text('address' , null, ['template' => 'horizontal-large', 'placeholder' => 'Dirección de Vivienda']) !!} 

<!--
    {{ Field::text('telephone' , null, ['template' => 'horizontal-large', 'placeholder' => 'Celular', 'required' => 'required'])  }} 
    {{ Field::text('telephone_alternative' , null, ['template' => 'horizontal-large', 'placeholder' => 'Celular alterno', 'checked' => 'true']) }} 
-->
    <!-- Línea telefónica principal -->
    <div class="form-group">
        <div class="input-group col-md-12">
            <label for="telephone" class="col-md-3 control-label">Teléfono principal</label>
            <div class="col-md-8">
                <div class="input-group">
                    <span class="input-group-addon" tooltip="La línea está habilitada para comunicación a través de whatsapp">
                        <i class="fa fa-whatsapp"></i>
                        <input type="checkbox" class="ui-wizard-content" id="check_primary" onclick="checkPrimary()" <?php if($voter->whatsapp == $voter->telephone) echo "checked"; ?> >
                    </span>
                    <input id="text_telephone" name="telephone" type="text" placeholder="Teléfono principal" aria-required="true" value="{{$voter->telephone}}" class="form-control ui-wizard-content">
                </div>
            </div>
        </div>
    </div>

    <!-- Línea telefónica alterna -->
    <div class="form-group">
        <div class="input-group col-md-12">
            <label for="telephone_alternative" class="col-md-3 control-label">Teléfono secundario </label>
            <div class="col-md-8">
                <div class="input-group">
                    <span class="input-group-addon" tooltip="La línea está habilitada para comunicación a través de whatsapp">
                        <i class="fa fa-whatsapp"></i>
                        <input type="checkbox" class="ui-wizard-content" id="check_secondary" onclick="checkSecondary()" <?php if($voter->whatsapp == $voter->telephone_alternative) echo "checked"; ?>>
                    </span>
                    <input id="text_telephone_alternative" name="telephone_alternative" type="text" placeholder="Teléfono alternativo" aria-required="true" value="{{$voter->telephone_alternative}}"class="form-control ui-wizard-content">
                </div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="input-group col-md-12">
            <label for="telephone" class="col-md-3 control-label">Casa marcada</label>
            <div class="col-md-8">
                <div class="input-group" style="display: flex">
                    <span>
                        <i class="fa fa-home" style="margin-right: 5px; align-items:center"></i>
                        <input type="checkbox" class="ui-wizard-content" id="house_check" name="house_check" <?php if($voter->house_check) echo "checked"; ?> >
                    </span>
                </div>
            </div>
        </div>
    </div>

    {!! Form::hidden('whatsapp', $voter->whatsapp,['id'=>'whatsapp']) !!}

    <!-- Facebook -->
    <div class="form-group">
        <label class="col-md-3 control-label" for="facebook_profile">Facebook</label>
        <div class="col-md-8">
            <div class="input-group">
                {!! Form::text('facebook_profile', $voter->facebook_profile, ['class' => 'form-control ui-wizard-content', 'template' => 'horizontal-large', 'placeholder' => 'URL del perfil en Facebook']) !!}
                <span class="input-group-addon"><i class="fa fa-facebook"></i></span>
            </div>
        </div>
    </div>

    <!-- Instagram -->
    <div class="form-group">
        <label class="col-md-3 control-label" for="instagram_profile">Instagram</label>
        <div class="col-md-8">
            <div class="input-group">
                {!! Form::text('instagram_profile', $voter->instagram_profile, ['class' => 'form-control ui-wizard-content', 'template' => 'horizontal-large', 'placeholder' => 'URL del perfil en Instagram']) !!}
                <span class="input-group-addon"><i class="fa fa-instagram"></i></span>
            </div>
        </div>
    </div>

    {!!  Field::email('email' , $voter->email, ['template' => 'horizontal-large', 'placeholder' => 'Correo electrónico']) !!} 

    {!!  Field::text('date_of_birth' , $voter->date_of_birth, ['template' => 'horizontal-large', 'placeholder' => 'año-mes-día', 'data-date-format' => 'yyyy-mm-dd', 'class' => 'input-datepicker']) !!} 

</div>
<!-- END Second Step -->

<!-- Third Step -->
<div id="validation-third" class="step">
    <!-- Step Info -->
    <div class="form-group hidden-xs">
        <div class="col-xs-12">
            <ul class="nav nav-pills nav-justified clickable-steps">
                <li><a href="javascript:void(0)" data-gotostep="validation-first" class="text-muted"> <i class="fa fa-user"></i> <strong>Principales</strong></a></li>
                <li><a href="javascript:void(0)" data-gotostep="validation-second"><i class="fa fa-info-circle"></i> <strong>Adicionales</strong></a></li>
                <li class="active"><a href="javascript:void(0)" data-gotostep="validation-third"><i class="fa fa-info-circle"></i> <strong>Segmentación</strong></a></li>
                <li><a href="javascript:void(0)" data-gotostep="validation-fourt"><i class="fa fa-info-circle"></i> <strong>Notas</strong></a></li>
            </ul>
        </div>
    </div>
    <!-- END Step Info -->
    @include('dashboard.pages.form-select.select', ["type" => "occupation", "label" => "Ocupación", "placeholder" => "Seleccione una Ocupación", "icon" => "fa fa-bars", "options" => $occupations, "selected" => $voter->occupation])

    {!!  Field::text('new-occupation' , '', ['template' => 'horizontal-large', 'placeholder' => 'Agregar nueva Ocupación']) !!}

    <div class="form-group form-group-communities">
        <label for="communities" class="col-md-3 control-label">Comunidades</label>
        <div class="col-md-8">
            <div class="input-group">
                <select class="js-example-basic-single select-chosen change-select" multiple style="width: 100%"  id="communities" name="communities[]" aria-required="true" aria-invalid="false" data-placeholder="Seleccione una Comunidad">
                    <option value=""></option>
                    @foreach($communities as  $key => $value)
                        @if($voter->communities_list && count($voter->communities_list) > 0)
                            @foreach($voter->communities_list as $selected)
                                @if($key == $selected)
                                    <option value="{{$key}}" selected>{{$value}}</option>
                                @else
                                    <option value="{{$key}}">{{$value}}</option>
                                @endif
                            @endforeach
                        @else
                            <option value="{{$key}}">{{$value}}</option>
                        @endif
                    @endforeach
                </select>
                <span class="input-group-addon"><i class="fa fa-bars"></i></span>
            </div>
        </div>
    </div>

    {!!  Field::text('new-communities' , null, ['template' => 'horizontal-large', 'class' => 'input-tags']) !!} 


</div>
<!-- END Third Step -->

<!-- Fourt Step -->
<div id="validation-fourt" class="step">
    <!-- Step Info -->
    <div class="form-group hidden-xs">
        <div class="col-xs-12">
            <ul class="nav nav-pills nav-justified clickable-steps">
                <li><a href="javascript:void(0)" data-gotostep="validation-first" class="text-muted"> <i class="fa fa-user"></i> <strong>Principales</strong></a></li>
                <li><a href="javascript:void(0)" data-gotostep="validation-second"><i class="fa fa-info-circle"></i> <strong>Adicionales</strong></a></li>
                <li><a href="javascript:void(0)" data-gotostep="validation-third"><i class="fa fa-info-circle"></i> <strong>Segmentación</strong></a></li>
                <li class="active"><a href="javascript:void(0)" data-gotostep="validation-fourt"><i class="fa fa-info-circle"></i> <strong>Notas</strong></a></li>
            </ul>
        </div>
    </div>
    <!-- END Step Info -->

    <!-- Estado del votante -->
    <div class="form-group">
        <div class="input-group col-md-12">
            <label for="telephone_alternative" class="col-md-3 control-label">Estado del votante</label>
            <div class="col-md-8">
                <div class="input-group" style="font-weight: normal important;">
                    @foreach([[0,'Por definir','silver'], [1,'Voto fijo','green'], [2,'Indeciso','orange'], [3,'Voto duro', 'red']] as $st)
                    <div class="col-md-3">
                        <p>
                            <svg style="width: 15px" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-circle fa-w-16 fa-lg">
                                <path fill="{!! $st[2] !!}" d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8z" class=""></path>
                            </svg>
                            {!! Form::radio('semaphore',$st[0],$st[0] == $voter->semaphore ? true : false,['class'=>'radio-inline with-gap', 'style' => 'background-color:red']) !!}
                            {!! Form::label('status',$st[1], ['style' => 'font-weight:normal;'] ) !!}
                        </p>  
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    
    <!-- Descripción y datos adicionales -->
    {!!  Field::textarea('description' , null, ['template' => 'horizontal-large', 'placeholder' => 'Descripción y datos adicionales']) !!} 

</div>
<!-- END Fourt Step -->