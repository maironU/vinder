@extends('dashboard.pages.layout')
@section('content_body_page')
<style>
    @media (max-width: 460px){
        .select2-container .select2-choice>.select2-chosen {
            max-width: 220px;
        }
    }
</style>
	<div class="row">
	    <div class="col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
            <img src="/images/placeholders/icons/loading.gif" id="loading-polling-station-gif" style="display:none;"> 
            <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />

            <div class="widget-image widget-image-xs" id="polling_station" style="">
                <img src="/images/placeholders/photos/photo16.jpg" alt="image">
                <div class="widget-image-content">
                    <h2 class="widget-heading text-light"><strong id="polling_station_location_name"></strong></h2>
                    <h3 class="widget-heading text-light-op h4" id="polling_station_name_and_table"></h3>
                    <h3 class="widget-heading text-light-op h4" id="polling_station_address"></h3>
                    <h3 class="widget-heading text-light h3" style="margin:2px;" id="polling_station_message"></h3>
                </div>
                <i class="fa fa-newspaper-o"></i>
            </div>

	        <div class="block">
	            <div class="block-title">
	                <h2>Datos del Votante</h2>
                    @if($voter->exists)
                        <div class="pull-right">
                            @if ($voter->colaborator)
                                <a href="{{route('database.team.remove', $voter->doc)}}" data-toggle="tooltip" title="Sacar del Equipo" class="btn btn-effect-ripple btn-warning">
                                    <i class="fa fa-exclamation-triangle"></i>
                                </a>
                            @elseif (Auth::user()->hasModule('add-to-team-voters'))
                                <a href="{{route('database.voters.add-to-team', $voter->id)}}" data-toggle="tooltip" title="Agregar al Equipo" class="btn btn-effect-ripple btn-success">
                                    <i class="fa fa-user-plus"></i>
                                </a>
                            @endif
                        </div>
                    @endif
	            </div>

                @include('dashboard.includes.alerts')
                
                {!! Form::model($voter, $form_data + array('id' => 'form-voters', 'class' => 'form-horizontal form-bordered')) !!}
                    <!-- Validation Wizard Content -->
                    <!-- First Step -->
                    <div id="validation-first" class="step">
                        <!-- Step Info -->
                        <div class="form-group hidden-xs">
                            <div class="col-xs-12">
                                <ul class="nav nav-pills nav-justified clickable-steps">
                                    <li class="active"><a href="javascript:void(0)" class="text-muted"> <i class="fa fa-user"></i> <strong>Principales</strong></a></li>
                                    <li><a href="javascript:void(0)" data-gotostep="validation-second"><i class="fa fa-info-circle"></i> <strong>Adicionales</strong></a></li>
                                    <li><a href="javascript:void(0)" data-gotostep="validation-third"><i class="fa fa-info-circle"></i> <strong>Segmentación</strong></a></li>
                                    <li><a href="javascript:void(0)" data-gotostep="validation-fourt"><i class="fa fa-info-circle"></i> <strong>Notas</strong></a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- END Step Info -->

                        @if(! $voter->result_scraping)
<!--                            {!! Form::input('hidden', 'polling_station_id', null) !!} -->
                            {!! Form::input('hidden', 'table_number', null) !!}
                        @endif

                        {!! Field::text('doc', null, ['template' => 'horizontal-large', 'placeholder' => 'Número de cédula', 'readonly']) !!}

                        @if($from_colaborators ==false) 
                            @if(Auth::user()->type->id == 6)
                                @include('dashboard.pages.form-select.select', ["type" => "superior", "label" => "Coordinador", "placeholder" => "Coordinador", "icon" => "gi gi-user", "options" => $team, "selected" => $voter->team_session, "disabled" => true])
                            @else
                                @include('dashboard.pages.form-select.select', ["type" => "superior", "label" => "Coordinador", "placeholder" => "Coordinador", "icon" => "gi gi-user", "options" => $team, "selected" => $voter->team_session])
                            @endif
                            
                            <!--Orientador-->
                            @if(Auth::user()->type->id == 1 || Auth::user()->type->id == 2 || Auth::user()->type->id == 3)
                                <?php 
                                    $users = \App\Entities\User::select('user.id', 'user.name')
                                    ->where('user.type_id', 6)
                                    ->lists('name', 'id')
                                    ->all();
                                ?>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" for="superior">Orientador<span class="text-danger">*</span></label>
                                    <div class="col-md-8">
                                        <div class="input-group">
                                                {!! Form::select('orientator', $users, $voter->orientador_session, ['class' => 'form-control select-chosen','required' => 'required']) !!}
                                            <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                        </div>
                                    </div>
                                </div>
                            @else
                                @if(Auth::user()->type->id == 6)
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="superior">Orientador<span class="text-danger">*</span></label>
                                        <div class="col-md-8">
                                            <div class="input-group">
                                                    {!! Form::select('orientator', [Auth::user()->name], Auth::user()->id, ['class' => 'form-control select-chosen','required' => 'required', 'disabled']) !!}
                                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" name="orientator" value={{Auth::user()->id}}>
                                @else
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="superior">Orientador<span class="text-danger">*</span></label>
                                        <div class="col-md-8">
                                            <div class="input-group">
                                                    {!! Form::select('orientator', [Auth::user()->name], Auth::user()->id, ['class' => 'form-control select-chosen','required' => 'required']) !!}
                                                <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endif
                        @endif

                        {!!  Field::text('name' , null, ['required', 'template' => 'horizontal-large-loading', 'placeholder' => 'Nombres y apellidos']) !!}

                        @yield('inputs-before')
                        {{--{!!  Field::select('sex', array('M' => 'Masculino', 'F' => 'Femenino') , null, ['template' => 'horizontal-large', 'data-placeholder' => 'Seleccione un Género', 'required']) !!} --}} 

                        <!-- Género -->
                        @include('dashboard.pages.form-select.select', ["type" => "sex", "label" => "Género", "placeholder" => "Selecciona un Género", "icon" => "fa fa-bars", "options" => ["M" => "Masculino", "F" => "Femenino"], "selected" => $voter->sex])

                        <!-- Puesto de votación -->
                        @include('dashboard.pages.form-select.select', ["type" => "polling_station_id", "label" => "Puesto de Votación", "placeholder" => "Puesto de Votación", "icon" => "fa fa-bars", "options" => $polling_stations, "selected" => $voter->polling_station_id])

                        <!-- Puesto de votación -->
                        {{-- {!!  Field::select('polling_station_id', $polling_stations, $voter->polling_station_id,['template' => 'horizontal-large', 'data-placeholder' => 'Puesto de votación', 'required' => 'required']) !!}--}}

                        <!-- MESA DE VOTACIÓN -->
                        {!!  Field::number('Mesa de votación', $voter->table_number, ['name' => 'table_number', 'class' => 'h-100 form-control', 'template' => 'horizontal-large', 'data-placeholder' => 'Mesa de votación', 'required' => 'required' ]) !!}

                    </div>
                    <!-- END First Step -->
                        
                    @include('dashboard.pages.database.voters.forms.inputs-default')
                    
                    @yield('inputs-after')

                    <!-- Form Buttons -->
                    <div class="form-group form-actions text-right">
                        <div class="col-md-8 col-md-offset-4">
                            <input type="reset" class="btn btn-warning" id="back3" value="anterior">
                            <input type="submit" class="btn btn-primary" id="next3" value="siguiente">
                        </div>
                    </div>
                    <!-- END Form Buttons -->

	            {!! Form::close() !!}

                <script>
                    function checkPrimary() {
                        if (document.getElementById("check_primary").checked) {
//                            document.getElementById("telephone").attributes["required"] = "required";
//                            document.getElementById("telephone_alternative").attributes["required"] = "";

                            document.getElementById("whatsapp").value = document.getElementById("text_telephone").value;
                            document.getElementById("check_secondary").checked = false;
                        }
                        if (document.getElementById("check_primary").checked == false && document.getElementById("check_secondary").checked == false) {
                            document.getElementById("whatsapp").value = "";
                        }
                    }

                    function checkSecondary() {
                        if (document.getElementById("check_secondary").checked) {
//                            document.getElementById("telephone_alternative").attributes["required"] = "required";
//                            document.getElementById("telephone").attributes["required"] = "";

                            document.getElementById("whatsapp").value = document.getElementById("text_telephone_alternative").value;
                            document.getElementById("check_primary").checked = false;
                        }
                        if (document.getElementById("check_primary").checked == false && document.getElementById("check_secondary").checked == false) {
                            document.getElementById("whatsapp").value = "";
                        }
                    }
                </script>


                @include('dashboard.includes.alerts')

	        </div>
	    </div>

	</div>
@endsection

@section('js_aditional')
	{!! Html::script('assets/js/pages/formVoters.js') !!}
	<!-- Load and execute javascript code used only in this page -->
    <script> $(function(){ FormVoters.init(); });</script>
    <script> $(function(){ FormVoters.findNameAndPollingStation(); });</script>
    
@endsection
