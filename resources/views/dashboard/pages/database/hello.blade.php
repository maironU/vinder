@extends('dashboard.pages.layout')

@section('title_page')
    Base de Datos
@endsection

@section('breadcrumbs')
	{!! Breadcrumbs::render('database') !!}
@endsection

@section('content_body_page')
    <div class="row" style="margin : 0">
        @foreach($modules as $module)
            <a href="/{{ $module->url }}">
                <div class="col-sm-4" style="padding: 0">
                    <img src="{{$module->image}}" width="100%" alt="" style="padding: 10px">
                    <img class="image-back" src="{{$module->image_back}}" width="100%" alt="" style="position: absolute; left: 0; border-radius: 30px; height: 100%; padding: 10px;object-fit: cover">
                    <div class="card-module-text">
                        <i class="{{$module->icon_class}} icon-text"></i>
                        <span>{{$module->description}}</span>
                    </div>
                </div>
            </a>
        @endforeach
        
        <div class="col-md-12 hidden-xs">
            {!! Html::image('images/placeholders/banners/info_databases.png', 'Base de Datos - Vinder', array('style' => 'position:relative; width:70%; display:block; margin-left: auto; margin-right:auto;')) !!}
        </div>
    </div>
@endsection