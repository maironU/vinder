<th title="{{$title}}" onclick="orderTable(this)" style="cursor: pointer">
   <div class="th-order">
        <span>{{$description}}</span>
        <div class="th-order parent-images-order">
            <img src="{{asset('images/arrow-top-black.png')}}" id="arrow-top" alt="">
            <img src="{{asset('images/arrow-down-black.png')}}" id="arrow-down" alt="">
        </div>
   </div>
</th>
