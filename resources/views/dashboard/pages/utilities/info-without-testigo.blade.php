<tr style="cursor: pointer" data-idbdd="{{$user->id}}" data-id="{{$seq}}" class="polling-items close-polling polling-station-{{$user->id}} {{$seq > 1 ? 'display-none' : 'display-content'}} {{$seq == 1 || $num_tables == $seq ? 'border-ddd' : ''}}">
    <td onclick="{{$seq == 1 ? 'expandOrClose('.$user->id.')' : ''}}" style="display: flex; width: 100%; height: 60px; border: 0"> 
        @if($seq == 1)
            <img title="Expandir" id="img-{{$user->id}}" onclick="expandOrClose('{{$user->id}}')" src="{{asset('images/arrow-bottom.png')}}" style="margin-right: 10px; cursor-pointer" width="20px" height="20px" alt="">
        @endif

        <span>
            {{ $user->name }} 
        </span>
    </td>
    <td onclick="{{$seq == 1 ? 'expandOrClose('.$user->id.')' : ''}}">{{$user->pollingStation->name}}</td>
    <td onclick="{{$seq == 1 ? 'expandOrClose('.$user->id.')' : ''}}">{{$user->pollingStation->address}}</td>
    <td onclick="{{$seq == 1 ? 'expandOrClose('.$user->id.')' : ''}}">{{$seq}}</td>
    
    <td onclick="{{$seq == 1 ? 'expandOrClose('.$user->id.')' : ''}}" style="border: 0" class="text-center">
        <span>No hay testigo para la mesa</span>
    </td>

    <td onclick="{{$seq == 1 ? 'expandOrClose('.$user->id.')' : ''}}" style="border: 0" class="text-center">
        <span>No hay testigo para la mesa</span>
    </td>

    <td onclick="{{$seq == 1 ? 'expandOrClose('.$user->id.')' : ''}}" style="border: 0" class="text-center">
        <a href="{{ route('coordinator.edit', $user->id)}}" data-toggle="tooltip" title="Editar tipo de usuario" style="height: fit-content" class="btn btn-effect-ripple btn-warning">
            <i class="fa fa-pencil"></i>
        </a>
    </td>

    <td onclick="{{$seq == 1 ? 'expandOrClose('.$user->id.')' : ''}}" style="border: 0" class="text-center">
        <span>No hay testigo para la mesa</span>
    </td>

    <td class="text-center">
        <span>No hay testigo para la mesa</span>
    </td>
</tr>