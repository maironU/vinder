<div style="padding-right: 0;margin-left: 10px">
    <div class="input-group" style="min-width: 150px">
        <select class="js-example-basic-single select-chosen change-select" multiple style="width: 100%" required="required" name="zone[]" id="zone" aria-required="true" aria-invalid="false" data-placeholder="Zona" onchange="filter()">
            <option value=""></option>
            @foreach($zones as  $key => $zone)
                @if(isset($request->zone))
                <?php $zones_request = json_decode($request->zone) ?>
                    @if(in_array($zone->id, $zones_request))
                        <option selected value="{{$zone->id}}">{{$zone->name}}</option>
                    @else
                        <option value="{{$zone->id}}">{{$zone->name}}</option>
                    @endif
                @else
                    <option value="{{$zone->id}}">{{$zone->name}}</option>
                @endif
            @endforeach
        </select>
        <span class="input-group-addon" style="width: 15px; cursor: pointer" onclick="emptyFilter('zone')"><i class="fa fa-remove"></i></span>
    </div>
</div>