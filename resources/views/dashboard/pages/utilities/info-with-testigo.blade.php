<tr style="cursor: pointer" data-idbdd="{{$user->id}}" data-id="{{$seq}}" class="polling-items close-polling polling-station-{{$user->id}} {{$seq > 1 ? 'display-none' : 'display-content'}} {{$seq == 1 || $num_tables == $seq ? 'border-ddd' : ''}}">
    <td onclick="{{$seq == 1 ? 'expandOrClose('.$user->id.')' : ''}}" style="display: flex; width: 100%; height: 60px; border: 0"> 
        @if($seq == 1)
            <img title="Expandir" id="img-{{$user->id}}" onclick="expandOrClose('{{$user->id}}')" src="{{asset('images/arrow-bottom.png')}}" style="margin-right: 10px; cursor-pointer" width="20px" height="20px" alt="">
        @endif

        <span>
            {{ $user->name }} 
        </span>
    </td>
    <td onclick="{{$seq == 1 ? 'expandOrClose('.$user->id.')' : ''}}">{{$user->pollingStation->name}}</td>
    <td onclick="{{$seq == 1 ? 'expandOrClose('.$user->id.')' : ''}}">{{$user->pollingStation->address}}</td>
    <td onclick="{{$seq == 1 ? 'expandOrClose('.$user->id.')' : ''}}">{{$seq}}</td>
    @if($testigoElectoral)
        <td onclick="{{$seq == 1 ? 'expandOrClose('.$user->id.')' : ''}}">{{$testigoElectoral->name}}</td>
    @else
        <td onclick="{{$seq == 1 ? 'expandOrClose('.$user->id.')' : ''}}" style="border: 0" class="text-center">
            <span>No hay testigo para la mesa</span>
        </td>
    @endif

    @if($testigoElectoral)
        <?php 
            $file = $form
                ->files()
                ->where('user_id', $testigoElectoral->id)
                ->where('table_number', $seq)
                ->first();
        ?>
        <td style="text-align: center">
            @if($file)
                @if($file->file)
                    <a target="_blank" href="{{asset('files_forme14/'.$file->file)}}" data-toggle="tooltip" title="Ver Formulario" class="btn btn-effect-ripple btn-warning" style="width: fit-content">
                        <i class="fa fa-eye"></i>
                    </a>
                @endif
                @if($file->file_secondary)
                    <a target="_blank" href="{{asset('files_forme14/'.$file->file_secondary)}}" data-toggle="tooltip" title="Ver Formulario Secundario" class="btn btn-effect-ripple btn-warning" style="width: fit-content">
                        <i class="fa fa-eye"></i>
                    </a>
                @endif
            @endif
            <a href="{{url("witnesses/editform/e14/$form->id/$seq/$testigoElectoral->id")}}" data-toggle="tooltip" title="Editar formulario" class="btn btn-effect-ripple btn-warning" style="width: fit-content">
                <i class="fa fa-pencil"></i>
            </a>
        </td>
    @else
        <td onclick="{{$seq == 1 ? 'expandOrClose('.$user->id.')' : ''}}" style="border: 0" class="text-center">
            <span>No hay testigo para la mesa</span>
        </td>
    @endif

    @if($testigoElectoral)
        <td onclick="{{$seq == 1 ? 'expandOrClose('.$user->id.')' : ''}}" style="border: 0" class="text-center">
            <a href="{{ route('coordinator.edit', $user->id)}}" data-toggle="tooltip" title="Editar tipo de usuario" style="height: fit-content" class="btn btn-effect-ripple btn-warning">
                <i class="fa fa-pencil"></i>
            </a>
        </td>
    @else
        <td style="border: 0" class="text-center">
            <span>No hay testigo para la mesa</span>
        </td>
    @endif

    @if($testigoElectoral)
        @if($request->check)
            <td style="text-align: center">
                @if($request->check == "verde")
                    <img width="35px" src="{{asset('images/check.png')}}" alt="" style="cursor: pointer"  data-toggle="modal" data-target="#exampleModalLong" onclick="getFormE14('{{$form->id}}', '{{$seq}}', '{{$testigoElectoral->id}}')">
                @elseif($request->check == "naranja")
                    <img width="35px" src="{{asset('images/check_orange.png')}}" alt="" style="cursor: pointer"  data-toggle="modal" data-target="#exampleModalLong" onclick="getFormE14('{{$form->id}}', '{{$seq}}', '{{$testigoElectoral->id}}')">
                @else
                    <div style="width: 35px; height:35px; border-radius: 50%; border: 1px solid #141313; margin: 0 auto; cursor: pointer"  data-toggle="modal" data-target="#exampleModalLong" onclick="getFormE14('{{$form->id}}', '{{$seq}}', '{{$testigoElectoral->id}}')"></div>
                @endif
            </td>
        @else
            <td style="text-align: center">
                <?php 
                    $color = $form->colors()->color($testigoElectoral->id, $seq)->first()->color ?? "blanca";
                ?>
                @if($color == "verde")
                    <img width="35px" src="{{asset('images/check.png')}}" alt="" style="cursor: pointer"  data-toggle="modal" data-target="#exampleModalLong" onclick="getFormE14('{{$form->id}}', '{{$seq}}', '{{$testigoElectoral->id}}')">
                @elseif($color == "naranja")
                    <img width="35px" src="{{asset('images/check_orange.png')}}" alt="" style="cursor: pointer"  data-toggle="modal" data-target="#exampleModalLong" onclick="getFormE14('{{$form->id}}', '{{$seq}}', '{{$testigoElectoral->id}}')">
                @else
                    <div style="width: 35px; height:35px; border-radius: 50%; border: 1px solid #141313; margin: 0 auto; cursor: pointer"  data-toggle="modal" data-target="#exampleModalLong" onclick="getFormE14('{{$form->id}}', '{{$seq}}', '{{$testigoElectoral->id}}')"></div>
                @endif
            </td>
        @endif
    @else
        <td onclick="{{$seq == 1 ? 'expandOrClose('.$user->id.')' : ''}}" style="border: 0" class="text-center">
            <span>No hay testigo para la mesa</span>
        </td>
    @endif

    @if($testigoElectoral)
        @include('dashboard.pages.utilities.filed_claim', ['testigo' => $testigoElectoral, 'table_number' => $seq, 'forme14_id' => $form->id])
    @else
        <td class="text-center">
            <span>No hay testigo para la mesa</span>
        </td>
    @endif
</tr>