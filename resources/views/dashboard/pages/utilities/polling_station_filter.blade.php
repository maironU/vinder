<div style="padding-right: 0">
    <div class="input-group" style="min-width: 150px">
        <select class="js-example-basic-single select-chosen change-select" style="width: 100%" required="required" name="Puesto de votación" id="polling_station" aria-required="true" aria-invalid="false" data-placeholder="Puesto de Votación" onchange="filter()">
            <option value=""></option>
            @foreach($polling_stations as  $key => $value)
                @if(isset($request->polling_station))
                    @if($key == $request->polling_station)
                        <option value="{{$key}}" selected>{{$value}}</option>
                    @else
                        <option value="{{$key}}">{{$value}}</option>
                    @endif
                @else
                    <option value="{{$key}}">{{$value}}</option>
                @endif
            @endforeach
        </select>
        <span class="input-group-addon" style="width: 15px; cursor: pointer" onclick="emptyFilter('polling_station')"><i class="fa fa-remove"></i></span>
    </div>
</div>