<?php 
    $filed_claim = \App\Entities\FormE14FiledClaim::where('forme14_id', $forme14_id)
        ->where('user_id', $testigo->id)
        ->where('table_number', $table_number)
        ->first();

        $obj_filed_claims = [
            "no" => "No hubo reclamación",
            "otro_candidato" => "Otro candidato",
            "excedio" => "Excedio el número de votos de la mesa",
            "error_aritmetico" => "Error aritmético",
            "error_nombre" => "Error en el nombre del candidato E14",
            "firmas_incompletas" => "Firmas incompletas",
            "1" => "SI",
            "0" => "No hubo reclamación"
];
?>
@if($filed_claim)
    <td class="text-center">{{$obj_filed_claims[$filed_claim->filed_claim]}}</td>
@else
    <td class="text-center">{{$obj_filed_claims["no"]}}</td>
@endif
