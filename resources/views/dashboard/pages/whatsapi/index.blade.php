@extends('dashboard.pages.layout')
@section('title_page', 'Administrar envíos de mensajes')

@section('content_body_page')
    <div class="row">
        <div class="col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
            <div class="block">
                <div class="block-title">
                    <h2>CONFIGURACIÓN MENSAJE BIENVENIDA</h2>
                </div>
                <form action="{{route('whatsapi.store')}}" method="POST" enctype="multipart/form-data" class="form-horizontal form-bordered">
                    <input type="hidden" name="title" id="title" value="bienvenida">
                    <div class="form-group">
                        <label for="from_id" class="col-md-4 control-label">Telefóno que hará el envío</label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <select class="js-example-basic-single select-chosen change-select" style="width: 100%" name="from_id" id="from_id" required data-placeholder="Número de teléfono">
                                    <option value=""></option>
                                    @if(isset($phones['data']) && count($phones['data']) > 0)
                                        @foreach($phones['data'] as $phone)
                                            @if($config_welcome && $phone['id'] == $config_welcome->from_id)
                                                <option selected value="{{$phone['id']}}">{{$phone['display_name']}} --- {{$phone['phone_number']}}</option>
                                            @else
                                                <option value="{{$phone['id']}}">{{$phone['display_name']}} --- {{$phone['phone_number']}}</option>
                                            @endif
                                        @endforeach
                                    @endif
                                </select>
                                <span class="input-group-addon"><i class="fa fa-bars"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="plantilla_id" class="col-md-4 control-label">Escoge la plantilla</label>
                        <div class="col-md-6">
                            <div class="input-group" style="width: 100%">
                                <select style="width: 100%" class="js-example-basic-single select-chosen change-select" style="width: 100%" required="required" name="plantilla_id" id="plantilla_id" data-placeholder="Plantilla">
                                    <option value=""></option>
                                    @if(isset($templates['data']) && count($templates['data']) > 0)
                                        @foreach($templates['data'] as $templat)
                                            @if($config_welcome && $templat['uuid'] == $config_welcome->plantilla_id)
                                                <option selected value="{{$templat['uuid']}}">{{$templat['template_id']}}</option>
                                            @else
                                                <option value="{{$templat['uuid']}}">{{$templat['template_id']}}</option>
                                            @endif
                                        @endforeach
                                    @endif
                                </select>
                                <span class="input-group-addon"><i class="fa fa-bars"></i></span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="template_id" class="col-md-4 control-label">¿Tiene Variable?</label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <select onchange="selectHasVariable()" class="js-example-basic-single select-chosen change-select" style="width: 100%" name="has_variable" id="has_variable" data-placeholder="Plantilla">
                                    @if($config_welcome && $config_welcome->has_variable)
                                        <option value="0">NO</option>
                                        <option selected value="1">SI</option>
                                    @else
                                        <option selected value="0">NO</option>
                                        <option value="1">SI</option>
                                    @endif
                                   
                                </select>
                                <span class="input-group-addon"><i class="fa fa-bars"></i></span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group" style="{{$config_welcome && $config_welcome->has_variable ? 'display: block' : 'display: none'}}" id="type_container">
                        <label for="type_variable" class="col-md-4 control-label">Escoge en que lugar, estará la variable</label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <select class="js-example-basic-single select-chosen change-select" style="width: 100%" name="type_variable" id="type_variable" data-placeholder="Plantilla">
                                    @if($config_welcome && $config_welcome->has_variable && $config_welcome->type_variable)
                                        @if($config_welcome && $config_welcome->type_variable == "body")
                                            <option value="">Seleccionar</option>
                                            <option selected value="body">Body</option>
                                            <option value="header">Header</option>
                                        @else
                                            <option value="">Seleccionar</option>
                                            <option value="body">Body</option>
                                            <option selected value="header">Header</option>
                                        @endif
                                    @else
                                        <option value="">Seleccionar</option>
                                        <option value="body">Body</option>
                                        <option value="header">Header</option>
                                    @endif
                                    
                                </select>
                                <span class="input-group-addon"><i class="fa fa-bars"></i></span>
                            </div>
                        </div>
                    </div>

                    {{ csrf_field() }}
                    <div class="form-group form-actions">
                        <div class="col-md-8 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">Guardar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
            <div class="block">
                <div class="block-title">
                    <h2>CONFIGURACIÓN MENSAJE INCIDENCIAS</h2>
                </div>
                <form action="{{route('whatsapi.store')}}" method="POST" enctype="multipart/form-data" class="form-horizontal form-bordered">
                    <input type="hidden" name="title" id="title" value="incidencias">
                    <input type="hidden" name="has_variable" id="has_variable" value="1">
                    <input type="hidden" name="type_variable" id="type_variable" value="body">

                    <div class="form-group">
                        <label for="from_id" class="col-md-4 control-label">Telefóno que hará el envío</label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <select class="js-example-basic-single select-chosen change-select" style="width: 100%" name="from_id" id="from_id" required data-placeholder="Número de teléfono">
                                    <option value=""></option>
                                    @if(isset($phones['data']) && count($phones['data']) > 0)
                                        @foreach($phones['data'] as $phone)
                                            @if($config_incidents && $phone['id'] == $config_incidents->from_id)
                                                <option selected value="{{$phone['id']}}">{{$phone['display_name']}} --- {{$phone['phone_number']}}</option>
                                            @else
                                                <option value="{{$phone['id']}}">{{$phone['display_name']}} --- {{$phone['phone_number']}}</option>
                                            @endif
                                        @endforeach
                                    @endif
                                </select>
                                <span class="input-group-addon"><i class="fa fa-bars"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="plantilla_id" class="col-md-4 control-label">Escoge la plantilla</label>
                        <div class="col-md-6">
                            <div class="input-group" style="width: 100%">
                                <select style="width: 100%" class="js-example-basic-single select-chosen change-select" style="width: 100%" required="required" name="plantilla_id" id="plantilla_id" data-placeholder="Plantilla">
                                    <option value=""></option>
                                    @if(isset($templates['data']) && count($templates['data']) > 0)
                                        @foreach($templates['data'] as $templat)
                                            @if($config_incidents && $templat['uuid'] == $config_incidents->plantilla_id)
                                                <option selected value="{{$templat['uuid']}}">{{$templat['template_id']}}</option>
                                            @else
                                                <option value="{{$templat['uuid']}}">{{$templat['template_id']}}</option>
                                            @endif
                                        @endforeach
                                    @endif
                                </select>
                                <span class="input-group-addon"><i class="fa fa-bars"></i></span>
                            </div>
                        </div>
                    </div>

                    {{ csrf_field() }}
                    <div class="form-group form-actions">
                        <div class="col-md-8 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">Guardar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('js_aditional')

    <script>
        function selectHasVariable(){
            let has_variable = $("#has_variable").val()
            if(has_variable && has_variable == "1"){
                $("#type_container").css({display: "block"})
            }else{
                $("#type_container").css({display: "none"})
                $("#type_variable").val("")
            }
        }
    </script>
@endsection
