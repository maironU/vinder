@extends('dashboard.layout')
@section('content_page')
    <?php 
        use App\Entities\FormE14;
        use App\Entities\User;

        $formse14 = FormE14::all();

        $users = User::where('type_id', '=', 5)->paginate(20);
    ?>

    <div class="row" style="cursor: pointer; margin: 0">
        @foreach($mainModules as $module)
            <a href="/{{ $module->url }}">
                <div class="col-sm-4" style="padding: 0">
                    <img src="{{$module->image}}" width="100%" alt="" style="padding: 10px">
                    <img class="image-back" src="{{$module->image_back}}" width="100%" alt="" style="position: absolute; left: 0; border-radius: 30px; height: 100%; padding: 10px;object-fit: cover">
                    <div class="card-module-text">
                        <i class="{{$module->icon_class}} icon-text"></i>
                        <span>{{$module->description}}</span>
                    </div>
                </div>
            </a>
        @endforeach
    </div>
	<!--<div class="row">
        @foreach($mainModules as $module)
            <div class="col-sm-4">
                <a href="/{{ $module->url }}" class="widget">
                    <div class="widget-content {{ $module->color_class }} text-light-op text-center">
                        <h2><strong>{{ $module->description }}</strong></h2>
                    </div>
                    <div class="widget-content themed-background-muted text-center">
                        {!! Html::image($module->image, 'icon vinder', array('class' => 'img-circle img-thumbnail')) !!}
                    </div>
                    <div class="widget-content text-center">
                    </div>
                </a>
            </div>
        @endforeach
    </div>-->

    <div class="row" style="margin: 0; cursor: pointer">
        @foreach($extraModules as $module)
        @if((Auth::user()->type->id == 1 || Auth::user()->type->id == 2) && $module->description != 'Orientadores' && $module->description != 'Puesto de Votación')
            <a href="/{{ $module->url }}">
                <div class="pull-right col-xs-12 col-sm-6 container-card" style="height: 190px; width: 20%">
                    <img src="{{asset('images/placeholders/icons/grey-back-color.png')}}" width="100%" alt="" style="position: absolute; left: 0; border-radius: 30px; height: 100%; padding: 10px">
                    <div class="card-module-footer">
                        <i class="{{$module->icon_class}} icon-text"></i>
                        <span style="width: 60%;text-align: center;">{{$module->description}}</span>
                    </div>
                </div>
            </a>
        @endif
        @endforeach
    </div>

    @if(Auth::user()->type->id == 5 || Auth::user()->type->id == 8)
        <div class="content-header">
            <div class="row">
                <div class="col-sm-6">
                    <div class="header-section">
                        <h1>@yield('title_page', 'Formularios E14')</h1>
                    </div>
                </div>
                <div class="col-sm-6 hidden-xs">
                    <div class="header-section">
                    <ul class="breadcrumb breadcrumb-top">
                        <li><a href="/"><i class="fa fa-home"></i></a></li>
                        <li><a href="{{route('witnesses.index')}}">Testigos electorales</a></li>
                        <li class="active">Formularios</li>
                    </ul>
                    </div>
                </div>
            </div>
            @yield('extra-header-section')
        </div>
        @include('dashboard/pages/witnesses/listFormsE14', ['formse14' => $formse14, 'request' => $request])
    @endif

    @if(Auth::user()->type->id == 7)
        @include('dashboard/pages/witnesses/resultsaccordingwitnesses', ['users' => $users, 'formse14' => $formse14])
    @endif
@endsection
@section('js_aditional')

@endsection