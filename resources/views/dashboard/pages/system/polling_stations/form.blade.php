@extends('dashboard.pages.layout')
@section('title_page')
    @if($polling_station->exists) Puesto de votación: {{ $polling_station->description }} @else Nuevo puesto de votación @endif
@endsection
@section('breadcrumbs') {!! Breadcrumbs::render('polling-stations.create', $polling_station) !!} @endsection
@section('content_body_page')
    <div class="row">
        <div class="col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
            {!! Form::model($polling_station, $form_data + ['id' => 'form-polling-stations']) !!}
                <div class="block">
                    <div class="block-title">
                        <h2>Datos del puesto de votación</h2>
                    </div>

                    @include('dashboard.includes.alerts')
                    
                    <div class="form-horizontal form-bordered">

                        {!! Field::text('name', null, ['name' => 'name', 'template' => 'horizontal', 'placeholder' => 'El nombre del puesto de votación (Ej: Escuela El Naranjal)', 'title' => 'El nombre del puesto de votación (Ej: Escuela El Naranjal)', 'required' => 'required']) !!}

                        {!! Field::text('address', null, ['name' => 'address', 'template' => 'horizontal', 'placeholder' => 'Dirección del puesto de votación (Ej: Calle 1 # 2-34)', 'title' => 'Dirección del puesto de votación (Ej: Calle 1 # 2-34)']) !!}
<!--                        {!! Field::text('Descripción', '', ['name' => 'description', 'template' => 'horizontal', 'placeholder' => 'Nombre', 'placeholder' => 'Descripción del puesto de votación']) !!}
-->
                        <!-- MESA DE VOTACIÓN -->
                        {!!  Field::number('tables', null, ['name' => 'tables','template' => 'horizontal', 'data-placeholder' => 'Cantidad de mesas de votación', 'title' => 'Cantidad de mesas de votación', 'required' => 'required' ]) !!}
<!--
                        {!! Field::text('Hombres', '0', ['name' => 'men', 'template' => 'horizontal', 'placeholder' => 'Cantidad de hombres que votan en este puesto']) !!}
                        {!! Field::text('Mujeres', '0', ['name' => 'women', 'template' => 'horizontal', 'placeholder' => 'Cantidad de mujeres que votan en este puesto']) !!}
-->                        {!! Field::number('electoral_potential', null, ['name' => 'electoral_potential', 'template' => 'horizontal', 'placeholder' => 'Potencial electoral']) !!}

<!--
                        {!! Field::text('department', null, ['template' => 'horizontal', 'placeholder' => 'Departamento']) !!}
                        {!! Field::text('city', null, ['template' => 'horizontal', 'placeholder' => 'Municipio', 'title' => 'Municipio']) !!}
                        {!! Field::text('zone', null, ['template' => 'horizontal', 'placeholder' => 'Zona', 'title' => 'Zona']) !!}
                        {!! Field::text('place', null, ['template' => 'horizontal', 'placeholder' => 'Puesto', 'title' => 'Puesto']) !!}
-->
                        <!-- Latitud -->
                        {!! Field::text('latitude', null, ['template' => 'horizontal', 'placeholder' => 'Latitud', 'title' => 'Latitud']) !!}
                        <!-- Longitud -->
                        {!! Field::text('longitude', null, ['template' => 'horizontal', 'placeholder' => 'Longitud', 'title' => 'Longitud']) !!}

                        <!-- Ubicación -->
						@include('dashboard.pages.form-select.select', ["type" => "location_id", "label" => "Ubicación del puesto de votación", "placeholder" => "Ubicación del puesto de votación", "icon" => "fa fa-bars", "options" => $locations, "selected" => $polling_station->location_id, "col_label" => "col-md-4", "col_div" => "col-md-6"])

                       {{-- {!!
                            Field::select(
                                'location_id',
                                $locations,
                                null,
                                [
                                    'name' => 'location_id',
                                    'template' => 'horizontal',
                                    'title' => 'Ubicación del puesto de votación',
                                    'data-placeholder' => 'Ubicación del puesto de votación',
                                    'required' => 'required'
                                ]
                            )
                        !!} --}}

                        <!-- Registraduría -->
						@include('dashboard.pages.form-select.select', ["type" => "registraduria_location_id", "label" => "Sede de la registraduría titular", "placeholder" => "Sede de la registraduría titular", "icon" => "fa fa-bars", "options" => $locations, "selected" => $polling_station->registraduria_location_id, "col_label" => "col-md-4", "col_div" => "col-md-6"])

                        {{-- {!! Field::select(
                                'registraduria_location_id',
                                $locations,
                                null,
                                [
                                    'name' => 'registraduria_location_id',
                                    'template' => 'horizontal',
                                    'title' => 'Sede de la registraduría titular',
                                    'data-placeholder' => 'Sede de la registraduría titular',
                                    'required' => 'required'
                                ]
                            )
                        !!}--}}

<!--                        {!! Field::text('address', null, ['template' => 'horizontal', 'placeholder' => 'Dirección']) !!} -->
<!--                        {!! Field::select('location_id', $locations, null, ['template' => 'horizontal', 'data-placeholder' => 'Seleccione la ubicación']) !!} -->
<!--                        {!! Field::number('tables', null, ['template' => 'horizontal', 'placeholder' => 'Mesas']) !!} -->

                        <div class="form-group form-actions">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-effect-ripple btn-primary">Guardar</button>
                            </div>
                        </div>

                    </div>

                    @include('dashboard.includes.alerts')

                </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
@section('js_aditional')
    {!! Html::script('assets/js/pages/formlocations.js') !!}
    <!-- Load and execute javascript code used only in this page -->
    <script> $(function (){ Formlocations.init(); });</script>
@endsection