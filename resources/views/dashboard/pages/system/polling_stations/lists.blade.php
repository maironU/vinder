@extends('dashboard.pages.layout')
@section('class_icon_page') fa fa-users @endsection
@section('title_page') Puestos de Votación @endsection
@section('breadcrumbs') {!! Breadcrumbs::render('polling-stations') !!} @endsection
@section('content_body_page')
    <div class="block full">
	<a href="{{ route('system.polling-stations.create')}}" data-toggle="tooltip" title="Crear" class="btn btn-effect-ripple btn-primary">
        <button type="button" class="btn btn-primary"><i class="fa fa-pencil"></i>Nueva ubicación</button>
	</a>

    
        <div class="table-responsive">
            {!! $polling_stations->render() !!}
            <table id="datatable" class="table table-striped table-bordered table-vcenter">
                <thead>
                    <tr>
                        <th title="Nombre">Nombre</th>
<!--
                        <th title="Ubicación">Departamento</th>

                        <th title="Municipio">Municipio</th>
-->
<!--                        <th title="Zona">Zona</th>
                        <th title="Puesto">Puesto</th>
-->
<!--                        <th title="Latitud">Latitud</th>
                        <th title="Longitud">Longitud</th>
-->
                        <th title="Dirección">Dirección</th>
                        <th title="Potencial">Potencial Electoral</th>
                        <th title="Mesas">Mesas</th>
                        <th class="text-center" style="max-width: 115px;"><i class="fa fa-flash"></i></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($polling_stations as $polling_station)
                        <tr>
                            <td> {{ $polling_station->description }} </td>
<!--
                            <td> {{ $polling_station->locationName }} </td>

                            <td> {{ $polling_station->registraduriaLocationName }} </td>
-->
<!--                            <td> {{ $polling_station->zona }} </td>
                            <td> {{ $polling_station->puesto }} </td>
-->
<!--                            <td> {{ $polling_station->latitude }} </td>
                            <td> {{ $polling_station->longitude }} </td>
-->
                            <td> {{ $polling_station->address }} </td>
                           <td> {{ $polling_station->electoral_potential }} </td>
                            <td> {{ $polling_station->tables }} </td>
                            <td class="text-center">
                                <a href="{{ route('system.polling-stations.edit', $polling_station->id)}}" data-toggle="tooltip" title="Editar" class="btn btn-effect-ripple btn-warning">
                                    <i class="fa fa-pencil"></i>
                                </a>
                                <a href="{{ route('system.polling-stations.destroy', $polling_station->id)}}" data-toggle="tooltip" title="Eliminar" class="btn btn-effect-ripple btn-danger" onclick="return confirm('¿Está seguro de querer eliminar el punto de votación?')">
                                    <i class="fa fa-times"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach 
                </tbody>
            </table>
            {!! $polling_stations->render() !!}
        </div>
    </div>



@endsection
@section('js_aditional')
	<!-- Load and execute javascript code used only in this page -->
    {!! Html::script('assets/js/lists.js') !!}
	{!! Html::script('assets/js/pages/uiTables.js') !!}
    <script>$(function(){ UiTables.init(); });</script>
@endsection