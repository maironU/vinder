@extends('dashboard.pages.layout')
@section('title_page', 'Buscar Puestos De Votación')
@section('breadcrumbs') {!! Breadcrumbs::render('polling-stations') !!} @endsection


@section('content_body_page')
<style>
    .content-form {
        display: flex;
        align-items: flex-end;
        width: 100%;
    }

    .content-form > .form-group{
        margin-bottom: 0;
        margin-right: 10px;
        width: 270px;
    }

    @media (max-width: 560px){
        .content-form {
            flex-direction: column;
            align-items: center;
            justify-content: center;
        }

        .content-form > .form-group, input{
            width: 80%;
            margin: 0;
        }
    }
</style>
    <div class="row" id="title_page" style="margin-bottom: 10px;">
    	<div class="col-sm-8 content-form">
            <div class="form-group">
                <input id="search" type="text" class="form-control" placeholder="Cédula, Nombre, Apellido, Departamento" value="{{$request->search}}">
            </div>
            <input type="button" class="btn btn-primary" value="Buscar" onclick="searchPollingStation()">
        </div>
    </div>
    @if(count($voters) > 0)
        <div class="block full">
            <div class="table-responsive">
                <table id="datatable" class="table table-striped table-bordered table-vcenter">
                    <thead>
                        <tr>
                            <th title="Cédula">Cédula</th>
                            <th title="Primer Nombre">Nombre</th>
                            <th title="Segundo Nombre">Segundo Nombre</th>
                            <th title="Apellido">Apellido</th>
                            <th title="Segundo Apellido">Segundo Apellido</th>
                            <th title="Departamento">Departamento</th>
                            <th title="Municipio">Municipio</th>
                            <th title="Lugar de Votación">Lugar de Votación</th>
                            <th title="Mesa">Mesa</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($voters as $voter)
                            <tr>
                                <td>{{$voter->nuip}}</td>
                                <td>{{$voter->first_name}}</td>
                                <td>{{$voter->second_name}}</td>
                                <td>{{$voter->surname}}</td>
                                <td>{{$voter->second_surname}}</td>
                                <td>{{$voter->department}}</td>
                                <td>{{$voter->municipality}}</td>
                                <td>{{$voter->polling_station}}</td>
                                <td>{{$voter->table}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        
        @if(isset($request))
            {!! $voters->appends(['search' => $request->search])->render() !!}
        @else
            {!! $voters->render() !!}
        @endif
    @else
        <span>Nada para mostrar</span>
    @endif
@endsection
@section('js_aditional')

<script>
    function searchPollingStation(){
        let search = $("#search").val()
        location.href="/system/voters_polling?search=" + search
    }

    $('#search').keypress(function(e) {
        var keycode = (e.keyCode ? e.keyCode : e.which);
        if (keycode == '13') {
            searchPollingStation();
            e.preventDefault();
            return false;
        }
    });
</script>
@endsection

