@extends('dashboard.pages.layout')
@section('title_page')
    Configuración del Sistema
@endsection
@section('breadcrumbs')
	{!! Breadcrumbs::render('system') !!}
@endsection
@section('content_body_page')
    <div class="row" style="margin: 0">
        <a href="/system/users">
            <div class="col-sm-4" style="padding: 0">
                <img src="{{asset('images/placeholders/icons/bdd-back-color.png')}}" width="100%" alt="" style="padding: 10px">
                <img class="image-back" src="{{asset('images/placeholders/icons/bdd-back.png')}}" width="100%" alt="" style="position: absolute; left: 0; border-radius: 30px; height: 100%; padding: 10px;object-fit: cover">
                <div class="card-module-text">
                    <i class="fa fa-database icon-text"></i>
                    <span>Usuarios</span>
                </div>
            </div>
        </a>
        <a href="/system/user-types">
            <div class="col-sm-4" style="padding: 0">
                <img src="{{asset('images/placeholders/icons/bdd-back-color.png')}}" width="100%" alt="" style="padding: 10px">
                <img class="image-back" src="{{asset('images/placeholders/icons/bdd-back.png')}}" width="100%" alt="" style="position: absolute; left: 0; border-radius: 30px; height: 100%; padding: 10px;object-fit: cover">
                <div class="card-module-text">
                    <i class="fa fa-database icon-text"></i>
                    <span>Tipos de Usuario</span>
                </div>
            </div>
        </a>
        <a href="/system/locations">
            <div class="col-sm-4" style="padding: 0">
                <img src="{{asset('images/placeholders/icons/bdd-back-color.png')}}" width="100%" alt="" style="padding: 10px">
                <img class="image-back" src="{{asset('images/placeholders/icons/bdd-back.png')}}" width="100%" alt="" style="position: absolute; left: 0; border-radius: 30px; height: 100%; padding: 10px;object-fit: cover">
                <div class="card-module-text">
                    <i class="fa fa-database icon-text"></i>
                    <span>Ubicaciones</span>
                </div>
            </div>
        </a>
        <a href="/system/polling-stations">
            <div class="col-sm-4" style="padding: 0">
                <img src="{{asset('images/placeholders/icons/bdd-back-color.png')}}" width="100%" alt="" style="padding: 10px">
                <img class="image-back" src="{{asset('images/placeholders/icons/bdd-back.png')}}" width="100%" alt="" style="position: absolute; left: 0; border-radius: 30px; height: 100%; padding: 10px;object-fit: cover">
                <div class="card-module-text">
                    <i class="fa fa-database icon-text"></i>
                    <span>Puestos de Votación</span>
                </div>
            </div>
        </a>
        @if (Auth::user()->type->can_view_all == '1')
            <a href="/system/server">
                <div class="col-sm-4" style="padding: 0">
                    <img src="{{asset('images/placeholders/icons/bdd-back-color.png')}}" width="100%" alt="" style="padding: 10px">
                    <img class="image-back" src="{{asset('images/placeholders/icons/bdd-back.png')}}" width="100%" alt="" style="position: absolute; left: 0; border-radius: 30px; height: 100%; padding: 10px;object-fit: cover">
                    <div class="card-module-text">
                        <i class="fa fa-database icon-text"></i>
                        <span>Configuración servidor</span>
                    </div>
                </div>
            </a>
        @endif
    </div>

@endsection