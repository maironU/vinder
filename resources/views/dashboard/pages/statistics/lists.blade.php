@extends('dashboard.pages.layout')
@section('title_page')
    Estadísticas
@endsection
@section('breadcrumbs')
	{!! Breadcrumbs::render('statistics') !!}
@endsection
@section('content_body_page')
    <div class="row">
    	<div class="col-md-8 col-md-offset-2">
			<a class="widget" href="#">
				<div class="widget-content text-right clearfix">
					<h2 class="widget-heading h3"><strong>Alcanzando nuestra meta:</strong> {{$number_voters}} de {{$target_number}} </h2>
					<div class="progress progress-striped active">
		                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="{{$goal_percentage}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$goal_percentage}}%">{{$number_voters}} / {{$target_number}}</div>
		            </div>
				</div>
			</a>

			@foreach($statistics as $stat)
				<div class="widget">
					<div class="widget-content text-right clearfix">
						{!! Html::image($stat->image, 'icon', array('class' => 'img-circle img-thumbnail img-thumbnail-avatar pull-left')) !!}
						
						<p class="widget-heading h3"><strong>{{ $stat->description }}</strong></p>
						
						{!! Form::open(['route' => $stat->url, 'method' => 'GET', 'class' => 'float:right;', 'target' => '_blank']) !!}
							
							@if($stat->select)
								<div class="form-group col-lg-8">
									{!! Form::select($stat->select . '[]', $selects[$stat->select], null, ['class' => 'select-chosen', 'data-placeholder' => $stat->message, 'multiple']) !!}
								</div>
							@endif
							
							<div class="form-group">
								<button class="btn btn-effect-ripple btn-primary" type="submit"><span class="btn-ripple animate"></span>Ver Estadistica</button>
							</div>
							
						{!! Form::close() !!}
					</div>
				</div>
			@endforeach

			<div class="widget">
					<div class="widget-content text-right clearfix">
						<div class="col-md-12">
							<div id="barchart_values" style="width: 100%; margin:0 !important;"></div>
						</div>
					<br/>

					<div class="btn-group btn-group-justified">
						<a href="reports/semaphore?select[]=0" class="btn">Indefinido</a>
						<a href="reports/semaphore?select[]=1" class="btn btn-success">Voto fijo</a>
						<a href="reports/semaphore?select[]=2" class="btn btn-warning">Indeciso</a>
						<a href="reports/semaphore?select[]=3" class="btn btn-danger">Voto duro</a>
					</div> 

				</div>
			</div>

    	</div>
    </div>	


	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

	<script type="text/javascript">
		google.charts.load("current", {packages:["corechart"]});
		google.charts.setOnLoadCallback(drawChart);
		function drawChart() {
			var data = google.visualization.arrayToDataTable([
				['Intención de voto', 'Cantidad de votantes', { role: 'style' } ],
				@foreach($semaphores as $semaphore)
					['{{ $semaphore[0] }}', {{ $semaphore[1] }}, '{{ $semaphore[2] }}' ],
				@endforeach
			]);

			var view = new google.visualization.DataView(data);
			view.setColumns([0, 1,
							{ calc: "stringify",
								sourceColumn: 1,
								type: "string",
								role: "annotation" },
							2]);

			var options = {
				title: "Estadística según estado del votante",
				bar: {groupWidth: "95%"},
				legend: { position: "none" },
			};
			var chart = new google.visualization.BarChart(document.getElementById("barchart_values"));
			chart.draw(view, options);
		}

		window.addEventListener('resize', function(){ drawChart()}, true);
	</script>

@endsection