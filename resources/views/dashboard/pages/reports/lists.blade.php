@extends('dashboard.pages.layout')
@section('title_page')
    Reportes
@endsection
@section('breadcrumbs')
	{!! Breadcrumbs::render('reports') !!}
@endsection
@section('content_body_page')
    <div class="row">
    	<div class="col-md-8 col-md-offset-2">
    		@foreach($reports as $report)
				<div class="widget">
					<div class="widget-content text-right clearfix">
						{!! Html::image($report->image, 'icon', array('class' => 'img-circle img-thumbnail img-thumbnail-avatar pull-left')) !!}
						<p class="widget-heading h3"><strong>{{$report->description}}</strong></p>
						{!! Form::open(['route' => $report->url, 'method' => 'GET', 'class' => 'float:right;', 'target' => '_blank']) !!}
							@if($report->select)
								<div class="input-group col-lg-10 pull-right">
									{!! Form::select('select[]', $selects[$report->select], null, ['class' => 'select-chosen', 'data-placeholder' => $report->message, 'required', 'multiple']) !!}
									<div class="input-group-btn">
										<button type="submit" class="btn btn-primary"><span class="btn-ripple animate">Generar Reporte</span>Generar reporte</button>
										@if ($report->excel)
											<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												<span class="caret"></span>
												<span class="sr-only">Generar reporte</span>
											</button>
										@endif
										<ul class="dropdown-menu dropdown-menu-right">
											<li><a href="javascript:;" onclick="document.getElementById('xls-{{$report->name}}').value = false;">PDF</a></li>
											<li><a href="javascript:;" onclick="document.getElementById('xls-{{$report->name}}').value = true;">Excel</a></li>
										</ul>
									</div>
							</div>
							@else
								<div class="input-group-btn">
									<button class="btn btn-effect-ripple btn-primary" type="submit"><span class="btn-ripple animate">Generar Reporte</span>Generar Reporte</button>
									@if ($report->excel)
										<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											<span class="caret"></span>
											<span class="sr-only">Generar reporte</span>
										</button>
									@endif
									<ul class="dropdown-menu dropdown-menu-right">
										<li><a href="javascript:;" onclick="document.getElementById('xls-{{$report->name}}').value = false;">PDF</a></li>
										<li><a href="javascript:;" onclick="document.getElementById('xls-{{$report->name}}').value = true;">Excel</a></li>
									</ul>
								</div>
							@endif
						{!! Form::hidden('xls', false, array('id' => 'xls-'.$report->name)) !!}
						{!! Form::close() !!}
					</div>
				</div>
			@endforeach
    	</div>
    </div>
@endsection