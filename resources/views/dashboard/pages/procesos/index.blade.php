@extends('dashboard.pages.layout')
@section('title_page', 'Solicitudes para actualizaciones')

@section('content_body_page')

<div class="row" id="title_page" style="margin-bottom: 10px;">
        @include('flash::message')

    	<div class="col-md-12">
            <a href="{{ route('procesos.create')}}" class="btn btn-primary"><i class="fa fa-user"></i>Solicitar nueva actualización</a>
        </div>
    </div>
    <div class="block full">
        <div class="table-responsive">
            <table id="datatable" class="table table-striped table-bordered table-vcenter">
                <thead>
                    <tr>
                        <th title="Nombre de Usuario">#</th>
                        <th title="Nombre completo">Proceso</th>
                        <th title="Correo">Generado Por</th>
                        <th title="Correo">Fecha Creación</th>
                        <th title="Correo">Fecha Modificación</th>
                        <th title="Correo">Estado</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($cola_procesos as $cola_proceso)
                        <tr>
                            <td style="width: 10%">{{ $cola_proceso->id }}</td>
                            <td style="width: 20%"><strong>{{ $cola_proceso->proceso->name }}</strong></td>
                            <td style="width: 20%">{{ $cola_proceso->email }}</td>
                            <td style="width: 20%">{{ $cola_proceso->created_at}}</td>
                            <td style="width: 20%">{{ $cola_proceso->updated_at}}</td>
                            <td style="width: 10%" id="estado_{{$cola_proceso->id}}">
                                @if($cola_proceso->estado_proceso->slug == 'en-proceso')
                                    <span data-toggle="tooltip" title="" class="label label-primary" data-original-title="pendiente">
                                        <i class="fa fa-hourglass-half"></i>pendiente
                                    </span>
                                @elseif($cola_proceso->estado_proceso->slug == 'en-ejecucion')
                                    <span data-toggle="tooltip" title="" class="label label-warning" data-original-title="{!! $cola_proceso->estado_proceso->nombre !!}">
                                        <i class="fa fa-circle-o-notch fa-spin"></i>  {!! $cola_proceso->estado_proceso->nombre !!}
                                    </span>
                                @elseif($cola_proceso->estado_proceso->slug == 'finalizado')
                                    <span data-toggle="tooltip" title="" class="label label-success" data-original-title="{!! $cola_proceso->estado_proceso->nombre !!}">
                                        <i class="fa fa-check"></i>  {!! $cola_proceso->estado_proceso->nombre !!}
                                    </span>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection
@section('js_aditional')

    <script>
        setInterval(() => {
            obtenerProcesos()
        }, 3000);
        async function obtenerProcesos(){
            const response = await fetch(`${window.location.href}/api`);
            const jsonData = await response.json();

            jsonData.forEach(cola_proceso => {
                let estado = $("#estado_"+cola_proceso.id)
                if(cola_proceso.estado_proceso.slug == 'en-proceso'){
                    estado.html(`<span data-toggle="tooltip" title="" class="label label-primary" data-original-title="pendiente">
                        pendiente
                    </span>`)
                }else if(cola_proceso.estado_proceso.slug == 'en-ejecucion'){
                    estado.html(`<span data-toggle="tooltip" title="" class="label label-warning" data-original-title="${cola_proceso.estado_proceso.nombre}">
                        <i class="fa fa-circle-o-notch fa-spin"></i>  ${cola_proceso.estado_proceso.nombre}
                    </span>`)
                }else if(cola_proceso.estado_proceso.slug == 'finalizado'){
                    estado.html(`<span data-toggle="tooltip" title="" class="label label-success" data-original-title="${cola_proceso.estado_proceso.nombre}">
                        <i class="fa fa-check"></i>  ${cola_proceso.estado_proceso.nombre}
                    </span>`)
                }
           });
        }
    </script>
@endsection

