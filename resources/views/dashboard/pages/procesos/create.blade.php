@extends('dashboard.pages.layout')
@section('title_page')
    Nueva actualización
@endsection
@section('breadcrumbs') 
<ul class="breadcrumb breadcrumb-top">
    <li><a href="{{ route('procesos.index') }}">Actualizaciones</a></li>
    <li class="active">Nuevo</li>
</ul>
@endsection
@section('content_body_page')
	<div class="row">
	    <div class="col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
	        <div class="block">
	            <div class="block-title">
	                <h2>Datos de la actualización</h2>
	            </div>
                <form action="{{route('procesos.store')}}" method="POST" enctype="multipart/form-data" class="form-horizontal form-bordered">
                    <div class="form-group">
                        <label for="location" class="col-md-4 control-label">Puesto de Votación</label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <select class="js-example-basic-single select-chosen change-select" style="width: 100%" required="required" name="proceso_id" id="proceso_id" data-placeholder="Proceso">
                                    <option value=""></option>
                                    @foreach($procesos as $proceso)
                                        <option value="{{$proceso->id}}">{{$proceso->name}}</option>
                                    @endforeach
                                </select>
                                <span class="input-group-addon"><i class="fa fa-bars"></i></span>
                            </div>
                        </div>
                    </div>
    
                    {{ csrf_field() }}
                    <div class="form-group" style="display: flex; justify-content: center">
                        <input type="file" name="file" class="form-control-file">
                    </div>
                    <div class="form-group form-actions">
	                    <div class="col-md-8 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">Guardar</button>
	                    </div>
	                </div>
                </form>
	        </div>
	    </div>
	</div>
@endsection