@extends('dashboard.pages.layout')
@section('title_page')sdsd@endsection
@section('breadcrumbs'){!! Breadcrumbs::render('database') !!}@endsection
@section('meta_extra') <meta name="_token" content="{{ csrf_token() }}"/> @endsection
@section('exta-header-section') <style>#map_container{ width: 300px !important; height:300px !important;} </style> @endsection
@section('content_body_page')

    <div class="map-container">
        <div id="map_container"></div>
    </div>

    <script>
        let map_container;
        let markersArray = [];
        let polyline = null;
        let points = [];

        function initMap() {
            points = {!! $points !!};
            
            map_container = new google.maps.Map(document.getElementById('map_container'), {
                center: {lat: {!! $latitude !!}, lng: {!! $longitude !!}  },
                zoom: {!! $zoom !!}
            });

            // map onclick listener
            /*
            map_container.addListener('click', function(e) {
                //console.log(e);
                addMarker(e.latLng);
                drawPolyline();
            });
            */
            points.forEach(addPoints);
        }

        function addPoints(point) {
//            console.log("Name: " + name[0] + " Lat: " + name[1] + " Lon: " + name[2]);
            var myLatlng = new google.maps.LatLng(point[1],point[2]);
            var marker = new google.maps.Marker({
                position: myLatlng,
                title:point[0],
//                icon:'http://www.myiconfinder.com/uploads/iconsets/256-256-a5485b563efc4511e0cd8bd04ad0fe9e.png',
                animation: google.maps.Animation.DROP,
            });

            var contentString = '<div id="content">'+
            '<div id="siteNotice"></div>'+
            '<h4 id="firstHeading" class="firstHeading">' + point [4] + '</h4>'+
            '<div id="bodyContent">' + point[5] + '</div>'+
            '</div>';

            var infowindow = new google.maps.InfoWindow({
                content: contentString,
                maxWidth: 200,
            });

            marker.addListener('click', function() {
                infowindow.open(map_container, marker);
            });

            marker.setMap(map_container);
        }

        // define function to add marker at given lat & lng
        function addMarker(latLng) {
            let marker = new google.maps.Marker({
                map: map_container,
                position: latLng,
                draggable: true
            });
            // add listener to redraw the polyline when markers position change
            marker.addListener('position_changed', function() {
                drawPolyline();
            });
            //store the marker object drawn in global array
            markersArray.push(marker);
        }

        // define function to draw polyline that connect markers' position
        function drawPolyline() {
            let markersPositionArray = [];
            // obtain latlng of all markers on map
            markersArray.forEach(function(e) {
                markersPositionArray.push(e.getPosition());
            });
            // check if there is already polyline drawn on map
            // remove the polyline from map before we draw new one
            if (polyline !== null) {
                polyline.setMap(null);
            }
            // draw new polyline at markers' position 
            polyline = new google.maps.Polyline({
                map: map_container,
                path: markersPositionArray,
                strokeOpacity: 0.4
            });
        }
    </script>

    <script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDBnZmR6l9RwBoPkDzB3Z4V5T7Rqlxlnyc&callback=initMap">
    </script>

@endsection