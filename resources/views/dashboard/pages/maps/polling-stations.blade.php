@extends('dashboard.pages.layout')
@section('title_page', 'Mapas')
@section('breadcrumbs') {!! Breadcrumbs::render('maps') !!} @endsection

@section('content_body_page')
<style>

</style>

    <div style="margin-bottom: 20px; display:flex; justify-content: space-between">
        <button type="button" class="btn btn-warning" onclick="updateCoordinates()">Actualizar Coordenadas</button>
        <div style="display: flex; display:none" id="filter-house-check">
            
            <h4 style="margin-right: 20px" id="text-filter">Filtrar Por:</h4>
            <div>
                <div class="form-group" style="display: flex; align-items: center; padding-right: 18px; margin-bottom: 0">
                    <!--<label class="control-label" style="margin-right: 10px" for="orientador">Orientador</label>-->
                    <div style="padding-right: 0">
                        <div class="input-group" style="min-width: 150px">
                            <select class="js-example-basic-single select-chosen change-select" style="width: 100%" required="required" name="filter_by" id="filter_by" aria-required="true" aria-invalid="false" data-placeholder="Filtrar" onchange="filterBy()">
                                <option value=""></option>
                                <option value="orientador">Orientador</option>
                                <option value="coordinador">Coordinador</option>
                            </select>
                            <span class="input-group-addon" style="width: 15px; cursor: pointer" onclick="emptyFilter('filter_by')"><i class="fa fa-remove"></i></span>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <?php 
                $users = \App\Entities\User::select('user.id', 'user.name')
                ->where('user.type_id', 6)
                ->lists('name', 'id')
                ->all();
                ?>
                <div id="select-orientador" class="form-group" style="display: none; align-items: center; padding-right: 18px; margin-bottom: 0">
                    <!--<label class="control-label" style="margin-right: 10px" for="orientador">Orientador</label>-->
                    <div style="padding-right: 0">
                        <div class="input-group" style="min-width: 150px">
                            <select class="js-example-basic-single select-chosen change-select" style="width: 100%" required="required" name="orientador" id="orientador" aria-required="true" aria-invalid="false" data-placeholder="Orientador" onchange="filterVoter()">
                                <option value=""></option>
                                @foreach($users as  $key => $value)
                                    @if(isset($request->orientador))
                                        @if($key == $request->orientador)
                                            <option value="{{$key}}" selected>{{$value}}</option>
                                        @else
                                            <option value="{{$key}}">{{$value}}</option>
                                        @endif
                                    @else
                                        <option value="{{$key}}">{{$value}}</option>
                                    @endif
                                @endforeach
                            </select>
                            <span class="input-group-addon" style="width: 15px; cursor: pointer" onclick="emptyFilter('orientador')"><i class="fa fa-remove"></i></span>
                        </div>
                    </div>
                </div>

                <?php 
                    $coordinadores = \App\Entities\Voter::where('is_coordinator', 1)
                    ->lists('name', 'id')
                    ->all();
                ?>
                <div id="select-coordinador" class="form-group" style="display: none; align-items: center; padding-right: 18px; margin-bottom: 0">
                    <!-- <label class="control-label" style="margin-right: 10px" for="coordinador">Coordinador</label> -->
                    <div style="padding-right: 0">
                        <div class="input-group" style="min-width: 150px">
                            <select class="js-example-basic-single select-chosen change-select" style="width: 100%" required="required" name="coordinador" id="coordinador" aria-required="true" aria-invalid="false" data-placeholder="Coordinador" onchange="filterVoter()">
                                <option value=""></option>
                                @foreach($coordinadores as  $key => $value)
                                    @if(isset($request->coordinador))
                                        @if($key == $request->coordinador)
                                            <option value="{{$key}}" selected>{{$value}}</option>
                                        @else
                                            <option value="{{$key}}">{{$value}}</option>
                                        @endif
                                    @else
                                        <option value="{{$key}}">{{$value}}</option>
                                    @endif
                                @endforeach
                            </select>
                            <span class="input-group-addon" style="width: 15px; cursor: pointer" onclick="emptyFilter('coordinador')"><i class="fa fa-remove"></i></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="map" style="height: calc(100vh - 250px) !important"></div>


    <div class="btn-group btn-group-justified" role="group" aria-label="...">
        <div class="btn-group" role="group">
            <button id="btn-intenciones" type="button" class="btn btn-warning" onclick="filtrar(0)">Intenciones de voto</button>
        </div>
        <div class="btn-group" role="group">
            <button id="btn-sitios" type="button" class="btn btn-warning" onclick="filtrar(1)">Puestos de votación</button>
        </div>
        <div class="btn-group" role="group">
            <button id="btn-votantes" type="button" class="btn btn-warning" onclick="filtrar(2)">Votantes</button>
        </div>
        <div class="btn-group" role="group">
            <button id="btn-coordinadores" type="button" class="btn btn-warning" onclick="filtrar(3)">Coordinadores</button>
        </div>
        <div class="btn-group" role="group">
            <button id="btn-orientadores" type="button" class="btn btn-warning" onclick="filtrar(4)">Orientadores</button>
        </div>
        <div class="btn-group" role="group">
            <button id="btn-casas" type="button" class="btn btn-warning" onclick="filtrar(5)">Casas Marcadas</button>
        </div>
    </div>

    <!-- /* ***************************************************************************************** */ -->


    <script>
        let map;
        let infoWindow;
        let layerGroup;
        let showPoints = [false, false, false, false, false, false];
        let markersIntentions;
        let markersPollingStations;
        let markersVoters;
        let markersCoordinators;
        let markersOrientators;
        let markersHomes;

        function getUbicationCenter(){
            return { lat: parseFloat('<?php echo $latitude ?>'), lng: parseFloat('<?php echo $longitude ?>') }
        }

        function getUbicationMarker(element){
            return { lat: element['latitude'], lng: element['longitude'] }

        }

        function getPollingName(polling_id){
            let polling_stations = JSON.parse('<?php echo $polling_stations ?>')
            let pollingObj = polling_stations.find(elem => elem.id == polling_id)

            if(pollingObj) return pollingObj.name

            return ""
        }

        function initMap() {
            map = new google.maps.Map(document.getElementById("map"), {
                zoom: 14,
                center: getUbicationCenter()
            });

            infoWindow = new google.maps.InfoWindow({
                content: "",
                disableAutoPan: true,
            });
        }

        function addMarker(element, image) {
            const marker = new google.maps.Marker({
                position: element,
                icon: image,
            });
            return marker
        }

        function filtrar(_type=0){
            if (showPoints[_type] == false) {
                var textos = "";
                switch (_type) {
                    case 0: {
                        textos = "btn-intenciones"; 
                        break;
                    }
                    case 1: {
                        textos = "btn-sitios"; 
                        break;
                    }

                    case 2: {
                        textos = "btn-votantes"; 
                        break;
                    }

                    case 3: {
                        textos = "btn-coordinadores"; 
                        break;
                    }

                    case 4: {
                        textos = "btn-orientadores"; 
                        break;
                    }

                    case 5: {
                        textos = "btn-casas"; 
                        let filter = document.getElementById('filter-house-check')
                        filter.style.display = "flex";
                        break;
                    }
                }

                var _button = document.getElementById(textos);
                _button.className = "btn btn-success";
            }
            $.ajax ({
                beforeSend:function(jqXHR){
                    if (showPoints[_type] == true) {
                        var textos = "";
                        showPoints[_type] = false;
                        switch (_type) {
                            case 0: {
                                textos = "btn-intenciones";
                                markersIntentions.clearMarkers()
                                break;
                            }
                            case 1: {
                                textos = "btn-sitios";
                                markersPollingStations.clearMarkers()
                                break;
                            }

                            case 2: {
                                textos = "btn-votantes";
                                markersVoters.clearMarkers()
                                break;
                            }

                            case 3: {
                                textos = "btn-coordinadores";
                                markersCoordinators.clearMarkers()
                                break;
                            }

                            case 4: {
                                textos = "btn-orientadores";
                                markersOrientators.clearMarkers()
                                break;
                            }

                            case 5: {
                                textos = "btn-casas";
                                markersHomes.clearMarkers()
                                let filter = document.getElementById('filter-house-check')
                                filter.style.display = "none";
                                break;
                            }
                        }
                        var _button = document.getElementById(textos);
                        _button.className = "btn btn-warning";
                        jqXHR.abort(event);
                    }else{
                        showPoints[_type] = true
                    }
                },
                url: '/info/',
                type: 'get',
                data: { _type },
                success: function(resp) {
                    switch(_type) {
                        // Begin Caso 0 (Intención de voto)
                        case 0: {
                            // Create an array of alphabetical characters used to label the markers.
                            const labels = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                            var customIcon = [
                                    '/images/placeholders/icons/marker-icon-grey.png', 
                                    '/images/placeholders/icons/marker-icon-green.png', 
                                    '/images/placeholders/icons/marker-icon-orange.png', 
                                    '/images/placeholders/icons/marker-icon-red.png'
                                ];
                            var semaphore = ["Indefinido", "Voto fijo", "Indeciso", "Voto duro"];

                            // Add some markers to the map.
                            const markers = resp.map((element, i) => {
                                var intentions = [element['cero'], element['uno'], element['dos'], element['tres']];
                                var totalVotantes = element['total'];
                                var currentIcon = intentions.indexOf(Math.max(...intentions));

                                var popUpText = "<div><strong>" + element['puesto'] + "</strong></div>";
                                for (var i = 0; i < 4; i++) {
                                    popUpText += "<div><img style='height:20px' src='" + customIcon[i] + "'>" + semaphore[i] + ": " + intentions[i] + " (" + parseFloat(Math.round(intentions[i] * 100) / totalVotantes).toFixed(2)  + "%)</div>";
                                }
                                
                                let ubication = getUbicationMarker(element)
                                const marker = addMarker({ lat: element['latitud'], lng: element['longitud'] }, customIcon[currentIcon])

                                marker.addListener("click", () => {
                                    infoWindow.setContent(popUpText);
                                    infoWindow.open(map, marker);
                                });
                                return marker;
                            });
                            console.log(markers)

                            //Add a marker clusterer to manage the data.
                            markersIntentions = new MarkerClusterer(map, markers, { 
                                imagePath: 'https://cdn.rawgit.com/googlemaps/js-marker-clusterer/gh-pages/images/m' 
                            });
                            break;
                        }
                        case 1: {
                            // Create an array of alphabetical characters used to label the markers.
                            const labels = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                            // Add some markers to the map.
                            const markers = resp.map((element, i) => {
                                var popUpText = "<div><strong>" + element['name'] + "</strong></div>";
                                popUpText += "<div><strong>Dirección: </strong>" + element['address']+ "</div>";
                                popUpText += "<div><strong>Mesas: </strong>" + element['tables']+ "</div>";
                                
                                let ubication = getUbicationMarker(element)
                                const marker = addMarker(ubication, '/images/placeholders/icons/marker-polling-station.png')

                                marker.addListener("click", () => {
                                    infoWindow.setContent(popUpText);
                                    infoWindow.open(map, marker);
                                });
                                return marker;
                            });

                            //Add a marker clusterer to manage the data.
                            markersPollingStations = new MarkerClusterer(map, markers, { 
                                imagePath: 'https://cdn.rawgit.com/googlemaps/js-marker-clusterer/gh-pages/images/m' 
                            });
                            break;
                        }

                        case 2: {
                            // Create an array of alphabetical characters used to label the markers.
                            const labels = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                            // Add some markers to the map.
                            const markers = resp.map((element, i) => {    
                                let ubication = getUbicationMarker(element)
                                const marker = addMarker(ubication, '/images/placeholders/icons/marker-polling-station.png')

                                marker.addListener("click", () => {
                                    var popUpText = "<div><strong>" + element['name'] + "</strong></div>";
                                    popUpText += "<div><strong>Dirección: </strong>" + element['address']+ "</div>";
                                    popUpText += "<div><strong>Teléfono: </strong>" + element['phone']+ "</div>";
                                    popUpText += "<div><strong>Puesto de Votación: </strong>" + getPollingName(element['polling_station'])+ "</div>";
                                    popUpText += "<div><strong>Mesa: </strong>" + element['Mesa']+ "</div>";

                                    infoWindow.setContent(popUpText);
                                    infoWindow.open(map, marker);
                                });
                                return marker;
                            });
                            //Add a marker clusterer to manage the data.
                            markersVoters = new MarkerClusterer(map, markers, { 
                                imagePath: 'https://cdn.rawgit.com/googlemaps/js-marker-clusterer/gh-pages/images/m' 
                            });
                            break;
                        }

                        
                        case 3: {
                            // Icon options
                            const labels = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                            // Add some markers to the map.
                            const markers = resp.map((element, i) => {
                                var popUpText = "<div><strong>" + element['name'] + "</strong></div>";
                                popUpText += "<div><strong>Dirección: </strong>" + element['address']+ "</div>";
                                popUpText += "<div><strong>Mesa: </strong>" + element['Mesa']+ "</div>";
                                
                                let ubication = getUbicationMarker(element)
                                const marker = addMarker(ubication, '/images/placeholders/icons/maker-coordinator.jpg')

                                marker.addListener("click", () => {
                                    console.log(popUpText)
                                    infoWindow.setContent(popUpText);
                                    infoWindow.open(map, marker);
                                });
                                return marker;
                            });

                            //Add a marker clusterer to manage the data.
                            markersCoordinators = new MarkerClusterer(map, markers, { 
                                imagePath: 'https://cdn.rawgit.com/googlemaps/js-marker-clusterer/gh-pages/images/m' 
                            });
                            break;
                        }

                        case 4: {
                             // Icon options
                             const labels = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                            // Add some markers to the map.
                            const markers = resp.map((element, i) => {
                                var popUpText = "<div><strong>" + element['name'] + "</strong></div>";
                                popUpText += "<div><strong>Dirección: </strong>" + element['address']+ "</div>";
                                
                                let ubication = getUbicationMarker(element)
                                const marker = addMarker(ubication, '/images/placeholders/icons/maker-orientator.jpg')

                                marker.addListener("click", () => {
                                    console.log(popUpText)
                                    infoWindow.setContent(popUpText);
                                    infoWindow.open(map, marker);
                                });
                                return marker;
                            });

                            //Add a marker clusterer to manage the data.
                            markersOrientators = new MarkerClusterer(map, markers, { 
                                imagePath: 'https://cdn.rawgit.com/googlemaps/js-marker-clusterer/gh-pages/images/m' 
                            });
                            break;
                        }

                        case 5: {
                             // Icon options
                             const labels = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                            // Add some markers to the map.
                            const markers = resp.map((element, i) => {
                                var popUpText = "<div><strong>" + element['name'] + "</strong></div>";
                                popUpText += "<div><strong>Dirección: </strong>" + element['address']+ "</div>";
                                popUpText += "<div><strong>Teléfono: </strong>" + element['phone']+ "</div>";
                                popUpText += "<div><strong>Puesto de Votación: </strong>" + getPollingName(element['polling_station_id'])+ "</div>";
                                popUpText += "<div><strong>Mesa: </strong>" + element['mesa']+ "</div>";

                                let ubication = getUbicationMarker(element)
                                const marker = addMarker(ubication, '/images/placeholders/icons/maker-homes.png')

                                marker.addListener("click", () => {
                                    console.log(popUpText)
                                    infoWindow.setContent(popUpText);
                                    infoWindow.open(map, marker);
                                });
                                return marker;
                            });

                            //Add a marker clusterer to manage the data.
                            markersHomes = new MarkerClusterer(map, markers, { 
                                imagePath: 'https://cdn.rawgit.com/googlemaps/js-marker-clusterer/gh-pages/images/m' 
                            });
                            break;
                        }
                        default: {
                            
                        }
                    }
                },
                error: function(jqXHR, estado, error) {

                },

                complete:function(jqXHR, estado) {

                },

                timeout: 10000,
            })
        }

        function filterVoter(){
            let orientador = $("#orientador").val()
            let coordinador = $("#coordinador").val()

            let url = `/maps/filter`
            let data = {}
            

            if(orientador){
                data.orientador=orientador
            }

            if(coordinador){
                data.coordinador=coordinador
            }
console.log(data)

            $.ajax ({
                url: url,
                type: 'post',
                data: data,
                success: function(resp) {
                    console.log("request", resp)
                    markersHomes.clearMarkers()

                    // Icon options
                    const labels = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                            // Add some markers to the map.
                    const markers = resp.map((element, i) => {
                        let mesa = element['mesa'] ? element['mesa'] : ""
                        let phone = element['phone'] ? element['phone'] : ""

                        var popUpText = "<div><strong>" + element['name'] + "</strong></div>";
                        popUpText += "<div><strong>Dirección: </strong>" + element['address']+ "</div>";
                        popUpText += "<div><strong>Teléfono: </strong>" + phone + "</div>";
                        popUpText += "<div><strong>Puesto de Votación: </strong>" + getPollingName(element['polling_station_id'])+ "</div>";
                        popUpText += "<div><strong>Mesa: </strong>" + mesa + "</div>";

                        let ubication = getUbicationMarker(element)
                        const marker = addMarker(ubication, '/images/placeholders/icons/maker-homes.png')

                        marker.addListener("click", () => {
                            console.log(popUpText)
                            infoWindow.setContent(popUpText);
                            infoWindow.open(map, marker);
                        });
                        return marker;
                    });

                    //Add a marker clusterer to manage the data.
                    markersHomes = new MarkerClusterer(map, markers, { 
                        imagePath: 'https://cdn.rawgit.com/googlemaps/js-marker-clusterer/gh-pages/images/m' 
                    });
                },
                error: function(jqXHR, estado, error) {

                },

                complete:function(jqXHR, estado) {

                },

                timeout: 10000,
            })
            //location.href = url
        }

        function filterBy(){
            let filter = document.getElementById('filter_by').value

            if(filter == "orientador"){
                let select_orientador = document.getElementById('select-orientador')
                select_orientador.style.display = "flex"

                let select_coordinador = document.getElementById('select-coordinador')
                select_coordinador.style.display = "none"

                $("#coordinador")[0].value = ""
                $("#select2-chosen-2").text("")
            }

            if(filter == "coordinador"){
                let select_coordinador = document.getElementById('select-coordinador')
                select_coordinador.style.display = "flex"

                let select_orientador = document.getElementById('select-orientador')
                select_orientador.style.display = "none"
                $("#orientador")[0].value = ""
                $("#select2-chosen-2").text("")
            }
        }

        function emptyFilter(type){
            $("#" + type)[0].value = ""

            if(type == "filter_by"){
                $("#select2-chosen-1").text("")
                $("#select2-chosen-2").text("")
                $("#select2-chosen-3").text("")

                $("#select-orientador").css({display: "none"})
                $("#select-coordinador").css({display: "none"})

                $("#coordinador")[0].value = ""
                $("#orientador")[0].value = ""

                filterVoter()
            }

            if(type == "orientador"){
                $("#select2-chosen-2").text("")
            }

            if(type == "coordinador"){
                $("#select2-chosen-3").text("")
            }
        }

    //     // Variable global
    //     let map;
    //     let layerGroup;
    //     let showPoints = [false, false, false, false, false];
    //     let markersIntentions;
    //     let markersPollingStations;
    //     let markersVoters;
    //     let markersCoordinators;
    //     let markersOrientators;

    //     /**
    //      * Se ejecuta al cargar la vista web
    //      */
    //     function initMap() {
    //         mapWithSearch();
    //     }

    //     /**
    //      * Carga el mapa y lo muestra en la vista
    //      */
    //     function mapWithSearch() {
    //         // Markers Layer
    //         window.markersIntentions = L.layerGroup();
    //         window.markersPollingStations = L.layerGroup();
    //         window.markersVoters = L.layerGroup();
    //         window.markersCoordinators = L.layerGroup();
    //         window.markersOrientators = L.layerGroup();

    //         // Initialize the map and assign it to a variable for later use
    //         window.map = L.map('map', {
    //             // Set latitude and longitude of the map center (required)
    //             center: [{!! $latitude !!}, {!! $longitude !!}],
    //             fullscreenControl: true,
    //             // OR
    //             fullscreenControl: {
    //                 pseudoFullscreen: false // if true, fullscreen to page width and height
    //             },
    //             // Set the initial zoom level, values 0-18, where 0 is most zoomed-out (required)
    //             zoom: {!! $zoom !!},
    //             layers: [window.markersIntentions, window.markersPollingStations, window.markersVoters, window.markersCoordinators, window.markersOrientators]
    //         });

    //         L.control.scale().addTo(window.map);

    //         // Create a Tile Layer and add it to the map
    //         //var tiles = new L.tileLayer('http://{s}.tile.stamen.com/watercolor/{z}/{x}/{y}.png').addTo(map);
    //         L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    //             attribution: '&copy; <a href="/">Radar</a>'
    //         }).addTo(window.map);

    //     }

    //     /**
    //      * Se ejecuta al presionar los botones de la vista
    //      */
    //     function filtrar(_type=0){

    //         $.ajax ({

    //             beforeSend:function(jqXHR){

    //                 if (showPoints[_type] == true) {
    //                     var textos = "";
    //                     showPoints[_type] = false;
    //                     switch (_type) {
    //                         case 0: {
    //                             textos = "btn-intenciones";
    //                             window.markersIntentions.clearLayers();
    //                             break;
    //                         }
    //                         case 1: {
    //                             textos = "btn-sitios";
    //                             window.markersPollingStations.clearLayers();
    //                             break;
    //                         }

    //                         case 2: {
    //                             textos = "btn-votantes";
    //                             window.markersVoters.clearLayers();
    //                             break;
    //                         }

    //                         case 3: {
    //                             textos = "btn-coordinadores";
    //                             window.markersCoordinators.clearLayers();
    //                             break;
    //                         }

    //                         case 4: {
    //                             textos = "btn-orientadores";
    //                             window.markersOrientators.clearLayers();
    //                             break;
    //                         }
    //                     }
    //                     var _button = document.getElementById(textos);
    //                     _button.className = "btn btn-warning";
    //                     jqXHR.abort(event);
    //                 }
    //             },

    //             url: '/info/',
    //             type: 'get',
    //             data: { _type },

    //             success: function(resp) {

    //                 switch(_type) {
    //                     // Begin Caso 0 (Intención de voto)
    //                     case 0: {
    //                         // Icon options
    //                         var icon0 = {
    //                             iconUrl: '/images/placeholders/icons/marker-icon-grey.png',
    //                             shadowUrl: '/images/placeholders/icons/marker-shadow.png',
    //                             iconSize: [25, 41],
    //                             iconAnchor: [12, 41],
    //                             popupAnchor: [1, -34],
    //                             shadowSize: [41, 41]
    //                         }

    //                         var icon1 = {
    //                             iconUrl: '/images/placeholders/icons/marker-icon-green.png',
    //                             shadowUrl: '/images/placeholders/icons/marker-shadow.png',
    //                             iconSize: [25, 41],
    //                             iconAnchor: [12, 41],
    //                             popupAnchor: [1, -34],
    //                             shadowSize: [41, 41]
    //                         }

    //                         var icon2 = {
    //                             iconUrl: '/images/placeholders/icons/marker-icon-orange.png',
    //                             shadowUrl: '/images/placeholders/icons/marker-shadow.png',
    //                             iconSize: [25, 41],
    //                             iconAnchor: [12, 41],
    //                             popupAnchor: [1, -34],
    //                             shadowSize: [41, 41]
    //                         }

    //                         var icon3 = {
    //                             iconUrl: '/images/placeholders/icons/marker-icon-red.png',
    //                             shadowUrl: '/images/placeholders/icons/marker-shadow.png',
    //                             iconSize: [25, 41],
    //                             iconAnchor: [12, 41],
    //                             popupAnchor: [1, -34],
    //                             shadowSize: [41, 41]
    //                         }

    //                         // Creating a custom icon
    //                         var customIcon = [L.icon(icon0), L.icon(icon1), L.icon(icon2), L.icon(icon3)];
    //                         var semaphore = ["Indefinido", "Voto fijo", "Indeciso", "Voto duro"];
    //                         resp.forEach(element => {

    //                             var intentions = [element['cero'], element['uno'], element['dos'], element['tres']];
    //                             var totalVotantes = element['total'];
    //                             var currentIcon = intentions.indexOf(Math.max(...intentions));
    //                             // Options for the marker
    //                             var markerOptions = {
    //                                 title: element['puesto'] + ": " + totalVotantes + ' votantes',
    //                                 clickable: true,
    //                                 draggable: false,
    //                                 icon: customIcon[currentIcon]
    //                             }

    //                             var popUpText = "<div><strong>" + element['puesto'] + "</strong></div>";
    //                             for (var i = 0; i < 4; i++) {
    //                                 popUpText += "<div><img style='height:20px' src='" + customIcon[i].options.iconUrl + "'>" + semaphore[i] + ": " + intentions[i] + " (" + parseFloat(Math.round(intentions[i] * 100) / totalVotantes).toFixed(2)  + "%)</div>";
    //                             }
    //                             popUpText += "<div>Total de Votantes: " + totalVotantes + "</div>";                                
    //                             var marker = new L.Marker([element['latitud'] , element['longitud']], markerOptions).bindPopup(popUpText);
    //                             marker.addTo(window.markersIntentions);
    //                         });
    //                         break;
    //                     }
    //                     // End Caso 0 (Intención de voto)

    //                     // Begin Caso 1 (Puestos de votación)
    //                     case 1: {
    //                         // Icon options
    //                         var icon0 = {
    //                             iconUrl: '/images/placeholders/icons/marker-polling-station.png',
    //                             shadowUrl: '/images/placeholders/icons/marker-shadow.png',
    //                             iconSize: [25, 41],
    //                             iconAnchor: [12, 41],
    //                             popupAnchor: [1, -34],
    //                             shadowSize: [41, 41]
    //                         }

    //                         resp.forEach(element => {

    //                             // Options for the marker
    //                             var markerOptions = {
    //                                 title: element['name'],
    //                                 clickable: true,
    //                                 draggable: false,
    //                                 icon: L.icon(icon0)
    //                             }

    //                             var popUpText = "<div><strong>" + element['name'] + "</strong></div>";
    //                             popUpText += "<div><strong>Dirección: </strong>" + element['address']+ "</div>";
    //                             popUpText += "<div><strong>Mesas: </strong>" + element['tables']+ "</div>";

    //                             var marker = new L.Marker([element['latitude'] , element['longitude']], markerOptions).bindPopup(popUpText);
    //                             marker.addTo(window.markersPollingStations);
    //                         });

    //                         break;
    //                     }

    //                     case 2: {
    //                         // Icon options
    //                         var icon0 = {
    //                             iconUrl: '/images/placeholders/icons/maker-voter.jpg',
    //                             shadowUrl: '/images/placeholders/icons/marker-shadow.png',
    //                             iconSize: [50, 50],
    //                             iconAnchor: [12, 41],
    //                             popupAnchor: [1, -34],
    //                             shadowSize: [41, 41]
    //                         }

    //                         resp.forEach(element => {
    //                             // Options for the marker
    //                             var markerOptions = {
    //                                 title: element['name'],
    //                                 clickable: true,
    //                                 draggable: false,
    //                                 icon: L.icon(icon0)
    //                             }

    //                             var popUpText = "<div><strong>" + element['name'] + "</strong></div>";
    //                             popUpText += "<div><strong>Dirección: </strong>" + element['address']+ "</div>";
    //                             popUpText += "<div><strong>Mesa: </strong>" + element['Mesa']+ "</div>";

    //                             var marker = new L.Marker([element['latitude'] , element['longitude']], markerOptions).bindPopup(popUpText);

    //                             //var marker = new L.Marker([element['latitude'] , element['longitude']], markerOptions).bindPopup(popUpText);
    //                             marker.addTo(window.markersVoters);                                
    //                         });

    //                         break;
    //                     }

                        
    //                     case 3: {
    //                         // Icon options
    //                         var icon0 = {
    //                             iconUrl: '/images/placeholders/icons/maker-coordinator.jpg',
    //                             shadowUrl: '/images/placeholders/icons/marker-shadow.png',
    //                             iconSize: [50, 50],
    //                             iconAnchor: [12, 41],
    //                             popupAnchor: [1, -34],
    //                             shadowSize: [41, 41]
    //                         }

    //                         resp.forEach(element => {
    //                             // Options for the marker
    //                             var markerOptions = {
    //                                 title: element['name'],
    //                                 clickable: true,
    //                                 draggable: false,
    //                                 icon: L.icon(icon0)
    //                             }

    //                             var popUpText = "<div><strong>" + element['name'] + "</strong></div>";
    //                             popUpText += "<div><strong>Dirección: </strong>" + element['address']+ "</div>";
    //                             popUpText += "<div><strong>Mesa: </strong>" + element['Mesa']+ "</div>";

    //                             var marker = new L.Marker([element['latitude'] , element['longitude']], markerOptions).bindPopup(popUpText);

    //                             //var marker = new L.Marker([element['latitude'] , element['longitude']], markerOptions).bindPopup(popUpText);
    //                             marker.addTo(window.markersCoordinators);                                
    //                         });

    //                         break;
    //                     }

    //                     case 4: {
    //                         // Icon options
    //                         var icon0 = {
    //                             iconUrl: '/images/placeholders/icons/maker-orientator.jpg',
    //                             shadowUrl: '/images/placeholders/icons/marker-shadow.png',
    //                             iconSize: [50, 50],
    //                             iconAnchor: [12, 41],
    //                             popupAnchor: [1, -34],
    //                             shadowSize: [41, 41]
    //                         }

    //                         resp.forEach(element => {
    //                             // Options for the marker
    //                             var markerOptions = {
    //                                 title: element['name'],
    //                                 clickable: true,
    //                                 draggable: false,
    //                                 icon: L.icon(icon0)
    //                             }

    //                             var popUpText = "<div><strong>" + element['name'] + "</strong></div>";
    //                             popUpText += "<div><strong>Dirección: </strong>" + element['address']+ "</div>";

    //                             var marker = new L.Marker([element['latitude'] , element['longitude']], markerOptions).bindPopup(popUpText);

    //                             //var marker = new L.Marker([element['latitude'] , element['longitude']], markerOptions).bindPopup(popUpText);
    //                             marker.addTo(window.markersOrientators);                                
    //                         });

    //                         break;
    //                     }
    //                     // End Caso 1 (Puestos de votación)

    //                     // Begin Caso por defecto
    //                     default: {
                            
    //                     }
    //                     // End Caso por defecto
    //                 }

    //                 // Begin Cambia el texto de los botones
    //                 if (showPoints[_type] == false) {
    //                     var textos = "";
    //                     showPoints[_type] = true;
    //                     switch (_type) {
    //                         case 0: {
    //                             textos = "btn-intenciones"; 
    //                             break;
    //                         }
    //                         case 1: {
    //                             textos = "btn-sitios"; 
    //                             break;
    //                         }

    //                         case 2: {
    //                             textos = "btn-votantes"; 
    //                             break;
    //                         }

    //                         case 3: {
    //                             textos = "btn-coordinadores"; 
    //                             break;
    //                         }

    //                         case 4: {
    //                             textos = "btn-orientadores"; 
    //                             break;
    //                         }
    //                     }

    //                     var _button = document.getElementById(textos);
    //                     _button.className = "btn btn-success";
    //                 }
    //                 // End Cambia el texto de los botones

    //         },

    //         error: function(jqXHR, estado, error) {

    //         },

    //         complete:function(jqXHR, estado) {

    //         },

    //         timeout: 10000,

    //     });

    // };

    function updateCoordinates(){
        location.href="/save/coordinates/voters";
    }
    </script>


@endsection