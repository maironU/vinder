<div class="form-group form-group-{{$type}}">
    @if(isset($col_label))
        <label for="{{$type}}" class="{{$col_label}} control-label">{{$label}}
            @if(isset($required) && !$required)
            @else
                <span class="text-danger">*</span>
            @endif
        </label>
    @else
        <label for="{{$type}}" class="col-md-3 control-label">{{$label}}
            @if(isset($required) && !$required)
            @else
                <span class="text-danger">*</span>
            @endif
        </label>
    @endif
    @if(isset($col_div))
        <div class="{{$col_div}}">
    @else
        <div class="col-md-8">
    @endif
        <div class="input-group">
            <select class="js-example-basic-single select-chosen change-select" multiple style="width: 100%" required="required"  id="{{$type}}" name="{{$type}}[]" aria-required="true" aria-invalid="false" data-placeholder="{{$placeholder}}">
                <option value=""></option>
                @foreach($options as  $key => $value)
                    @if(isset($selecteds) && count($selecteds) > 0)
                        @foreach($selecteds as $selected)
                            @if($key == $selected)
                                <option value="{{$key}}" selected>{{$value}}</option>
                            @else
                                <option value="{{$key}}">{{$value}}</option>
                            @endif
                        @endforeach
                    @else
                        <option value="{{$key}}">{{$value}}</option>
                    @endif
                @endforeach
            </select>
            <span class="input-group-addon"><i class="{{$icon}}"></i></span>
        </div>
    </div>
</div>
