<div class="form-group form-group-{{$type}}" style="min-width: 100%; margin-bottom: 0">
    @if(isset($col_div))
        <div class="{{$col_div}}" style="padding: 0">
    @else
        <div class="col-md-8" style="padding: 0">
    @endif
        <div class="input-group" style="min-width: 100%">
            @if(isset($required))
                @if(isset($disabled) && $disabled)
                    <input type="hidden" name="{{$type}}" value={{$selected}}>
                    <select class="js-example-basic-single select-chosen change-select" style="width: 100%" name="{{$type}}" id="{{$type}}" aria-required="true" aria-invalid="false" data-placeholder="{{$placeholder}}" disabled>
                @else
                    <select class="js-example-basic-single select-chosen change-select" style="width: 100%" name="{{$type}}" id="{{$type}}" aria-required="true" aria-invalid="false" data-placeholder="{{$placeholder}}">
                @endif
            @else
                @if(isset($disabled) && $disabled)
                    <input type="hidden" name="{{$type}}" value={{$selected}}>
                    <select class="js-example-basic-single select-chosen change-select" style="width: 100%" required="required" name="{{$type}}" id="{{$type}}" aria-required="true" aria-invalid="false" data-placeholder="{{$placeholder}}" disabled>
                @else
                    <select class="js-example-basic-single select-chosen change-select" style="width: 100%" required="required" name="{{$type}}" id="{{$type}}" aria-required="true" aria-invalid="false" data-placeholder="{{$placeholder}}">
                @endif
            @endif
                <option value=""></option>
                @foreach($options as  $key => $value)
                    @if(isset($selected) && $selected)
                        @if($key == $selected)
                            <option value="{{$key}}" selected>{{$value}}</option>
                        @else
                            <option value="{{$key}}">{{$value}}</option>
                        @endif
                    @else
                        <option value="{{$key}}">{{$value}}</option>
                    @endif
                @endforeach
            </select>
        </div>
    </div>
</div>
