<div class="form-group form-group-{{$type}}">
    @if(isset($col_label))
        <label for="{{$type}}" class="{{$col_label}} control-label">{{$label}}
            <span class="text-danger">*</span>
        </label>
    @else
        <label for="{{$type}}" class="col-md-3 control-label">{{$label}}
            <span class="text-danger">*</span>
        </label>
    @endif
    @if(isset($col_div))
        <div class="{{$col_div}}">
    @else
        <div class="col-md-8">
    @endif
        <div class="input-group">
            @if(isset($required))
                @if(isset($disabled) && $disabled)
                    <input type="hidden" name="{{$type}}" value={{$selected}}>
                    <select class="js-example-basic-single select-chosen change-select" style="width: 100%" name="{{$type}}" id="{{$type}}" aria-required="true" aria-invalid="false" data-placeholder="{{$placeholder}}" disabled>
                @else
                    <select class="js-example-basic-single select-chosen change-select" style="width: 100%" name="{{$type}}" id="{{$type}}" aria-required="true" aria-invalid="false" data-placeholder="{{$placeholder}}">
                @endif
            @else
                @if(isset($disabled) && $disabled)
                    <input type="hidden" name="{{$type}}" value={{$selected}}>
                    <select class="js-example-basic-single select-chosen change-select" style="width: 100%" required="required" name="{{$type}}" id="{{$type}}" aria-required="true" aria-invalid="false" data-placeholder="{{$placeholder}}" disabled>
                @else
                    <select class="js-example-basic-single select-chosen change-select" style="width: 100%" required="required" name="{{$type}}" id="{{$type}}" aria-required="true" aria-invalid="false" data-placeholder="{{$placeholder}}">
                @endif
            @endif
                <option value=""></option>
                @foreach($options as  $key => $value)
                    @if(isset($selected) && $selected)
                        @if($key == $selected)
                            <option  value="{{$key}}" {{old("$type") == $key ? 'selected' : ''}} selected>{{$value}}</option>
                        @else
                            <option value="{{$key}}" {{old("$type") == $key ? 'selected' : ''}} >{{$value}}</option>
                        @endif
                    @else
                        <option value="{{$key}}" {{old("$type") == $key ? 'selected' : ''}} >{{$value}}</option>
                    @endif
                @endforeach
            </select>
            <span class="input-group-addon"><i class="{{$icon}}"></i></span>
        </div>
    </div>
</div>
