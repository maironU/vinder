@extends('dashboard.pages.layout')
@section('title_page', 'Resultados día D')
@section('breadcrumbs') {!! Breadcrumbs::render('witnesses-results') !!} @endsection

@section('content_body_page')
<style>
    @media (max-width: 576px){
        .content-results {
            flex-direction: column;
        }

        .content-results > div, a {
            width: 100%;
            margin-bottom: 3px;
        }
    }.content-form {
        display: flex;
        align-items: center;
        width: 100%;
        padding: 0;
    }

    .content-form > .form-group{
        margin-bottom: 0;
        margin-right: 10px;
        width: 270px;
    }

    .modal-backdrop {
        height: 1000px !important;
    }

    @media (max-width: 560px){
        .content-form {
            flex-direction: column;
            align-items: center;
            justify-content: center;
        }

        .content-form > .form-group, input{
            width: 80%;
            margin: 0;
        }
    }
</style>
<script>
    let rol_auth = "{{Auth::user()->type->id}}"
</script>
<div class="row content-results" id="title_page" style="margin-bottom: 10px;margin: 0; margin-bottom: 10px">
    <div style="display: flex;justify-content: space-between; margin: 0;">        
        <div style="display: flex; align-items: center" class="cont-form-voters-small">
            @if(Auth::user()->type->id == 1)
                <div style="margin-right: 10px">
                    <a href="{{ route('witnesses.create')}}" class="btn btn-primary"><i class="fa fa-user"></i> Nuevo Testigo Electoral</a>
                </div>
            @endif

            <div>
                <a href="{{ route('witnesses.resultstatistic')}}" class="btn btn-primary"><i  style="margin-right: 5px" class="hi hi-stats"></i><span>Resultados Estadísticos</span></a>
            </div>
        </div>

        <?php 
            $polling_stations = \App\Entities\PollingStation::pollingWithMunicipaly();
            $zones = \App\Entities\Zone::all();
        ?>
        <div class="form-group cont-form-voters" style="display: flex; align-items: center; padding-right: 18px; margin-bottom: 0">
            <!--<label class="control-label" style="margin-right: 10px" for="orientador">Orientador</label>-->
            <h4 style="margin-right: 20px" id="text-filter">Filtrar Por:</h4>
            @include('dashboard.pages.utilities.polling_station_filter', ['polling_stations' => $polling_stations])
            @include('dashboard.pages.utilities.zone_filter', ['zones' => $zones])
        </div>
</div>
    <div class="row" style="margin: 0; margin-top: 30px" id="title_page" style="margin-bottom: 10px;">
        <div class="col-sm-8 content-form">
            <div class="form-group">
                <input id="search" type="text" class="form-control" placeholder="Cedula, Nombre, Email, Teléfono" value="{{isset($request) ? $request->search : ''}}" onkeypress="keyPress(event)">
            </div>
            <input type="button" class="btn btn-primary" value="Buscar" onclick="filter()">
            @include('dashboard.pages.utilities.refresh')
        </div>
    </div>
    </div>

    <div class="block full">
        <div class="table-responsive">
            <table id="datatable" class="table table-striped table-bordered table-vcenter">
                <thead>
                        <tr>
                            @include('dashboard.pages.utilities.th_order', ['title' => 'Lugar de Votación', 'description' => 'Lugar de Votación'])
                            <th title="Nombre completo">Mesa Votación</th>
                            <th title="Nombre">Nombre</th>
                            <th title="Teléfono">Teléfono</th>
                            <th title="Nombre Formulario">Nombre Formulario</th>
                            <th title="Ver Formulario">Ver Formulario</th>
                            <th class="text-center" style="width: 115px;"><i class="fa fa-flash"></i></th>
                            <th title="Votos">Votos</th>
                        </tr>
                </thead>
                <tbody id="tbody">
                    @foreach($users as $user)
                        @foreach($formse14 as $form)
                            @foreach($user->tables_numbers as $table_number)
                                <tr>
                                    <td>{{ $user->pollingStation ? $user->pollingStation->name." >> ".$user->pollingStation->address : 'Sin Lugar de votación asignado' }}</td>
                                    <td style="width: 80px"><strong>{{ $table_number->table_number }}</strong></td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->phone }}</td>
                                    <td>{{ $form->name }}</td>
                                    <?php 
                                        $file = $form->files->where('user_id', $user->id)
                                        ->where('table_number', $table_number->table_number)
                                        ->first();
                                    ?>
                                    <td style="text-align: center">
                                        @if($file)
                                            <a target="_blank" href="{{asset('files_forme14/'.$file->file)}}" data-toggle="tooltip" title="Ver Formulario" class="btn btn-effect-ripple btn-warning">
                                                <i class="fa fa-eye"></i>
                                            </a>
                                        @endif

                                        <a href="{{url("witnesses/editform/e14/$form->id/$table_number->table_number/$user->id")}}" data-toggle="tooltip" title="Editar formulario" class="btn btn-effect-ripple btn-warning">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        <form action="{{route('infoForm.delete', [$user->id, $table_number->table_number, $form->id])}}" method="POST" id="form-info-{{$user->id}}-{{$form->id}}-{{$table_number->table_number}}" style="margin: 0">
                                            {!! csrf_field() !!}
                                            {!! method_field('DELETE') !!}

                                            <button data-toggle="tooltip" title="Borrar Información del Formulario" class="btn btn-effect-ripple btn-danger" onclick="clearInfo(event, '{{$user->id}}', '{{$table_number->table_number}}', '{{$form->id}}')">
                                                <i class="fa fa-times"></i>
                                            </button>
                                        </form>
                                    
                                    </td>
                                    <td style="text-align: center">
                                        <?php 
                                            $votes = \App\Entities\FormE14Vote::where('user_id', $user->id)
                                            ->where('table_number', $table_number->table_number)
                                            ->join('forme14_candidates as fc', 'forme14_votes.candidate_id', '=', 'fc.id')
                                            ->where('fc.forme14_id', $form->id)
                                            ->get();

                                            $candidates = $form->candidates;
                                            $file = $form->files->where('user_id', $user->id)->first();

                                        ?>
                                        @if(count($votes) > 0 && $file && $file->file && $file->file_secondary)
                                            <img width="35px" src="{{asset('images/check.png')}}" alt="" style="cursor: pointer"  data-toggle="modal" data-target="#exampleModalLong" onclick="getFormE14('{{$form->id}}', '{{$table_number->table_number}}', '{{$user->id}}')">
                                        @elseif((count($votes) > 0 && !$file) || (count($votes) > 0 && $file && $file->file && !$file->file_secondary))
                                            <img width="35px" src="{{asset('images/check_orange.png')}}" alt="" style="cursor: pointer"  data-toggle="modal" data-target="#exampleModalLong" onclick="getFormE14('{{$form->id}}', '{{$table_number->table_number}}', '{{$user->id}}')">
                                        @else
                                            <div style="width: 35px; height:35px; border-radius: 50%; border: 1px solid #ddd; margin: 0 auto; cursor: pointer"  data-toggle="modal" data-target="#exampleModalLong" onclick="getFormE14('{{$form->id}}', '{{$table_number->table_number}}', '{{$user->id}}')"></div>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        @endforeach
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    @if(isset($request))
        {!! $users->appends(['search' => $request->search])->render() !!}
    @else
        {!! $users->render() !!}
    @endif
    <!-- END Datatables Block -->
    @include('dashboard/pages/witnesses/modal_forme14')

    <script>
        function keyPress(e){
            var keycode = (e.keyCode ? e.keyCode : e.which);
            if (keycode == '13') {
                filter();
                e.preventDefault();
                return false;
            }
        };

        function clearInfo(e, user_id, table_number, form_id){
            e.preventDefault();
            Swal.fire({
                title: '¿Estás Seguro?',
                text: "Se borrará toda la información de votos del testigo electoral",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si borrar'
            }).then((result) => {
                if (result.isConfirmed) {
                    $("#form-info-" + user_id + "-" + form_id + "-" + table_number).submit()
                }
            })
        }
    </script>
@endsection

