@extends('dashboard.pages.layout')
@section('title_page')
    @if($user->exists) Usuario: {{ $user->name }} @else Nuevo Orientador @endif
@endsection
@section('breadcrumbs') {!! Breadcrumbs::render('witnesses-create_leaders', $user) !!} @endsection
@section('content_body_page')
	<div class="row">  
	    <div class="col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
	        <div class="block">
	            <div class="block-title">
	                <h2>Datos del Usuario</h2>
	            </div>
                @if(Session::get('exist'))
					<div class="has-error">
						<div id="name-error" class="help-block animation-pullUp">
							{{ Session::get('exist') }}
						</div>
					</div>
				@endif
                {!! Form::model($user, $form_data + array('id' => 'form-users', 'class' => 'form-horizontal form-bordered'))!!}

                    {!! Field::text('username', old("username"), ['template' => 'horizontal', 'placeholder' => 'Nombre de usuario', 'old' => 'username']) !!}

                    {!! Field::password('password', ['template' => 'horizontal', 'placeholder' => 'Contraseña']) !!}

                    {!! Field::password('password_confirmation', ['template' => 'horizontal', 'placeholder' => 'Repita la Contraseña']) !!}

                    {!! Field::text('name', old('name'), ['template' => 'horizontal', 'placeholder' => 'Nombre Completo', 'id' => 'name']) !!}

                    {!! Field::email('email', old("email"), ['template' => 'horizontal', 'placeholder' => 'Correo Electrónico']) !!}
					@include('dashboard.pages.form-select.select', ["type" => "coordinator_id", "label" => "Coordinador", "placeholder" => "Coordinador", "icon" => "gi gi-user", "col_label" => 'col-md-4', "col_div" => "col-md-6", "options" => $team, "selected" => $user->coordinator_id])

					<?php 
						$locations = \App\Entities\Location::getAllOrder(1);
					?>

    				@include('dashboard.pages.form-select.select', ["type" => "location_id", "label" => "Ubicación", "placeholder" => "Seleccione una Ubicación", "icon" => "fa fa-bars", "options" => $locations, "col_div" => "col-md-6", "col_label" => "col-md-4", "selected" => $user->location_id, "required" => false])

    				{!!  Field::text('address' , null, ['template' => 'horizontal', 'placeholder' => 'Dirección de Vivienda']) !!} 
					<div class="form-group">
						<div class="input-group col-md-12">
							<label for="telephone" class="col-md-4 control-label">Teléfono principal</label>
							<div class="col-md-6">
								<div class="input-group">
									<input id="phone" name="phone" type="text" placeholder="Teléfono principal" value="{{old('phone')}}" aria-required="true" class="form-control ui-wizard-content" value="{{$user->phone ? $user->phone : ""}}">
									<span class="input-group-addon"><i class="fa fa-bars"></i></span>
								</div>
							</div>
						</div>
					</div>

					<div class="form-group">
						<div class="input-group col-md-12">
							<label for="telephone" class="col-md-4 control-label">Casa marcada</label>
							<div class="col-md-6">
								<div class="input-group" style="display: flex">
									<span>
										<i class="fa fa-home" style="margin-right: 5px; align-items:center"></i>
										<input type="checkbox" class="ui-wizard-content" {{ old('house_check') == 'on' ? 'checked' : '' }} id="house_check" name="house_check" <?php if($user->house_check) echo "checked"; ?> >
									</span>
								</div>
							</div>
						</div>
					</div>
	                <div class="form-group form-actions">
	                    <div class="col-md-8 col-md-offset-4">
                            <button type="submit" class="btn btn-effect-ripple btn-primary">Guardar</button>
	                    </div>
	                </div>

	            {!! Form::close()!!}

	        </div>
	    </div>
	</div>
@endsection
@section('js_aditional')
	{!! Html::script('assets/js/pages/formPolling.js') !!}
@endsection