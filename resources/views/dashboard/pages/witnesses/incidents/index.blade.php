@extends('dashboard.pages.layout')
@section('title_page') Incidencias @endsection
@section('breadcrumbs') 
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="/"><i class="fa fa-home"></i></a></li>
        <li><a href="{{route('witnesses.index')}}">Testigos electorales</a></li>
        <li class="active">Incidencias</li>
    </ul>
@endsection
@section('content_body_page')
    <script>
        let rol_auth = "{{Auth::user()->type->id}}"
    </script>
    @if($request->bandera)
        <input type="hidden" id="bandera" value="{{$request->bandera}}">
    @endif
    <div class="row">
        <div class="col-sm-12">
            <div class="block full">
                <div class="block-title">
                    <h2>Total Votantes: {{ $number_voters }} </h2>
                </div>
                <div style="display: flex; justify-content: space-between; margin-bottom: 20px">
                    <div class="flex align-items-center">
                        <div style="display: flex; align-items: center; margin-right: 20px">
                            <span style="margin-right: 5px">Expandir Todos</span>
                            <input style="margin: 0" type="checkbox" id="expand-all" onclick="expandOrCloseAll()">
                        </div>
                        <div id="title_page">
                            <div style="display: flex; justify-content: space-between">
                                <?php 
                                    $polling_stations = \App\Entities\PollingStation::pollingWithMunicipaly();
                                    $zones = \App\Entities\Zone::all();
                                ?>
                                <div class="form-group cont-form-voters" style="display: flex; align-items: center; padding-right: 18px; margin-bottom: 0;">
                                    <!--<label class="control-label" style="margin-right: 10px" for="orientador">Orientador</label>-->
                                    <h4 style="margin-right: 20px" id="text-filter">Filtrar Por:</h4>
                                    @include('dashboard.pages.utilities.polling_station_filter', ['polling_stations' => $polling_stations])
                                    @include('dashboard.pages.utilities.zone_filter', ['zones' => $zones])
                                    @include('dashboard.pages.utilities.refresh')
                                </div>
                            </div>
                        </div>
                    </div>
                
                    <div style="display: flex; align-items: center">
                        <span style="margin-right: 10px">Filtrar</span>
                        <div style="display: flex">
                            @if($request->polling_station && $request->zone)
                                <a href="/witnesses/incidents?polling_station={{$request->polling_station}}&zone={{$request->zone}}&bandera=roja">
                                    <img style="margin-right: 10px; cursor: pointer" src="{{asset('images/bandera_roja.png')}}" alt="" width="30px">
                                </a>
                                <a href="/witnesses/incidents?polling_station={{$request->polling_station}}&zone={{$request->zone}}&bandera=amarilla">
                                    <img style="margin-right: 10px; cursor: pointer" src="{{asset('images/bandera_amarilla.png')}}" alt="" width="30px">
                                </a>
                                <a href="/witnesses/incidents?polling_station={{$request->polling_station}}&zone={{$request->zone}}&bandera=verde">
                                    <img style="margin-right: 10px; cursor: pointer" src="{{asset('images/bandera_verde.png')}}" alt="" width="30px">
                                </a>
                                <a href="/witnesses/incidents?polling_station={{$request->polling_station}}&zone={{$request->zone}}&bandera=gris">
                                    <img style="margin-right: 10px; cursor: pointer" src="{{asset('images/bandera_gris.png')}}" alt="" width="30px">
                                </a>
                                <a href="/witnesses/incidents?polling_station={{$request->polling_station}}&zone={{$request->zone}}" style="display: flex; align-items:center">
                                    <span>Todas</span>
                                </a>
                            @elseif($request->polling_station)
                                <a href="/witnesses/incidents?polling_station={{$request->polling_station}}&bandera=roja">
                                    <img style="margin-right: 10px; cursor: pointer" src="{{asset('images/bandera_roja.png')}}" alt="" width="30px">
                                </a>
                                <a href="/witnesses/incidents?polling_station={{$request->polling_station}}&bandera=amarilla">
                                    <img style="margin-right: 10px; cursor: pointer" src="{{asset('images/bandera_amarilla.png')}}" alt="" width="30px">
                                </a>
                                <a href="/witnesses/incidents?polling_station={{$request->polling_station}}&bandera=verde">
                                    <img style="margin-right: 10px; cursor: pointer" src="{{asset('images/bandera_verde.png')}}" alt="" width="30px">
                                </a>
                                <a href="/witnesses/incidents?polling_station={{$request->polling_station}}&bandera=gris">
                                    <img style="margin-right: 10px; cursor: pointer" src="{{asset('images/bandera_gris.png')}}" alt="" width="30px">
                                </a>
                                <a href="/witnesses/incidents?polling_station={{$request->polling_station}}" style="display: flex; align-items:center">
                                    <span>Todas</span>
                                </a>
                            @elseif($request->zone)
                                <a href="/witnesses/incidents?zone={{$request->zone}}&bandera=roja">
                                    <img style="margin-right: 10px; cursor: pointer" src="{{asset('images/bandera_roja.png')}}" alt="" width="30px">
                                </a>
                                <a href="/witnesses/incidents?zone={{$request->zone}}&bandera=amarilla">
                                    <img style="margin-right: 10px; cursor: pointer" src="{{asset('images/bandera_amarilla.png')}}" alt="" width="30px">
                                </a>
                                <a href="/witnesses/incidents?zone={{$request->zone}}&bandera=verde">
                                    <img style="margin-right: 10px; cursor: pointer" src="{{asset('images/bandera_verde.png')}}" alt="" width="30px">
                                </a>
                                <a href="/witnesses/incidents?zone={{$request->zone}}&bandera=gris">
                                    <img style="margin-right: 10px; cursor: pointer" src="{{asset('images/bandera_gris.png')}}" alt="" width="30px">
                                </a>
                                <a href="/witnesses/incidents?zone={{$request->zone}}" style="display: flex; align-items:center">
                                    <span>Todas</span>
                                </a>
                            @else
                                <a href="/witnesses/incidents?bandera=roja">
                                    <img style="margin-right: 10px; cursor: pointer" src="{{asset('images/bandera_roja.png')}}" alt="" width="30px">
                                </a>
                                <a href="/witnesses/incidents?bandera=amarilla">
                                    <img style="margin-right: 10px; cursor: pointer" src="{{asset('images/bandera_amarilla.png')}}" alt="" width="30px">
                                </a>
                                <a href="/witnesses/incidents?bandera=verde">
                                    <img style="margin-right: 10px; cursor: pointer" src="{{asset('images/bandera_verde.png')}}" alt="" width="30px">
                                </a>
                                <a href="/witnesses/incidents?bandera=gris">
                                    <img style="margin-right: 10px; cursor: pointer" src="{{asset('images/bandera_gris.png')}}" alt="" width="30px">
                                </a>
                                <a href="/witnesses/incidents" style="display: flex; align-items:center">
                                    <span>Todas</span>
                                </a>
                            @endif
                            
                            
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table id="example-datatable" class="table table-bordered table-center">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Ubicación</th>
                                <th>Dirección</th>
                                <th class="text-center">Mesa</th>
                                <th class="text-center">Potencial</th>
                                <th class="text-center" style="width: 153px">Votos Esperados</th>
                                <th class="text-center" style="width: 140px">Votos Testigo</th>
                                <th class="text-center">Alerta</th>
                                <th class="text-center">Chat</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($request->bandera || $request->polling_station)
                                @foreach($pollingStations as $pollingStation)
                                    <tr style="cursor: pointer" class="close-polling display-content">
                                        <td style="display: flex; width: 100%; height: 60px; border: 0"> 
                                            <span>
                                                {{ $pollingStation->description }} 
                                            </span>
                                        </td>
                                        <td style="border: 0"> {{ $pollingStation->location->name }} </td>
                                        <td style="border: 0"> {{ $pollingStation->address }} </td>
                                        <td style="border: 0" class="text-center"> {{ $pollingStation->number_table }} </td>
                                        <td style="border: 0" class="text-center"> {{ $pollingStation->electoral_potential }} </td>
                                        <td style="border: 0" class="text-center"> 
                                            <a href="/reports/people-of-polling-stations?select%5B%5D={{$pollingStation->id}}" target="_blank">
                                                {{ $pollingStation->number_voters_by_table }} 
                                            </a>
                                            <div class="progress progress-striped progress-mini active remove-margin">
                                                <div class="progress-bar progress-bar-danger" role="progressbar" 
                                                    aria-valuenow="{{ $number_voters > 0 ? (($pollingStation->number_voters_by_table / $number_voters) * 100) : 0 }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $number_voters > 0 ? (($pollingStation->number_voters_by_table / $number_voters) * 100) : 0 }}%"></div>
                                            </div>
                                        </td>

                                        <?php 
                                            $forme14 = App\Entities\FormE14::first();
                                            $candidate_check = $forme14->candidate_check;
                                            $vote = null;
                                            if($pollingStation->testigo){
                                                $vote = $pollingStation->testigo->votes()
                                                ->where('table_number', $pollingStation->number_table)
                                                ->where('candidate_id', $candidate_check)
                                                ->first();
                                            }
                                           
                                        ?>

                                        @if($pollingStation->testigo)
                                            @if($vote)
                                                <td data-toggle="modal" data-target="#exampleModalLong" onclick="getFormE14('{{$vote->candidate->forme14_id}}', '{{$pollingStation->number_table}}', '{{$pollingStation->testigo->id}}')" style="border: 0" class="text-center">
                                                    <span>{{$vote->votes_number}}</span>
                                                    <div class="progress progress-striped progress-mini active remove-margin">
                                                        <div class="progress-bar progress-bar-danger" role="progressbar" 
                                                            aria-valuenow="{{ $pollingStation->number_voters_by_table > 0 ? ($vote->votes_number*100) / $pollingStation->number_voters_by_table : 100}}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $pollingStation->number_voters_by_table > 0 ? (($vote->votes_number*100) / $pollingStation->number_voters_by_table) : 100}}%"></div>
                                                    </div>
                                                </td>
                                            @else
                                                <td style="border: 0" class="text-center">
                                                    <span>El testigo no ha marcado check al candidato</span>
                                                </td>
                                            @endif
                                        @else
                                            <td style="border: 0" class="text-center">
                                                <span>No hay testigo para la mesa</span>
                                            </td>
                                        @endif

                                        <td style="border: 0">
                                            <?php 
                                                $total = $pollingStation->number_voters_by_table;
                                                $bandera = asset('images/bandera_gris.png');
                                                $percent = 0;

                                                if($pollingStation->testigo && $vote){
                                                    $percent = $total > 0 ? (($vote->votes_number*100)/$total) : 100;

                                                    if($percent >=80){
                                                        $bandera = asset('images/bandera_verde.png');
                                                    }else if($percent > 50 && $percent < 80){
                                                        $bandera = asset('images/bandera_amarilla.png');
                                                    }else {
                                                        $bandera = asset('images/bandera_roja.png');
                                                    }
                                                }
                                            ?>
                                            <img style="cursor: pointer" src="{{$bandera}}" alt="" width="40px" title="{{$percent}}% de los votos">
                                        </td>
                                        <td style="border: 0">
                                            @if($pollingStation->testigo)
                                                <a target="_blank" href="https://wa.me/{{$pollingStation->testigo->phone}}">
                                                    <img src="{{asset('images/wpp.svg')}}" width="30px" alt="">
                                                </a>
                                            @else
                                                <span>No hay testigo para la mesa</span>
                                            @endif       
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                @foreach($pollingStations as $pollingStation)
                                    <tr style="cursor: pointer" data-idbdd="{{$pollingStation->id}}" data-id="{{$pollingStation->number_table}}" class="polling-items close-polling polling-station-{{$pollingStation->id}} {{$pollingStation->oculto ? 'display-none' : 'display-content'}} {{$pollingStation->number_table == 1 || $pollingStation->last ? 'border-ddd' : ''}}">
                                        <td onclick="{{$pollingStation->number_table == 1 ? 'expandOrClose('.$pollingStation->id.')' : ''}}" style="display: flex; width: 100%; height: 60px; border: 0"> 
                                            @if($pollingStation->number_table == 1)
                                                <img title="Expandir" id="img-{{$pollingStation->id}}" onclick="expandOrClose('{{$pollingStation->id}}')" src="{{asset('images/arrow-bottom.png')}}" style="margin-right: 10px; cursor-pointer" width="20px" height="20px" alt="">
                                            @endif

                                            <span>
                                                {{ $pollingStation->description }} 
                                            </span>
                                        </td>
                                        <td onclick="{{$pollingStation->number_table == 1 ? 'expandOrClose('.$pollingStation->id.')' : ''}}" style="border: 0"> {{ $pollingStation->location->name }} </td>
                                        <td onclick="{{$pollingStation->number_table == 1 ? 'expandOrClose('.$pollingStation->id.')' : ''}}" style="border: 0"> {{ $pollingStation->address }} </td>
                                        <td onclick="{{$pollingStation->number_table == 1 ? 'expandOrClose('.$pollingStation->id.')' : ''}}" style="border: 0" class="text-center"> {{ $pollingStation->number_table }} </td>
                                        <td onclick="{{$pollingStation->number_table == 1 ? 'expandOrClose('.$pollingStation->id.')' : ''}}" style="border: 0" class="text-center"> {{ $pollingStation->electoral_potential }} </td>
                                        <td onclick="{{$pollingStation->number_table == 1 ? 'expandOrClose('.$pollingStation->id.')' : ''}}" style="border: 0" class="text-center"> 
                                            <a href="/reports/people-of-polling-stations?select%5B%5D={{$pollingStation->id}}" target="_blank">
                                                {{ $pollingStation->number_voters_by_table }} 
                                            </a>
                                            <div class="progress progress-striped progress-mini active remove-margin">
                                                <div class="progress-bar progress-bar-danger" role="progressbar" 
                                                    aria-valuenow="{{ $number_voters > 0 ? (($pollingStation->number_voters_by_table / $number_voters) * 100) : 0 }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $number_voters > 0 ? (($pollingStation->number_voters_by_table / $number_voters) * 100) : 0 }}%"></div>
                                            </div>
                                        </td>

                                         <?php 
                                            $forme14 = App\Entities\FormE14::first();
                                            $candidate_check = $forme14->candidate_check;
                                            $vote = null;
                                            if($pollingStation->testigo){
                                                $vote = $pollingStation->testigo->votes()
                                                ->where('table_number', $pollingStation->number_table)
                                                ->where('candidate_id', $candidate_check)
                                                ->first();
                                            }
                                        ?>

                                        @if($pollingStation->testigo)
                                            @if($vote)
                                                <td data-toggle="modal" data-target="#exampleModalLong" onclick="getFormE14('{{$vote->candidate->forme14_id}}', '{{$pollingStation->number_table}}', '{{$pollingStation->testigo->id}}')" style="border: 0" class="text-center">
                                                    <span>{{$vote->votes_number}}</span>
                                                    <div class="progress progress-striped progress-mini active remove-margin">
                                                        <div class="progress-bar progress-bar-danger" role="progressbar" 
                                                            aria-valuenow="{{ $pollingStation->number_voters_by_table > 0 ? (($vote->votes_number*100) / $pollingStation->number_voters_by_table) : 100}}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $pollingStation->number_voters_by_table > 0 ? (($vote->votes_number*100) / $pollingStation->number_voters_by_table) : 100}}%"></div>
                                                    </div>
                                                </td>
                                            @else
                                                <td style="border: 0" class="text-center">
                                                    <span>El testigo no ha llenado el formulario de votos</span>
                                                </td>
                                            @endif
                                        @else
                                            <td style="border: 0" class="text-center">
                                                <span>No hay testigo para la mesa</span>
                                            </td>
                                        @endif

                                        <td onclick="{{$pollingStation->number_table == 1 ? 'expandOrClose('.$pollingStation->id.')' : ''}}" style="border: 0">
                                            <?php 
                                                $total = $pollingStation->number_voters_by_table;
                                                $bandera = asset('images/bandera_gris.png');
                                                $percent = 0;

                                                if($pollingStation->testigo && $vote){
                                                    $percent = $total > 0 ? (($vote->votes_number*100)/$total) : 100;

                                                    if($percent >=80){
                                                        $bandera = asset('images/bandera_verde.png');
                                                    }else if($percent > 50 && $percent < 80){
                                                        $bandera = asset('images/bandera_amarilla.png');
                                                    }else {
                                                        $bandera = asset('images/bandera_roja.png');
                                                    }
                                                }
                                            ?>
                                            <img style="cursor: pointer" src="{{$bandera}}" alt="" width="40px" title="{{$percent}}% de los votos">
                                        </td>
                                        <td onclick="{{$pollingStation->number_table == 1 ? 'expandOrClose('.$pollingStation->id.')' : ''}}" style="border: 0">
                                            @if($pollingStation->testigo)
                                                <a target="_blank" href="https://wa.me/{{$pollingStation->testigo->phone}}">
                                                    <img src="{{asset('images/wpp.svg')}}" width="30px" alt="">
                                                </a>
                                            @else
                                                <span>No hay testigo para la mesa</span>
                                            @endif       
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @if($pollingStationsNew instanceof \Illuminate\Pagination\LengthAwarePaginator )
        {!! $pollingStationsNew->render() !!}
    @endif

    @include('dashboard/pages/witnesses/modal_forme14')

<script>
    window.addEventListener("load", (event) => {
        setInterval(() => {
            getIncidents()
        }, 10000);
    });

    async function getIncidents(){
        const response = await fetch(`${window.location.href}/api`);
        const res = await response.json();

        if(res.success){
            if(res.data.length > 0){
                res.data.forEach(el => {
                    Toastify({
                        text: el.message,
                        duration: 3000,
                        avatar: '/images/bandera_roja.png',
                        style: {
                            background: "#63000A"
                        },
                        close: true,
                        }).showToast();
                })
            } 
        }
    }

    function expandOrCloseAll(){
        console.log("si");
        let check = $("#expand-all")
        let open = false

        $(".polling-items").each(function(){
            let mesa = $(this).data("id")
            let id = $(this).data("idbdd")

            if(mesa == 1){
                if(!check.is(':checked')){
                    open = true;
                    $(this).css({background: "#ffff"})
                    $("#img-" + id).attr('src', '/images/arrow-bottom.png')
                    $("#img-" + id).attr('title', 'Cerrar')
                    expand_all = false
                }else{
                    $(this).css({background: "#E3E3E3"})
                    $("#img-" + id).attr('src', '/images/arrow-up.png')
                    $("#img-" + id).attr('title', 'Expandir')
                    expand_all = true
                }
            }else{
                if(open){
                    if(mesa > 1){
                        $(this).removeClass('display-content')
                        $(this).addClass('display-none')
                    }else{
                        $(this).removeClass('open-polling')
                        $(this).addClass('close-polling')
                    }
                }else{
                    if(mesa > 1){
                        $(this).removeClass('display-none')
                        $(this).addClass('display-content')
                    }else{
                        $(this).addClass('close-polling')
                        $(this).addClass('open-polling')
                    }
                }
            }            
        })
    }

    function expandOrClose(id){
        let tr_elements = $(".polling-station-"+id)
        let open = false
        tr_elements.each(function(){
            let mesa = $(this).data('id')

            if(mesa == 1){
                if($(this).hasClass('open-polling')){
                    open = true;
                    $(this).css({background: "#ffff"})
                    $("#img-" + id).attr('src', '/images/arrow-bottom.png')
                    $("#img-" + id).attr('title', 'Cerrar')
                }else{
                    $(this).css({background: "#E3E3E3"})
                    $("#img-" + id).attr('src', '/images/arrow-up.png')
                    $("#img-" + id).attr('title', 'Expandir')
                }
            }else{
                if(open){
                    if(mesa > 1){
                        $(this).removeClass('display-content')
                        $(this).addClass('display-none')
                    }else{
                        $(this).removeClass('open-polling')
                        $(this).addClass('close-polling')
                    }
                }else{
                    if(mesa > 1){
                        $(this).removeClass('display-none')
                        $(this).addClass('display-content')
                    }else{
                        $(this).addClass('close-polling')
                        $(this).addClass('open-polling')
                    }
                }
            }
        })

        tr_elements.each(function(){
            let mesa = $(this).data('id')

            if(open){
                if(mesa > 1){
                    $(this).removeClass('display-content')
                    $(this).addClass('display-none')
                }else{
                    $(this).removeClass('open-polling')
                    $(this).addClass('close-polling')
                }
            }else{
                if(mesa > 1){
                    $(this).removeClass('display-none')
                    $(this).addClass('display-content')
                }else{
                    $(this).addClass('close-polling')
                    $(this).addClass('open-polling')
                }
            }
            
            console.log(mesa)

        })
    }
</script>
@endsection

@section('js_aditional')

@endsection

