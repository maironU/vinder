@extends('dashboard.pages.layout')
@section('title_page', 'Coordinador de zonas')
@section('breadcrumbs') {!! Breadcrumbs::render('coordinator_zone-index') !!} @endsection

@section('content_body_page')
<style>
    @media (max-width: 576px){
        .content-results {
            flex-direction: column;
        }

        .content-results > div, a {
            width: 100%;
            margin-bottom: 3px;
        }
    }.content-form {
        display: flex;
        align-items: center;
        width: 100%;
        padding: 0;
    }

    .content-form > .form-group{
        margin-bottom: 0;
        margin-right: 10px;
        width: 270px;
    }

    .modal-backdrop {
        height: 1000px !important;
    }

    @media (max-width: 560px){
        .content-form {
            flex-direction: column;
            align-items: center;
            justify-content: center;
        }

        .content-form > .form-group, input{
            width: 80%;
            margin: 0;
        }
    }
</style>
    <div class="row content-results" id="title_page" style="margin-bottom: 10px;margin: 0; margin-bottom: 10px">
        <div style="display: flex;justify-content: space-between; margin: 0;">        
            <div style="display: flex; align-items: center">
                @if(Auth::user()->type->id == 1)
                    <div style="margin-right: 10px">
                        <a href="{{ route('coordinator_zone.create')}}" class="btn btn-primary"><i class="fa fa-user"></i> Nuevo Coordinador de Zona</a>
                    </div>
                @endif
            </div>
    </div>
    @if (session('message'))
        <div class="btn-alert-danger" id="alert">
            <span>{{ session('message') }}</span> <span style="position: absolute; right: 12px; cursor:pointer;font-size: 12px;line-height: 22px" id="close">X</span>
        </div>
    @endif
    <?php 
        $zones = \App\Entities\Zone::all();
    ?>
    <div class="row" style="margin: 0; margin-top: 30px; margin-bottom: 10px;display:flex; justify-content: space-between" id="title_page" style="margin-bottom: 10px;">
        <div class="col-sm-8 content-form">
            <div class="form-group">
                <input id="search" type="text" class="form-control" placeholder="Cedula, Nombre, Email, Teléfono" value="{{isset($request) ? $request->search : ''}}" onkeypress="keyPress(event)">
            </div>
            <input type="button" class="btn btn-primary" value="Buscar" onclick="filter()">
            <input type="button" class="btn btn-primary ml-2 btn_find_create" value="Buscar y Crear" onclick="findAndCreate()">
            @include('dashboard.pages.utilities.refresh')
        </div>
        <div class="form-group cont-form-voters" style="display: flex; align-items: center; padding-right: 18px; margin-bottom: 0">
            <h4 style="margin-right: 20px; width: max-content" id="text-filter">Filtrar Por:</h4>
            @include('dashboard.pages.utilities.zone_filter', ['zones' => $zones])
        </div>  
    </div>

    <div class="block full">
        <div class="table-responsive">
            <table id="example-datatable" class="table table-bordered table-center">
                <thead>
                    <tr>
                        <th style="min-width: 200px">Nombre</th>
                        <th style="min-width: 200px">Cédula</th>
                        <th style="min-width: 200px">Teléfono</th>
                        <th>Zona</th>
                        <th class="text-center" style="width: 115px;"><i class="fa fa-flash"></i></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td style="width: fit-content">{{ $user->name }}</td>
                            <td style="width: fit-content">{{ $user->username }}</td>
                            <td style="width: fit-content">{{ $user->phone }}</td>
                            <td style="width: fit-content">{{ $user->zone->zone->name }}</td>
                            <td class="text-center" style="display: flex; gap: 4px">
                                <a href="{{ route('coordinator_zone.edit', $user->id)}}" data-toggle="tooltip" title="Editar coordinador de zona" class="btn btn-effect-ripple btn-warning">
                                    <i class="fa fa-pencil"></i>
                                </a>
                                <a href="#" data-id="{{ $user->id }}" id="btn-delete-{{ $user->id }}" onclick="deleteModel('btn-delete-{{ $user->id }}')"  data-toggle="tooltip" title="Borrar coordinador de zona" class="btn btn-effect-ripple btn-danger">
                                    <i class="fa fa-times"></i>
                                </a>
                                <a href="{{ route('coordinator.index', $user->id)}}" data-toggle="tooltip" title="" class="btn btn-effect-ripple btn-warning" style="overflow: hidden; position: relative;" data-original-title="Ver información de coordinadores de puestos"><span class="btn-ripple animate" style="height: 40px; width: 40px; top: -11.4688px; left: 6.45312px;"></span>
                                    <i class="fa fa-eye"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    @if(isset($request))
        {!! $users->appends(['search' => $request->search])->render() !!}
    @else
        {!! $users->render() !!}
    @endif
    
    {!! Form::open(array('route' => array('system.users.destroy', 'ID') , 'method' => 'DELETE', 'role' => 'form', 'id' => 'form-delete'))!!}


    <script>
        function filter(){
            let search = $("#search").val()
            let polling_station = $("#polling_station").val()
            let url = "/witnesses/coordinator?"

            if(search){
                url+= "search=" + search + "&"
            }
            if(polling_station){
                url+= "polling_station=" + polling_station
            }
            location.href=url
        }

        function findAndCreate(){
            let search = $("#search").val()
            if(!search) return alert("Por favor escriba la cédula")

            location.href  = "/witnesses/coordinator_zone/create/" + search
        }

        function emptyFilter(){
            let search = $("#search").val()
            let url = "/witnesses/coordinator?"

            if(search){
                url+= "search=" + search + "&"
            }
            location.href=url
        }

        function keyPress(e){
            var keycode = (e.keyCode ? e.keyCode : e.which);
            if (keycode == '13') {
                filter();
                e.preventDefault();
                return false;
            }
        };
    </script>
@endsection

