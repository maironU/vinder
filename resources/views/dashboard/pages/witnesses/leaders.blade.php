@extends('dashboard.pages.layout')
@section('title_page', 'Asignar Orientadores')
@section('breadcrumbs') {!! Breadcrumbs::render('witnesses-leaders') !!} @endsection

@section('content_body_page')

<div class="row" id="title_page" style="margin-bottom: 10px;">
    	<div class="col-md-12">
            <a href="{{ route('witnesses.leaders.create')}}" class="btn btn-primary"><i class="fa fa-user"></i> Nuevo Orientador</a>
        </div>
    </div>
    <div class="block full">
        <div class="table-responsive">
            <table id="datatable" class="table table-striped table-bordered table-vcenter">
                <thead>
                    <tr>
                        <th title="Nombre de Usuario">Nombre de usuario</th>
                        <th title="Nombre completo">Nombre completo</th>
                        <th title="Correo">Email</th>
                        <th title="Correo">Dirección</th>
                        <th title="Correo">Ubicación</th>
                        <th title="Correo">Teléfono</th>
                        <th title="Correo">Coordinador</th>
                        <th title="Correo">Casa Marcada</th>
                        <th class="text-center" style="width: 115px;"><i class="fa fa-flash"></i></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                        <tr>
                            
                            <td>{{ $user->username }}</td>
                            <td><strong>{{ $user->name }}</strong></td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->address ? $user->address : "Sin Dirección"}}</td>
                            <td>{{ $user->location ? $user->location->name : "Sin Ubicación"}}</td>
                            <td>{{ $user->phone ? $user->phone : "Sin Teléfono"}}</td>
                            <td>{{ $user->coordinator ? $user->coordinator->name : "Sin Coordinador Asignado"}}</td>
                            <td>{{ $user->house_check ? "Si" : "No"}}</td>
                            <td class="text-center">
                                <a href="{{ route('witnesses.leaders.edit', $user->id)}}" data-toggle="tooltip" title="Editar tipo de usuario" class="btn btn-effect-ripple btn-warning">
                                    <i class="fa fa-pencil"></i>
                                </a>
                                <a href="#" data-id="{{ $user->id }}" id="btn-delete-{{ $user->id }}" onclick="deleteModel('btn-delete-{{ $user->id }}')"  data-toggle="tooltip" title="Borrar líder" class="btn btn-effect-ripple btn-danger">
                                    <i class="fa fa-times"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- END Datatables Block -->
    {!! Form::open(array('route' => array('system.users.destroy', 'ID') , 'method' => 'DELETE', 'role' => 'form', 'id' => 'form-delete'))!!}

@endsection
@section('js_aditional')
@endsection

