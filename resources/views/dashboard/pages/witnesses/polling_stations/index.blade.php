@extends('dashboard.pages.layout')
@section('title_page') Puestos de Votación @endsection
@section('breadcrumbs') 
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="/"><i class="fa fa-home"></i></a></li>
        <li><a href="{{route('witnesses.index')}}">Testigos electorales</a></li>
        <li class="active">Puestos de Votación</li>
    </ul>
@endsection
@section('content_body_page')
    <script>
        let rol_auth = "{{Auth::user()->type->id}}"
    </script>
    <div class="row">
        <div class="col-sm-12">
            <div class="block full">
                <div class="block-title">
                    <h2>Total Votantes: {{ $number_voters }} </h2>
                </div>
                <div style="display: flex; justify-content: space-between; margin-bottom: 20px">
                    <div class="flex align-items-center">
                        <div style="display: flex; align-items: center; margin-right: 20px">
                            <span style="margin-right: 5px">Expandir Todos</span>
                            <input style="margin: 0" type="checkbox" id="expand-all" onclick="expandOrCloseAll()">
                        </div>
                        <div id="title_page">
                            <div style="display: flex; justify-content: space-between">
                                <?php 
                                    $polling_stations = \App\Entities\PollingStation::pollingWithMunicipaly();
                                    $zones = \App\Entities\Zone::all();

                                ?>
                                <div class="form-group cont-form-voters" style="display: flex; align-items: center; padding-right: 18px; margin-bottom: 0;">
                                    <!--<label class="control-label" style="margin-right: 10px" for="orientador">Orientador</label>-->
                                    <h4 style="margin-right: 20px" id="text-filter">Filtrar Por:</h4>
                                    @include('dashboard.pages.utilities.polling_station_filter', ['polling_stations' => $polling_stations])
                                    @include('dashboard.pages.utilities.zone_filter', ['zones' => $zones])
                                    @include('dashboard.pages.utilities.refresh')
                                </div>
                            </div>
                        </div>
                    </div>
                
                    <div style="display: flex; align-items: center">
                        <span style="margin-right: 10px">Filtrar</span>
                        <div style="display: flex">
                            <div style="padding-right: 0">
                                <div class="input-group" style="min-width: 150px">
                                    <select style="max-width: 180px" class="js-example-basic-single select-chosen change-select" required="required" name="Testigos" id="testigo" aria-required="true" aria-invalid="false" data-placeholder="Testigos" onchange="filter()">
                                        @if($request->testigo && $request->testigo == "with")
                                            <option value="">Todas</option>
                                            <option value="with" selected>Con testigos asignados</option>
                                            <option value="without">Sin testigos asignados</option>
                                        @elseif($request->testigo && $request->testigo == "without")
                                            <option value="">Todas</option>
                                            <option value="with">Con testigos asignados</option>
                                            <option value="without" selected>Sin testigos asignados</option>
                                        @else
                                            <option value="" selected>Todas</option>
                                            <option value="with">Con testigos asignados</option>
                                            <option value="without">Sin testigos asignados</option>
                                        @endif
                                        
                                    </select>
                                    <span class="input-group-addon" style="width: 15px; cursor: pointer" onclick="emptyFilter('testigo')"><i class="fa fa-remove"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table id="example-datatable" class="table table-bordered table-center">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Ubicación</th>
                                <th>Dirección</th>
                                <th class="text-center">Mesa</th>
                                <th class="text-center">Potencial</th>
                                <th class="text-center" style="width: 153px">Votos Esperados</th>
                                <th class="text-center">Votos Testigo</th>
                                <th class="text-center">Editar</th>
                            </tr>
                        </thead>
                        <tbody id="tbody">
                            @if($request->bandera || $request->polling_station)
                                @foreach($pollingStations as $pollingStation)
                                    <tr style="cursor: pointer" class="close-polling display-content">
                                        <td style="display: flex; width: 100%; height: 60px; border: 0"> 
                                            <span>
                                                {{ $pollingStation->description }} 
                                            </span>
                                        </td>
                                        <td style="border: 0"> {{ $pollingStation->location->name }} </td>
                                        <td style="border: 0"> {{ $pollingStation->address }} </td>
                                        <td style="border: 0" class="text-center"> {{ $pollingStation->number_table }} </td>
                                        <td style="border: 0" class="text-center"> {{ $pollingStation->electoral_potential }} </td>
                                        <td style="border: 0" class="text-center"> 
                                            <a href="/reports/people-of-polling-stations?select%5B%5D={{$pollingStation->id}}" target="_blank">
                                                {{ $pollingStation->number_voters_by_table }} 
                                            </a>
                                            <div class="progress progress-striped progress-mini active remove-margin">
                                                <div class="progress-bar progress-bar-danger" role="progressbar" 
                                                    aria-valuenow="{{ $number_voters > 0 ? (($pollingStation->number_voters_by_table / $number_voters) * 100) : 0 }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $number_voters > 0 ? (($pollingStation->number_voters_by_table / $number_voters) * 100) : 0 }}%"></div>
                                            </div>
                                        </td>

                                        @if($pollingStation->testigo)
                                            @if($pollingStation->testigo->votes->count() > 0)
                                                <td data-toggle="modal" data-target="#exampleModalLong" onclick="getFormE14('{{$pollingStation->testigo->votes[0]->candidate->forme14_id}}', '{{$pollingStation->number_table}}', '{{$pollingStation->testigo->id}}')" style="border: 0" class="text-center">
                                                    <span>{{$pollingStation->testigo->votes[0]->votes_number}}</span>
                                                    <div class="progress progress-striped progress-mini active remove-margin">
                                                        <div class="progress-bar progress-bar-danger" role="progressbar" 
                                                            aria-valuenow="{{ $pollingStation->number_voters_by_table > 0 ? ($pollingStation->testigo->votes[0]->votes_number*100) / $pollingStation->number_voters_by_table : 100}}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $pollingStation->number_voters_by_table > 0 ? (($pollingStation->testigo->votes[0]->votes_number*100) / $pollingStation->number_voters_by_table) : 100}}%"></div>
                                                    </div>
                                                </td>
                                            @else
                                                <td style="border: 0" class="text-center">
                                                    <span>El testigo no ha marcado check al candidato</span>
                                                </td>
                                            @endif
                                        @else
                                            <td style="border: 0" class="text-center">
                                                <span>No hay testigo para la mesa</span>
                                            </td>
                                        @endif

                                        @if(!$pollingStation->testigo)
                                           <td>
                                                <a href="#" data-toggle="modal" data-target="#exampleModal" title="Asignar testigo a la mesa" class="btn btn-effect-ripple btn-warning" onclick="editPolling({{$pollingStation->id}}, {{$pollingStation->number_table}})">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                           </td>
                                        @endif
                                    </tr>
                                @endforeach
                            @else
                                @foreach($pollingStations as $pollingStation)
                                    <tr style="cursor: pointer" data-idbdd="{{$pollingStation->id}}" data-id="{{$pollingStation->number_table}}" class="polling-items close-polling polling-station-{{$pollingStation->id}} {{$pollingStation->oculto ? 'display-none' : 'display-content'}} {{$pollingStation->number_table == 1 || $pollingStation->last ? 'border-ddd' : ''}}">
                                        <td onclick="{{$pollingStation->number_table == 1 ? 'expandOrClose('.$pollingStation->id.')' : ''}}" style="display: flex; width: 100%; height: 60px; border: 0"> 
                                            @if($pollingStation->number_table == 1)
                                                <img title="Expandir" id="img-{{$pollingStation->id}}" onclick="expandOrClose('{{$pollingStation->id}}')" src="{{asset('images/arrow-bottom.png')}}" style="margin-right: 10px; cursor-pointer" width="20px" height="20px" alt="">
                                            @endif

                                            <span>
                                                {{ $pollingStation->description }} 
                                            </span>
                                        </td>
                                        <td onclick="{{$pollingStation->number_table == 1 ? 'expandOrClose('.$pollingStation->id.')' : ''}}" style="border: 0"> {{ $pollingStation->location->name }} </td>
                                        <td onclick="{{$pollingStation->number_table == 1 ? 'expandOrClose('.$pollingStation->id.')' : ''}}" style="border: 0"> {{ $pollingStation->address }} </td>
                                        <td onclick="{{$pollingStation->number_table == 1 ? 'expandOrClose('.$pollingStation->id.')' : ''}}" style="border: 0" class="text-center"> {{ $pollingStation->number_table }} </td>
                                        <td onclick="{{$pollingStation->number_table == 1 ? 'expandOrClose('.$pollingStation->id.')' : ''}}" style="border: 0" class="text-center"> {{ $pollingStation->electoral_potential }} </td>
                                        <td onclick="{{$pollingStation->number_table == 1 ? 'expandOrClose('.$pollingStation->id.')' : ''}}" style="border: 0" class="text-center"> 
                                            <a href="/reports/people-of-polling-stations?select%5B%5D={{$pollingStation->id}}" target="_blank">
                                                {{ $pollingStation->number_voters_by_table }} 
                                            </a>
                                            <div class="progress progress-striped progress-mini active remove-margin">
                                                <div class="progress-bar progress-bar-danger" role="progressbar" 
                                                    aria-valuenow="{{ $number_voters > 0 ? (($pollingStation->number_voters_by_table / $number_voters) * 100) : 0 }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $number_voters > 0 ? (($pollingStation->number_voters_by_table / $number_voters) * 100) : 0 }}%"></div>
                                            </div>
                                        </td>

                                        @if($pollingStation->testigo)
                                            @if($pollingStation->testigo->votes->count() > 0)
                                                <td data-toggle="modal" data-target="#exampleModalLong" onclick="getFormE14('{{$pollingStation->testigo->votes[0]->candidate->forme14_id}}', '{{$pollingStation->number_table}}', '{{$pollingStation->testigo->id}}')" style="border: 0" class="text-center">
                                                    <span>{{$pollingStation->testigo->votes[0]->votes_number}}</span>
                                                    <div class="progress progress-striped progress-mini active remove-margin">
                                                        <div class="progress-bar progress-bar-danger" role="progressbar" 
                                                            aria-valuenow="{{ $pollingStation->number_voters_by_table > 0 ? (($pollingStation->testigo->votes[0]->votes_number*100) / $pollingStation->number_voters_by_table) : 100}}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $pollingStation->number_voters_by_table > 0 ? (($pollingStation->testigo->votes[0]->votes_number*100) / $pollingStation->number_voters_by_table) : 100}}%"></div>
                                                    </div>
                                                </td>
                                            @else
                                                <td style="border: 0" class="text-center">
                                                    <span>El testigo no ha llenado el formulario de votos</span>
                                                </td>
                                            @endif
                                        @else
                                            <td style="border: 0" class="text-center">
                                                <span>No hay testigo para la mesa</span>
                                            </td>
                                        @endif

                                        @if(!$pollingStation->testigo)
                                            <td>
                                                <a href="#" data-toggle="modal" data-target="#exampleModal" title="Asignar testigo a la mesa" class="btn btn-effect-ripple btn-warning" onclick="editPolling({{$pollingStation->id}}, {{$pollingStation->number_table}})">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                            </td>
                                        @endif
                                    </tr>
                                @endforeach
                            @endif
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" style="background: rgba(34,34,34,0.85)" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Asignar testigo a la mesa</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <h3 class="text-center">Escoge el testigo que quieres asignar a la mesa</h3>
                <div style="padding-right: 0">
                    <div class="input-group" style="width: 90%; margin: 0 auto">
                        <select style="width: 100%" class="js-example-basic-single select-chosen change-select" required="required" name="Testigos" id="assign_testigo" aria-required="true" aria-invalid="false" data-placeholder="Testigos">
                            <option value="" selected></option>
                            @foreach($testigosSinMesa as  $key => $testigo)
                                <option value="{{$testigo->id}}">{{$testigo->username}}-{{$testigo->name}} {{$testigo->pollingStation ? "-".$testigo->pollingStation->name : ''}}</option>
                            @endforeach
                        </select>
                        <span class="input-group-addon" style="width: 15px; cursor: pointer" onclick="emptyFilter('polling_station')"><i class="fa fa-remove"></i></span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" onclick="assignTestigo()">Asignar</button>
            </div>
        </div>
        </div>
    </div>
    @if($pollingStationsNew instanceof \Illuminate\Pagination\LengthAwarePaginator )
        {!! $pollingStationsNew->render() !!}
    @endif

    @include('dashboard/pages/witnesses/modal_forme14')

<script>

    let polling_station_id = null;
    let mesa = null;

    function editPolling(polling, num_table){
        console.log(polling_station_id)
        console.log(mesa)

        polling_station_id = polling;
        mesa = num_table;
    }

    function assignTestigo(){
        let testigo = $("#assign_testigo").val()

        if(testigo && polling_station_id && mesa){
                $.post(`${url_home}/witnesses/polling_stations/assign_testigo`, {
                    'polling_station_id': polling_station_id,
                    'mesa': mesa,
                    'testigo_id': testigo
                }).then(response => {
                console.log(response)
                if (response.success) {
                    Toastify({
                    text: response.message,
                    className: "info",
                    close: true,
                    style: {
                    background: "linear-gradient(to right, #00b09b, #96c93d)",
                }
            }).showToast();
                    location.reload() 

                }
            });
        }
    }

    function expandOrCloseAll(){
        console.log("si");
        let check = $("#expand-all")
        let open = false

        $(".polling-items").each(function(){
            let mesa = $(this).data("id")
            let id = $(this).data("idbdd")

            if(mesa == 1){
                if(!check.is(':checked')){
                    open = true;
                    $(this).css({background: "#ffff"})
                    $("#img-" + id).attr('src', '/images/arrow-bottom.png')
                    $("#img-" + id).attr('title', 'Cerrar')
                    expand_all = false
                }else{
                    $(this).css({background: "#E3E3E3"})
                    $("#img-" + id).attr('src', '/images/arrow-up.png')
                    $("#img-" + id).attr('title', 'Expandir')
                    expand_all = true
                }
            }else{
                if(open){
                    if(mesa > 1){
                        $(this).removeClass('display-content')
                        $(this).addClass('display-none')
                    }else{
                        $(this).removeClass('open-polling')
                        $(this).addClass('close-polling')
                    }
                }else{
                    if(mesa > 1){
                        $(this).removeClass('display-none')
                        $(this).addClass('display-content')
                    }else{
                        $(this).addClass('close-polling')
                        $(this).addClass('open-polling')
                    }
                }
            }            
        })
    }

    function expandOrClose(id){
        let tr_elements = $(".polling-station-"+id)
        let open = false
        tr_elements.each(function(){
            let mesa = $(this).data('id')

            if(mesa == 1){
                if($(this).hasClass('open-polling')){
                    open = true;
                    $(this).css({background: "#ffff"})
                    $("#img-" + id).attr('src', '/images/arrow-bottom.png')
                    $("#img-" + id).attr('title', 'Cerrar')
                }else{
                    $(this).css({background: "#E3E3E3"})
                    $("#img-" + id).attr('src', '/images/arrow-up.png')
                    $("#img-" + id).attr('title', 'Expandir')
                }
            }else{
                if(open){
                    if(mesa > 1){
                        $(this).removeClass('display-content')
                        $(this).addClass('display-none')
                    }else{
                        $(this).removeClass('open-polling')
                        $(this).addClass('close-polling')
                    }
                }else{
                    if(mesa > 1){
                        $(this).removeClass('display-none')
                        $(this).addClass('display-content')
                    }else{
                        $(this).addClass('close-polling')
                        $(this).addClass('open-polling')
                    }
                }
            }
        })

        tr_elements.each(function(){
            let mesa = $(this).data('id')

            if(open){
                if(mesa > 1){
                    $(this).removeClass('display-content')
                    $(this).addClass('display-none')
                }else{
                    $(this).removeClass('open-polling')
                    $(this).addClass('close-polling')
                }
            }else{
                if(mesa > 1){
                    $(this).removeClass('display-none')
                    $(this).addClass('display-content')
                }else{
                    $(this).addClass('close-polling')
                    $(this).addClass('open-polling')
                }
            }
            
            console.log(mesa)

        })
    }
</script>
@endsection

@section('js_aditional')

@endsection

