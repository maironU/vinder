@extends('dashboard.pages.layout')
@section('title_page', 'Coordinador de puestos')
@section('breadcrumbs') {!! Breadcrumbs::render('coordinator-index') !!} @endsection

@section('content_body_page')
<style>
    @media (max-width: 576px){
        .content-results {
            flex-direction: column;
        }

        .content-results > div, a {
            width: 100%;
            margin-bottom: 3px;
        }
    }.content-form {
        display: flex;
        align-items: center;
        width: 100%;
        padding: 0;
    }

    .content-form > .form-group{
        margin-bottom: 0;
        margin-right: 10px;
        width: 270px;
    }

    .modal-backdrop {
        height: 1000px !important;
    }

    @media (max-width: 560px){
        .content-form {
            flex-direction: column;
            align-items: center;
            justify-content: center;
        }

        .content-form > .form-group, input{
            width: 80%;
            margin: 0;
        }
    }
</style>
<script>
    let rol_auth = "{{Auth::user()->type->id}}"
</script>
<div class="row content-results" id="title_page" style="margin-bottom: 10px;margin: 0; margin-bottom: 10px">
    @if($coordinator_zone_id)
        <input type="hidden" id="coordinator_zone_id" value="{{$coordinator_zone_id}}">
    @endif
    <div style="display: flex;justify-content: space-between; margin: 0;">        
        <div style="display: flex; align-items: center">
            @if(Auth::user()->type->id == 1 && !$coordinator_zone_id)
                <div style="margin-right: 10px">
                    <a href="{{ route('coordinator.create')}}" class="btn btn-primary"><i class="fa fa-user"></i> Nuevo Coordinador de Puesto</a>
                </div>
            @endif
        </div>

        <?php 
            $polling_stations = null;
            if(Auth::user()->type->id == 9){
                $polling_stations = \App\Entities\PollingStation::PollingWithOnlyMyZoneAvailables(Auth::user()->zone->zone_id);
            }elseif($coordinator_zone_id){
                $user_zone = App\Entities\User::find($coordinator_zone_id);
                if($user_zone && $user_zone->zone){
                    $zone = $user_zone->zone->zone_id;
                    $polling_stations = \App\Entities\PollingStation::PollingWithOnlyMyZoneAvailables($zone);
                }else{
                    $polling_stations = [];
                }   
            }else{
                $polling_stations = \App\Entities\PollingStation::pollingWithMunicipaly();
            }
            $zones = \App\Entities\Zone::all();

        ?>
        <div class="form-group cont-form-voters" style="display: flex; align-items: center; padding-right: 18px; margin-bottom: 0">
            <!--<label class="control-label" style="margin-right: 10px" for="orientador">Orientador</label>-->
            <div style="display: flex; align-items: center; margin-right: 20px">
                <span style="margin-right: 5px">Expandir Todos</span>
                <input style="margin: 0" type="checkbox" id="expand-all" onclick="expandOrCloseAll()">
            </div>
            <h4 style="margin-right: 20px" id="text-filter">Filtrar Por:</h4>
            @include('dashboard.pages.utilities.polling_station_filter', ['polling_stations' => $polling_stations])
            @include('dashboard.pages.utilities.zone_filter', ['zones' => $zones])
        </div>
</div>
    <div class="row" style="margin: 0; margin-top: 30px; display:flex; justify-content: space-between" id="title_page" style="margin-bottom: 10px;">
        <div class="col-sm-8 content-form">
            <div class="form-group">
                <input id="search" type="text" class="form-control" placeholder="Cedula, Nombre, Email, Teléfono" value="{{isset($request) ? $request->search : ''}}" onkeypress="keyPress(event)">
            </div>
            <input type="button" class="btn btn-primary" value="Buscar" onclick="filter()">
            @include('dashboard.pages.utilities.refresh')
        </div>

        @if(Auth::user()->type->id == 9)
            <div style="display: flex; align-items: center; margin-right: 10px">
                <div style="display: flex; justify-content: center; border: 0">
                    @if($request->polling_station)
                        <a href="/witnesses/coordinator?polling_station={{$request->polling_station}}&check=verde">
                            <img style="margin-right: 10px; cursor: pointer" src="{{asset('images/check.png')}}" alt="" width="30px">
                        </a>
                        <a href="/witnesses/coordinator?polling_station={{$request->polling_station}}&check=rojo">
                            <img style="margin-right: 10px; cursor: pointer" src="{{asset('images/check_rojo.png')}}" alt="" width="30px">
                        </a>
                        <a href="/witnesses/coordinator?polling_station={{$request->polling_station}}" style="display: flex; align-items:center; margin-left: 8px">
                            <span>Todas</span>
                        </a>
                    @else
                        <a href="/witnesses/coordinator?check=verde">
                            <img style="margin-right: 10px; cursor: pointer" src="{{asset('images/check.png')}}" alt="" width="30px">
                        </a>
                        <a href="/witnesses/coordinator?check=rojo">
                            <img style="margin-right: 10px; cursor: pointer" src="{{asset('images/check_rojo.png')}}" alt="" width="30px">
                        </a>
                        <a href="/witnesses/coordinator" style="display: flex; align-items:center;  margin-left: 8px">
                            <span>Todas</span>
                        </a>
                    @endif        
                </div>
            </div>
        @elseif(Auth::user()->type->id == 1 && $coordinator_zone_id)
            <div style="display: flex; align-items: center; margin-right: 10px">
                <div style="display: flex; justify-content: center; border: 0">
                    @if($request->polling_station)
                        <a href="/witnesses/coordinator/{{$coordinator_zone_id}}?polling_station={{$request->polling_station}}&check=verde">
                            <img style="margin-right: 10px; cursor: pointer" src="{{asset('images/check.png')}}" alt="" width="30px">
                        </a>
                        <a href="/witnesses/coordinator/{{$coordinator_zone_id}}?polling_station={{$request->polling_station}}&check=rojo">
                            <img style="margin-right: 10px; cursor: pointer" src="{{asset('images/check_rojo.png')}}" alt="" width="30px">
                        </a>
                        <a href="/witnesses/coordinator/{{$coordinator_zone_id}}?polling_station={{$request->polling_station}}" style="display: flex; align-items:center; margin-left: 8px">
                            <span>Todas</span>
                        </a>
                    @else
                        <a href="/witnesses/coordinator/{{$coordinator_zone_id}}?check=verde">
                            <img style="margin-right: 10px; cursor: pointer" src="{{asset('images/check.png')}}" alt="" width="30px">
                        </a>
                        <a href="/witnesses/coordinator/{{$coordinator_zone_id}}?check=rojo">
                            <img style="margin-right: 10px; cursor: pointer" src="{{asset('images/check_rojo.png')}}" alt="" width="30px">
                        </a>
                        <a href="/witnesses/coordinator/{{$coordinator_zone_id}}" style="display: flex; align-items:center;  margin-left: 8px">
                            <span>Todas</span>
                        </a>
                    @endif        
                </div>
            </div>
        @else
            <div style="display: flex; align-items: center; margin-right: 10px">
                <div style="display: flex; justify-content: center; border: 0">
                    @if($request->polling_station)
                        <a href="/witnesses/coordinator?polling_station={{$request->polling_station}}&check=verde">
                            <img style="margin-right: 10px; cursor: pointer" src="{{asset('images/check.png')}}" alt="" width="30px">
                        </a>
                        <a href="/witnesses/coordinator?polling_station={{$request->polling_station}}&check=naranja">
                            <img style="margin-right: 10px; cursor: pointer" src="{{asset('images/check_orange.png')}}" alt="" width="30px">
                        </a>
                        <a href="/witnesses/coordinator?polling_station={{$request->polling_station}}&check=blanca">
                            <div style="width: 30px; height:30px; border-radius: 50%; border: 1px solid #141313; margin: 0 auto; cursor: pointer"></div>
                        </a>
                        <a href="/witnesses/coordinator?polling_station={{$request->polling_station}}" style="display: flex; align-items:center; margin-left: 8px">
                            <span>Todas</span>
                        </a>
                    @else
                        <a href="/witnesses/coordinator?check=verde">
                            <img style="margin-right: 10px; cursor: pointer" src="{{asset('images/check.png')}}" alt="" width="30px">
                        </a>
                        <a href="/witnesses/coordinator?check=naranja">
                            <img style="margin-right: 10px; cursor: pointer" src="{{asset('images/check_orange.png')}}" alt="" width="30px">
                        </a>
                        <a href="/witnesses/coordinator?check=blanca">
                            <div style="width: 30px; height:30px; border-radius: 50%; border: 1px solid #141313; margin: 0 auto; cursor: pointer"></div>
                        </a>
                        <a href="/witnesses/coordinator" style="display: flex; align-items:center;  margin-left: 8px">
                            <span>Todas</span>
                        </a>
                    @endif
                    
                    
                </div>
            </div>
        @endif
       
    </div>
    </div>
    <div class="block full">
        <div class="table-responsive">
            @if(Auth::user()->type->id == 9 || (Auth::user()->type->id == 1 && $coordinator_zone_id))
                <table id="example-datatable" class="table table-bordered table-center">
                    <thead>
                        <tr>
                            <th>Puesto de votación</th>
                            <th class="text-center">Mesa</th>
                            <th class="text-center">Testigo</th>
                            <th title="Ver Formulario">Ver Formulario</th>
                            <th title="Votos">Votos</th>
                            <th title="Presentó Reclamación">Hubo reclamación</th>
                            <th title="Presentó Reclamación">Hubo reclamación Escrutador</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($pollingStationsZone as $pollingStation)
                            <?php 
                                $num_tables =  $pollingStation->tables;
                            ?>
                            @for($i = 1; $i <= $num_tables; $i++)
                                <?php 
                                    $testigoElectoral = \App\Entities\User::where('type_id', '=', 5)
                                                        ->whereHas('tables_numbers', function($subquery) use($i){
                                                            $subquery->where('table_number', $i)
                                                            ->orWhere('table_number', 'Mesa '.$i);
                                                        })
                                                        ->where('polling_station_id', $pollingStation->id)
                                                        ->with(['votes' => function($q){
                                                            $q->join('forme14_candidates as fc', 'fc.id', '=', 'forme14_votes.candidate_id');
                                                            $q->join('formse14 as fe', 'fe.candidate_check', '=', 'fc.id');
                                                        }])
                                                        ->first();
                                ?>
                                @if($request->check && ($request->check == "verde" || $request->check == "rojo"))
                                    @if($testigoElectoral)
                                        <?php 
                                            $votes = \App\Entities\FormE14Vote::where('user_id', $testigoElectoral->id)
                                                                ->where('table_number', $i)
                                                    ->join('forme14_candidates as fc', 'forme14_votes.candidate_id', '=', 'fc.id')
                                                    ->where('fc.forme14_id', $form->id)
                                                    ->get();

                                            $candidates = $form->candidates;

                                            $file = $form
                                                ->files()
                                                ->where('user_id', $testigoElectoral->id)
                                                ->where('table_number', $i)
                                                ->first();
                                        ?>
                                        @if($request->check == "verde")
                                            @if(count($votes) > 0 && $file && $file->file && $file->file_secondary)
                                                <tr style="cursor: pointer" data-idbdd="{{$pollingStation->id}}" data-id="{{$i}}" class="polling-items close-polling polling-station-{{$pollingStation->id}}">
                                                    <td>{{$pollingStation->name}}</td>
                                                    <td>{{$i}}</td>
                                                    
                                                    @if($testigoElectoral)
                                                        <td>{{$testigoElectoral->name}}</td>
                                                    @else
                                                        <td style="border: 0" class="text-center">
                                                            <span>No hay testigo para la mesa</span>
                                                        </td>
                                                    @endif
                    
                                                    @if($testigoElectoral)
                                                        <?php 
                                                            $file = $form
                                                                ->files()
                                                                ->where('user_id', $testigoElectoral->id)
                                                                ->where('table_number', $i)
                                                                ->first();
                                                        ?>
                                                        <td style="text-align: center" class="center-row">
                                                            <form style="margin-right: 3px; margin: 0" action="{{route('infoForm.delete', [$testigoElectoral->id, $i, $form->id])}}" method="POST" id="form-info-{{$testigoElectoral->id}}-{{$form->id}}-{{$i}}" style="margin: 0">
                                                                {!! csrf_field() !!}
                                                                {!! method_field('DELETE') !!}
                    
                                                                <button data-toggle="tooltip" title="Borrar Información del Formulario" class="btn btn-effect-ripple btn-danger" onclick="clearInfo(event, '{{$testigoElectoral->id}}', '{{$i}}','{{$form->id}}')">
                                                                    <i class="fa fa-times"></i>
                                                                </button>
                                                            </form>
                                                            @if($file)
                                                                @if($file->file)
                                                                    <a target="_blank" href="{{asset('files_forme14/'.$file->file)}}" data-toggle="tooltip" title="Ver Formulario" class="btn btn-effect-ripple btn-warning" style="width: fit-content">
                                                                        <i class="fa fa-eye"></i>
                                                                    </a>
                                                                @endif
                                                                @if($file->file_secondary)
                                                                    <a target="_blank" href="{{asset('files_forme14/'.$file->file_secondary)}}" data-toggle="tooltip" title="Ver Formulario Secundario" class="btn btn-effect-ripple btn-warning" style="width: fit-content">
                                                                        <i class="fa fa-eye"></i>
                                                                    </a>
                                                                @endif
                                                            @endif
                                                            <a href="{{url("witnesses/editform/e14/$form->id/$i/$testigoElectoral->id")}}" data-toggle="tooltip" title="Editar formulario" class="btn btn-effect-ripple btn-warning">
                                                                <i class="fa fa-pencil"></i>
                                                            </a>
                                                        </td>
                                                    @else
                                                        <td style="border: 0" class="text-center">
                                                            <span>No hay testigo para la mesa</span>
                                                        </td>
                                                    @endif
                                                    <?php 
                                                        $votes = \App\Entities\FormE14Vote::where('user_id', $testigoElectoral->id)
                                                                ->where('table_number', $i)
                                                        ->join('forme14_candidates as fc', 'forme14_votes.candidate_id', '=', 'fc.id')
                                                        ->where('fc.forme14_id', $form->id)
                                                        ->get();

                                                        $votes_escrutador = \App\Entities\FormE14VoteEscrutador::where('user_id', $testigoElectoral->id)
                                                                ->where('table_number', $i)
                                                        ->join('forme14_candidates as fc', 'forme14_vote_escrutador.candidate_id', '=', 'fc.id')
                                                        ->where('fc.forme14_id', $form->id)
                                                        ->get();
                                                    ?>
                                                    @if($testigoElectoral)
                                                        <td style="text-align: center; width: 103px">
                                                            <img width="35px" style="float:left;margin-right: 5px" src="{{asset('images/check.png')}}" alt="" style="cursor: pointer"  data-toggle="modal" data-target="#exampleModalLong" onclick="getFormE14('{{$form->id}}', '{{$i}}', '{{$testigoElectoral->id}}')">

                                                            <?php $sw = true ?>
                                                            @if(count($votes_escrutador) == 0)
                                                                <?php $sw = false ?>
                                                            @endif
                                                            @foreach($votes as $vote)
                                                                @foreach($votes_escrutador as $vote_escrutador)
                                                                    @if($vote->candidate_id == $vote_escrutador->candidate_id && $vote->user_id == $vote_escrutador->user_id)
                                                                        @if($vote->votes_number != $vote_escrutador->votes_number)
                                                                            <?php $sw = false ?>
                                                                        @endif
                                                                    @endif
                                                                @endforeach
                                                            @endforeach
            
                                                            @if($sw)
                                                                <img width="35px" style="float:left" src="{{asset('images/check.png')}}" alt="" style="cursor: pointer"  data-toggle="modal" data-target="#exampleModalLong" onclick="getFormE14('{{$form->id}}', '{{$i}}', '{{$testigoElectoral->id}}')">
                                                            @else
                                                                <img width="35px" style="float:left" src="{{asset('images/check_rojo.png')}}" alt="" style="cursor: pointer"  data-toggle="modal" data-target="#exampleModalLong" onclick="getFormE14('{{$form->id}}', '{{$i}}', '{{$testigoElectoral->id}}')">
                                                            @endif
                                                        </td>
                                                    @else
                                                        <td style="border: 0" class="text-center">
                                                            <span>No hay testigo para la mesa</span>
                                                        </td>
                                                    @endif

                                                    @if($testigoElectoral)
                                                        <?php 
                                                            $filed_claim = \App\Entities\FormE14FiledClaim::where('forme14_id', $form->id)
                                                                ->where('user_id', $testigoElectoral->id)
                                                                ->where('table_number', $i)
                                                                ->first();

                                                            $obj_filed_claims = [
                                                                "no" => "No hubo reclamación",
                                                                "otro_candidato" => "Otro candidato",
                                                                "excedio" => "Excedio el número de votos de la mesa",
                                                                "error_aritmetico" => "Error aritmético",
                                                                "error_nombre" => "Error en el nombre del candidato E14",
                                                                "firmas_incompletas" => "Firmas incompletas",
                                                                "1" => "SI",
                                                                "0" => "No hubo reclamación"
                                                        ];
                                                        ?>
                                                        @if($filed_claim)
                                                            <td class="text-center">{{$obj_filed_claims[$filed_claim->filed_claim]}}</td>
                                                        @else
                                                            <td class="text-center">{{$obj_filed_claims["no"]}}</td>
                                                        @endif
                                                    @else
                                                        <td class="text-center">
                                                            <span>No hay testigo para la mesa</span>
                                                        </td>
                                                    @endif

                                                    @if($testigoElectoral)
                                                        <?php 
                                                            $filed_claim = \App\Entities\FormE14FiledClaim::where('forme14_id', $form->id)
                                                                ->where('user_id', $testigoElectoral->id)
                                                                ->where('table_number', $i)
                                                                ->first();
                                                            $obj_filed_claims = [
                                                                "no" => "No hubo reclamación",
                                                                "otro_candidato" => "Otro candidato",
                                                                "excedio" => "Excedio el número de votos de la mesa",
                                                                "error_aritmetico" => "Error aritmético",
                                                                "error_nombre" => "Error en el nombre del candidato E14",
                                                                "firmas_incompletas" => "Firmas incompletas",
                                                                "1" => "SI",
                                                                "0" => "No hubo reclamación"
                                                        ];
                                                        ?>
                                                        @if($filed_claim && $filed_claim->filed_claim_encrutador)
                                                            <td class="text-center">{{$obj_filed_claims[$filed_claim->filed_claim_encrutador]}}</td>
                                                        @else
                                                            <td class="text-center">{{$obj_filed_claims["no"]}}</td>
                                                        @endif
                                                    @else
                                                        <td class="text-center">
                                                            <span>No hay testigo para la mesa</span>
                                                        </td>
                                                    @endif
                                                </tr>
                                            @endif  
                                        @else 
                                            @if((count($votes) && !$file) || (count($votes) > 0 && $file && $file->file && !$file->file_secondary) || !count($votes))
                                                <tr style="cursor: pointer" data-idbdd="{{$pollingStation->id}}" data-id="{{$i}}" class="polling-items close-polling polling-station-{{$pollingStation->id}}">
                                                    <td>{{$pollingStation->name}}</td>
                                                    <td>{{$i}}</td>
                                                    
                                                    @if($testigoElectoral)
                                                        <td>{{$testigoElectoral->name}}</td>
                                                    @else
                                                        <td style="border: 0" class="text-center">
                                                            <span>No hay testigo para la mesa</span>
                                                        </td>
                                                    @endif
                    
                                                    @if($testigoElectoral)
                                                        <?php 
                                                            $file = $form
                                                                ->files()
                                                                ->where('user_id', $testigoElectoral->id)
                                                                ->where('table_number', $i)
                                                                ->first();
                                                        ?>
                                                        <td style="text-align: center" class="center-row">
                                                            <form style="margin-right: 3px; margin: 0" action="{{route('infoForm.delete', [$testigoElectoral->id, $i, $form->id])}}" method="POST" id="form-info-{{$testigoElectoral->id}}-{{$form->id}}-{{$i}}" style="margin: 0">
                                                                {!! csrf_field() !!}
                                                                {!! method_field('DELETE') !!}
                    
                                                                <button data-toggle="tooltip" title="Borrar Información del Formulario" class="btn btn-effect-ripple btn-danger" onclick="clearInfo(event, '{{$testigoElectoral->id}}', '{{$i}}',{{$form->id}}')">
                                                                    <i class="fa fa-times"></i>
                                                                </button>
                                                            </form>
                                                            @if($file)
                                                                @if($file->file)
                                                                    <a target="_blank" href="{{asset('files_forme14/'.$file->file)}}" data-toggle="tooltip" title="Ver Formulario" class="btn btn-effect-ripple btn-warning" style="width: fit-content">
                                                                        <i class="fa fa-eye"></i>
                                                                    </a>
                                                                @endif
                                                                @if($file->file_secondary)
                                                                    <a target="_blank" href="{{asset('files_forme14/'.$file->file_secondary)}}" data-toggle="tooltip" title="Ver Formulario Secundario" class="btn btn-effect-ripple btn-warning" style="width: fit-content">
                                                                        <i class="fa fa-eye"></i>
                                                                    </a>
                                                                @endif
                                                            @endif
                                                            <a href="{{url("witnesses/editform/e14/$form->id/$i/$testigoElectoral->id")}}" data-toggle="tooltip" title="Editar formulario" class="btn btn-effect-ripple btn-warning" style="width: fit-content">
                                                                <i class="fa fa-pencil"></i>
                                                            </a>
                                                        </td>
                                                    @else
                                                        <td style="border: 0" class="text-center">
                                                            <span>No hay testigo para la mesa</span>
                                                        </td>
                                                    @endif
                    
                    
                                                    @if($testigoElectoral)
                                                        <td style="text-align: center; width: 103px">
                                                            <img width="35px" src="{{asset('images/check_rojo.png')}}" alt="" style="cursor: pointer"  data-toggle="modal" data-target="#exampleModalLong" onclick="getFormE14('{{$form->id}}', '{{$i}}', '{{$testigoElectoral->id}}')">
                                                            <img width="35px" style="float:left" src="{{asset('images/check_rojo.png')}}" alt="" style="cursor: pointer"  data-toggle="modal" data-target="#exampleModalLong" onclick="getFormE14('{{$form->id}}', '{{$i}}', '{{$testigoElectoral->id}}')">
                                                        </td>
                                                    @else
                                                        <td style="border: 0" class="text-center">
                                                            <span>No hay testigo para la mesa</span>
                                                        </td>
                                                    @endif

                                                    @if($testigoElectoral)
                                                        <?php 
                                                            $filed_claim = \App\Entities\FormE14FiledClaim::where('forme14_id', $form->id)
                                                                ->where('user_id', $testigoElectoral->id)
                                                                ->where('table_number', $i)
                                                                ->first();

                                                            $obj_filed_claims = [
                                                                "no" => "No hubo reclamación",
                                                                "otro_candidato" => "Otro candidato",
                                                                "excedio" => "Excedio el número de votos de la mesa",
                                                                "error_aritmetico" => "Error aritmético",
                                                                "error_nombre" => "Error en el nombre del candidato E14",
                                                                "firmas_incompletas" => "Firmas incompletas",
                                                                "1" => "SI",
                                                                "0" => "No hubo reclamación"
                                                        ];
                                                        ?>
                                                        @if($filed_claim)
                                                            <td class="text-center">{{$obj_filed_claims[$filed_claim->filed_claim]}}</td>
                                                        @else
                                                            <td class="text-center">{{$obj_filed_claims["no"]}}</td>
                                                        @endif
                                                    @else
                                                        <td class="text-center">
                                                            <span>No hay testigo para la mesa</span>
                                                        </td>
                                                    @endif

                                                    @if($testigoElectoral)
                                                        <?php 
                                                            $filed_claim = \App\Entities\FormE14FiledClaim::where('forme14_id', $form->id)
                                                                ->where('user_id', $testigoElectoral->id)
                                                                ->where('table_number', $i)
                                                                ->first();
                                                            $obj_filed_claims = [
                                                                "no" => "No hubo reclamación",
                                                                "otro_candidato" => "Otro candidato",
                                                                "excedio" => "Excedio el número de votos de la mesa",
                                                                "error_aritmetico" => "Error aritmético",
                                                                "error_nombre" => "Error en el nombre del candidato E14",
                                                                "firmas_incompletas" => "Firmas incompletas",
                                                                "1" => "SI",
                                                                "0" => "No hubo reclamación"
                                                        ];
                                                        ?>
                                                        @if($filed_claim && $filed_claim->filed_claim_encrutador)
                                                            <td class="text-center">{{$obj_filed_claims[$filed_claim->filed_claim_encrutador]}}</td>
                                                        @else
                                                            <td class="text-center">{{$obj_filed_claims["no"]}}</td>
                                                        @endif
                                                    @else
                                                        <td class="text-center">
                                                            <span>No hay testigo para la mesa</span>
                                                        </td>
                                                    @endif
                                                </tr>
                                            @endif
                                        @endif
                                    @endif
                                @else
                                    <tr style="cursor: pointer" data-idbdd="{{$pollingStation->id}}" data-id="{{$i}}" class="polling-items close-polling polling-station-{{$pollingStation->id}} {{$i > 1 ? 'display-none' : 'display-content'}} {{$i == 1 || $num_tables == $i ? 'border-ddd' : ''}}">
                                        <td onclick="{{$i == 1 ? 'expandOrClose('.$pollingStation->id.')' : ''}}" style="display: flex; width: 100%; height: 60px; border: 0"> 
                                            @if($i == 1)
                                                <img title="Expandir" id="img-{{$pollingStation->id}}" onclick="expandOrClose('{{$pollingStation->id}}')" src="{{asset('images/arrow-bottom.png')}}" style="margin-right: 10px; cursor-pointer" width="20px" height="20px" alt="">
                                            @endif

                                            <span>
                                                {{ $pollingStation->name }} 
                                            </span>
                                        </td>
                                        <td onclick="{{$i == 1 ? 'expandOrClose('.$pollingStation->id.')' : ''}}">{{$i}}</td>
                                        
                                        @if($testigoElectoral)
                                            <td onclick="{{$i == 1 ? 'expandOrClose('.$pollingStation->id.')' : ''}}">{{$testigoElectoral->name}}</td>
                                        @else
                                            <td onclick="{{$i == 1 ? 'expandOrClose('.$pollingStation->id.')' : ''}}" style="border: 0" class="text-center">
                                                <span>No hay testigo para la mesa</span>
                                            </td>
                                        @endif

                                        @if($testigoElectoral)
                                            <?php 
                                                $file = $form
                                                    ->files()
                                                    ->where('user_id', $testigoElectoral->id)
                                                    ->where('table_number', $i)
                                                    ->first();
                                            ?>
                                            <td style="text-align: center" class="center-row">
                                                <form style="margin-right: 3px; margin: 0" action="{{route('infoForm.delete', [$testigoElectoral->id, $i, $form->id])}}" method="POST" id="form-info-{{$testigoElectoral->id}}-{{$form->id}}-{{$i}}" style="margin: 0">
                                                    {!! csrf_field() !!}
                                                    {!! method_field('DELETE') !!}
        
                                                    <button data-toggle="tooltip" title="Borrar Información del Formulario" class="btn btn-effect-ripple btn-danger" onclick="clearInfo(event, '{{$testigoElectoral->id}}', '{{$i}}',{{$form->id}}')">
                                                        <i class="fa fa-times"></i>
                                                    </button>
                                                </form>
                                                @if($file)
                                                    @if($file->file)
                                                        <a target="_blank" href="{{asset('files_forme14/'.$file->file)}}" data-toggle="tooltip" title="Ver Formulario" class="btn btn-effect-ripple btn-warning" style="width: fit-content">
                                                            <i class="fa fa-eye"></i>
                                                        </a>
                                                    @endif
                                                    @if($file->file_secondary)
                                                        <a target="_blank" href="{{asset('files_forme14/'.$file->file_secondary)}}" data-toggle="tooltip" title="Ver Formulario Secundario" class="btn btn-effect-ripple btn-warning" style="width: fit-content">
                                                            <i class="fa fa-eye"></i>
                                                        </a>
                                                    @endif
                                                @endif
                                                <a href="{{url("witnesses/editform/e14/$form->id/$i/$testigoElectoral->id")}}" data-toggle="tooltip" title="Editar formulario" class="btn btn-effect-ripple btn-warning" style="width: fit-content">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                            </td>
                                        @else
                                            <td onclick="{{$i == 1 ? 'expandOrClose('.$pollingStation->id.')' : ''}}" style="border: 0" class="text-center">
                                                <span>No hay testigo para la mesa</span>
                                            </td>
                                        @endif

                                        @if($testigoElectoral)
                                            <td style="text-align: center">
                                                <?php 
                                                    $votes = \App\Entities\FormE14Vote::where('user_id', $testigoElectoral->id)
                                                                ->where('table_number', $i)
                                                    ->join('forme14_candidates as fc', 'forme14_votes.candidate_id', '=', 'fc.id')
                                                    ->where('fc.forme14_id', $form->id)
                                                    ->get();

                                                    $votes_escrutador = \App\Entities\FormE14VoteEscrutador::where('user_id', $testigoElectoral->id)
                                                                ->where('table_number', $i)
                                                    ->join('forme14_candidates as fc', 'forme14_vote_escrutador.candidate_id', '=', 'fc.id')
                                                    ->where('fc.forme14_id', $form->id)
                                                    ->get();

                                                    $candidates = $form->candidates;
                                                    $file = $form
                                                        ->files()
                                                        ->where('user_id', $testigoElectoral->id)
                                                        ->where('table_number', $i)
                                                        ->first();
                                                ?>
                                                @if(count($votes) > 0 && $file && $file->file && $file->file_secondary)
                                                    <img width="35px" src="{{asset('images/check.png')}}" alt="" style="cursor: pointer"  data-toggle="modal" data-target="#exampleModalLong" onclick="getFormE14('{{$form->id}}', '{{$i}}', '{{$testigoElectoral->id}}')">
                                                @else
                                                    <img width="35px" src="{{asset('images/check_rojo.png')}}" alt="" style="cursor: pointer"  data-toggle="modal" data-target="#exampleModalLong" onclick="getFormE14('{{$form->id}}', '{{$i}}', '{{$testigoElectoral->id}}')">
                                                @endif

                                                <?php $sw = true ?>
                                                @if(count($votes_escrutador) == 0)
                                                    <?php $sw = false ?>
                                                @endif
                                                @foreach($votes as $vote)
                                                    @foreach($votes_escrutador as $vote_escrutador)
                                                        @if($vote->candidate_id == $vote_escrutador->candidate_id && $vote->user_id == $vote_escrutador->user_id)
                                                            @if($vote->votes_number != $vote_escrutador->votes_number)
                                                                <?php $sw = false ?>
                                                            @endif
                                                        @endif
                                                    @endforeach
                                                @endforeach

                                                @if($sw)
                                                    <img width="35px" src="{{asset('images/check.png')}}" alt="" style="cursor: pointer"  data-toggle="modal" data-target="#exampleModalLong" onclick="getFormE14('{{$form->id}}', '{{$i}}', '{{$testigoElectoral->id}}')">
                                                @else
                                                    <img width="35px" src="{{asset('images/check_rojo.png')}}" alt="" style="cursor: pointer"  data-toggle="modal" data-target="#exampleModalLong" onclick="getFormE14('{{$form->id}}', '{{$i}}', '{{$testigoElectoral->id}}')">
                                                @endif
                                            </td>
                                        @else
                                            <td onclick="{{$i == 1 ? 'expandOrClose('.$pollingStation->id.')' : ''}}" style="border: 0" class="text-center">
                                                <span>No hay testigo para la mesa</span>
                                            </td>
                                        @endif

                                        @if($testigoElectoral)
                                            <?php 
                                                $filed_claim = \App\Entities\FormE14FiledClaim::where('forme14_id', $form->id)
                                                    ->where('user_id', $testigoElectoral->id)
                                                    ->where('table_number', $i)
                                                    ->first();

                                                    $obj_filed_claims = [
                                                        "no" => "No hubo reclamación",
                                                        "otro_candidato" => "Otro candidato",
                                                        "excedio" => "Excedio el número de votos de la mesa",
                                                        "error_aritmetico" => "Error aritmético",
                                                        "error_nombre" => "Error en el nombre del candidato E14",
                                                        "firmas_incompletas" => "Firmas incompletas",
                                                        "1" => "SI",
                                                        "0" => "No hubo reclamación"
                                            ];
                                            ?>
                                            @if($filed_claim)
                                                <td class="text-center">{{$obj_filed_claims[$filed_claim->filed_claim]}}</td>
                                            @else
                                                <td class="text-center">{{$obj_filed_claims["no"]}}</td>
                                            @endif
                                        @else
                                            <td class="text-center">
                                                <span>No hay testigo para la mesa</span>
                                            </td>
                                        @endif

                                        @if($testigoElectoral)
                                            <?php 
                                                $filed_claim = \App\Entities\FormE14FiledClaim::where('forme14_id', $form->id)
                                                    ->where('user_id', $testigoElectoral->id)
                                                    ->where('table_number', $i)
                                                    ->first();

                                                    $obj_filed_claims = [
                                                        "no" => "No hubo reclamación",
                                                        "otro_candidato" => "Otro candidato",
                                                        "excedio" => "Excedio el número de votos de la mesa",
                                                        "error_aritmetico" => "Error aritmético",
                                                        "error_nombre" => "Error en el nombre del candidato E14",
                                                        "firmas_incompletas" => "Firmas incompletas",
                                                        "1" => "SI",
                                                        "0" => "No hubo reclamación"
                                            ];
                                            ?>
                                            @if($filed_claim && $filed_claim->filed_claim_encrutador)
                                                <td class="text-center">{{$obj_filed_claims[$filed_claim->filed_claim_encrutador]}}</td>
                                            @else
                                                <td class="text-center">{{$obj_filed_claims["no"]}}</td>
                                            @endif
                                        @else
                                            <td class="text-center">
                                                <span>No hay testigo para la mesa</span>
                                            </td>
                                        @endif
                                    </tr>
                                @endif
                                
                            @endfor                
                        @endforeach
                    </tbody>
                </table>
            @else
                <table id="example-datatable" class="table table-bordered table-center">
                    <thead>
                        <tr>
                            <th style="min-width: 200px">Nombre</th>
                            <th>Puesto de votación</th>
                            <th>Dirección</th>
                            <th class="text-center">Mesa</th>
                            <th class="text-center">Testigo</th>
                            <th title="Ver Formulario">Ver Formulario</th>
                            <th class="text-center" style="width: 115px;"><i class="fa fa-flash"></i></th>
                            <th title="Votos">Votos</th>
                            <th title="Presentó Reclamación">Hubo reclamación</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $user)
                            <?php 
                                $num_tables =  $user->pollingStation->tables;
                                $tables_numbers = [];
                                $form_id = $form->id;

                                for($i = 1; $i <= $num_tables; $i++){
                                    array_push($tables_numbers, $i);
                                }
                                $testigos = \App\Entities\User::where('type_id', '=', 5)
                                    ->select('user.id', 'tn.table_number', 'user.name')
                                    ->join('table_numbers_user as tn', 'tn.user_id', '=', 'user.id')
                                    ->whereIn('tn.table_number', $tables_numbers)
                                    ->orderBy('tn.table_number', 'ASC')
                                    ->groupBy('tn.id')
                                    /*$q->whereHas('tables_numbers', function($subquery) use($tables_numbers){
                                        $subquery->whereIn('table_number', $tables_numbers);
                                    });*/
                                    ->join('polling_stations as ps', 'ps.id', '=', 'user.polling_station_id')
                                    ->where('ps.id', $user->pollingStation->id)
                                    ->with(['votes' => function($q){
                                        $q->join('forme14_candidates as fc', 'fc.id', '=', 'forme14_votes.candidate_id');
                                        $q->join('formse14 as fe', 'fe.candidate_check', '=', 'fc.id');
                                    }]);

                                    if($request->check){
                                        if($request->check == "blanca"){
                                            $testigos->doesntHave('colors')
                                            ->orWhereHas('colors', function($q) use($form_id){
                                                $q->where('forme14_id', $form_id)
                                                ->where('color', 'blanca');
                                            });
                                        }else{
                                            $color = $request->check;
                                            $testigos->whereHas('colors', function($q) use($color, $form_id){
                                                $q->where('forme14_id', $form_id)
                                                ->where('color', $color);
                                            });
                                        }
                                    }                                    
                                    $testigos = $testigos->get();
                            ?>
                            @if(count($testigos) > 0)
                                <?php 
                                    $sequence = 1;
                                 ?>
                                @foreach($testigos as $testigoElectoral)
                                    <?php 
                                        $i = $testigoElectoral->table_number;
                                    ?>
                                    @if($request->check)
                                        @include('dashboard.pages.utilities.info-with-testigo-filter', ['user' => $user, 'seq' => $i, 'testigoElectoral' => $testigoElectoral])
                                    @else
                                        @if($i == $sequence)
                                            @include('dashboard.pages.utilities.info-with-testigo', ['user' => $user, 'seq' => $i])
                                            <?php $sequence = $sequence + 1 ?>
                                        @else
                                            @for($seq = $sequence; $seq <= $i; $seq++)
                                                @if($seq == $i)
                                                    @include('dashboard.pages.utilities.info-with-testigo', ['user' => $user, 'seq' => $seq, 'testigoElectoral' => $testigoElectoral])
                                                @else
                                                    @include('dashboard.pages.utilities.info-without-testigo', ['user' => $user, 'seq' => $seq])
                                                @endif
                                            @endfor
                                            <?php $sequence = $i + 1 ?>
                                        @endif
                                    @endif
                                    
                                @endforeach
                            @else
                                @if(!$request->check)
                                    @for($i = 1; $i <= $num_tables; $i++)
                                        @include('dashboard.pages.utilities.info-without-testigo', ['user' => $user, 'seq' => $i])
                                    @endfor
                                @endif
                            @endif                          
                        @endforeach
                    </tbody>
                </table>
            @endif
            
        </div>
    </div>
    @if(isset($request) && $users instanceof \Illuminate\Pagination\LengthAwarePaginator)
        {!! $users->appends(['search' => $request->search, 'zone' => $request->zone])->render() !!}
    @else
        @if($users instanceof \Illuminate\Pagination\LengthAwarePaginator)
            {!! $users->render() !!}
        @endif
    @endif
    <!-- END Datatables Block -->
    @include('dashboard/pages/witnesses/modal_forme14')

    <script>
        function keyPress(e){
            var keycode = (e.keyCode ? e.keyCode : e.which);
            if (keycode == '13') {
                filter();
                e.preventDefault();
                return false;
            }
        };

        function clearInfo(e, user_id, table_number, form_id){
            e.preventDefault();
            Swal.fire({
                title: '¿Estás Seguro?',
                text: "Se borrará toda la información de votos del testigo electoral",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si borrar'
            }).then((result) => {
                if (result.isConfirmed) {
                    $("#form-info-" + user_id + "-" + form_id + "-" + table_number).submit()
                }
            })
        }

        function expandOrCloseAll(){
            let check = $("#expand-all")
            let open = false

            $(".polling-items").each(function(){
                let mesa = $(this).data("id")
                let id = $(this).data("idbdd")

                if(mesa == 1){
                    if(!check.is(':checked')){
                        open = true;
                        $(this).css({background: "#ffff"})
                        $("#img-" + id).attr('src', '/images/arrow-bottom.png')
                        $("#img-" + id).attr('title', 'Cerrar')
                        expand_all = false
                    }else{
                        $(this).css({background: "#E3E3E3"})
                        $("#img-" + id).attr('src', '/images/arrow-up.png')
                        $("#img-" + id).attr('title', 'Expandir')
                        expand_all = true
                    }
                }else{
                    if(open){
                        if(mesa > 1){
                            $(this).removeClass('display-content')
                            $(this).addClass('display-none')
                        }else{
                            $(this).removeClass('open-polling')
                            $(this).addClass('close-polling')
                        }
                    }else{
                        if(mesa > 1){
                            $(this).removeClass('display-none')
                            $(this).addClass('display-content')
                        }else{
                            $(this).addClass('close-polling')
                            $(this).addClass('open-polling')
                        }
                    }
                }            
            })
        }

        function expandOrClose(id){
            let tr_elements = $(".polling-station-"+id)
            let open = false
            tr_elements.each(function(){
                let mesa = $(this).data('id')

                if(mesa == 1){
                    if($(this).hasClass('open-polling')){
                        open = true;
                        $(this).css({background: "#ffff"})
                        $("#img-" + id).attr('src', '/images/arrow-bottom.png')
                        $("#img-" + id).attr('title', 'Cerrar')
                    }else{
                        $(this).css({background: "#E3E3E3"})
                        $("#img-" + id).attr('src', '/images/arrow-up.png')
                        $("#img-" + id).attr('title', 'Expandir')
                    }
                }else{
                    if(open){
                        if(mesa > 1){
                            $(this).removeClass('display-content')
                            $(this).addClass('display-none')
                        }else{
                            $(this).removeClass('open-polling')
                            $(this).addClass('close-polling')
                        }
                    }else{
                        if(mesa > 1){
                            $(this).removeClass('display-none')
                            $(this).addClass('display-content')
                        }else{
                            $(this).addClass('close-polling')
                            $(this).addClass('open-polling')
                        }
                    }
                }
            })

            tr_elements.each(function(){
                let mesa = $(this).data('id')

                if(open){
                    if(mesa > 1){
                        $(this).removeClass('display-content')
                        $(this).addClass('display-none')
                    }else{
                        $(this).removeClass('open-polling')
                        $(this).addClass('close-polling')
                    }
                }else{
                    if(mesa > 1){
                        $(this).removeClass('display-none')
                        $(this).addClass('display-content')
                    }else{
                        $(this).addClass('close-polling')
                        $(this).addClass('open-polling')
                    }
                }
                
                console.log(mesa)

            })
        }
    </script>
@endsection

