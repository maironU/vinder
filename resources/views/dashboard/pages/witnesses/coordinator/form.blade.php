@extends('dashboard.pages.layout')
@section('title_page')
    @if($user->exists) Usuario: {{ $user->name }} @else Nuevo Coordinador De Puestos @endif
@endsection
@section('breadcrumbs') {!! Breadcrumbs::render('coordinator-create', $user) !!} @endsection
@section('content_body_page')
	<div class="row">
	    <div class="col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
	        <div class="block">
	            <div class="block-title">
	                <h2>Datos del Usuario</h2>
	            </div>
                @if(Session::get('exist'))
					<div class="has-error">
						<div id="name-error" class="help-block animation-pullUp">
							{{ Session::get('exist') }}
						</div>
					</div>
				@endif
                {!! Form::model($user, $form_data + array('id' => 'form-users-coordinator', 'class' => 'form-horizontal form-bordered'))!!}

                    {{-- {!! Field::text('username', null, ['template' => 'horizontal', 'placeholder' => 'Cedula']) !!} --}}
                    <div class="form-group">
                        <label for="" class="col-md-4 control-label">Cédula</label>
                        <div class="col-md-6">
                            <div class="input-group">
                                @if($user->exists)   
                                    <input type="text" style="background: white; border-color: #dae2e8 !important; border: 1px solid #ccc; height: 34px" class="form-control" id="username" name="username" value="{{$user->username}}" placeholder="Cédula">
                                @else
                                    <input type="text" style="background: white; border-color: #dae2e8 !important; border: 1px solid #ccc; height: 34px" class="form-control" id="username" name="username" value="{{old('username')}}"placeholder="Cédula">
                                @endif
                                <span class="input-group-addon"><i class="fa fa-bars"></i></span>
                            </div>
                        </div>
                    </div>

                    {!! Field::password('password', ['template' => 'horizontal', 'placeholder' => 'Contraseña']) !!}

                    {!! Field::password('password_confirmation', ['template' => 'horizontal', 'placeholder' => 'Repita la Contraseña']) !!}

                    {!! Field::text('name', old('name'), ['template' => 'horizontal', 'placeholder' => 'Nombre Completo']) !!}

                    {!! Field::email('email', old('email'), ['template' => 'horizontal', 'placeholder' => 'Correo Electrónico']) !!}
                    
                    <div class="form-group">
                        <label for="" class="col-md-4 control-label">Teléfono</label>
                        <div class="col-md-6">
                            <div class="input-group">
                                @if($user->exists)   
                                    <input type="number" style="background: white; border-color: #dae2e8 !important; border: 1px solid #ccc; height: 34px" class="form-control" id="phone" name="phone" value="{{$user->phone}}" placeholder="Teléfono">
                                @else
                                    <input type="number" style="background: white; border-color: #dae2e8 !important; border: 1px solid #ccc; height: 34px" class="form-control" id="phone" name="phone" value="{{old('phone')}}" placeholder="Teléfono">
                                @endif
                                <span class="input-group-addon"><i class="fa fa-bars"></i></span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="polling_station_id" class="col-md-4 control-label">Puesto de Votación</label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <select class="js-example-basic-single select-chosen change-select" style="width: 100%" required="required" name="polling_station_id" id="polling_station_id" data-placeholder="Puesto de Votación">
                                    <option value=""></option>
                                    @if($user->exists)   
                                        @foreach($locations as $key => $value)
                                            @if($key == $user->polling_station_id)
                                                <option selected value="{{$key}}">{{$value}}</option>
                                            @else
                                                <option value="{{$key}}">{{$value}}</option>
                                            @endif
                                        @endforeach
                                    @else
                                        @foreach($locations as $key => $value)
                                            <option {{old("polling_station_id") == $key ? 'selected' : ''}} value="{{$key}}">{{$value}}</option>
                                        @endforeach
                                    @endif
                                </select>
                                <span class="input-group-addon"><i class="fa fa-bars"></i></span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group form-actions">
	                    <div class="col-md-8 col-md-offset-4">
                            <button type="submit" class="btn btn-effect-ripple btn-primary">Guardar</button>
	                    </div>
	                </div>

	            {!! Form::close()!!}

	        </div>
	    </div>
	</div>
@endsection
@section('js_aditional')
	{!! Html::script('assets/js/pages/formCoordinator.js') !!}
	<!-- Load and execute javascript code used only in this page -->
    <script> 
        $(function (){ FormUsers.init(); });
    </script>
@endsection