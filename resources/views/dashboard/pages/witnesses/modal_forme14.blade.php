<div class="modal fade" style="background: rgba(34,34,34,0.85)" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document" style="max-width: 500px">
    <div class="modal-content">
      <div class="modal-header" style="display: flex; justify-content: space-between;padding:10px">
        <h5 class="modal-title" id="exampleModalLongTitle">Visualización Formulario</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="padding-top: 10px">
        <div class="flex justify-content-between" style="max-width: 500px; margin: 0 auto">
          <input disabled class="form-control text-center text-primary" style="border: 0; background: white; height: 20px" type="text" placeholder="Gobernación" id="name-form">
        </div>

        <div class="forme14">
            <div class="forme14-info" id="forme14-info">
            </div>
            
            <div class="linea">
            </div>
            <div class="forme14-info" id="forme14-votes">
                
            </div>

            <div class="forme14-info" style="padding-top: 0" id="div-total-votes">
              <div class="segment" style="margin-top: 0">
                  <div class="segment-info" data-id="">
                      <div class="flex align-items-center" style="width: 100%">
                          <input disabled type="text" class="form-control" style="{{Auth::user()->type->id == 9 || Auth::user()->type->id == 1 ?  'min-width: 50%' : ''}}" value="Total Votos" disabled>
                          <input type="number" class="form-control number-vote" style="text-align: center" id="total-votes" disabled>
                          @if(Auth::user()->type->id == 9 || Auth::user()->type->id == 1)
                            <input type="number" class="form-control number-vote" style="text-align: center" id="total-votes-escrutador" disabled>
                          @endif
                      </div>
                  </div>
              </div>  
            </div>
            <div class="flex justify-content-between" id="content-filed-claim">
              <div class="forme14-info" style="padding-top: 0;" class="flex align-items-center" id="content-filed_claim">
                <div>
                  <input disabled type="checkbox" id="filed_claim">
                  <label for="filed_claim m-0">Hubo reclamación</label>
                </div>
                <div id="mensaje-filed_claims">
                  <span></span>
                </div>
              </div>
              @if(Auth::user()->type->id == 9 || Auth::user()->type->id == 1)
                <div class="forme14-info" style="padding-top: 0;" class="flex align-items-center" id="content-filed_claim_escrutador">
                  <div>
                    <input disabled type="checkbox" id="filed_claim">
                    <div class="tooltip2">
                      <label class="ml-2" for="filed_claim_encrutador">Hubo reclamación</label>
                      <span class="tooltiptext">Hubo reclamación escrutador</span>
                    </div>
                  </div>
                  <div id="mensaje-filed_claims_escrutador">
                    <span></span>
                  </div>
                </div>
              @endif
              <div class="forme14-save m-0">
                  <div class="flex" style="padding-right: 10px; width: 100%; justify-content: flex-end">
                      <span id="name-forme14-save-upload">
                      </span>
                  </div>
              </div>
            </div>
            
            <div id="preview-form-upload" class="flex justify-content-center" style="flex-direction: column;align-items: center">

            </div>
            
        </div>
        <!--
        <div class="modal-footer" style="background: white">
          <button type="button" class="btn btn-secondary" style="background: white" data-dismiss="modal">Cerrar</button>
        </div>-->
    </div>
  </div>
</div>