@extends('dashboard.pages.layout')
@section('title_page')
    @if($zone->exists) Zona: {{ $zone->name }} @else Nueva Zona @endif
@endsection
@section('breadcrumbs') 
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="/"><i class="fa fa-home"></i></a></li>
        <li><a href="{{route('zone.index')}}">Zonas</a></li>
        <li class="active">Nuevo</li>
    </ul>
@endsection
@section('content_body_page')
	<div class="row">
	    <div class="col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
	        <div class="block">
	            <div class="block-title">
	                <h2>Datos de la zona</h2>
	            </div>
                @if(Session::get('exist'))
					<div class="has-error">
						<div id="name-error" class="help-block animation-pullUp">
							{{ Session::get('exist') }}
						</div>
					</div>
				@endif
                {!! Form::model($zone, $form_data + array('id' => 'form-users-coordinator', 'class' => 'form-horizontal form-bordered'))!!}
                    {!! Field::text('name', old('name'), ['template' => 'horizontal', 'placeholder' => 'Nombre de la zona']) !!}
                    
                    <div class="form-group">
                        <label for="polling_station_id" class="col-md-4 control-label">Puestos de votación</label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <select class="js-example-basic-single select-chosen change-select" multiple style="width: 100%"  id="polling_stations" name="polling_stations[]" aria-required="true" aria-invalid="false" data-placeholder="Seleccione puestos de votación">
                                    <option value=""></option>
                                    @if($zone->exists)
                                        @foreach($locations as $key => $value)
                                            <?php $sw = 0 ?>
                                            @foreach($zone->pollingStations as $polling)
                                                @if($key == $polling->id)
                                                    <?php $sw = 1 ?>
                                                @endif
                                            @endforeach    
                                            @if($sw)
                                                <option selected value="{{$key}}">{{$value}}</option>
                                            @else
                                                <option value="{{$key}}">{{$value}}</option>
                                            @endif
                                        @endforeach
                                    @else
                                        @foreach($locations as  $key => $value)
                                            <option value="{{$key}}">{{$value}}</option>
                                        @endforeach
                                    @endif
                                   
                                </select>
                                <span class="input-group-addon"><i class="fa fa-bars"></i></span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group form-actions">
	                    <div class="col-md-8 col-md-offset-4">
                            <button type="submit" class="btn btn-effect-ripple btn-primary">Guardar</button>
	                    </div>
	                </div>

	            {!! Form::close()!!}

	        </div>
	    </div>
	</div>
@endsection