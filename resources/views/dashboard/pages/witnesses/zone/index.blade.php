@extends('dashboard.pages.layout')
@section('title_page', 'Zonas')
@section('breadcrumbs') {!! Breadcrumbs::render('zone-index') !!} @endsection

@section('content_body_page')
<style>
    @media (max-width: 576px){
        .content-results {
            flex-direction: column;
        }

        .content-results > div, a {
            width: 100%;
            margin-bottom: 3px;
        }
    }.content-form {
        display: flex;
        align-items: center;
        width: 100%;
        padding: 0;
    }

    .content-form > .form-group{
        margin-bottom: 0;
        margin-right: 10px;
        width: 270px;
    }

    .modal-backdrop {
        height: 1000px !important;
    }

    @media (max-width: 560px){
        .content-form {
            flex-direction: column;
            align-items: center;
            justify-content: center;
        }

        .content-form > .form-group, input{
            width: 80%;
            margin: 0;
        }
    }
</style>
<div class="row content-results" id="title_page" style="margin-bottom: 10px;margin: 0; margin-bottom: 10px">
    <div style="display: flex;justify-content: space-between; margin: 0;">        
        <div style="display: flex; align-items: center">
            @if(Auth::user()->type->id == 1)
                <div style="margin-right: 10px">
                    <a href="{{ route('zone.create')}}" class="btn btn-primary"><i class="fa fa-user"></i> Nueva Zona</a>
                </div>
            @endif
        </div>

        <?php 
            $polling_stations = \App\Entities\PollingStation::pollingWithMunicipaly();
        ?>
        <div class="form-group" style="display: flex; align-items: center; padding-right: 18px; margin-bottom: 0">
            <h4 style="margin-right: 20px" id="text-filter">Filtrar Por:</h4>
            <div style="padding-right: 0">
                <div class="input-group" style="min-width: 150px">
                    <select class="js-example-basic-single select-chosen change-select" style="width: 100%" required="required" name="Puesto de votación" id="polling_station" aria-required="true" aria-invalid="false" data-placeholder="Puesto de Votación" onchange="filter()">
                        <option value=""></option>
                        @foreach($polling_stations as  $key => $value)
                            @if(isset($request->polling_station))
                                @if($key == $request->polling_station)
                                    <option value="{{$key}}" selected>{{$value}}</option>
                                @else
                                    <option value="{{$key}}">{{$value}}</option>
                                @endif
                            @else
                                <option value="{{$key}}">{{$value}}</option>
                            @endif
                        @endforeach
                    </select>
                    <span class="input-group-addon" style="width: 15px; cursor: pointer" onclick="emptyFilter('polling_station')"><i class="fa fa-remove"></i></span>
                </div>
            </div>
        </div>
</div>
    <div class="row" style="margin: 0; margin-top: 30px; margin-bottom: 10px; display:flex; justify-content: space-between" id="title_page" style="margin-bottom: 10px;">
        <div class="col-sm-8 content-form">
            <div class="form-group">
                <input id="search" type="text" class="form-control" placeholder="Nombre" value="{{isset($request) ? $request->search : ''}}" onkeypress="keyPress(event)">
            </div>
            <input type="button" class="btn btn-primary" value="Buscar" onclick="filter()">
            @include('dashboard.pages.utilities.refresh')
        </div>
    </div>

    <div class="block full">
        <div class="table-responsive">
            <table id="example-datatable" class="table table-bordered table-center">
                <thead>
                    <tr>
                        <th style="min-width: 200px">Nombre</th>
                        <th>Puestos de votación</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($zones as $zone)
                        <tr>
                            <td style="width: fit-content">{{ $zone->name }}</td>
                            <td>
                                <div>
                                    @foreach($zone->pollingStations()->get() as $polling)
                                        <div class="btn-primary" style="padding: 5px; margin: 5px; width: fit-content;cursor: pointer">{{$polling->name}}</div>
                                    @endforeach
                                </div>
                            </td>
                            <td class="text-center">
                                <a href="{{ route('zone.edit', $zone->id)}}" data-toggle="tooltip" title="Editar zona" class="btn btn-effect-ripple btn-warning">
                                    <i class="fa fa-pencil"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- END Datatables Block -->
    @include('dashboard/pages/witnesses/modal_forme14')

    <script>
        function filter(){
            let search = $("#search").val()
            let polling_station = $("#polling_station").val()
            let url = "/witnesses/zone?"

            if(search){
                url+= "search=" + search + "&"
            }
            if(polling_station){
                url+= "polling_station=" + polling_station
            }
            location.href=url
        }

        function emptyFilter(){
            let search = $("#search").val()
            let url = "/witnesses/zone?"

            if(search){
                url+= "search=" + search + "&"
            }
            location.href=url
        }

        function keyPress(e){
            var keycode = (e.keyCode ? e.keyCode : e.which);
            if (keycode == '13') {
                filter();
                e.preventDefault();
                return false;
            }
        };

    </script>
@endsection

