@extends('dashboard.pages.layout')
@section('title_page', 'Mesas')

@section('content_body_page')
<style>
    .content-form {
        display: flex;
        align-items: center;
        width: 100%;
    }

    .content-form > .form-group{
        margin-bottom: 0;
        margin-right: 10px;
        width: 270px;
    }

    @media (max-width: 560px){
        .content-form {
            flex-direction: column;
            align-items: center;
            justify-content: center;
        }

        .content-form > .form-group, input{
            width: 80%;
            margin: 0;
        }
    }
</style>
<div class="row" id="title_page" style="margin-bottom: 10px;">
    <div class="block full">
        <div class="table-responsive">
            @if(Auth::user()->tables_numbers->count())
                <table id="datatable" class="table table-striped table-bordered table-vcenter">
                    <thead>
                        <tr>
                            <th title="Puesto de votación">Puesto de votación</th>
                            <th title="mesa">Mesa</th>
                            <th title="mesa">Editar</th>
                        </tr>
                    </thead>
                    <tbody id="tbody">
                        @foreach(Auth::user()->tables_numbers as $table_user)
                            <tr>
                                <td style="width: fit-content">{{ Auth::user()->pollingStation->name }}</td>
                                <td><strong>{{ $table_user->table_number }}</strong></td>
                                <td>
                                    <a href="{{url("witnesses/editform/e14/$forme14->id/$table_user->table_number")}}" data-toggle="tooltip" title="Editar formulario" class="btn btn-effect-ripple btn-warning">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @else
                <span>No tiene ninguna mesa asignada</span>
            @endif
        </div>
    </div>
@endsection

