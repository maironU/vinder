@extends('dashboard.pages.layout')
@section('title_page', 'Crear Formulario E14')
@section('breadcrumbs') 
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="/"><i class="fa fa-home"></i></a></li>
        <li><a href="{{route('witnesses.createform')}}">Formularios</a></li>
        <li class="active">Crear formulario</li>
    </ul>
@endsection
@section('content_body_page')

@include('dashboard.pages.witnesses.formE14', ['user' => null, 'formE14' => null, 'create' => true])
@endsection