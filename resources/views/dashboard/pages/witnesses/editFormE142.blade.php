@extends('dashboard.pages.layout')
@section('title_page', 'Editar Formulario E14')

@section('breadcrumbs') 
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="/"><i class="fa fa-home"></i></a></li>
        @if(Auth::user()->type->id == 5)
            <li><a href="{{route('witnesses.list_tables')}}">Mesas</a></li>
        @else
            <li><a href="{{route('witnesses.index')}}">Testigos electorales</a></li>
        @endif
        <li class="active">Editar formulario</li>
    </ul>
@endsection

@section('breadcrumbssm') 
    <ul class="breadcrumb breadcrumb-top" style="padding-top: 0">
        @if(Auth::user()->type->id == 5)
            <div class="forme14-save" style="display: flex; justify-content: flex-end;">
                <a href="{{route('witnesses.list_tables')}}"  style="width: 200px">
                    <img src="{{asset('images/arrow-left.png')}}" width="35px" alt="">
                    <span style="margin-left: 10px">ATRÁS</span>
                </a>
            </div>
        @else
            <div class="forme14-save" style="display: flex; justify-content: flex-end;">
                <a href="{{route('witnesses.index')}}"  style="width: 200px">
                    <img src="{{asset('images/arrow-left.png')}}" width="35px" alt="">
                    <span style="margin-left: 10px">ATRÁS</span>
                </a>
            </div>
        @endif
    </ul>
@endsection
@section('content_body_page')

@include('dashboard.pages.witnesses.formE14', ['user' => $user, 'formE14' => $formE14])

@endsection