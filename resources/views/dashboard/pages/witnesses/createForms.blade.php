@extends('dashboard.pages.layout')
@section('title_page', 'Formularios E14')
@section('breadcrumbs') 
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="/"><i class="fa fa-home"></i></a></li>
        <li><a href="{{route('witnesses.index')}}">Testigos electorales</a></li>
        <li class="active">Formularios</li>
    </ul>
@endsection
@section('content_body_page')
    @include('dashboard/pages/witnesses/listFormsE14', ['formse14' => $formse14, 'request' => $request])
@endsection

@section('js_aditional')
    <script>
        let msg = sessionStorage.getItem('showMsg')
        if(msg){
            Toastify({
                text: "El formulario ha sido actualizado correctamente",
                className: "info",
                close: true,
                style: {
                background: "linear-gradient(to right, #00b09b, #96c93d)",
                }
            }).showToast();

            sessionStorage.removeItem('showMsg')
        }
    </script>
@endsection