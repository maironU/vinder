@extends('dashboard.pages.layout')
@section('title_page')
    @if($user->exists) Usuario: {{ $user->name }} @else Nuevo Testigo Electoral @endif
@endsection
@section('breadcrumbs') {!! Breadcrumbs::render('witnesses-create', $user) !!} @endsection
@section('content_body_page')
	<div class="row">
	    <div class="col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
	        <div class="block">
	            <div class="block-title">
	                <h2>Datos del Usuario</h2>
	            </div>
                <input type="hidden" name="num_table" id="num_table" value="{{$user->exists ? $user->tables_numbers()->get() : null}}">
                <input type="hidden" name="polling_id" id="polling_id" value="{{$user->exists ? $user->polling_station_id : null}}">

                @if (session('message'))
                    <div class="btn-alert-danger" id="alert">
                        <span>{{ session('message') }}</span> <span style="position: absolute; right: 12px; cursor:pointer;font-size: 12px;line-height: 22px" id="close">X</span>
                    </div>
                @endif

                {!! Form::model($user, $form_data + array('id' => 'form-users', 'class' => 'form-horizontal form-bordered'))!!}

                    {{-- {!! Field::text('username', null, ['template' => 'horizontal', 'placeholder' => 'Cedula']) !!} --}}
                    <div class="form-group">
                        <label for="" class="col-md-4 control-label">Cédula</label>
                        <div class="col-md-6">
                            <div class="input-group">
                                @if($user->exists || $fromFindAndcreate)   
                                    <input type="text" style="background: white; border-color: #dae2e8 !important; border: 1px solid #ccc; height: 34px" class="form-control" id="username" name="username" value="{{$user->username}}" placeholder="Cédula">
                                @else
                                    <input type="text" style="background: white; border-color: #dae2e8 !important; border: 1px solid #ccc; height: 34px" class="form-control" id="username" name="username" placeholder="Cédula">
                                @endif
                                <span class="input-group-addon"><i class="fa fa-bars"></i></span>
                            </div>
                        </div>
                    </div>

                    @if($fromFindAndcreate)
                        <div class="form-group">
                            <label for="" class="col-md-4 control-label">Contraseña</label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="password" style="background: white; border-color: #dae2e8 !important; border: 1px solid #ccc; height: 34px" class="form-control" id="password" name="password" value="{{$user->username}}" placeholder="Contraseña">
                                    <span class="input-group-addon"><i class="fa fa-bars"></i></span>
                                </div>
                            </div>
                        </div>
                    @else
                        {!! Field::password('password', ['template' => 'horizontal', 'placeholder' => 'Contraseña']) !!}
                    @endif

                    @if($fromFindAndcreate)
                        <div class="form-group">
                            <label for="" class="col-md-4 control-label">Confirmar contraseña</label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="password" style="background: white; border-color: #dae2e8 !important; border: 1px solid #ccc; height: 34px" class="form-control" id="password_confirmation" name="password_confirmation" value="{{$user->username}}" placeholder="Repita la Contraseña">
                                    <span class="input-group-addon"><i class="fa fa-bars"></i></span>
                                </div>
                            </div>
                        </div>
                    @else
                        {!! Field::password('password_confirmation', ['template' => 'horizontal', 'placeholder' => 'Repita la Contraseña']) !!}
                    @endif


                    {!! Field::text('name', null, ['template' => 'horizontal', 'placeholder' => 'Nombre Completo']) !!}

                    {!! Field::email('email', null, ['template' => 'horizontal', 'placeholder' => 'Correo Electrónico']) !!}
                    
                    <div class="form-group">
                        <label for="" class="col-md-4 control-label">Teléfono</label>
                        <div class="col-md-6">
                            <div class="input-group">
                                @if($user->exists)   
                                    <input type="number" style="background: white; border-color: #dae2e8 !important; border: 1px solid #ccc; height: 34px" class="form-control" id="phone" name="phone" value="{{$user->phone}}" placeholder="Teléfono">
                                @else
                                    <input type="number" style="background: white; border-color: #dae2e8 !important; border: 1px solid #ccc; height: 34px" class="form-control" id="phone" name="phone" placeholder="Teléfono">
                                @endif
                                <span class="input-group-addon"><i class="fa fa-bars"></i></span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="location" class="col-md-4 control-label">Puesto de Votación</label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <select onchange="getTableByLocationId()" class="js-example-basic-single select-chosen change-select" style="width: 100%" required="required" name="polling_station_id" id="location" data-placeholder="Puesto de Votación">
                                    <option value=""></option>
                                    @if($user->exists || $fromFindAndcreate)   
                                        @foreach($locations as $key => $value)
                                            @if($key == $user->polling_station_id)
                                                <option selected value="{{$key}}">{{$value}}</option>
                                            @else
                                                <option value="{{$key}}">{{$value}}</option>
                                            @endif
                                        @endforeach
                                    @else
                                        @foreach($locations as $key => $value)
                                            <option value="{{$key}}">{{$value}}</option>
                                        @endforeach
                                    @endif
                                </select>
                                <span class="input-group-addon"><i class="fa fa-bars"></i></span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="polling_station_id" class="col-md-4 control-label">Mesas</label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <select class="js-example-basic-single" multiple style="width: 100%"  id="table_multiple" name="table_multiple[]" aria-required="true" aria-invalid="false">
                                    @if($user->exists || $fromFindAndcreate)   
                                        @foreach($tables as $table)
                                        <?php 
                                            $num_table = trim(preg_replace('/Mesa/m',"", $table));
                                            $has_table = $user->tables_numbers()->where('table_number', $num_table)
                                                ->exists();
                                        ?>
                                            @if($has_table)
                                                <option selected value="{{$table}}">{{$table}}</option>
                                            @else
                                                <option value="{{$table}}">{{$table}}</option>
                                            @endif
                                        @endforeach
                                    @endif
                                </select>
                                <span class="input-group-addon"><i class="fa fa-bars"></i></span>
                            </div>
                        </div>
                    </div>
                    {{--{!! Field::select('polling_station_id', $locations, null, ['template' => 'horizontal', 'data-placeholder' => 'Lugar de votación', 'onchange' => "getTableByLocationId()", 'id' => 'location']) !!} --}}

	                <div class="form-group form-actions">
	                    <div class="col-md-8 col-md-offset-4">
                            <button type="submit" class="btn btn-effect-ripple btn-primary">Guardar</button>
	                    </div>
	                </div>

	            {!! Form::close()!!}

	        </div>
	    </div>
	</div>
@endsection
@section('js_aditional')
	{!! Html::script('assets/js/pages/formPolling.js') !!}
	<!-- Load and execute javascript code used only in this page -->
    <script> 
        $(function (){ FormUsers.init(); });
        
        function getTableByLocationId(){    
            let location_id = $("#location").val()
            $.get(`${url_home}/polling_stations/${location_id}`).then(response => {
                console.log(response)
                let tables = response.tables
                
                let num_table_user = $("#num_table").val() ? JSON.parse($("#num_table").val()) : []
                let polling_station_id = $("#polling_id").val()

                if(num_table_user.length > 0 && response.location.id == polling_station_id){
                    for(let number of num_table_user){
                        tables.push("Mesa " +number.table_number)
                    }
                }

                $('#table_multiple').empty();
                $('#table_multiple').val(null).trigger('change');

                for(let table of tables){
                    var newOption = new Option(table, table, false, false);
                    $("#table_multiple").append(newOption).trigger('change')
                }

                if(num_table_user.length > 0 && response.location.id == polling_station_id){
                    let tables_value = []
                    for(let number of num_table_user){
                        tables_value.push("Mesa "+ number.table_number)
                    }

                    $("#table_multiple").val(tables_value).trigger('change')
                }
            })
        }
    </script>
@endsection