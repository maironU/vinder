@extends('dashboard.pages.layout')
@section('title_page')
 Resultados Estadísticos
@endsection
@section('breadcrumbs') {!! Breadcrumbs::render('witnesses-results-statistic') !!} @endsection
{!! Html::script('assets/js/vendor/jquery-2.1.3.min.js'); !!}
@section('content_body_page')
  <style>
    .highcharts-figure,
    .highcharts-data-table table {
      min-width: 320px;
      max-width: 660px;
      margin: 1em auto;
    }

    .highcharts-data-table table {
      font-family: Verdana, sans-serif;
      border-collapse: collapse;
      border: 1px solid #ebebeb;
      margin: 10px auto;
      text-align: center;
      width: 100%;
      max-width: 500px;
    }

    .highcharts-data-table caption {
      padding: 1em 0;
      font-size: 1.2em;
      color: #555;
    }

    .highcharts-data-table th {
      font-weight: 600;
      padding: 0.5em;
    }

    .highcharts-data-table td,
    .highcharts-data-table th,
    .highcharts-data-table caption {
      padding: 0.5em;
    }

    .highcharts-data-table thead tr,
    .highcharts-data-table tr:nth-child(even) {
      background: #f8f8f8;
    }

    .highcharts-data-table tr:hover {
      background: #f1f7ff;
    }
    @media (max-width: 576px){
  .form{
    flex-direction: column;
    align-items: center !important;
  }

  .form > .form-son {
    width: 90% !important;
  }
}
  </style>
   <script src="https://code.highcharts.com/highcharts.js"></script>
  <script src="https://code.highcharts.com/highcharts-3d.js"></script>
  <script src="https://code.highcharts.com/modules/exporting.js"></script>
  <script src="https://code.highcharts.com/modules/export-data.js"></script>
  <script src="https://code.highcharts.com/modules/accessibility.js"></script>
  <div class="row">
    <?php 
      $candidate_number = null;
      $forme14 = null;
      $polling_station_name = null;
    ?>
    <div style="width: 100%; display: flex; justify-content: flex-end">
      <a href="{{route('witnesses.resultstatistic')}}" class="btn">Resultados Lugar de Votación</a>
    </div>
    <figure class="highcharts-figure" method="GET" class="form" >
      <form  action="" style="width: 100%; margin: 0 auto; margin-bottom: 25px; padding: 10px; display: flex; align-items: flex-end; justify-content: space-between" id="form_filter">
        <div class="form-group form-son" style="width:24%;">
          <label for="">Formulario</label>
          <select name="" id="forme14" class="form-control" onchange="getCandidatesForForm()">
            <option value="">Seleccionar</option>
            @foreach($formse14 as $form)
              @if($request->forme14_id && $request->candidate_id)
                @if($request->forme14_id == $form->id)
                  <?php $forme14 = $form->name ?>
                  <option value="{{$form->id}}" selected>{{$form->name}}</option>
                @else
                <option value="{{$form->id}}">{{$form->name}}</option>

                @endif
              @else
                <option value="{{$form->id}}">{{$form->name}}</option>
              @endif
            @endforeach
          </select>
  </div>

        <div class="form-group form-son" style="width:24%;">
          <label for="">Candidato</label>
          <select name="" id="candidate" class="form-control">
            <?php
              use App\Entities\FormE14Candidate;
              if($request){
                $candidates = FormE14Candidate::where('forme14_id', $request->forme14_id)->get();
              }
            ?>
            @if(count($candidates) > 0)
              @foreach($candidates as $candidate)
                @if($candidate->id == $request->candidate_id)
                  <?php $candidate_number = $candidate->candidate_number ?>
                  <option value="{{$candidate->id}}" selected>{{$candidate->candidate_number}}</option>
                @else
                  <option value="{{$candidate->id}}">{{$candidate->candidate_number}}</option>
                @endif
              @endforeach
            @endif
          </select>
        </div>

        <div class="form-group form-son" style="width:24%;">
          <label for="">Lugar de votación</label>
          <select name="" id="polling_station" class="form-control">
            <option value="">Seleccionar</option>
            @foreach($polling_stations as $polling_station)
              @if($request->forme14_id && $request->candidate_id && $request->polling_station_id)
                @if($request->polling_station_id == $polling_station->id)
                  <?php $polling_station_name = $polling_station->name ?>
                  <option value="{{$polling_station->id}}" selected>{{$polling_station->name}}</option>
                @else
                    <option value="{{$polling_station->id}}">{{$polling_station->name}}</option>
                @endif
              @else
                <option value="{{$polling_station->id}}">{{$polling_station->name}}</option>
              @endif
            @endforeach
          </select>
  </div>
        <div class="form-group form-son" style="width:24%;">
          <button class="btn btn-success btn-block" onclick="getStatisticForFilter()">Filtrar</button>
        </div>
      </form>
      <div id="container"></div>
    </figure>
  </div>

    <script>
      let chart = null
      function initChart(data){
        chart = Highcharts.chart('container', {
        chart: {
          type: 'pie',
          backgroundColor: "#ebeef2",
          style: {"fontFamily": "\"Lato\", \"Helvetica Neue\", Helvetica, Arial, sans-serif"},
          options3d: {
            enabled: true,
            alpha: 45
          }
        },
        title: {
            text: `
                Resultados Total Votos para el candidado numero <?php echo $candidate_number ?> 
                en el lugar de votación <?php echo $polling_station_name ?>
             `,
          },
          tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b><br>Total Votos: {point.total_votes}',
          },
        subtitle: {
          text: 'Total votos: <?php echo $data['total_votos'] ?>'
        },
        plotOptions: {
          pie: {
            innerSize: 100,
            depth: 45
          }
        },
        series: [{
          name: 'Porcentaje',
          vote: "Total votos",
          data:data
        }]
      });
      }


      $(document).ready(function(){
        let data = '<?php echo json_encode($data['data']) ?>'
        data = JSON.parse(data)
        if(data.length > 0){
          initChart(data)
        }

        updateStatisticTable(30)
      })

      function updateStatisticTable(time){
        let url = `/witnesses/update/results/statistic/table?` 
        let forme14_id = '<?php echo $request->forme14_id ?>'
        let candidate_id = '<?php echo $request->candidate_id ?>'
        let polling_station_id = '<?php echo $request->polling_station_id ?>'

        if(forme14_id) {
          url+= `forme14_id=${forme14_id}&`
        }
        if(candidate_id) {
          url+= `candidate_id=${candidate_id}&`
        }

        if(polling_station_id) {
          url+= `polling_station_id=${polling_station_id}&`
        }

        setInterval(function(){
          $.get(url).then(response => {
            let data = response.data

            chart.update(
              {
                tooltip: {
                  pointFormat: `{series.name}: <b>{point.percentage:.1f}%</b><br>Total Votos: {point.total_votes}`,
                },
                series: [{
                  name: 'Porcentaje',
                  vote: "Total votos",
                  data:data.data
                }],
                subtitle: {
                  text: `Total votos: ${data.total_votos}`
                },
              }
            )
          })
        }, 10000)
      }
      /*function getResultStatistic(forme14_id, candidate_id){
        let url = '/witnesses/results/statistic/polling_station'
        if(forme14_id){
          url = url + `/${forme14_id}`
        }
        if(candidate_id){
          url = url + `/${candidate_id}`
        }
        console.log("urlk", url)
        $.get(`${url}`).then(response => {
            console.log("repsonse", response.data   )
            if(response.success){
              initChart(response.data)
            }
        })
      }
*/
      function getCandidatesForForm(){
        let form_id = $("#forme14").val()
        $("#candidate").html("")
        if(form_id){
          $.get(`/witnesses/candidates/${form_id}`).then(response => {
            let data = response.data
              if(response.success){
                $("#candidate").append(`
                  <option value="">Seleccionar</option>
                `)
                data.forEach(elem => {
                  $("#candidate").append(`
                    <option value="${elem.id}">${elem.candidate_number}</option>
                  `)
                })
              }
          })
        }else{
          $("#candidate").html("")
        }
      }

      function getStatisticForFilter(){
        let forme14_id = $("#forme14").val()
        let candidate_id = $("#candidate").val()
        let polling_station_id = $("#polling_station").val()
        console.log("forme14_id", forme14_id)
        console.log("candidate", candidate_id)
        console.log("polling_station", polling_station)

        if(!forme14_id || !candidate_id || !polling_station_id) return alert("Por favor llene los campos")

        let form = $("#form_filter").attr("action", `${url_home}/witnesses/results/statistic/table/${polling_station_id}/${candidate_id}/${forme14_id}`)
        form.submit()
      }
    </script>
@endsection
