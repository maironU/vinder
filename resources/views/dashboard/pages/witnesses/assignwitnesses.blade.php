@extends('dashboard.pages.layout')
@section('title_page', 'Asignar testigos electorales')
@section('breadcrumbs') {!! Breadcrumbs::render('witnesses-assignwitness') !!} @endsection

@section('content_body_page')
<style>
    .content-form {
        display: flex;
        align-items: center;
        width: 100%;
    }

    .content-form > .form-group{
        margin-bottom: 0;
        margin-right: 10px;
        width: 270px;
    }

    @media (max-width: 560px){
        .content-form {
            flex-direction: column;
            align-items: center;
            justify-content: center;
        }

        .content-form > .form-group, input{
            width: 80%;
            margin: 0;
        }
    }
</style>
<div class="row" id="title_page" style="margin-bottom: 10px;">
    	<div style="display: flex; justify-content: space-between">
            <div class="col-md-12 flex">
                <div>
                    <a href="{{ route('witnesses.create')}}" class="btn btn-primary"><i class="fa fa-user"></i> Nuevo Testigo Electoral</a>
                </div>

                <div class="ml-2">
                    <a href="{{ route('witnesses.assignwitnesses.cedula')}}" class="btn btn-primary"><i class="fa fa-user"></i> Testigos con error</a>
                </div>
            </div>
            <?php 
                $polling_stations = \App\Entities\PollingStation::pollingWithMunicipaly();
                $zones = \App\Entities\Zone::all();
            ?>
            <div class="form-group cont-form-voters" style="display: flex; align-items: center; padding-right: 18px; margin-bottom: 0">
                <!--<label class="control-label" style="margin-right: 10px" for="orientador">Orientador</label>-->
                <h4 style="margin-right: 20px" id="text-filter">Filtrar Por:</h4>
                @include('dashboard.pages.utilities.polling_station_filter', ['polling_stations' => $polling_stations])
                @include('dashboard.pages.utilities.zone_filter', ['zones' => $zones])
            </div>
        </div>
        @if (session('message'))
            <div class="btn-alert-danger" id="alert">
                <span>{{ session('message') }}</span> <span style="position: absolute; right: 12px; cursor:pointer;font-size: 12px;line-height: 22px" id="close">X</span>
            </div>
        @endif
        <div class="row" style="margin: 0; margin-top: 30px" id="title_page" style="margin-bottom: 10px;">
            <div class="col-sm-8 content-form">
                <div class="form-group">
                    <input id="search" type="text" class="form-control" placeholder="Cedula, Nombre, Email, Teléfono" value="{{$request->search}}">
                </div>
                <input type="button" class="btn btn-primary" value="Buscar" onclick="filter()">
                <input type="button" class="btn btn-primary ml-2 btn_find_create" value="Buscar y Crear" onclick="findAndCreate()">
                @include('dashboard.pages.utilities.refresh')
            </div>
        </div>
    </div>
    <div class="block full">
        <div class="table-responsive">
            <table id="datatable" class="table table-striped table-bordered table-vcenter">
                <thead>
                    <tr>
                        <th title="Nombre de Usuario">Cedula</th>
                        @include('dashboard.pages.utilities.th_order', ['title' => 'Nombre completo', 'description' => 'Nombre completo'])
                        <th title="Teléfono">Teléfono</th>
                        <th title="Correo">Email</th>
                        <th title="Tipo">Tipo de usuario</th>
                        @include('dashboard.pages.utilities.th_order', ['title' => 'Lugar de Votación', 'description' => 'Lugar de Votación'])
                        <th title="mesa">Mesas</th>
                        <th class="text-center" style="width: 115px;"><i class="fa fa-flash"></i></th>
                    </tr>
                </thead>
                <tbody id="tbody">
                    @foreach($users as $user)
                        <tr>
                            <td style="width: fit-content">{{ $user->username }}</td>
                            <td><strong>{{ $user->name }}</strong></td>
                            <td style="width: fit-content">{{ $user->phone }}</td>
                            <td style="width: fit-content">{{ $user->email }}</td>
                            <td style="width: 70px">{{ $user->type->name }}</td>
                            <td>{{ $user->pollingStation ? $user->pollingStation->name." >> ".$user->pollingStation->address : 'Sin Lugar de votación asignado' }}</td>
                            <td style="display: flex; flex-direction: column">
                                @if(count($user->tables_numbers) > 0)
                                    @foreach($user->tables_numbers as $table)
                                        <span style="width: max-content">{{"Mesa ".$table->table_number}}</span>
                                    @endforeach
                                @endif
                            </td>
                            <td class="text-center">
                                <a href="{{ route('witnesses.edit', $user->id)}}" data-toggle="tooltip" title="Editar tipo de usuario" class="btn btn-effect-ripple btn-warning">
                                    <i class="fa fa-pencil"></i>
                                </a>
                                <a href="#" data-id="{{ $user->id }}" id="btn-delete-{{ $user->id }}" onclick="deleteModel('btn-delete-{{ $user->id }}')"  data-toggle="tooltip" title="Borrar tipo de usuario" class="btn btn-effect-ripple btn-danger">
                                    <i class="fa fa-times"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    @if(isset($request))
        {!! $users->appends(['search' => $request->search])->render() !!}
    @else
        {!! $users->render() !!}
    @endif
    <!-- END Datatables Block -->
    {!! Form::open(array('route' => array('system.users.destroy', 'ID') , 'method' => 'DELETE', 'role' => 'form', 'id' => 'form-delete'))!!}

@endsection
@section('js_aditional')
<script>
    function findAndCreate(){
        let search = $("#search").val()
        if(!search) return alert("Por favor escriba la cédula")

        location.href  = "/witnesses/assign/create/" + search
    }

    $('#search').keypress(function(e) {
        var keycode = (e.keyCode ? e.keyCode : e.which);
        if (keycode == '13') {
            filter();
            e.preventDefault();
            return false;
        }
    });
</script>
@endsection

