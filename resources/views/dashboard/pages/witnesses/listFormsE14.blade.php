<script>
    let rol_auth = "{{Auth::user()->type->id}}"
</script>
<div class="row" style="margin-bottom: 10px;">
    @if(Auth::user()->type->id == 1 || Auth::user()->type->id == 2)
        <div class="col-md-12">
            <a href="{{ route('witnesses.createforme14')}}" class="btn btn-primary"><i class="fa fa-user"></i> Nuevo Formulario</a>
        </div>
    @endif
</div>
<div class="block full">
    @if(Auth::user()->type->id == 5 || Auth::user()->type->id == 1 || Auth::user()->type->id == 2)
        <div class="table-responsive">
            <table id="datatable" class="table table-striped table-bordered table-vcenter">
                <thead>
                    <tr>
                        <th title="Nombre del formulario">Nombre del formulario</th>
                        <th class="text-center" style="width: 115px;"><i class="fa fa-flash"></i></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($formse14 as $form)
                        <tr>
                            <td>{{ $form->name }}</td>
                            <td class="text-center">
                                <a href="{{ route('witnesses.editforme14', $form->id)}}" data-toggle="tooltip" title="Editar formulario" class="btn btn-effect-ripple btn-warning">
                                    <i class="fa fa-pencil"></i>
                                </a>
                                @if(Auth::user()->type->id == 1 || Auth::user()->type->id == 2)
                                    <a href="#" data-id="{{ $form->id }}" id="btn-delete-{{ $form->id }}" onclick="deleteModel('btn-delete-{{ $form->id }}')"  data-toggle="tooltip" title="Borrar formulario" class="btn btn-effect-ripple btn-danger">
                                        <i class="fa fa-times"></i>
                                    </a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    @endif

    @if(Auth::user()->type->id == 8)
        <div class="text-center">
            <h2>{{Auth::user()->pollingStation ? Auth::user()->pollingStation->name : ''}}</h2>
        </div>
        <div style="display:flex; justify-content: flex-end">
            <div style="display: flex; align-items: center; margin-right: 10px">
                <div style="display: flex">
                    <a href="/witnesses/createform?check=verde">
                        <img style="margin-right: 10px; cursor: pointer" src="{{asset('images/check.png')}}" alt="" width="30px">
                    </a>
                    <a href="/witnesses/createform?check=naranja">
                        <img style="margin-right: 10px; cursor: pointer" src="{{asset('images/check_orange.png')}}" alt="" width="30px">
                    </a>
                    <a href="/witnesses/createform?check=blanca">
                        <div style="width: 30px; height:30px; border-radius: 50%; border: 1px solid #141313; margin: 0 auto; cursor: pointer"></div>
                    </a>
                    <a href="/witnesses/createform" style="display: flex; align-items:center;  margin-left: 8px">
                        <span>Todas</span>
                    </a>
                </div>
            </div>
        </div>
    @endif

    @if(Auth::user()->type->id == 8)
        @foreach($formse14 as $forme14)
            <h2>Formulario: {{$forme14->name}}</h2>
            <div class="table-responsive">
                <table id="datatable" class="table table-bordered table-vcenter">
                    <thead>
                        <tr>
                            <th>Mesa</th>
                            <th>Testigo</th>
                            <th title="Ver Formulario">Ver Formulario</th>
                            <th title="Votos">Votos</th>
                            <th title="Presentó Reclamación">Presentó reclamación</th>
                            <th class="text-center" style="width: 115px;"><i class="fa fa-flash"></i></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                            $num_tables =  Auth::user()->pollingStation->tables ?? 0; 
                        ?>
                        @for($i = 1; $i <= $num_tables; $i++)
                            <?php 
                                $testigoElectoral = \App\Entities\User::where('type_id', '=', 5)
                                    ->where(function($q) use($i){
                                        $q->whereHas('tables_numbers', function($subquery) use($i){
                                            $subquery->where('table_number', $i)
                                            ->orWhere('table_number', 'Mesa '.$i);
                                        });
                                    })
                                    ->where('polling_station_id', Auth::user()->pollingStation->id)
                                    ->with(['votes' => function($q){
                                        $q->join('forme14_candidates as fc', 'fc.id', '=', 'forme14_votes.candidate_id');
                                        $q->join('formse14 as fe', 'fe.candidate_check', '=', 'fc.id');
                                    }])
                                    ->first();
                            ?>
                            @if($request->check && ($request->check == "verde" || $request->check == "naranja" || $request->check == "blanca"))
                                @if($testigoElectoral)
                                    <?php 
                                        $votes = \App\Entities\FormE14Vote::where('user_id', $testigoElectoral->id)
                                                    ->where('table_number', $i)

                                        ->join('forme14_candidates as fc', 'forme14_votes.candidate_id', '=', 'fc.id')
                                        ->where('fc.forme14_id', $forme14->id)
                                        ->get();

                                        $candidates = $forme14->candidates;

                                        $file = $forme14->files()->where('user_id', $testigoElectoral->id)
                                                    ->where('table_number', $i)
                                        ->first();

                                    ?>
                                    @if($request->check == "verde")
                                        @if(count($votes) && $file && $file->file && $file->file_secondary)
                                            <tr>
                                                <td>{{$i}}</td>
                                                @if($testigoElectoral)
                                                    <td class="text-center">{{$testigoElectoral->name}}</td>
                                                @else
                                                    <td class="text-center">
                                                        <span>No hay testigo para la mesa</span>
                                                    </td>
                                                @endif
            
                                                @if($testigoElectoral)
                                                    <?php 
                                                        $file = $forme14->files()->where('user_id', $testigoElectoral->id)
                                                        ->where('table_number', $i)
                                                        ->first();
                                                    ?>
                                                    <td style="text-align: center">
                                                        @if($file)
                                                            @if($file->file)
                                                                <a target="_blank" href="{{asset('files_forme14/'.$file->file)}}" data-toggle="tooltip" title="Ver Formulario" class="btn btn-effect-ripple btn-warning" style="width: fit-content">
                                                                    <i class="fa fa-eye"></i>
                                                                </a>
                                                            @endif
                                                            @if($file->file_secondary)
                                                                <a target="_blank" href="{{asset('files_forme14/'.$file->file_secondary)}}" data-toggle="tooltip" title="Ver Formulario Secundario" class="btn btn-effect-ripple btn-warning" style="width: fit-content">
                                                                    <i class="fa fa-eye"></i>
                                                                </a>
                                                            @endif
                                                        @else
                                                            <span>No ha subido el archivo</span>
                                                        @endif
                                                    </td>
                                                @else
                                                    <td class="text-center">
                                                        <span>No hay testigo para la mesa</span>
                                                    </td>
                                                @endif
            
                                                @if($testigoElectoral)
                                                    <td style="text-align: center">
                                                        <img width="35px" src="{{asset('images/check.png')}}" alt="" style="cursor: pointer"  data-toggle="modal" data-target="#exampleModalLong" onclick="getFormE14('{{$forme14->id}}', '{{$i}}', '{{$testigoElectoral->id}}')">
                                                    </td>
                                                @else
                                                    <td class="text-center">
                                                        <span>No hay testigo para la mesa</span>
                                                    </td>
                                                @endif
            
                                                @if($testigoElectoral)
                                                    <?php 
                                                        $filed_claim = \App\Entities\FormE14FiledClaim::where('forme14_id', $forme14->id)
                                                            ->where('user_id', $testigoElectoral->id)
                                                        ->where('table_number', $i)

                                                            ->first();

                                                        $obj_filed_claims = [
                                                        "no" => "No hubo reclamación",
                                                        "otro_candidato" => "Otro candidato",
                                                        "excedio" => "Excedio el número de votos de la mesa",
                                                        "error_aritmetico" => "Error aritmético",
                                                        "error_nombre" => "Error en el nombre del candidato E14",
                                                        "firmas_incompletas" => "Firmas incompletas",
                                                        "1" => "SI",
                                                        "0" => "No hubo reclamación"
                                        ];

                                                    ?>
                                                    @if($filed_claim)
                                                        <td class="text-center">{{$obj_filed_claims[$filed_claim->filed_claim]}}</td>
                                                        @else
                                                        <td class="text-center">{{$obj_filed_claims["no"]}}</td>
                                                    @endif                                            
                                                @else
                                                    <td class="text-center">
                                                        <span>No hay testigo para la mesa</span>
                                                    </td>
                                                @endif
            
                                                @if($testigoElectoral)
                                                    <td class="text-center" style="display: flex">
                                                        <a style="margin-right: 5px" href="{{ route('witnesses.editforme14',['forme14_id' => $forme14->id, 'table_number' => $i, 'testigo_id' => $testigoElectoral->id])}}" data-toggle="tooltip" title="Editar formulario" class="btn btn-effect-ripple btn-warning">
                                                            <i class="fa fa-pencil"></i>
                                                        </a>
                                                        <form action="{{route('infoForm.delete', [$testigoElectoral->id, $i, $forme14->id])}}" method="POST" id="form-info-{{$testigoElectoral->id}}-{{$forme14->id}}-{{$i}}" style="margin: 0">
                                                            {!! csrf_field() !!}
                                                            {!! method_field('DELETE') !!}
            
                                                            <button data-toggle="tooltip" title="Borrar Información del Formulario" class="btn btn-effect-ripple btn-danger" onclick="clearInfo(event, '{{$testigoElectoral->id}}', '{{$i}}','{{$forme14->id}}')">
                                                                <i class="fa fa-times"></i>
                                                            </button>
                                                        </form>
                                                    </td>
                                                @endif
                                            </tr>
                                        @endif
                                    @elseif($request->check == "naranja")
                                        @if((count($votes) > 0 && !$file) || (count($votes) > 0 && $file && $file->file && !$file->file_secondary))
                                            <tr>
                                                <td>{{$i}}</td>
                                                @if($testigoElectoral)
                                                    <td class="text-center">{{$testigoElectoral->name}}</td>
                                                @else
                                                    <td class="text-center">
                                                        <span>No hay testigo para la mesa</span>
                                                    </td>
                                                @endif
            
                                                @if($testigoElectoral)
                                                    <?php 
                                                        $file = $forme14->files()->where('user_id', $testigoElectoral->id)
                                                    ->where('table_number', $i)
                                                        ->first();
                                                    ?>
                                                    <td style="text-align: center">
                                                        @if($file)
                                                            @if($file->file)
                                                                <a target="_blank" href="{{asset('files_forme14/'.$file->file)}}" data-toggle="tooltip" title="Ver Formulario" class="btn btn-effect-ripple btn-warning" style="width: fit-content">
                                                                    <i class="fa fa-eye"></i>
                                                                </a>
                                                            @endif
                                                            @if($file->file_secondary)
                                                                <a target="_blank" href="{{asset('files_forme14/'.$file->file_secondary)}}" data-toggle="tooltip" title="Ver Formulario Secundario" class="btn btn-effect-ripple btn-warning" style="width: fit-content">
                                                                    <i class="fa fa-eye"></i>
                                                                </a>
                                                            @endif
                                                        @else
                                                            <span>No ha subido el archivo</span>
                                                        @endif
                                                    </td>
                                                @else
                                                    <td class="text-center">
                                                        <span>No hay testigo para la mesa</span>
                                                    </td>
                                                @endif
            
                                                @if($testigoElectoral)
                                                    <td style="text-align: center">
                                                        <img width="35px" src="{{asset('images/check_orange.png')}}" alt="" style="cursor: pointer"  data-toggle="modal" data-target="#exampleModalLong" onclick="getFormE14('{{$forme14->id}}', '{{$i}}', '{{$testigoElectoral->id}}')">
                                                    </td>
                                                @else
                                                    <td class="text-center">
                                                        <span>No hay testigo para la mesa</span>
                                                    </td>
                                                @endif

                                                @if($testigoElectoral)
                                                    <?php 
                                                        $filed_claim = \App\Entities\FormE14FiledClaim::where('forme14_id', $forme14->id)
                                                            ->where('user_id', $testigoElectoral->id)
                                                        ->where('table_number', $i)

                                                            ->first();

                                                        $obj_filed_claims = [
                                                        "no" => "No hubo reclamación",
                                                        "otro_candidato" => "Otro candidato",
                                                        "excedio" => "Excedio el número de votos de la mesa",
                                                        "error_aritmetico" => "Error aritmético",
                                                        "error_nombre" => "Error en el nombre del candidato E14",
                                                        "firmas_incompletas" => "Firmas incompletas",
                                                        "1" => "SI",
                                                        "0" => "No hubo reclamación"
                                        ];

                                                    ?>
                                                    @if($filed_claim)
                                                        <td class="text-center">{{$obj_filed_claims[$filed_claim->filed_claim]}}</td>
                                                        @else
                                                        <td class="text-center">{{$obj_filed_claims["no"]}}</td>
                                                    @endif                                            
                                                @else
                                                    <td class="text-center">
                                                        <span>No hay testigo para la mesa</span>
                                                    </td>
                                                @endif
            
                                                @if($testigoElectoral)
                                                    <td class="text-center" style="display: flex">
                                                        <a style="margin-right: 5px" href="{{ route('witnesses.editforme14',['forme14_id' => $forme14->id, 'table_number' => $i, 'testigo_id' => $testigoElectoral->id])}}" data-toggle="tooltip" title="Editar formulario" class="btn btn-effect-ripple btn-warning">
                                                            <i class="fa fa-pencil"></i>
                                                        </a>
                                                        <form action="{{route('infoForm.delete', [$testigoElectoral->id, $i, $forme14->id])}}" method="POST" id="form-info-{{$testigoElectoral->id}}-{{$forme14->id}}-{{$i}}" style="margin: 0">
                                                            {!! csrf_field() !!}
                                                            {!! method_field('DELETE') !!}
            
                                                            <button data-toggle="tooltip" title="Borrar Información del Formulario" class="btn btn-effect-ripple btn-danger" onclick="clearInfo(event, '{{$testigoElectoral->id}}','{{$i}}','{{$forme14->id}}')">
                                                                <i class="fa fa-times"></i>
                                                            </button>
                                                        </form>
                                                    </td>
                                                @endif
                                            </tr>
                                        @endif
                                    @else 
                                            <tr>
                                                <td>{{$i}}</td>
                                                @if($testigoElectoral)
                                                    <td class="text-center">{{$testigoElectoral->name}}</td>
                                                @else
                                                    <td class="text-center">
                                                        <span>No hay testigo para la mesa</span>
                                                    </td>
                                                @endif
            
                                                @if($testigoElectoral)
                                                    <?php 
                                                        $file = $forme14->files()->where('user_id', $testigoElectoral->id)
                                                    ->where('table_number', $i)
                                                        ->first();
                                                    ?>
                                                    <td style="text-align: center">
                                                        @if($file)
                                                            @if($file->file)
                                                                <a target="_blank" href="{{asset('files_forme14/'.$file->file)}}" data-toggle="tooltip" title="Ver Formulario" class="btn btn-effect-ripple btn-warning" style="width: fit-content">
                                                                    <i class="fa fa-eye"></i>
                                                                </a>
                                                            @endif
                                                            @if($file->file_secondary)
                                                                <a target="_blank" href="{{asset('files_forme14/'.$file->file_secondary)}}" data-toggle="tooltip" title="Ver Formulario Secundario" class="btn btn-effect-ripple btn-warning" style="width: fit-content">
                                                                    <i class="fa fa-eye"></i>
                                                                </a>
                                                            @endif
                                                        @else
                                                            <span>No ha subido el archivo</span>
                                                        @endif
                                                    </td>
                                                @else
                                                    <td class="text-center">
                                                        <span>No hay testigo para la mesa</span>
                                                    </td>
                                                @endif
            
                                                @if($testigoElectoral)
                                                    <td style="text-align: center">
                                                        <div style="width: 35px; height:35px; border-radius: 50%; border: 1px solid #141313; margin: 0 auto; cursor: pointer"  data-toggle="modal" data-target="#exampleModalLong" onclick="getFormE14('{{$forme14->id}}', '{{$i}}', '{{$testigoElectoral->id}}')"></div>
                                                    </td>
                                                @else
                                                    <td class="text-center">
                                                        <span>No hay testigo para la mesa</span>
                                                    </td>
                                                @endif

                                                @if($testigoElectoral)
                                                    <?php 
                                                        $filed_claim = \App\Entities\FormE14FiledClaim::where('forme14_id', $forme14->id)
                                                            ->where('user_id', $testigoElectoral->id)
                                                        ->where('table_number', $i)

                                                            ->first();

                                                        $obj_filed_claims = [
                                                        "no" => "No hubo reclamación",
                                                        "otro_candidato" => "Otro candidato",
                                                        "excedio" => "Excedio el número de votos de la mesa",
                                                        "error_aritmetico" => "Error aritmético",
                                                        "error_nombre" => "Error en el nombre del candidato E14",
                                                        "firmas_incompletas" => "Firmas incompletas",
                                                        "1" => "SI",
                                                        "0" => "No hubo reclamación"
                                        ];

                                                    ?>
                                                    @if($filed_claim)
                                                        <td class="text-center">{{$obj_filed_claims[$filed_claim->filed_claim]}}</td>
                                                        @else
                                                        <td class="text-center">{{$obj_filed_claims["no"]}}</td>
                                                    @endif                                            
                                                @else
                                                    <td class="text-center">
                                                        <span>No hay testigo para la mesa</span>
                                                    </td>
                                                @endif
            
            
                                                @if($testigoElectoral)
                                                    <td class="text-center" style="display: flex">
                                                        <a style="margin-right: 5px" href="{{ route('witnesses.editforme14',['forme14_id' => $forme14->id, 'table_number' => $i, 'testigo_id' => $testigoElectoral->id])}}" data-toggle="tooltip" title="Editar formulario" class="btn btn-effect-ripple btn-warning">
                                                            <i class="fa fa-pencil"></i>
                                                        </a>
                                                        <form action="{{route('infoForm.delete', [$testigoElectoral->id, $i, $forme14->id])}}" method="POST" id="form-info-{{$testigoElectoral->id}}-{{$forme14->id}}-{{$i}}" style="margin: 0">
                                                            {!! csrf_field() !!}
                                                            {!! method_field('DELETE') !!}
            
                                                            <button data-toggle="tooltip" title="Borrar Información del Formulario" class="btn btn-effect-ripple btn-danger" onclick="clearInfo(event, '{{$testigoElectoral->id}}','{{$i}}','{{$forme14->id}}')">
                                                                <i class="fa fa-times"></i>
                                                            </button>
                                                        </form>
                                                    </td>
                                                @endif
                                            </tr>
                                    @endif
                                @endif
                            
                            @else
                                <tr>
                                    <td>{{$i}}</td>
                                    @if($testigoElectoral)
                                        <td class="text-center">{{$testigoElectoral->name}}</td>
                                    @else
                                        <td class="text-center">
                                            <span>No hay testigo para la mesa</span>
                                        </td>
                                    @endif

                                    @if($testigoElectoral)
                                        <?php 
                                            $file = $forme14->files()->where('user_id', $testigoElectoral->id)
                                                    ->where('table_number', $i)
                                            ->first();
                                        ?>
                                        <td style="text-align: center">
                                            @if($file)
                                                @if($file)
                                                    @if($file->file)
                                                        <a target="_blank" href="{{asset('files_forme14/'.$file->file)}}" data-toggle="tooltip" title="Ver Formulario" class="btn btn-effect-ripple btn-warning" style="width: fit-content">
                                                            <i class="fa fa-eye"></i>
                                                        </a>
                                                    @endif
                                                    @if($file->file_secondary)
                                                        <a target="_blank" href="{{asset('files_forme14/'.$file->file_secondary)}}" data-toggle="tooltip" title="Ver Formulario Secundario" class="btn btn-effect-ripple btn-warning" style="width: fit-content">
                                                            <i class="fa fa-eye"></i>
                                                        </a>
                                                    @endif
                                                @endif
                                            @else
                                                <span>No ha subido el archivo</span>
                                            @endif
                                        </td>
                                    @else
                                        <td class="text-center">
                                            <span>No hay testigo para la mesa</span>
                                        </td>
                                    @endif

                                    @if($testigoElectoral)
                                        <td style="text-align: center">
                                            <?php 
                                                $votes = \App\Entities\FormE14Vote::where('user_id', $testigoElectoral->id)
                                                    ->where('table_number', $i)

                                                ->join('forme14_candidates as fc', 'forme14_votes.candidate_id', '=', 'fc.id')
                                                ->where('fc.forme14_id', $forme14->id)
                                                ->get();

                                                $candidates = $forme14->candidates;

                                                $file = $forme14->files()->where('user_id', $testigoElectoral->id)
                                                    ->where('table_number', $i)
                                                ->first();  
                                            ?>
                                            @if(count($votes) && $file && $file->file && $file->file_secondary)
                                                <img width="35px" src="{{asset('images/check.png')}}" alt="" style="cursor: pointer"  data-toggle="modal" data-target="#exampleModalLong" onclick="getFormE14('{{$forme14->id}}', '{{$i}}', '{{$testigoElectoral->id}}')">
                                            @elseif((count($votes) > 0 && !$file) || (count($votes) > 0 && $file && $file->file && !$file->file_secondary))
                                                <img width="35px" src="{{asset('images/check_orange.png')}}" alt="" style="cursor: pointer"  data-toggle="modal" data-target="#exampleModalLong" onclick="getFormE14('{{$forme14->id}}', '{{$i}}', '{{$testigoElectoral->id}}')">
                                            @else
                                                <div style="width: 35px; height:35px; border-radius: 50%; border: 1px solid #141313; margin: 0 auto; cursor: pointer"  data-toggle="modal" data-target="#exampleModalLong" onclick="getFormE14('{{$forme14->id}}', '{{$i}}', '{{$testigoElectoral->id}}')"></div>
                                            @endif
                                        </td>
                                    @else
                                        <td class="text-center">
                                            <span>No hay testigo para la mesa</span>
                                        </td>
                                    @endif

                                    @if($testigoElectoral)
                                        <?php 
                                            $filed_claim = \App\Entities\FormE14FiledClaim::where('forme14_id', $forme14->id)
                                                ->where('user_id', $testigoElectoral->id)
                                            ->where('table_number', $i)

                                                ->first();

                                            $obj_filed_claims = [
                                            "no" => "No hubo reclamación",
                                            "otro_candidato" => "Otro candidato",
                                            "excedio" => "Excedio el número de votos de la mesa",
                                            "error_aritmetico" => "Error aritmético",
                                            "error_nombre" => "Error en el nombre del candidato E14",
                                            "firmas_incompletas" => "Firmas incompletas",
                                            "1" => "SI",
                                            "0" => "No hubo reclamación"
                            ];

                                        ?>
                                        @if($filed_claim)
                                            <td class="text-center">{{$obj_filed_claims[$filed_claim->filed_claim]}}</td>
                                            @else
                                            <td class="text-center">{{$obj_filed_claims["no"]}}</td>
                                        @endif                                            
                                    @else
                                        <td class="text-center">
                                            <span>No hay testigo para la mesa</span>
                                        </td>
                                    @endif

                                    @if($testigoElectoral)
                                        <td class="text-center" style="display: flex">
                                            <a style="margin-right: 5px" href="{{ route('witnesses.editforme14',['forme14_id' => $forme14->id, 'table_number' => $i, 'testigo_id' => $testigoElectoral->id])}}" data-toggle="tooltip" title="Editar formulario" class="btn btn-effect-ripple btn-warning">
                                                <i class="fa fa-pencil"></i>
                                            </a>
                                            <form action="{{route('infoForm.delete', [$testigoElectoral->id, $i, $forme14->id])}}" method="POST" id="form-info-{{$testigoElectoral->id}}-{{$forme14->id}}-{{$i}}" style="margin: 0">
                                                {!! csrf_field() !!}
                                                {!! method_field('DELETE') !!}

                                                <button data-toggle="tooltip" title="Borrar Información del Formulario" class="btn btn-effect-ripple btn-danger" onclick="clearInfo(event, '{{$testigoElectoral->id}}', '{{$i}}','{{$forme14->id}}')">
                                                    <i class="fa fa-times"></i>
                                                </button>
                                            </form>
                                        </td>
                                    @endif
                                </tr>
                            @endif
                        @endfor
                    </tbody>
                </table>
            </div>
        @endforeach
    @endif
</div>
@include('dashboard/pages/witnesses/modal_forme14')

{!! Form::open(array('route' => array('witnesses.destroy', 'ID') , 'method' => 'DELETE', 'role' => 'form', 'id' => 'form-delete'))!!}
