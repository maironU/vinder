@extends('dashboard.pages.layout')
@section('title_page')
 Resultados Estadísticos
@endsection
@section('breadcrumbs') {!! Breadcrumbs::render('witnesses-results-statistic') !!} @endsection
{!! Html::script('assets/js/vendor/jquery-2.1.3.min.js'); !!}
@section('content_body_page')
  <style>
    #container {
  height: 400px;
}

.highcharts-figure,
.highcharts-data-table table {
  min-width: 310px;
  max-width: 800px;
  margin: 1em auto;
}

.highcharts-data-table table {
  font-family: Verdana, sans-serif;
  border-collapse: collapse;
  border: 1px solid #ebebeb;
  margin: 10px auto;
  text-align: center;
  width: 100%;
  max-width: 500px;
}

.highcharts-data-table caption {
  padding: 1em 0;
  font-size: 1.2em;
  color: #555;
}

.highcharts-data-table th {
  font-weight: 600;
  padding: 0.5em;
}

.highcharts-data-table td,
.highcharts-data-table th,
.highcharts-data-table caption {
  padding: 0.5em;
}

.highcharts-data-table thead tr,
.highcharts-data-table tr:nth-child(even) {
  background: #f8f8f8;
}

.highcharts-data-table tr:hover {
  background: #f1f7ff;
}

.highcharts-figure,
.highcharts-data-table table {
  min-width: 310px;
  max-width: 800px;
  margin: 1em auto;
}

#container {
  height: 400px;
}

.highcharts-data-table table {
  font-family: Verdana, sans-serif;
  border-collapse: collapse;
  border: 1px solid #ebebeb;
  margin: 10px auto;
  text-align: center;
  width: 100%;
  max-width: 500px;
}

.highcharts-data-table caption {
  padding: 1em 0;
  font-size: 1.2em;
  color: #555;
}

.highcharts-data-table th {
  font-weight: 600;
  padding: 0.5em;
}

.highcharts-data-table td,
.highcharts-data-table th,
.highcharts-data-table caption {
  padding: 0.5em;
}

.highcharts-data-table thead tr,
.highcharts-data-table tr:nth-child(even) {
  background: #f8f8f8;
}

.highcharts-data-table tr:hover {
  background: #f1f7ff;
}

@media (max-width: 576px){
  .form{
    flex-direction: column;
    align-items: center !important;
  }

  .form > .form-son {
    width: 90% !important;
  }
}
  </style>
  <script src="https://code.highcharts.com/highcharts.js"></script>
  <script src="https://code.highc harts.com/modules/data.js"></script>
  <script src="https://code.highcharts.com/modules/drilldown.js"></script>
  <script src="https://code.highcharts.com/highcharts-3d.js"></script>
  <script src="https://code.highcharts.com/modules/exporting.js"></script>
  <script src="https://code.highcharts.com/modules/export-data.js"></script>
  <script src="https://code.highcharts.com/modules/accessibility.js"></script>
  <div class="row">
    <?php 
      $candidate_number = null;
      $forme14 = null;
    ?>
    <div style="width: 100%; display: flex; justify-content: flex-end">
      <a href="{{route('witnesses.resultstatistictable')}}" class="btn">Resultados Mesas de Votación</a>
    </div>

      <form  method="GET" action="" class="row form" style="width: 70%; margin: 0 auto; margin-bottom: 25px; display: flex; align-items: flex-end; justify-content: space-between;padding: 10px" id="form_filter">
        <div class="form-group form-son" style="width: 31.33%;">
          <label for="">Formulario</label>
          <select name="" id="forme14" class="form-control" onchange="getCandidatesForForm()">
            <option value="">Seleccionar</option>
            @foreach($formse14 as $form)
              @if($request->forme14_id)
                @if($request->forme14_id == $form->id)
                  <?php $forme14 = $form->name ?>
                  <option value="{{$form->id}}" selected>{{$form->name}}</option>
                @else
                <option value="{{$form->id}}">{{$form->name}}</option>
                @endif
              @else
                <option value="{{$form->id}}">{{$form->name}}</option>
              @endif
            @endforeach
          </select>
        </div>

        <div class="form-group form-son" style="width: 31.33%;">
          <label for="">Candidato</label>
          <select name="" id="candidate" class="form-control">
            <?php
              use App\Entities\FormE14Candidate;
              if($request){
                $candidates = FormE14Candidate::where('forme14_id', $request->forme14_id)->get();
              }
            ?>
            @if(count($candidates) > 0)
              <option value="">Seleccionar</option>
              @foreach($candidates as $candidate)
                @if($candidate->id == $request->candidate_id)
                  <?php $candidate_number = $candidate->candidate_number ?>
                  <option value="{{$candidate->id}}" selected>{{$candidate->candidate_number}}</option>
                @else
                  <option value="{{$candidate->id}}">{{$candidate->candidate_number}}</option>
                @endif
              @endforeach
            @endif
          </select>
        </div>
        <div class="form-group form-son" style="width: 31.33%;">
          <button class="btn btn-success btn-block" onclick="getStatisticForFilter()">Filtrar</button>
        </div>
      </form>
            
      @if($request->forme14_id)
        <div style="width: 95%; margin: 0 auto">
          <figure class="highcharts-figure">
            <div id="container-general"></div>
          </figure>
        </div>
      @endif


    <div style="width: 95%; margin: 0 auto">
      <figure class="highcharts-figure">
        <div id="container"></div>
      </figure>
    </div>
  </div>

    <script>
      let chart = null;

      function initChart(data){
        chart = Highcharts.chart('container', {
        chart: {
          type: 'pie',
          backgroundColor: "#ebeef2",
          style: {"fontFamily": "\"Lato\", \"Helvetica Neue\", Helvetica, Arial, sans-serif"},
          options3d: {
            enabled: true,
            alpha: 45
          }
        },
        title: {
          text: 'Resultados Total Votos para el candidato <?php echo $candidate_number ?>
                      en todos los lugares de votación'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b><br>Total Votos: {point.total_votes}',
        },
        subtitle: {
          text: 'Total votos: <?php echo $data['total_votos'] ?>'
        },
        plotOptions: {
          pie: {
            innerSize: 100,
            depth: 45
          }
        },
        series: [{
          name: 'Porcentaje',
          vote: "Total votos",
          data:data
        }]
      });
      }

      let chart_general = null;

      function initChartGeneral(data_general, data_series_table){
        // Create the chart
        chart_general = Highcharts.chart('container-general', {
          chart: {
            type: 'column'
          },
          title: {
            text: 'Resultados Mesas escrutadas con porcentaje'
          },
          subtitle: {
            text: 'Dale Click en la columna para ver los porcentajes de cada mesa<br> <div style="font-size: 16px">Porcentaje General <?php echo number_format($data['percent_general'], 2) ?>%</div>'
          },
          accessibility: {
            announceNewData: {
              enabled: true
            }
          },
          xAxis: {
            type: 'category'
          },
          yAxis: {
            title: {
              text: 'Total porcentaje'
            }

          },
          legend: {
            enabled: false
          },
          plotOptions: {
            series: {
              borderWidth: 0,
              dataLabels: {
                enabled: true,
                format: '{point.y:.1f}%'
              }
            }
          },

          tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b><br/>'
          },

          series: [
            {
              name: "Puesto de votación",
              colorByPoint: true,
              data: data_general
            }
          ],
          drilldown: {
            series: data_series_table
          }
        });
      }

      $(document).ready(function(){
        let data = '<?php echo json_encode($data['data']) ?>'
        let data_general = '<?php echo json_encode($data['data_general_table']) ?>'
        let data_series_table = '<?php echo json_encode($data['data_series_table']) ?>'


        data = JSON.parse(data)
        data_general = JSON.parse(data_general)
        data_series_table = JSON.parse(data_series_table)

        if(data.length > 0){
          initChart(data)
        }
        console.log("data_general", data_general)
        console.log("data_series_table", data_series_table)

        initChartGeneral(data_general, data_series_table)

        updateStatistic(30)
      })

      function updateStatistic(time){
        let url = `/witnesses/update/results/statistic/?` 
        let forme14_id = '<?php echo $request->forme14_id ?>'
        let candidate_id = '<?php echo $request->candidate_id ?>'

        if(forme14_id) {
          url+= `forme14_id=${forme14_id}&`
        }
        if(candidate_id) {
          url+= `candidate_id=${candidate_id}&`
        }

        setInterval(function(){
          $.get(url).then(response => {
            let data = response.data

            chart_general.update(
              {
                  series: [
                    {
                      name: "Puesto de votación",
                      colorByPoint: true,
                      data: data.data_general_table
                    }
                  ],
                  drilldown: {
                    series: data.data_series_table
                  },
                  subtitle: {
                    text: `Dale Click en la columna para ver los porcentajes de cada mesa<br> <div style="font-size: 16px">Porcentaje General ${data.percent_general.toFixed(2)}%</div>`
                  }
              }
            )

            chart.update(
              {
                subtitle: {
                  text: `Total votos: ${data.total_votos}`
                },
                series: [{
                  name: 'Porcentaje',
                  vote: "Total votos",
                  data:data.data
                }]
              }
            )
          })
        }, 50000)
      }

      function getCandidatesForForm(){
        let form_id = $("#forme14").val()
        $("#candidate").html("")

        if(form_id){
          $.get(`/witnesses/candidates/${form_id}`).then(response => {
            let data = response.data
              if(response.success){
                $("#candidate").append(`
                  <option value="">Seleccionar</option>
                `)
                data.forEach(elem => {
                  $("#candidate").append(`
                    <option value="${elem.id}">${elem.candidate_number}</option>
                  `)
                })
              }
          })
        }else{
          $("#candidate").html("")
        }
      }

      function getStatisticForFilter(){
        let forme14_id = $("#forme14").val()
        let candidate_id = $("#candidate").val()
        
        if(!forme14_id) return alert("Por favor escoge el formulario")

        let form = $("#form_filter").attr("action", `${url_home}/witnesses/results/statistic/${forme14_id}/${candidate_id}`)
        form.submit()
      }
    </script>
@endsection
