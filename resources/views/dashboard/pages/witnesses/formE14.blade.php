
<script>
    let rol = "{{$user ? $user->type->id : null}}"
    let rol_auth = "{{Auth::user()->type->id}}"
    let testigo_id = "{{Auth::user()->type->id == 8 || Auth::user()->type->id == 1 || Auth::user()->type->id == 9 ? $user ? $user->id : null : null}}"
    let forme14_id = "{{$formE14 ? $formE14->id : null}}"
    let table_number = "{{$table_number ?? null}}"
</script>
<input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
<div class="btn-alert-success display-none" id="alert">
    <span>El formulario ha sido actualizado con éxito</span> <span style="position: absolute; right: 12px; cursor:pointer;font-size: 12px;line-height: 22px" id="close">X</span>
</div>
<?php 
    $create_bool = isset($create) && $create ? true : false;
?> 
<div class="card-form">        
    @if(Auth::user()->type->id == 5 || Auth::user()->type->id == 8 || (Auth::user()->type->id == 1 && !$create_bool) || Auth::user()->type->id == 9)
        <div class="flex justify-content-between" style="max-width: 500px; margin: 0 auto">
            <input disabled class="form-control text-center text-primary" style="border: 0; background: white; height: 20px; font-size: 19px;padding:10px" type="text" placeholder="Gobernación" id="name-form" value="{{$formE14 ? strtoupper($formE14->name) : null}}">
        </div>
        @if($formE14)
            <div class="forme14" >
                <div class="forme14-info" id="forme14-info">
                    @foreach($formE14->info as $info)
                        @if(strtoupper($info->name) == strtoupper("Mesa"))
                            <div class="segment">
                                <div class="segment-info" data-id="{{$info->id}}">
                                    <input disabled type="text" class="form-control" value="{{$info->name}}">
                                    <input disabled type="text" class="form-control" value="{{$table_number}}">
                                </div>
                            </div>
                        @elseif(strtoupper($info->name) == strtoupper("Puesto de Votación"))
                            <div class="segment">
                                <div class="segment-info" data-id="{{$info->id}}">
                                    <input disabled type="text" class="form-control" value="{{$info->name}}">
                                    <input disabled type="text" class="form-control" value="{{$user->pollingStation ? $user->pollingStation->name : null}}">
                                </div>
                            </div>
                        @elseif(strtoupper($info->name) == strtoupper("Zona"))
                            <?php
                                $zone_num = null;
                                if($user->type->id == 5){
                                    $polling_station_id = $user->polling_station_id;
                                    $zone_db = App\Entities\Zone::whereHas('pollingStations', function($query) use($polling_station_id){
                                        $query->where('polling_station_id', $polling_station_id);
                                    })->first();

                                    $zone = $zone_db->name ?? null;
                                    $zone_num = trim(preg_replace('/Zona /m',"", $zone));
                                } 
                            ?>
                            <div class="segment">
                                <div class="segment-info" data-id="{{$info->id}}">
                                    <input disabled type="text" class="form-control" value="{{$info->name}}">
                                    <input disabled type="text" class="form-control" value="{{$zone_num}}">
                                </div>
                            </div>
                        @else
                            @if($info->value)
                                <div class="segment">
                                    <div class="segment-info" data-id="{{$info->id}}">
                                        <input disabled type="text" class="form-control" value="{{$info->name}}">
                                        <input disabled type="text" class="form-control" value="{{$info->value}}">
                                    </div>
                                </div>
                            @else
                                <?php 
                                    $info_testigo = $user->info_testigo()->where('info_id', $info->id)
                                        ->where('table_number', $table_number)
                                        ->first();
                                ?>
                                <div class="segment">
                                    <div class="segment-info" data-id="{{$info->id}}">
                                        <input disabled type="text" class="form-control" value="{{$info->name}}">
                                        @if($info_testigo)
                                            <input data-can="true" type="text" class="form-control" value="{{$info_testigo->value}}">
                                        @else
                                            <input data-can="true" type="text" class="form-control" value="">
                                        @endif
                                    </div>
                                </div>
                            @endif
                        @endif
                    @endforeach 
                </div>
                <div class="linea">
                </div>
                <div class="forme14-info" id="forme14-votes">
                    <div class="flex justify-content-between">
                        <div class="flex grow">
                            @if(Auth::user()->type->id == 9 || Auth::user()->type->id == 1)
                                <span class="grow">NOMBRE CANDIDATO</span>
                            @else
                                <span class="grow" style="width: 30%">NOMBRE CANDIDATO</span>
                            @endif
                            <span class="grow">VOTOS</span>  
                            @if(Auth::user()->type->id == 9 || Auth::user()->type->id == 1)
                                <span class="grow">ESCRUTINIO</span>      
                            @endif
                        </div>
                    </div>

                    @foreach($formE14->candidates as $key => $candidate)
                        <?php 
                            $vote = \App\Entities\FormE14Vote::where('candidate_id', $candidate->id)
                                ->where('user_id', $user->id)
                                ->where('table_number', $table_number)
                                ->first();
                        ?>
                        <div class="segment">
                            <div class="segment-info" data-id="{{$candidate->id}}">
                                <div class="flex align-items-center" style="width: 88%">
                                    <div class="flex align-items-center" style="position: relative; min-width: 50%">
                                        @if($candidate->file && file_exists(public_path('files_candidates/'.$candidate->file)))
                                            <div class="content-avatar-candidate" style="margin-left: 7px">
                                                <img src="{{asset('files_candidates/'.$candidate->file)}}" alt="" width="100%" height="100%">
                                            </div>
                                        @endif
                                        <input disabled type="text" class="form-control" value="{{$candidate->candidate_number}}">
                                        @if($candidate->id == $formE14->candidate_check)
                                            <img src="{{asset('images/check.png')}}" alt="" width="15px" style="position: absolute; left: -12px" {{$key == 0 ? 'autofocus' : ''}}>
                                        @endif
                                    </div>
                                    <input type="number" class="form-control number-vote text-center" id="input-{{$key}}" onkeydown="cambiarFoco(event, 'input-{{$key + 1}}')" oninput="check(this)" min="0" value="{{$vote ? $vote->votes_number : ''}}" onkeyup="handleNumberVoters(this)" onchange="handleNumberVoters(this)" {{$key == 0 ? 'autofocus' : ''}}>

                                    @if(Auth::user()->type->id == 9 || Auth::user()->type->id == 1)
                                        <?php 
                                            $vote = \App\Entities\FormE14VoteEscrutador::where('candidate_id', $candidate->id)
                                                ->where('user_id', $user->id)
                                                ->where('table_number', $table_number)
                                                ->first();
                                        ?>
                                        <input type="number" class="form-control number-vote-escrutador text-center" oninput="check(this)" min="0" value="{{$vote ? $vote->votes_number : ''}}" id="input-escrutador-{{$key}}" onkeydown="cambiarFoco(event, 'input-escrutador-{{$key + 1}}')" onkeyup="handleNumberVotersEscrutador(this)" onchange="handleNumberVotersEscrutador(this)">
                                    @endif
                                </div>
                            </div>
                        </div>      
                    @endforeach 
                </div>
                
                <div class="forme14-info" style="padding-top: 0">
                    <div class="segment" style="margin-top: 0">
                        <div class="segment-info" data-id="">
                            <div class="flex align-items-center" style="width: 88%">
                                <div class="flex align-items-center" style="position: relative; min-width: 50%">
                                    <input disabled type="text" class="form-control" value="Total Votos" disabled>
                                </div>
                                <?php 
                                    $total_vote = \App\Entities\FormE14TotalVote::where('forme14_id', $formE14->id)
                                        ->where('user_id', $user->id)
                                        ->where('table_number', $table_number)
                                        ->first();
                                ?>
                                <input type="number" class="form-control number-vote text-center" oninput="check(this)" min="0" value="{{$total_vote ? $total_vote->total_votes : ''}}" id="total-votes" disabled>
                                @if(Auth::user()->type->id == 9 || Auth::user()->type->id == 1)
                                    <?php 
                                        $total_vote_escrutador = \App\Entities\FormE14TotalVoteEscrutador::where('forme14_id', $formE14->id)
                                            ->where('user_id', $user->id)
                                            ->where('table_number', $table_number)
                                            ->first();
                                    ?>
                                    <input type="number" class="form-control number-vote-escrutador text-center" oninput="check(this)" min="0" value="{{$total_vote_escrutador ? $total_vote_escrutador->total_votes : ''}}" id="total-votes-escrutador" disabled>
                                @endif
                            </div>
                        </div>
                    </div>  
                </div>

                <?php 
                    $filed_claim =  \App\Entities\FormE14FiledClaim::where('forme14_id', $formE14->id)
                                            ->where('user_id', $user->id)
                                            ->where('table_number', $table_number)
                                            ->first();

                    $options = ["excedio", "error_aritmetico", "error_nombre", "firmas_incompletas"];
                ?>
                <div class="flex">
                    <div class="forme14-info ml-1" style="padding-top: 0; padding-bottom: 8px;{{Auth::user()->type->id == 9 || Auth::user()->type->id == 1 ? "width:50%" : "width:100%"}}" class="flex align-items-center">
                        @if($filed_claim && $filed_claim->filed_claim != "no")
                            <input type="checkbox" id="filed_claim" checked style="transform: scale(1.3)">
                        @else
                            <input type="checkbox" id="filed_claim" style="transform: scale(1.3)">
                        @endif                    
                        <label class="ml-2" for="filed_claim">Hubo reclamación</label>
                        <div style="display: {{$filed_claim && $filed_claim->filed_claim && $filed_claim->filed_claim != "no" ? 'block' : 'none'}}" id="div_filed_claim_we" class="select-up">
                            <select class="js-example-basic-single select-chosen change-select" style="width: 100%" required="required" name="filed_claim_we" id="filed_claim_we" data-placeholder="Escoge">
                                @if($filed_claim && $filed_claim->filed_claim && $filed_claim->filed_claim != "no")
                                    @if($filed_claim->filed_claim == "otro_candidato")
                                        <option value=""></option>
                                        <option value="nosotros">Nosotros</option>
                                        <option value="otro_candidato" selected>Otro Candidato</option>
                                    @elseif($filed_claim->filed_claim == "nosotros" || in_array($filed_claim->filed_claim, $options))
                                        <option value=""></option>
                                        <option value="nosotros" selected>Nosotros</option>
                                        <option value="otro_candidato">Otro Candidato</option>
                                    @else
                                        <option value=""></option>
                                        <option value="nosotros">Nosotros</option>
                                        <option value="otro_candidato">Otro Candidato</option>
                                    @endif
                                @else
                                    <option value=""></option>
                                    <option value="nosotros">Nosotros</option>
                                    <option value="otro_candidato">Otro Candidato</option>
                                @endif
                            </select>
                        </div>
    
                        <div style="display: {{$filed_claim && $filed_claim->filed_claim && $filed_claim->filed_claim != "no" && ($filed_claim->filed_claim == "nosotros" || in_array($filed_claim->filed_claim, $options)) ? 'block' : 'none'}}; margin-top: 10px" id="div_filed_claim_we_other_candidate" class="select-up">
                            <select class="js-example-basic-single select-chosen change-select" style="width: 100%" required="required" name="filed_claim_we_other_candidate" id="filed_claim_we_other_candidate" data-placeholder="Escoge">
                                @if($filed_claim && $filed_claim->filed_claim && $filed_claim->filed_claim != "no" && ($filed_claim->filed_claim == "nosotros" || in_array($filed_claim->filed_claim, $options)))
                                    @if($filed_claim->filed_claim == "excedio")
                                        <option value=""></option>
                                        <option value="excedio" selected>Excedio el numero de votos de la mesa</option>
                                        <option value="error_aritmetico">Error aritmetico</option>
                                        <option value="error_nombre">Error en el nombre del candidato E14</option>
                                        <option value="firmas_incompletas">Firmas incompletas</option>
                                    @elseif($filed_claim->filed_claim == "error_aritmetico")
                                        <option value=""></option>
                                        <option value="excedio">Excedio el numero de votos de la mesa</option>
                                        <option value="error_aritmetico" selected>Error aritmetico</option>
                                        <option value="error_nombre">Error en el nombre del candidato E14</option>
                                        <option value="firmas_incompletas">Firmas incompletas</option>
                                    @elseif($filed_claim->filed_claim == "error_nombre")
                                        <option value=""></option>
                                        <option value="excedio">Excedio el numero de votos de la mesa</option>
                                        <option value="error_aritmetico">Error aritmetico</option>
                                        <option value="error_nombre" selected>Error en el nombre del candidato E14</option>
                                        <option value="firmas_incompletas">Firmas incompletas</option>
                                    @elseif($filed_claim->filed_claim == "firmas_incompletas")
                                        <option value=""></option>
                                        <option value="excedio">Excedio el numero de votos de la mesa</option>
                                        <option value="error_aritmetico">Error aritmetico</option>
                                        <option value="error_nombre">Error en el nombre del candidato E14</option>
                                        <option value="firmas_incompletas" selected>Firmas incompletas</option>
                                    @else
                                        <option value=""></option>
                                        <option value="excedio">Excedio el numero de votos de la mesa</option>
                                        <option value="error_aritmetico">Error aritmetico</option>
                                        <option value="error_nombre">Error en el nombre del candidato E14</option>
                                        <option value="firmas_incompletas">Firmas incompletas</option>
                                    @endif
                                @else
                                    <option value=""></option>
                                    <option value="excedio">Excedio el numero de votos de la mesa</option>
                                    <option value="error_aritmetico">Error aritmetico</option>
                                    <option value="error_nombre">Error en el nombre del candidato E14</option>
                                    <option value="firmas_incompletas">Firmas incompletas</option>
                                @endif 
                            </select>
                        </div>
                    </div>
                    @if(Auth::user()->type->id == 9 || Auth::user()->type->id == 1)
                        <?php 
                        $filed_claim_encrutador =  \App\Entities\FormE14FiledClaim::where('forme14_id', $formE14->id)
                                                ->where('user_id', $user->id)
                                                ->where('table_number', $table_number)
                                                ->first();

                        $options = ["excedio", "error_aritmetico", "error_nombre", "firmas_incompletas"];
                    ?>

                        <div class="forme14-info" style="padding-top: 0; padding-bottom: 8px;width:50%" class="flex align-items-center">
                            @if($filed_claim_encrutador && $filed_claim_encrutador->filed_claim_encrutador && $filed_claim_encrutador->filed_claim_encrutador != "no")
                                <input type="checkbox" id="filed_claim_encrutador" checked style="transform: scale(1.3)">
                            @else
                                <input type="checkbox" id="filed_claim_encrutador" style="transform: scale(1.3)">
                            @endif                    
                            <div class="tooltip2">
                                <label class="ml-2" for="filed_claim_encrutador">Hubo reclamación</label>
                                <span class="tooltiptext">Hubo reclamación escrutador</span>
                            </div>
                            <div style="display: {{$filed_claim_encrutador && $filed_claim_encrutador->filed_claim_encrutador && $filed_claim_encrutador->filed_claim_encrutador != "no" ? 'block' : 'none'}}" id="div_filed_claim_encrutador_we" class="select-up">
                                <select class="js-example-basic-single select-chosen change-select" style="width: 100%" required="required" name="filed_claim_encrutador_we" id="filed_claim_encrutador_we" data-placeholder="Escoge">
                                    @if($filed_claim_encrutador && $filed_claim_encrutador->filed_claim_encrutador && $filed_claim_encrutador->filed_claim_encrutador != "no")
                                        @if($filed_claim_encrutador->filed_claim_encrutador == "otro_candidato")
                                            <option value=""></option>
                                            <option value="nosotros">Nosotros</option>
                                            <option value="otro_candidato" selected>Otro Candidato</option>
                                        @elseif($filed_claim_encrutador->filed_claim_encrutador == "nosotros" || in_array($filed_claim_encrutador->filed_claim_encrutador, $options))
                                            <option value=""></option>
                                            <option value="nosotros" selected>Nosotros</option>
                                            <option value="otro_candidato">Otro Candidato</option>
                                        @else
                                            <option value=""></option>
                                            <option value="nosotros">Nosotros</option>
                                            <option value="otro_candidato">Otro Candidato</option>
                                        @endif
                                    @else
                                        <option value=""></option>
                                        <option value="nosotros">Nosotros</option>
                                        <option value="otro_candidato">Otro Candidato</option>
                                    @endif
                                </select>
                            </div>
        
                            <div style="display: {{$filed_claim_encrutador && $filed_claim_encrutador->filed_claim_encrutador && $filed_claim_encrutador->filed_claim_encrutador != "no" && ($filed_claim_encrutador->filed_claim_encrutador == "nosotros" || in_array($filed_claim_encrutador->filed_claim_encrutador, $options)) ? 'block' : 'none'}}; margin-top: 10px" id="div_filed_claim_encrutador_we_other_candidate" class="select-up">
                                <select class="js-example-basic-single select-chosen change-select" style="width: 100%" required="required" name="filed_claim_encrutador_we_other_candidate" id="filed_claim_encrutador_we_other_candidate" data-placeholder="Escoge">
                                    @if($filed_claim_encrutador && $filed_claim_encrutador->filed_claim_encrutador && $filed_claim_encrutador->filed_claim_encrutador != "no" && ($filed_claim_encrutador->filed_claim_encrutador == "nosotros" || in_array($filed_claim_encrutador->filed_claim_encrutador, $options)))
                                        @if($filed_claim_encrutador->filed_claim_encrutador == "excedio")
                                            <option value=""></option>
                                            <option value="excedio" selected>Excedio el numero de votos de la mesa</option>
                                            <option value="error_aritmetico">Error aritmetico</option>
                                            <option value="error_nombre">Error en el nombre del candidato E14</option>
                                            <option value="firmas_incompletas">Firmas incompletas</option>
                                        @elseif($filed_claim_encrutador->filed_claim_encrutador == "error_aritmetico")
                                            <option value=""></option>
                                            <option value="excedio">Excedio el numero de votos de la mesa</option>
                                            <option value="error_aritmetico" selected>Error aritmetico</option>
                                            <option value="error_nombre">Error en el nombre del candidato E14</option>
                                            <option value="firmas_incompletas">Firmas incompletas</option>
                                        @elseif($filed_claim_encrutador->filed_claim_encrutador == "error_nombre")
                                            <option value=""></option>
                                            <option value="excedio">Excedio el numero de votos de la mesa</option>
                                            <option value="error_aritmetico">Error aritmetico</option>
                                            <option value="error_nombre" selected>Error en el nombre del candidato E14</option>
                                            <option value="firmas_incompletas">Firmas incompletas</option>
                                        @elseif($filed_claim_encrutador->filed_claim_encrutador == "firmas_incompletas")
                                            <option value=""></option>
                                            <option value="excedio">Excedio el numero de votos de la mesa</option>
                                            <option value="error_aritmetico">Error aritmetico</option>
                                            <option value="error_nombre">Error en el nombre del candidato E14</option>
                                            <option value="firmas_incompletas" selected>Firmas incompletas</option>
                                        @else
                                            <option value=""></option>
                                            <option value="excedio">Excedio el numero de votos de la mesa</option>
                                            <option value="error_aritmetico">Error aritmetico</option>
                                            <option value="error_nombre">Error en el nombre del candidato E14</option>
                                            <option value="firmas_incompletas">Firmas incompletas</option>
                                        @endif
                                    @else
                                        <option value=""></option>
                                        <option value="excedio">Excedio el numero de votos de la mesa</option>
                                        <option value="error_aritmetico">Error aritmetico</option>
                                        <option value="error_nombre">Error en el nombre del candidato E14</option>
                                        <option value="firmas_incompletas">Firmas incompletas</option>
                                    @endif 
                                </select>
                            </div>
                        </div>
                    @endif
                </div>

                <div class="forme14-save">
                    <?php 
                        $file = \App\Entities\FormE14File::where('forme14_id', $formE14->id)
                            ->where('user_id', $user->id)
                            ->where('table_number', $table_number)
                            ->first();
                    ?>
                    <div style="padding-right: 10px; width: 50%">
                        <label for="forme14-save-upload" class="label-save m-0 flex align-items-center justify-content-center">
                            <div class="flex align-items-center" id="uploadE141">
                                <img src="{{asset('images/camera.png')}}" alt="" width="25px" height="25px">
                                <span style="line-height: 1px; margin-left: 5px">Subir E14</span>
                            </div>
                            <progress value="0" max="100" id="uploadProgress" style="display:none"></progress>
                            <div id="dot-spinner" style="flex-direction:column; align-items:center;justify-content:center;display:none">
                                <div class="loader-dots"></div>
                                <span>Subiendo imagen....</span>
                            </div>
                        </label>
                        <input type="file" id="forme14-save-upload" name="forme14-save-upload" onchange="pruebaUploadImageTemp(true)" accept="application/pdf,image/png,image/jpg,image/jpeg">
                        <span id="name-forme14-save-upload">
                            {{$file ? $file->file : ''}}
                        </span>
                        <span id="image-uploaded" style="color: green; display:none">(imagen cargada)</span>

                        <span id="uploading_file"></span>
                    </div>
                    <div style="padding-right: 10px; width: 50%">
                        <label for="forme14-save-upload-secondary" class="label-save m-0 flex align-items-center justify-content-center">
                            <div class="flex align-items-center" id="uploadE142">
                                <img src="{{asset('images/camera.png')}}" alt="" width="25px" height="25px">
                                <span style="line-height: 1px; margin-left: 5px">Subir E14</span>
                            </div>
                            <progress value="0" max="100" id="uploadProgress2" style="display:none"></progress>
                            <div id="dot-spinner2" style="flex-direction:column; align-items:center;justify-content:center;display:none">
                                <div class="loader-dots"></div>
                                <span>Subiendo imagen....</span>
                            </div>
                        </label>
                        <input type="file" id="forme14-save-upload-secondary" name="forme14-save-upload-secondary" onchange="pruebaUploadImageTemp(false)" accept="application/pdf,image/png,image/jpg,image/jpeg">
                        <span id="name-forme14-save-upload-secondary">
                            {{$file ? $file->file_secondary : ''}}
                        </span>
                        <img src="" id="output">

                        <span id="image-uploaded2" style="color: green; display:none">(imagen cargada)</span>
                        <span id="uploading_file_secondary"></span>
                    </div>  

                    
                </div>

                <div class="forme14-save">
                    <button id="form-update-form" class="label-save flex justify-content-center align-items-center" style="width: 100%">
                        <span>GUARDAR</span>
                    </button> 
                </div>
            </div>
        @endif
    @else
        <div class="flex justify-content-between" style="max-width: 500px; margin: 0 auto">
            <input class="form-control text-center text-primary" style="border: 0; height: 20px;font-size: 19px" type="text" placeholder="Gobernación" id="name-form" value="{{$formE14 ? strtoupper($formE14->name) : null}}">
            <div style="display: flex; justify-content: flex-end; align-items: center; padding: 5px">
                <i class="fa fa-edit" style="cursor: pointer" onclick="newFormE14Info()"></i>
            </div>
        </div>  
        @if($formE14)
            <div class="forme14">
                <div class="forme14-info" id="forme14-info">
                    @foreach($formE14->info as $info)   
                        @if(strtoupper($info->name) == strtoupper("Mesa") || strtoupper($info->name) == strtoupper("Puesto de Votación"))
                            <div class="segment">
                                <div class="segment-info" data-id="{{$info->id}}">
                                    <input type="text" class="form-control" value="{{$info->name}}" disabled>
                                    <input type="text" class="form-control" value="{{$info->value}}" disabled>
                                </div>
                            </div>
                        @else
                            <div class="segment">
                                <div class="segment-info" data-id="{{$info->id}}">
                                    <input type="text" class="form-control" value="{{$info->name}}">
                                    <input type="text" class="form-control" value="{{$info->value}}">
                                </div>
                                <span style="color: red" onclick="removeSegment(event)">X</span>
                            </div>
                        @endif
                    @endforeach 
                </div>
                <div class="linea">
                </div>
                <div class="forme14-info" id="forme14-votes">
                    <div class="flex justify-content-between">
                        <div class="flex grow">
                            <span class="grow">NOMBRE CANDIDATO</span>
                            <span class="grow">VOTOS</span>    
                        </div>
                        <div style="display: flex; justify-content: flex-end; align-items: center; padding: 5px">
                            <i class="fa fa-edit" style="cursor: pointer" onclick="newColumnVoteUpdate()"></i>
                        </div>
                    </div>

                    @foreach($formE14->candidates as $key => $candidate)
                        <div class="segment">
                            <div class="segment-info" data-id="{{$candidate->id}}">
                                <div class="flex align-items-center" style="min-width: 50%">
                                    <div class="{{$candidate->file && file_exists(public_path('files_candidates/'.$candidate->file)) ? 'content-avatar-candidate' : 'content-avatar-candidate-no-photo'}}">
                                        @if($candidate->file && file_exists(public_path('files_candidates/'.$candidate->file)))
                                            <img src="{{asset('files_candidates/'.$candidate->file)}}" alt="" width="100%" height="100%">
                                        @endif
                                        <label for="file_candidate_{{$candidate->id}}">
                                            <i class="fa fa-pencil display-none cursor-"></i>
                                        </label>
                                        <input type="file" style="display: none" name="file_candidate_{{$candidate->id}}" id="file_candidate_{{$candidate->id}}" onchange="uploadFileCandidate(this)">
                                    </div>
                                    <input type="text" class="form-control" value="{{$candidate->candidate_number}}" placeholder="">
                                    @if($candidate->id == $formE14->candidate_check)
                                        <input checked class="check-hijos m-0" id="check-{{$candidate->id}}" type="checkbox" style="float: right" onchange="checkCandidate('{{$candidate->id}}')" {{$key == 0 ? 'autofocus' : ''}}>
                                    @else
                                        <input class="check-hijos m-0" id="check-{{$candidate->id}}" type="checkbox" style="float: right" onchange="checkCandidate('{{$candidate->id}}')" {{$key == 0 ? 'autofocus' : ''}}>
                                    @endif
                                </div>
                            </div>
                            <span style="color: red" onclick="removeColumnVote(event)">X</span>
                        </div>
                    @endforeach
                </div>
                <div class="forme14-info" style="padding-top: 0">
                    <div class="segment" style="margin-top: 0">
                        <div class="segment-info" data-id="">
                            <div class="flex align-items-center" style="min-width: 50%">
                                <input type="text" class="form-control" value="Total Votos" disabled>
                            </div>
                            <input type="text" class="form-control text-center" disabled id="total-votes">
                        </div>
                    </div> 
                </div>

                <div class="forme14-save">
                    <div class="label-save" onclick="updateFormE14()">
                        ACTUALIZAR
                    </div>   
                </div>
            </div>
        @else
            <div class="forme14">
                <div class="forme14-info" id="forme14-info">
                    <div class="segment">
                        <div class="segment-info" data-id="">
                            <input type="text" class="form-control" value="Ciudad / Municipio">
                            <input type="text" class="form-control" value="Villavicencio">
                        </div>
                        <span style="color: red" onclick="removeSegment(event)">X</span>
                    </div>
                    <div class="segment">
                        <div class="segment-info" data-id="">
                            <input type="text" class="form-control" value="Zona">
                            <input type="text" class="form-control">
                        </div>
                        <span style="color: red" onclick="removeSegment(event)">X</span>
                    </div>
                    <div class="segment">
                        <div class="segment-info" data-id="">
                            <input type="text" class="form-control" value="Puesto de Votación" disabled="">
                            <input type="text" class="form-control" disabled="">
                        </div>
                        <!--<span style="color: red" onclick="removeSegment(event)">X</span>-->
                    </div>
                    <div class="segment">
                        <div class="segment-info" data-id="">
                            <input type="text" class="form-control" value="Mesa" disabled="">
                            <input type="text" class="form-control" disabled="">
                        </div>
                        <!--<span style="color: red" onclick="removeSegment(event)">X</span>-->
                    </div>   
                </div>
                <div class="linea">
                </div>
                <div class="forme14-info" id="forme14-votes">
                    <div class="flex justify-content-between">
                        <div class="flex grow">
                            <span class="grow">NOMBRE CANDIDATO</span>
                            <span class="grow">VOTOS</span>    
                        </div>
                        <div style="display: flex; justify-content: flex-end; align-items: center; padding: 5px">
                            <i class="fa fa-edit" style="cursor: pointer" onclick="newColumnVote()"></i>
                        </div>
                    </div>
                    
                    <div class="segment">
                        <div class="segment-info" data-id="">
                            <div class="flex align-items-center" style="min-width: 50%">
                                <div class="content-avatar-candidate-no-photo">
                                    <label for="file_candidate_1">
                                        <i class="fa fa-pencil display-none cursor-"></i>
                                    </label>
                                    <input class="file_candidate" type="file" style="display: none" name="file_candidate" id="file_candidate_1" onchange="uploadFileCandidate(this)">
                                </div>
                                <input type="text" class="form-control" value="" placeholder="Candidato 1">
                                <input class="check-hijos-create m-0" id="" type="checkbox" style="float: right" onchange="checkCandidateCreate(this)">
                            </div>
                        </div>
                        <span style="color: red" onclick="removeColumnVote(event)">X</span>
                    </div>
                    <div class="segment">
                        <div class="segment-info" data-id="">
                            <div class="flex align-items-center" style="min-width: 50%">
                                <div class="content-avatar-candidate-no-photo">
                                    <label for="file_candidate_2">
                                        <i class="fa fa-pencil display-none cursor-"></i>
                                    </label>
                                    <input class="file_candidate" type="file" style="display: none" name="file_candidate" id="file_candidate_2" onchange="uploadFileCandidate(this)">
                                </div>
                                <input type="text" class="form-control" value="" placeholder="Candidato 2">
                                <input class="check-hijos-create m-0" id="" type="checkbox" style="float: right" onchange="checkCandidateCreate(this)">
                            </div>
                        </div>
                        <span style="color: red" onclick="removeColumnVote(event)">X</span>
                    </div>
                    <div class="segment">
                        <div class="segment-info" data-id="">
                            <div class="flex align-items-center" style="min-width: 50%">
                                <div class="content-avatar-candidate-no-photo">
                                    <label for="file_candidate_3">
                                        <i class="fa fa-pencil display-none cursor-"></i>
                                    </label>
                                    <input class="file_candidate" type="file" style="display: none" name="file_candidate" id="file_candidate_3" onchange="uploadFileCandidate(this)">
                                </div>
                                <input type="text" class="form-control" value="" placeholder="Candidato 3">
                                <input class="check-hijos-create m-0" id="" type="checkbox" style="float: right" onchange="checkCandidateCreate(this)">
                            </div>
                        </div>
                        <span style="color: red" onclick="removeColumnVote(event)">X</span>
                    </div>  
                </div>
            <div class="forme14-info" style="padding-top: 0">
                    <div class="segment" style="margin-top: 0">
                        <div class="segment-info" data-id="">
                            <div class="flex align-items-center" style="min-width: 50%">
                                <input type="text" class="form-control" value="Total Votos" disabled>
                            </div>
                            <input type="text" class="form-control text-center" disabled id="total-votes">
                        </div>
                    </div> 
                </div>

                <div class="forme14-save">
                    <div class="label-save flex justify-content-center align-items-center" onclick="saveFormE14()">
                        <span>GUARDAR</span>
                    </div>   
                </div>
            </div>
        @endif
    @endif
    
</div>

@section('js_aditional2')
    <script>
    /*    $(document).ready(function(){
    setInterval(() => {

        var regex = /^[/]+|[./]+$/g;
        let endpoint = `/witnesses/forme14/validate_state_file/${forme14_id}/${ }${testigo_id && "/" + testigo_id}`
        console.log("antes:", endpoint)
        endpoint = endpoint.replace(regex, '');

console.log("despuess:", endpoint)

        $.get("/" + endpoint).then(response => {
            let data = response.data
            if(response.success){
                if(data){
                    let mensaje = null;
                    let mensaje_secondary = null;
                    if(!updated_upload_form){
                        if(data.state_file == "EN ESPERA"){
                            mensaje = "Espernado para subir...";
                        }else if(data.state_file == "SUBIENDO"){
                            mensaje = "Subiendo imagen...";
                        }else if(data.state_file == "SUBIDA"){
                            mensaje = data.state_file
                        }else if(data.state_file == "ERROR"){
                            mensaje = "No se pudo subir la imagen"
                        }
                    }
                    
                    if(!updated_upload_form_secondary){
                        if(data.state_file_secondary == "EN ESPERA"){
                            mensaje_secondary = "Esperando para subir...";
                        }else if(data.state_file_secondary == "SUBIENDO"){
                            mensaje_secondary = "Subiendo imagen...";
                        }else if(data.state_file_secondary == "SUBIDA"){
                            mensaje_secondary = data.state_file_secondary
                        }else if(data.state_file_secondary == "ERROR"){
                            mensaje_secondary = "No se pudo subir la imagen"
                        }
                    }
                    if(!updated_upload_form){
                        if(mensaje == "SUBIDA"){
                            $("#uploading_file").text("")
                            $("#name-forme14-save-upload").text(data.file)
                        }else{
                            $("#uploading_file").text(mensaje)     
                        }
                    }

                    if(!updated_upload_form_secondary){
                        if(mensaje_secondary == "SUBIDA"){
                            $("#uploading_file_secondary").text("")
                            $("#name-forme14-save-upload-secondary").text(data.file_secondary)
                        }else{
                            $("#uploading_file_secondary").text(mensaje_secondary)     
                        }
                    } 
                }
            }
        })
    }, 3000);
})*/

    </script>
@endsection