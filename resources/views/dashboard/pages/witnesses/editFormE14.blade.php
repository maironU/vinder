@extends('dashboard.pages.layout')
@section('title_page', 'Editar Formulario E14')
@section('breadcrumbs') {!! Breadcrumbs::render('witnesses-createform') !!} @endsection

@section('content_body_page')
<script>
    let rol = "{{$user->type->id}}"
    let rol_auth = "{{Auth::user()->type->id}}"
    let testigo_id = "{{Auth::user()->type->id == 8 ? $user->id : null}}"
    let forme14_id = "{{$formE14->id}}"
</script>
<div class="row" style="margin-bottom: 30px">
    @if($user->type->id == 5)
        <div style="width: 400px; margin: 0 auto; margin-bottom: 30px">
            <label for="">Nombre del formulario</label>
            <input disabled class="form-control" type="text" placeholder="Nombre" id="name-form" value="{{$formE14->name}}">
        </div>

        <?php 
            $filed_claim =  \App\Entities\FormE14FiledClaim::where('forme14_id', $formE14->id)
                                    ->where('user_id', $user->id)
                                    ->first();
        ?>

        <div style="width: 400px; margin: 0 auto; margin-bottom: 30px">
            <label for="filed_claim">Presentó reclamo</label>
            @if($filed_claim && $filed_claim->filed_claim)
                <input type="checkbox" id="filed_claim" checked>
            @else
                <input type="checkbox" id="filed_claim">
            @endif
        </div>
    @else
        <div style="width: 400px; margin: 0 auto; margin-bottom: 30px">
            <label for="">Ingresa un nombre del formulario</label>
            <input class="form-control" type="text" placeholder="Nombre" id="name-form" value="{{$formE14->name}}">
        </div>
    @endif
    @if($user->type->id == 5)
        <div class="forme14">
            <div class="forme14-info" id="forme14-info">
                @foreach($formE14->info as $info)
                    @if(strtoupper($info->name) == strtoupper("Mesa"))
                        <div class="segment">
                            <div class="segment-info" data-id="{{$info->id}}">
                                <input disabled type="text" class="form-control" value="{{$info->name}}">
                                <input disabled type="text" class="form-control" value="{{$user->num_table}}">
                            </div>
                        </div>
                    @elseif(strtoupper($info->name) == strtoupper("Puesto"))
                        <div class="segment">
                            <div class="segment-info" data-id="{{$info->id}}">
                                <input disabled type="text" class="form-control" value="{{$info->name}}">
                                <input disabled type="text" class="form-control" value="{{$user->pollingStation ? $user->pollingStation->name : null}}">
                            </div>
                        </div>
                    @else
                        <div class="segment">
                            <div class="segment-info" data-id="{{$info->id}}">
                                <input disabled type="text" class="form-control" value="{{$info->name}}">
                                <input disabled type="text" class="form-control" value="{{$info->value}}">
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
            <div class="forme14-votes">
                <div class="forme14-votes-logo" id="forme14-votes-logo" style="position: relative">
                    <div id="preview-logo-part">
                        @if($formE14->match_logo)
                            <img src="{{asset('logos_part/'.$formE14->match_logo)}}" width="70px" id="image-logo-part" style="margin: 5px 32px"/>
                        @endif
                    </div>
                </div>
                <div class="forme14-votes-cand">
                    <table border="1">
                        <thead>
                            <tr>
                                <th>Número del candidato</th>
                                <th>Votos</th>
                            </tr>
                        </thead>
                        <tbody class="body-vote" id="body-vote">
                            @foreach($formE14->candidates as $candidate)
                                <?php 
                                    $vote = \App\Entities\FormE14Vote::where('candidate_id', $candidate->id)
                                        ->where('user_id', $user->id)
                                        ->first();
                                ?>
                                <tr data-id="{{$candidate->id}}">
                                    <td>
                                        <input disabled style="background: #C7E8CA!important; width: 130px" type="text" class="form-control" value="{{$candidate->candidate_number}}">
                                        @if($candidate->id == $formE14->candidate_check)
                                            <img src="{{asset('images/check.png')}}" alt="" width="15px" style="float: right;margin-top: 6px">
                                        @endif
                                    </td>
                                    <td>
                                        <input type="number" class="form-control number-vote" value="{{$vote ? $vote->votes_number : ''}}" onkeyup="handleNumberVoters(this)" onchange="handleNumberVoters(this)">
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tr>
                            <td>Total votos</td>
                            <td>
                                <?php 
                                    $total_vote = \App\Entities\FormE14TotalVote::where('forme14_id', $formE14->id)
                                        ->where('user_id', $user->id)
                                        ->first();
                                ?>
                                <input type="number" disabled style="background: #C7E8CA !important" min="1" class="form-control" id="total-votes" value="{{$total_vote ? $total_vote->total_votes : ''}}">
                            </td>
                        </tr>
                    </table>
                </div>
            </div>

            <div class="forme14-save">
                <?php 
                    $file = \App\Entities\FormE14File::where('forme14_id', $formE14->id)
                        ->where('user_id', $user->id)
                        ->first();
                ?>
                <div style="padding-right: 10px; width: 50%">
                    <label for="forme14-save-upload" class="label-save">
                        Subir formulario E14
                    </label>
                    <input type="file" id="forme14-save-upload" name="forme14-save-upload" onchange="uploadForm()" accept="application/pdf,image/png,image/jpg,image/jpeg">
                    <span id="name-forme14-save-upload">
                        {{$file ? $file->file : ''}}
                    </span>
                </div>
                <div style="width: 50%">
                    <div class="label-save" onclick="updateFormE14()">
                        Guardar
                    </div>
                </div>
            </div>
        </div>
    @else
        <div class="forme14">
            <div class="forme14-info" id="forme14-info">
                <div style="display: flex; justify-content: flex-end; align-items: center; padding: 5px">
                    <i class="fa fa-edit" style="cursor: pointer" onclick="newFormE14Info()"></i>
                </div>
                @foreach($formE14->info as $info)
                    @if(strtoupper($info->name) == strtoupper("Mesa") || strtoupper($info->name) == strtoupper("Puesto"))
                        <div class="segment">
                            <div class="segment-info" data-id="{{$info->id}}">
                                <input type="text" class="form-control" value="{{$info->name}}" disabled>
                                <input type="text" class="form-control" value="{{$info->value}}" disabled>
                            </div>
                        </div>
                    @else
                        <div class="segment">
                            <div class="segment-info" data-id="{{$info->id}}">
                                <input type="text" class="form-control" value="{{$info->name}}">
                                <input type="text" class="form-control" value="{{$info->value}}">
                            </div>
                            <span style="color: red" onclick="removeSegment(event)">X</span>
                        </div>
                    @endif
                @endforeach
            </div>
            <div class="forme14-votes">
                <div class="forme14-votes-logo" id="forme14-votes-logo" style="position: relative">
                    <div id="preview-logo-part">
                        @if($formE14->match_logo)
                            <img src="{{asset('logos_part/'.$formE14->match_logo)}}" width="70px" id="image-logo-part" style="margin: 5px 32px"/>
                        @endif
                    </div>
                    <!--<span class="logo-part" id="logo-part">Logo Partido</span>-->
                    <div class="forme14-votes-upload" style="margin-left: 30px">
                        <label for="upload-logo-part">
                            <img class="image-logo-part" src="{{asset('images/upload.png')}}" width="20px" alt="" style="cursor: pointer">
                        </label>
                        <input type="file" id="upload-logo-part" name="upload-logo-part" accept="image/jpeg, image/jpg, image/png" onchange="uploadLogoPart()">
                    </div>
                    <span class="delete-logo-part" style="color: red; position: absolute; right: 0; margin-right: 5px; top: 5px; cursor: pointer" onclick="removeLogoPart()">X</span>
                </div>
                <div class="forme14-votes-cand">
                    <div style="display: flex; justify-content: flex-end; align-items: center; padding: 5px; position: absolute; right: 0">
                        <i class="fa fa-edit" style="cursor: pointer" onclick="newTr()"></i>
                    </div>
                    <table border="1">
                        <thead>
                            <tr>
                                <th>Número del candidato</th>
                                <th>Votos</th>
                            </tr>
                        </thead>
                        <tbody class="body-vote" id="body-vote">
                            @foreach($formE14->candidates as $candidate)
                                <tr data-id="{{$candidate->id}}">
                                    <td>
                                        <input type="text" class="form-control" value="{{$candidate->candidate_number}}" style="background: #C7E8CA!important; width: 130px">
                                        @if($candidate->id == $formE14->candidate_check)
                                            <input checked="" class="check-hijos" id="check-{{$candidate->id}}" type="checkbox" style="float: right; margin-top: 7px" onchange="checkCandidate('{{$candidate->id}}')">
                                        @else
                                            <input class="check-hijos" id="check-{{$candidate->id}}" type="checkbox" style="float: right; margin-top: 7px" onchange="checkCandidate('{{$candidate->id}}')">
                                        @endif
                                    </td>
                                    <td>
                                        <span class="delete-td" onclick="removeTr(event)">X</span>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tr>
                            <td>Total votos</td>
                            <td style="height: 26px">
                            </td>
                        </tr>
                    </table>
                </div>
            </div>

            <div class="forme14-save" style="justify-content: center">
                <div style="width: 50%">
                    <div class="label-save" onclick="updateFormE14()">
                        Actualizar
                    </div>
                </div>
            </div>
        </div>
    @endif
</div>

@endsection