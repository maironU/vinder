@extends('dashboard.pages.layout')
@section('title_page', 'Testigos electorales')
@section('breadcrumbs') {!! Breadcrumbs::render('witnesses') !!} @endsection

@section('content_body_page')

<div class="row" style="margin: 0">
<!--
        @foreach($mainModules as $module)
        <p>{{ $module }}</p>
        @endforeach

        @if(isset($campaing) && sizeof($campaing) > 0)
        <div>se enviaron datos</div>
        @endif
<hr>
        {{ Auth::user()->type_id }}
<hr>
-->
    <!-- Testigos electorales -->
    @if(Auth::user()->type->id == 1 || Auth::user()->type->id == 2)
    <a href="{{ route('witnesses.assignwitnesses') }}">
        <div class="col-sm-4" style="padding: 0">
            <img src="{{asset('images/placeholders/icons/logistic-back-color.png')}}" width="100%" alt="" style="padding: 10px">
            <img class="image-back" src="{{asset('images/placeholders/icons/bdd-back.png')}}" width="100%" alt="" style="position: absolute; left: 0; border-radius: 30px; height: 100%; padding: 10px;object-fit: cover">
            <div class="card-module-text">
                <i class="fa fa-database icon-text"></i>
                <span>Testigos electorales</span>
            </div>
        </div>
    </a>
       <!-- Crear formularios -->
    <a href="{{ route('witnesses.createform') }}">
        <div class="col-sm-4" style="padding: 0">
            <img src="{{asset('images/placeholders/icons/agend-back-color.png')}}" width="100%" alt="" style="padding: 10px">
            <img class="image-back" src="{{asset('images/placeholders/icons/bdd-back.png')}}" width="100%" alt="" style="position: absolute; left: 0; border-radius: 30px; height: 100%; padding: 10px;object-fit: cover">
            <div class="card-module-text">
                <i class="fa fa-database icon-text"></i>
                <span>Formularios</span>
            </div>
        </div>
    </a>
    @endif

    <!-- Resultados día D -->
    @if(Auth::user()->type->id == 1 || Auth::user()->type->id == 2 || Auth::user()->type->id == 7)
    <!-- Crear formularios -->
    <a href="{{ route('witnesses.resultsaccordingwitnesses') }}">
        <div class="col-sm-4" style="padding: 0">
            <img src="{{asset('images/placeholders/icons/bdd-back-color.png')}}" width="100%" alt="" style="padding: 10px">
            <img class="image-back" src="{{asset('images/placeholders/icons/bdd-back.png')}}" width="100%" alt="" style="position: absolute; left: 0; border-radius: 30px; height: 100%; padding: 10px;object-fit: cover">
            <div class="card-module-text">
                <i class="fa fa-database icon-text"></i>
                <span>Resultados día D</span>
            </div>
        </div>
    </a>
    @endif

    @if(Auth::user()->type->id == 1 || Auth::user()->type->id == 2)
        <a href="{{ route('witnesses.incidents.index') }}">
            <div class="col-sm-4" style="padding: 0">
                <img src="{{asset('images/placeholders/icons/advertising-back-color.png')}}" width="100%" alt="" style="padding: 10px">
                <img class="image-back" src="{{asset('images/placeholders/icons/bdd-back.png')}}" width="100%" alt="" style="position: absolute; left: 0; border-radius: 30px; height: 100%; padding: 10px;object-fit: cover">
                <div class="card-module-text">
                    <i class="fa fa-database icon-text"></i>
                    <span>Incidencias</span>
                </div>
            </div>
        </a>

        <a href="{{ route('coordinator.index') }}">
            <div class="col-sm-4" style="padding: 0">
                <img src="{{asset('images/placeholders/icons/statistic-back-color.png')}}" width="100%" alt="" style="padding: 10px">
                <img class="image-back" src="{{asset('images/placeholders/icons/bdd-back.png')}}" width="100%" alt="" style="position: absolute; left: 0; border-radius: 30px; height: 100%; padding: 10px;object-fit: cover">
                <div class="card-module-text">
                    <i class="fa fa-database icon-text"></i>
                    <span>Coordinador de Puestos</span>
                </div>
            </div>
        </a>

        <a href="{{ route('zone.index') }}">
            <div class="col-sm-4" style="padding: 0">
                <img src="{{asset('images/placeholders/icons/report-back-color.png')}}" width="100%" alt="" style="padding: 10px">
                <img class="image-back" src="{{asset('images/placeholders/icons/bdd-back.png')}}" width="100%" alt="" style="position: absolute; left: 0; border-radius: 30px; height: 100%; padding: 10px;object-fit: cover">
                <div class="card-module-text">
                    <i class="fa fa-database icon-text"></i>
                    <span>Zonas</span>
                </div>
            </div>
        </a>

        <a href="{{ route('coordinator_zone.index') }}">
            <div class="col-sm-4" style="padding: 0">
                <img src="{{asset('images/placeholders/icons/1.png')}}" width="100%" alt="" style="padding: 10px">
                <img class="image-back" src="{{asset('images/placeholders/icons/bdd-back.png')}}" width="100%" alt="" style="position: absolute; left: 0; border-radius: 30px; height: 100%; padding: 10px;object-fit: cover">
                <div class="card-module-text">
                    <i class="fa fa-database icon-text"></i>
                    <span>Coordinador de Zonas</span>
                </div>
            </div>
        </a>

        <a href="{{ route('witnesses.polling_stations.index') }}">
            <div class="col-sm-4" style="padding: 0">
                <img src="{{asset('images/placeholders/icons/2.png')}}" width="100%" alt="" style="padding: 10px">
                <img class="image-back" src="{{asset('images/placeholders/icons/bdd-back.png')}}" width="100%" alt="" style="position: absolute; left: 0; border-radius: 30px; height: 100%; padding: 10px;object-fit: cover">
                <div class="card-module-text">
                    <i class="fa fa-database icon-text"></i>
                    <span>Puestos de Votación</span>
                </div>
            </div>
        </a>
    @endif

    @if(Auth::user()->type->id == 5 || Auth::user()->type->id == 8)
        <a href="{{ route('witnesses.createform') }}">
            <div class="col-sm-4" style="padding: 0">
                <img src="{{asset('images/placeholders/icons/logistic-back-color.png')}}" width="100%" alt="" style="padding: 10px">
                <img class="image-back" src="{{asset('images/placeholders/icons/bdd-back.png')}}" width="100%" alt="" style="position: absolute; left: 0; border-radius: 30px; height: 100%; padding: 10px;object-fit: cover">
                <div class="card-module-text">
                    <i class="fa fa-database icon-text"></i>
                    <span>Formularios</span>
                </div>
            </div>
        </a>
    @endif

</div>

@endsection