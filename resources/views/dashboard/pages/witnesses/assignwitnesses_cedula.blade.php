@extends('dashboard.pages.layout')
@section('title_page', 'Asignar testigos electorales')
@section('breadcrumbs') {!! Breadcrumbs::render('witnesses-assignwitness') !!} @endsection

@section('content_body_page')
<style>
    .content-form {
        display: flex;
        align-items: flex-end;
        width: 100%;
    }

    .content-form > .form-group{
        margin-bottom: 0;
        margin-right: 10px;
        width: 270px;
    }

    @media (max-width: 560px){
        .content-form {
            flex-direction: column;
            align-items: center;
            justify-content: center;
        }

        .content-form > .form-group, input{
            width: 80%;
            margin: 0;
        }
    }
</style>
<div class="row" id="title_page" style="margin-bottom: 10px;">
    	<div style="display: flex; justify-content: space-between">
            <div class="col-md-12 flex">
                <div>
                    <a href="{{ route('witnesses.create')}}" class="btn btn-primary"><i class="fa fa-user"></i> Nuevo Testigo Electoral</a>
                </div>

                <div class="ml-2">
                    <a href="{{ route('witnesses.assignwitnesses')}}" class="btn btn-primary"><i class="fa fa-user"></i>Testigos</a>
                </div>
            </div>
            <div class="filter_by">
                <?php 
                    $polling_stations = \App\Entities\PollingStation::pollingWithMunicipaly();
                ?>
                <div class="form-group" style="display: flex; align-items: center; padding-right: 18px; margin-bottom: 0">
                    <!--<label class="control-label" style="margin-right: 10px" for="orientador">Orientador</label>-->
                    <h4 style="margin-right: 20px" id="text-filter">Filtrar Por:</h4>
                    <div style="padding-right: 0">
                        <div class="input-group" style="min-width: 150px">
                            <select class="js-example-basic-single select-chosen change-select" style="width: 100%" required="required" name="Puesto de votación" id="polling_station" aria-required="true" aria-invalid="false" data-placeholder="Puesto de Votación" onchange="filter()">
                                <option value=""></option>
                                @foreach($polling_stations as  $key => $value)
                                    @if(isset($request->polling_station))
                                        @if($key == $request->polling_station)
                                            <option value="{{$key}}" selected>{{$value}}</option>
                                        @else
                                            <option value="{{$key}}">{{$value}}</option>
                                        @endif
                                    @else
                                        <option value="{{$key}}">{{$value}}</option>
                                    @endif
                                @endforeach
                            </select>
                            <span class="input-group-addon" style="width: 15px; cursor: pointer" onclick="emptyFilter('polling_station')"><i class="fa fa-remove"></i></span>
                        </div>
                    </div>
                </div>

                <div class="form-group" style="display: flex; align-items: center; padding-right: 18px; margin-bottom: 0">
                    <div style="padding-right: 0">
                        <div class="input-group" style="min-width: 150px">
                            <select class="js-example-basic-single select-chosen change-select" style="width: 100%" required="required" name="Alertas" id="alert" aria-required="true" aria-invalid="false" data-placeholder="Alertas" onchange="filter()">
                                <option value=""></option>
                                @if(isset($request->alert))
                                    @if($request->alert == "puesto")
                                        <option value="puesto" selected>Puesto de votación no existe</option>
                                    @else
                                        <option value="puesto">Puesto de votación no existe</option>
                                    @endif
                                @else
                                    <option value="puesto">Puesto de votación no existe</option>
                                @endif

                                @if(isset($request->alert))
                                    @if($request->alert == "mesa")
                                        <option value="mesa" selected>Mesa ocupada por otro testigo</option>
                                    @else
                                        <option value="mesa">Mesa ocupada por otro testigo</option>
                                    @endif
                                @else
                                    <option value="mesa">Mesa ocupada por otro testigo</option>
                                @endif

                                @if(isset($request->alert))
                                    @if($request->alert == "bdd")
                                        <option value="bdd" selected>Usuario no existe en la base de datos</option>
                                    @else
                                        <option value="bdd">Usuario no existe en la base de datos</option>
                                    @endif
                                @else
                                    <option value="bdd">Usuario no existe en la base de datos</option>
                                @endif
                            </select>
                            <span class="input-group-addon" style="width: 15px; cursor: pointer" onclick="emptyFilter('alert')"><i class="fa fa-remove"></i></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if (session('message'))
            <div class="btn-alert-danger" id="alert">
                <span>{{ session('message') }}</span> <span style="position: absolute; right: 12px; cursor:pointer;font-size: 12px;line-height: 22px" id="close">X</span>
            </div>
        @endif
        <div class="row" style="margin: 0; margin-top: 30px" id="title_page" style="margin-bottom: 10px;">
            <div class="col-sm-8 content-form">
                <div class="form-group">
                    <input id="search" type="text" class="form-control" placeholder="Cedula, Nombre, Email, Teléfono" value="{{$request->search}}">
                </div>
                <input type="button" class="btn btn-primary" value="Buscar" onclick="filter()">
                <input type="button" class="btn btn-primary ml-2 btn_find_create" value="Buscar y Crear" onclick="findAndCreate()">
            </div>
        </div>
    </div>
    <div class="block full">
        <div class="table-responsive">
            <table id="datatable" class="table table-striped table-bordered table-vcenter">
                <thead>
                    <tr>
                        <th title="Nombre de Usuario">Cedula</th>
                        <th title="Nombre completo">Nombre completo</th>
                        <th title="Nombre completo">Teléfono</th>
                        <th title="Correo">Email</th>
                        <th title="Tipo">Tipo de usuario</th>
                        <th title="Lugar de votación">Lugar de votación</th>
                        <th title="mesa">Mesa</th>
                        <th title="alert">Alerta</th>
                        <th class="text-center" style="width: 115px;"><i class="fa fa-flash"></i></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                        <tr>
                            
                            <td style="width: fit-content">{{ $user->username }}</td>
                            <td><strong>{{ $user->name }}</strong></td>
                            <td style="width: fit-content">{{ $user->phone }}</td>
                            <td style="width: fit-content">{{ $user->email }}</td>
                            <td style="width: 70px">{{ $user->type->name }}</td>
                            <td>{{ $user->pollingStation ? $user->pollingStation->name." >> ".$user->pollingStation->address : 'Sin Lugar de votación asignado' }}</td>
                            <td>{{ $user->num_table ? $user->num_table : 'Sin mesa asignada' }}</td>
                            <td style="width: 300px">
                                @if(!$user->name && !$user->email)
                                    El usuario no se encuentra en la base de datos
                                @elseif($user->name && $user->email && !$user->polling_station_id)
                                    El puesto de votación no existe
                                @elseif($user->name && $user->email && !$user->num_table)
                                    La Mesa está ocupada por otro testigo
                                @endif
                            </td>
                            <td class="text-center">
                                <a href="{{ route('witnesses.edit', $user->id)}}" data-toggle="tooltip" title="Editar tipo de usuario" class="btn btn-effect-ripple btn-warning">
                                    <i class="fa fa-pencil"></i>
                                </a>
                                <a href="#" data-id="{{ $user->id }}" id="btn-delete-{{ $user->id }}" onclick="deleteModel('btn-delete-{{ $user->id }}')"  data-toggle="tooltip" title="Borrar tipo de usuario" class="btn btn-effect-ripple btn-danger">
                                    <i class="fa fa-times"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    @if(isset($request))
        {!! $users->appends(['search' => $request->search])->render() !!}
    @else
        {!! $users->render() !!}
    @endif
    <!-- END Datatables Block -->
    {!! Form::open(array('route' => array('system.users.destroy', 'ID') , 'method' => 'DELETE', 'role' => 'form', 'id' => 'form-delete'))!!}

@endsection
@section('js_aditional')
<script>
    function filter(){
        let search = $("#search").val()
        let polling_station = $("#polling_station").val()
        let alert = $("#alert").val()
        let url = "/witnesses/assign_cedula?"

        if(search){
            url+= "search=" + search + "&"
        }
        if(polling_station){
            url+= "polling_station=" + polling_station + "&"
        }

        if(alert){
            url+= "alert=" + alert+ "&"
        }
        location.href=url
    }

    function findAndCreate(){
        let search = $("#search").val()
        if(!search) return alert("Por favor escriba la cédula")

        location.href  = "/witnesses/assign/create/" + search
    }

    function emptyFilter(empty){
        let search = $("#search").val()
        let polling_station = $("#polling_station").val()
        let alert = $("#alert").val()
        let url = "/witnesses/assign_cedula?"

        if(search && empty != "search"){
            url+= "search=" + search + "&"
        }
        if(polling_station && empty != "polling_station"){
            url+= "polling_station=" + polling_station
        }

        if(alert && empty != "alert"){
            url+= "alert=" + alert
        }
        location.href=url
    }

    $('#search').keypress(function(e) {
        var keycode = (e.keyCode ? e.keyCode : e.which);
        if (keycode == '13') {
            filter();
            e.preventDefault();
            return false;
        }
    });
</script>
@endsection

