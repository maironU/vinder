@extends('layout')
	@section('title_page') Bienvenido a RADAR @endsection
	@section('meta')
		<meta name="description" content="AppUI is a Web App Bootstrap Admin Template created by pixelcave and published on Themeforest">
        <meta name="author" content="pixelcave">
        <meta name="robots" content="noindex, nofollow">
        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">
        <meta http-equiv="Access-Control-Allow-Origin" content="*"/>
        <meta name="csrf-token" content="{{ csrf_token() }}" />

        @yield('meta_extra')
	@endsection
	@section('css_files')
		{{-- Css Files --}}
		@include('dashboard.includes.css')
		<link rel="stylesheet" href="{{ asset('assets/css/leaflet/leaflet.css') }}" />
		<link rel="stylesheet" href="{{ asset('assets/js/leaflet-zoom/Control.FullScreen.css') }}" />


<script>
    var url_home = "{{url('/')}}"
</script>

		@yield('css_aditional')
	@endsection
	@section('content_body')
		{{-- Content Body --}}
		<div id="page-wrapper" class="page-loading">
			<div class="preloader">
                <div class="inner">
                    <!-- Animation spinner for all modern browsers -->
                    <div class="preloader-spinner themed-background hidden-lt-ie10"></div>

                    <!-- Text for IE9 -->
                    <h3 class="text-primary visible-lt-ie10"><strong>Cargando..</strong></h3>
                </div>
            </div>
            <div id="page-container" class="header-fixed-top sidebar-visible-lg-full">
                <!-- Alternative Sidebar -->
                @include('dashboard.includes.right_sidebar')
                <!-- END Alternative Sidebar -->

                <!-- Main Sidebar -->
                @include('dashboard.includes.sidebar')
                <!-- END Main Sidebar -->

                <!-- Main Container -->
                <div id="main-container">
                    @include('dashboard.includes.header')
                    <!-- END Header -->

                    <!-- Page content -->
                    <div id="page-content" style="position:relative;">
                        @yield('content_page', 'Contenido del Dashboard')
                    </div>
                    <!-- END Page Content -->
                </div>
                <!-- END Main Container -->
            </div>
            <!-- END Page Container -->
            
		</div>
    
	@endsection
    <div class="container-spinner" id="spinner">
        <span class="spinner"></span>
    </div>
	@section('js_files')
		{{-- Js Files --}}
		@include('dashboard.includes.script')
		@yield('js_aditional')
        @include('flash::message')
        <script src="{{ asset('assets/js/leaflet/leaflet.js') }}"></script>
		<script src="{{ asset('assets/js/leaflet/esri-leaflet.js') }}"></script>
		<script src="{{ asset('assets/js/leaflet/leaflet-heatmap.js') }}"></script>
        <script src="{{asset('assets/js/forme14.js?v=10')}}"></script>
        <script src="{{asset('assets/js/order.js')}}"></script>
        <script src="{{asset('assets/js/general.js')}}"></script>
        <script src="{{asset('assets/js/leaflet-zoom/Control.FullScreen.js')}}"></script>
		@yield('js_aditional2')

        <!-- <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDBnZmR6l9RwBoPkDzB3Z4V5T7Rqlxlnyc&callback=initMap&libraries=visualization">
        </script> -->
        <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

         <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDNy9BfpOnvvIKlRkyXIboKbfEbtmNmkFU&callback=initMap&libraries=visualization">
        </script>
        <script src="{{asset('/assets/js/markerclusterer.js')}}"></script>
       
	@endsection