<table>
    <thead>
        <tr>
            <th>Nombre</th>
            <th>Documento</th>
            <th>Teléfono</th>
            <th>Mesa</th>
            <th>Puesto votación</th>
        </tr>
    </thead>
    <tbody>
        @foreach($voters as $voter)
            <tr>
                <td>{{$voter->name}}</td>
                <td>{{$voter->doc}}</td>
                <td>{{$voter->telephone}}</td>
                <td>{{$voter->table_number}}</td>
                <td>{{$voter->pollingStation ? $voter->pollingStation->name : ''}}</td>
            </tr>
        @endforeach
    </tbody> 
</table>