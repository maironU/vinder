<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCandidateCheckToFormse14Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('formse14', function (Blueprint $table) {
            $table->integer('candidate_check')->after('total_votes')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('formse14', function (Blueprint $table) {
            $table->dropColumn('candidate_check');
        });
    }
}
