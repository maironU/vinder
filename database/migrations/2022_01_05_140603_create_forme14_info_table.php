<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForme14InfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forme14_info', function($table)
        {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('value')->nullable();

            $table->unsignedInteger('forme14_id');
            $table->foreign('forme14_id')->references('id')->on('formse14')->onDelete('cascade')->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('forme14_info', function (Blueprint $table) 
        {
			$table->dropForeign(['forme14_id']);
		});
        Schema::drop('forme14_info');
    }
}
