<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFileSecondaryToForme14FilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('formse14_files', function (Blueprint $table) {
            $table->string('file_secondary')->nullable()->after('file');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('formse14_files', function (Blueprint $table) {
            $table->dropColumn('file_secondary');
        });
    }
}
