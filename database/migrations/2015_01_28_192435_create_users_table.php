<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		 Schema::create('user', function($table)
        {
            $table->increments('id');
            $table->string('username', 100)->unique();        
            $table->string('name', 100)->nullable();
            $table->string('email', 100)->nullable()->unique();
            $table->string('address', 255)->nullable();
            $table->string('phone', 12)->nullable();
			$table->double('latitude')->nullable()->default(0);
			$table->double('longitude')->nullable()->default(0);
			$table->integer('location_id')->unsigned()->nullable();
			$table->foreign('location_id')->references('id')->on('locations');

			$table->integer('polling_station_id')->nullable();
            $table->string('num_table', 30);
            $table->string('password', 255);
			$table->rememberToken();
			$table->timestamps();

			$table->integer('type_id')->unsigned()->nullable();
		    $table->foreign('type_id')->references('id')->on('user_types');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user');
	}

}
