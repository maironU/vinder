<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVotersnewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('votersnew', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nuip');
            $table->string('first_name')->nullable();
            $table->string('second_name')->nullable();
            $table->string('surname')->nullable();
            $table->string('second_surname')->nullable();
            $table->string('department');
            $table->string('municipality');
            $table->string('polling_station');
            $table->unsignedInteger('table');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('votersnew');
    }
}
