<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViewMapSemaphores extends Migration 
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE VIEW `view_semaphores` AS
            SELECT 
                `ps`.`name` AS `Puesto de votación`,
                `v`.`semaphore` AS `Intención de voto`,
                COUNT(`v`.`semaphore`) AS `Cantidad`,
                `ps`.`latitude` AS `Latitud`,
                `ps`.`longitude` AS `Longitud`,
                `ps`.`city` AS `Ciudad`,
                `ps`.`electoral_potential` AS `Potencial electoral`
            FROM
                (`voters` `v`
                JOIN `polling_stations` `ps`)
            WHERE
                (`ps`.`id` = `v`.`polling_station_id`)
            GROUP BY `ps`.`id` , `v`.`semaphore`
            ORDER BY `ps`.`name` , `v`.`semaphore`"
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW view_semaphores");
    }
}