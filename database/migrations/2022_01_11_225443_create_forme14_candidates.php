<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForme14Candidates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forme14_candidates', function($table)
        {
            $table->increments('id');
            $table->string('candidate_number')->nullable();

            $table->unsignedInteger('forme14_id');
            $table->foreign('forme14_id')->references('id')->on('formse14')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('forme14_candidates');
    }
}
