<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForme14TotalVotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forme14_total_votes', function($table)
        {
            $table->increments('id');
            $table->unsignedInteger('forme14_id');
            $table->foreign('forme14_id')->references('id')->on('formse14')->onDelete('cascade')->onUpdate('cascade');

            $table->bigInteger('total_votes')->nullable();

            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('user')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('forme14_total_votes');
    }
}
