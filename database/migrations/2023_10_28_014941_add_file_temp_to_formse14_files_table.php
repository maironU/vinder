<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFileTempToFormse14FilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('formse14_files', function (Blueprint $table) {
            $table->string('file_temp')->nullable()->after('file');
            $table->string('file_secondary_temp')->nullable()->after('file_secondary');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('formse14_files', function (Blueprint $table) {
            $table->dropColumn('file_temp');
            $table->dropColumn('file_secondary_temp');
        });
    }
}
