<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterViewMapSemaphores extends Migration 
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW view_semaphores");
        DB::statement("CREATE VIEW `view_semaphores` AS
            SELECT
                `ps`.`name` AS `Puesto de votación`,
                COUNT(CASE WHEN `v`.`semaphore` = 0 THEN `v`.`semaphore` END) `cero`,
                COUNT(CASE WHEN `v`.`semaphore` = 1 THEN `v`.`semaphore` END) `uno`,
                COUNT(CASE WHEN `v`.`semaphore` = 2 THEN `v`.`semaphore` END) `dos`,
                COUNT(CASE WHEN `v`.`semaphore` = 3 THEN `v`.`semaphore` END) `tres`,
                COUNT(`v`.`semaphore`) AS `Total`,
                `ps`.`latitude` AS `Latitud`,
                `ps`.`longitude` AS `Longitud`,
                `ps`.`city` AS `Ciudad`,
                `ps`.`electoral_potential` AS `Potencial electoral`
            FROM
                `voters` `v`
                JOIN `polling_stations` `ps` ON`ps`.`id` = `v`.`polling_station_id`
            GROUP BY `ps`.`id`
            ORDER BY `ps`.`name`"
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW view_semaphores");
        DB::statement("CREATE VIEW `view_semaphores` AS
            SELECT 
                `ps`.`name` AS `Puesto de votación`,
                `v`.`semaphore` AS `Intención de voto`,
                COUNT(`v`.`semaphore`) AS `Cantidad`,
                `ps`.`latitude` AS `Latitud`,
                `ps`.`longitude` AS `Longitud`,
                `ps`.`city` AS `Ciudad`,
                `ps`.`electoral_potential` AS `Potencial electoral`
            FROM
                (`voters` `v`
                JOIN `polling_stations` `ps`)
            WHERE
                (`ps`.`id` = `v`.`polling_station_id`)
            GROUP BY `ps`.`id` , `v`.`semaphore`
            ORDER BY `ps`.`name` , `v`.`semaphore`"
        );
    }
}