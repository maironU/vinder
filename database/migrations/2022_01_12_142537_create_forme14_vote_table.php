<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForme14VoteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forme14_votes', function($table)
        {
            $table->increments('id');
            $table->unsignedInteger('candidate_id');
            $table->foreign('candidate_id')->references('id')->on('forme14_candidates')->onDelete('cascade')->onUpdate('cascade');

            $table->integer('votes_number')->nullable();

            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('user')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('forme14_votes', function (Blueprint $table) 
        {
			$table->dropForeign(['user_id']);
		});
        Schema::drop('forme14_votes');
    }
}
