<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSemaphoreToVotersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('voters', function (Blueprint $table) 
        {
            $table->integer('semaphore')->default(0);
            /*
                0 => Por definir
                1 => Voto fijo
                2 => Indeciso
                3 => Voto duro
            */
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('voters', function (Blueprint $table) 
        {
            $table->dropColumn('semaphore');
        });
    }
}