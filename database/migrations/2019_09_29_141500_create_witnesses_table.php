<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWitnessesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('witnesses', function($table)
        {
            $table->increments('id');
            $table->unsignedInteger('user_id')->unique();
            $table->unsignedInteger('voter_id')->unique();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('user');
            $table->foreign('voter_id')->references('id')->on('voters');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('witnesses', function (Blueprint $table) 
        {
			$table->dropForeign(['user_id']);
			$table->dropForeign(['voter_id']);
		});
		Schema::drop('witnesses');
	}

}
