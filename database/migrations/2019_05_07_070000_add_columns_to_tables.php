<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('polling_stations', function (Blueprint $table) 
        {
            $table->string('city',255)->default('');
            $table->string('department',255)->default('');
            $table->double('latitude',10,7)->default(0);
            $table->double('longitude',10,7)->default(0);
            $table->string('place',255)->default('');
            $table->string('zone',255)->default('');
            // SoftDeletes agrega la columna 'deleted_at'
            $table->softDeletes();
        });

        Schema::table('voters', function (Blueprint $table) 
        {
            $table->string('facebook_profile',255)->default('');
            $table->string('instagram_profile',255)->default('');
            $table->string('telephone_alternative',30)->default('');
            $table->string('whatsapp',255)->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('polling_stations', function (Blueprint $table) 
        {
            $table->dropColumn('city');
            $table->dropColumn('department');
            $table->dropColumn('latitude');
            $table->dropColumn('longitude');
            $table->dropColumn('place');
            $table->dropColumn('zone');
            // SoftDeletes elimina la columna 'deleted_at'
            $table->dropSoftDeletes();
        });

        Schema::table('voters', function (Blueprint $table) 
        {
            $table->dropColumn('facebook_profile');
            $table->dropColumn('instagram_profile');
            $table->dropColumn('telephone_alternative');
            $table->dropColumn('whatsapp');
        });
    }
}