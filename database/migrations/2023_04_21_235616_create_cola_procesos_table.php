<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateColaProcesosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cola_procesos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('proceso_id')->unsigned();
            $table->integer('estado_proceso_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->boolean('bloqueado')->default(false);
            $table->string('email');
            $table->foreign('proceso_id')->references('id')->on('procesos');
            $table->foreign('estado_proceso_id')->references('id')->on('estado_procesos');
            $table->foreign('user_id')->references('id')->on('user');  
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cola_procesos');
    }
}
