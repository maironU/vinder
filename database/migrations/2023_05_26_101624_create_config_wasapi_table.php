<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfigWasapiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('config_wasapi', function (Blueprint $table) {
            $table->increments('id');
            $table->longText('plantilla_id');
            $table->integer('from_id');
            $table->boolean('has_variable')->default(0);
            $table->enum('type_variable', ["body", 'header'])->default('header');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('config_wasapi');
    }
}
