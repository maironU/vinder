<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWitnessesPollingStationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('witnesses_polling_stations', function($table)
        {
            $table->increments('id');
            $table->unsignedInteger('witness_id')->unique();
            $table->unsignedInteger('polling_station_id')->unique();
            $table->integer('table');
            $table->timestamps();

            $table->foreign('witness_id')->references('id')->on('witnesses');
            $table->foreign('polling_station_id')->references('id')->on('polling_stations');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('witnesses_polling_stations', function (Blueprint $table) 
        {
			$table->dropForeign(['witness_id']);
			$table->dropForeign(['polling_station_id']);
		});
		Schema::drop('witnesses_polling_stations');
	}

}
