<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableNumberToForme14VoteEscrutadorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('forme14_vote_escrutador', function (Blueprint $table) {
            $table->string('table_number')->after('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('forme14_vote_escrutador', function (Blueprint $table) {
            $table->dropColumn('table_number');
        });
    }
}
