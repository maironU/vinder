<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFiledClaimEncrutadorToFiledClaimsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('forme14_filed_claims', function (Blueprint $table) {
            $table->string('filed_claim_encrutador')->nullable()->after('filed_claim');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('forme14_filed_claims', function (Blueprint $table) {
            $table->dropColumn('filed_claim_encrutador');
        });
    }
}
