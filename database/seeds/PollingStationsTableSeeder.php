<?php 

use \Illuminate\Database\Seeder;
use App\Entities\PollingStation;
use App\Libraries\Campaing;

class PollingStationsTableSeeder extends Seeder
{
    public function getPollingStations($location)
    {
        $pollingStations = [];
        switch(mb_strtolower($location))
        {
            case mb_strtolower('LocationsMetaTableSeeder'):
            {
                // https://www.registraduria.gov.co/?page=divipol_delegaciones&delegacion=meta&id_rubrique=592&seccion=85&cod=52
                // name, address, tables, registraduria_location_id, electoral_potential, location_id, latitude, longitude, department, city
                $pollingStations = [
                    ['Inst. Educativo 12  De Octubr','Cl 46 # 50 Br.Santa Josefa','23','2','8603','1','0','0','Meta','Villavicencio',],
                    ['Colegio General Santander','Transversal 25 # 39D-46','14','2','5225','1','0','0','Meta','Villavicencio',],
                    ['Escuela Policarpa Salavarrieta','Cl 25 No 42 22 Barrio La Grama','13','2','4968','1','0','0','Meta','Villavicencio',],
                    ['Colegio Antonio Nariño','Cl L 45 # 39-40 B. Esmerald','16','2','6109','1','0','0','Meta','Villavicencio',],
                    ['Esc. Juan Pablo Ii Sede Chapinero Bajo','Cra. 52 # 48 A -16/34 Chapinero Bajo','0','2','0','1','0','0','Meta','Villavicencio',],
                    ['Colegio La Salle','Calle 39 No. 34 56 Centro','39','2','10530','1','0','0','Meta','Villavicencio',],
                    ['Unillanos Sede San Antonio','Cl 37 # 41-02 Barzal','30','2','11540','1','0','0','Meta','Villavicencio',],
                    ['Esc. Concepcion Palacios','Cra. 32 # 41 - 40 Centro','0','2','0','1','0','0','Meta','Villavicencio',],
                    ['Esc. Francisco Arango Sede Campestre','Barrio Mesetas','4','2','1454','1','0','0','Meta','Villavicencio',],
                    ['Colegio Francisco Arango','Calle 33 B No. 39-55','12','2','4348','1','0','0','Meta','Villavicencio',],
                    ['Ie Siete De Agosto','Calle 7 No 38 57 Siete De Agosto','14','2','5511','1','0','0','Meta','Villavicencio',],
                    ['Colegio Don Bosco','Barrio El Buque Sector Buganbiles','20','2','7504','1','0','0','Meta','Villavicencio',],
                    ['Escuela Francisco Miranda','Cl 35 # 29-09 Frente A Bomberos','12','2','6129','1','0','0','Meta','Villavicencio',],
                    ['Colegio Nuevo Gimnasio','Calle 15 # 55-97 El Buque','0','2','0','1','0','0','Meta','Villavicencio',],
                    ['Colegio Neil Armstrong','Cra. 44 Calle 14 Buque','0','2','0','1','0','0','Meta','Villavicencio',],
                    ['Escuela Antonio Ricaurte','Cr 24C No 26C 09 Barrio 20 De Julio','14','2','5453','1','0','0','Meta','Villavicencio',],
                    ['Hogar Infantil 20 De Julio','Cra. 22C # 27 -15','12','2','4416','1','0','0','Meta','Villavicencio',],
                    ['Colegio Santa Ines','Calle 37A No 24 04 Barrio Santa Ines','13','2','4777','1','0','0','Meta','Villavicencio',],
                    ['Colegio Cofrem','Av.Circunvalar19-55 Via Catama','9','2','2390','1','0','0','Meta','Villavicencio',],
                    ['Col Abrahan Lincoln Sd La Vainilla','Cra. 20C # 26A 27 La Vainilla','0','2','0','1','0','0','Meta','Villavicencio',],
                    ['Carcel','Transv 26C No 22A 14 Antonio Ricaurte','2','2','402','1','0','0','Meta','Villavicencio',],
                    ['Escuela La Ceiba','Calle 30 Cra 15 Esquina','29','2','11136','1','0','0','Meta','Villavicencio',],
                    ['Col. Narciso Matus Torres','Kr 14 # 40B-02 Br.Bastilla','28','2','10501','1','0','0','Meta','Villavicencio',],
                    ['Unidad Basica 6 De Abril','Cl 32 Con Cra. 6 B Seis De Abril','5','2','1464','1','0','0','Meta','Villavicencio',],
                    ['Esc. Atanasio Girardot','Cl 40A # 17-28 E Br.Covisan','11','2','4271','1','0','0','Meta','Villavicencio',],
                    ['Colegio Jorge Eliecer Gaitan','Calle 36 A No 17 - 0 Barrio Morichal','28','2','10201','1','0','0','Meta','Villavicencio',],
                    ['Esc. Isaac Tacha Sede La Reliquia','Av.1O. De Mayo Calle 29C 64','16','2','6140','1','0','0','Meta','Villavicencio',],
                    ['Megacolegio Rodolfo Llinas','B. Trece De Mayo Sector La Union','8','2','2391','1','0','0','Meta','Villavicencio',],
                    ['Terminal De Transportes','Cra. 1 No 15 05 Anillo Vial','8','2','1957','1','0','0','Meta','Villavicencio',],
                    ['Col. Alberto Lleras Camargo','Cl 5A # 10A-21 Br.Estero','23','2','8084','1','0','0','Meta','Villavicencio',],
                    ['Col. Manuela Beltran','Cl 25 # 6-115 Br.Popular','27','2','10263','1','0','0','Meta','Villavicencio',],
                    ['Colegio Eduardo Carranza','Calle 25 No 12A 175 Barrio Popular','16','2','6086','1','0','0','Meta','Villavicencio',],
                    ['Escuela Las Camelias','Cl 19 # 8-62 Br.Camelias','15','2','5506','1','0','0','Meta','Villavicencio',],
                    ['Villa Olimpica','Carrera 19 Entre Calle 25 Y 22','23','2','8610','1','0','0','Meta','Villavicencio',],
                    ['Colegio Rafael Nuñez','Cra. 11 # 29-45 El Rodeo','15','2','5561','1','0','0','Meta','Villavicencio',],
                    ['Escuela Marco Antonio Pinilla','Via Catama Km 8  Marco Antonio Pinilla','12','2','4434','1','0','0','Meta','Villavicencio',],
                    ['Colegio Silvia Aponte','Cra.7 Este Calle 15 Sur Valles De Aragon','16','2','7521','1','0','0','Meta','Villavicencio',],
                    ['Liceo Mayor Jaime Triana Restrepo','Smz1 Cs1 Camino Ganadero Sn. Antonio','10','2','4651','1','0','0','Meta','Villavicencio',],
                    ['Col. Antony Phiipps','Kr 37 Cl 22A Bis','17','2','6181','1','0','0','Meta','Villavicencio',],
                    ['Col. Abrahan Lincoln','Cl 25 # 23-150','25','2','9604','1','0','0','Meta','Villavicencio',],
                    ['Col Miguel Angel Martin','Cr 25 No 15 - 60 Br Nuevo Ricaurte','13','2','4903','1','0','0','Meta','Villavicencio',],
                    ['Col De Bachillerato Femenino','Carrera 33 No 18 A 43','24','2','20821','1','0','0','Meta','Villavicencio',],
                    ['Col. Dptal La Esperanza','Kr 45 # 13B-25','22','2','8506','1','0','0','Meta','Villavicencio',],
                    ['Col. German Arciniegas','Kr 47B # 7B-20','19','2','6955','1','0','0','Meta','Villavicencio',],
                    ['Esc Jhon F. Kennedy','Cr 46 No 11 - 28   2A.Etapa Esperanza','13','2','5104','1','0','0','Meta','Villavicencio',],
                    ['I.E. La Alborada','Kr 25 Cl 48 Br.Alborada','14','2','6678','1','0','0','Meta','Villavicencio',],
                    ['Col. Gilberto Alzate Avendaño','Kr 30A Con Cl 5A','15','2','6941','1','0','0','Meta','Villavicencio',],
                    ['Col. San Francisco De Asis','Cl 11 # 25-15 Br.Cooperativo','8','2','3480','1','0','0','Meta','Villavicencio',],
                    ['Escuela Los Centauros','Kr 27 Y 28 Cl 9 La Rosita','6','2','2269','1','0','0','Meta','Villavicencio',],
                    ['Uniminuto','Cra. 36 # 5A-21 Sur Smz 5 Lote 2','6','2','1818','1','0','0','Meta','Villavicencio',],
                    ['Escuela La Rochela','Cl 20 Sur # 44-15 Br.Rochela','14','2','6265','1','0','0','Meta','Villavicencio',],
                    ['Academia Militar Jose Antonio Paez','Calle 24A No 40B - 27 Montecarlo','12','2','5272','1','0','0','Meta','Villavicencio',],
                    ['Colegio Catumare','Kr 46 Con Cl 18 Sur','11','2','4639','1','0','0','Meta','Villavicencio',],
                    ['Colegio Catumare Sede Campestre','Cra.44 # 16A Sur B. El Palmar Catumare','0','2','0','1','0','0','Meta','Villavicencio',],
                    ['Escuela San Jorge','Carrera 41 No. 20A Sur 03','7','2','3204','1','0','0','Meta','Villavicencio',],
                    ['Unidad Educativa Playa Rica','Kr 54 # 21A-09','7','2','3284','1','0','0','Meta','Villavicencio',],
                    ['Col Juan B. Caballero Medina','Km 1 Nueva Via Bogota','9','2','3863','1','0','0','Meta','Villavicencio',],
                    ['Bemposta','Frente Al Br.La Nora Via Acacias','5','2','2283','1','0','0','Meta','Villavicencio',],
                    ['Escuela Las Palmas Porfia','Manzana 35 Lote 15 Y 16 Las Palmas','21','2','9273','1','0','0','Meta','Villavicencio',],
                    ['Megacolegio Porfia Pinares De Oriente','Calle 67 Sur Cra 47 Barrio Porfia','7','2','2376','1','0','0','Meta','Villavicencio',],
                    ['Col Luis Carlos Galan Sarmiento','Carrera 40 No 51 - 10 Sur Porfia','24','2','10919','1','0','0','Meta','Villavicencio',],
                    ['Megacolegio Luis Carlos Galan Sarmiento','Smz 5 Sector 2 Ciudad Milenio Porfia','0','2','0','1','0','0','Meta','Villavicencio',],
                    ['La Concepcion','Inst. Educativa La Concepcion','6','2','2004','1','0','0','Meta','Villavicencio',],
                    ['Buenavista','Inst. Educativa Buenavista','3','2','994','1','0','0','Meta','Villavicencio',],
                    ['Pipiral','Inst. Educativa Pipiral','2','2','450','1','0','0','Meta','Villavicencio',],
                    ['Servita','Inst. Educativa Servita','1','2','353','1','0','0','Meta','Villavicencio',],
                    ['Santa Maria La Baja','Inst. Educativa Santa Maria La Baja','2','2','470','1','0','0','Meta','Villavicencio',],
                    ['Alto De Pompeya','Inst. Educativa Alto De Pompeya','4','2','1477','1','0','0','Meta','Villavicencio',],
                    ['Puerto Colombia','Inst. Educativa Puerto Colombia','1','2','280','1','0','0','Meta','Villavicencio',],
                    ['Rincon De Pompeya','Inst. Educativa Rincon De Pompeya','4','2','1501','1','0','0','Meta','Villavicencio',],
                    ['Vanguardia','Inst. Educativa Vanguardia','8','2','2851','1','0','0','Meta','Villavicencio',],
                    ['Santa Teresa','Inst. Educativa Santa Teresa','1','2','310','1','0','0','Meta','Villavicencio',],
                    ['Cocuy','Inst. Educativa Cocuy','3','2','1096','1','0','0','Meta','Villavicencio',],
                    ['Apiay','Inst. Educativa Apiay','8','2','2735','1','0','0','Meta','Villavicencio',],
                    ['Santa Rosa De Rionegro','Inst. Educativa Santa Rosa De Rio Negro','2','2','496','1','0','0','Meta','Villavicencio',],
                    ['Conc. Escolar Gabriela Mistral','Calle 13 No 14 55 Barrio Centro','14','2','4899','1','0','0','Meta','Acacias',],
                    ['I.E. Enrique Daniels','Calle 16 No 18 34 Barrio San Cristobal','11','2','3832','1','0','0','Meta','Acacias',],
                    ['Col Mpal Luis Carlos Galan Sarmiento','Carrera 12 No 13 44 Barrio Juan Mellao','15','2','5742','1','0','0','Meta','Acacias',],
                    ['Colegio  Maria Montesori','Carrera 17A No 9 49 Barrio Ciudad Jardin','11','2','3775','1','0','0','Meta','Acacias',],
                    ['I. E. Pablo Sexto','Cra. 6 # 16 -11 Barrio Popular','14','2','5200','1','0','0','Meta','Acacias',],
                    ['Conc Esc Rafael Pombo','Calle 23 Carrera 24 Y 25 Barrio La Tiza','3','2','577','1','0','0','Meta','Acacias',],
                    ['Ie Juan Humberto Baquero Soler','Calle 16A Carrera 29 Esquina Barrio El Bosque','22','2','7762','1','0','0','Meta','Acacias',],
                    ['Ie 20 De Julio','Calle 11 No 36B 11 Barrio Independencia','23','2','7712','1','0','0','Meta','Acacias',],
                    ['Ie Lilia Castro De Parrado','Calle 14 Carrera 44 Y 45 Barrio Las Colinas','12','2','4594','1','0','0','Meta','Acacias',],
                    ['Colegio Cofrem','Calle 11 No 24 44 Barrio El Dorado','10','2','2740','1','0','0','Meta','Acacias',],
                    ['Esc Normal Sup De Acacias','Avenida 23 No 41 50 Vereda Cola De Pato','6','2','1255','1','0','0','Meta','Acacias',],
                    ['Gimnasio Integ. Juan Pablo Ii','Cll 15 No 22-57 B. Cooperativo','0','2','0','1','0','0','Meta','Acacias',],
                    ['Col. Nacionalizado Pablo Emilio Riveros','Cra. 23 # 8 - 00','0','2','0','1','0','0','Meta','Acacias',],
                    ['Coliseo Antigua Normal Superior','Cl 14 # 21-32 B. Cooperativo','8','2','6785','1','0','0','Meta','Acacias',],
                    ['Carcel','Km. 4 Via Villavicencio','1','2','175','1','0','0','Meta','Acacias',],
                    ['Dinamarca','Ins.Educ. Inspeccion Dinamarca','4','2','1429','1','0','0','Meta','Acacias',],
                    ['La Loma De San Juan','Esc. Rural','1','2','75','1','0','0','Meta','Acacias',],
                    ['Manzanares','Esc. Rural','1','2','79','1','0','0','Meta','Acacias',],
                    ['San Cristobal','Esc. Rural','1','2','25','1','0','0','Meta','Acacias',],
                    ['San Isidro De Chichimene','Ins.Educ. San Isidro De Chichimene','4','2','1292','1','0','0','Meta','Acacias',],
                    ['San Jose De Las Palomas','Esc. San Jose De Las Palomas','1','2','287','1','0','0','Meta','Acacias',],
                    ['Puesto Cabecera Municipal','Cll 12 No 2-46 B. Centro Ie Fco Walter Sd Primaria','11','12','4210','1','0','0','Meta','Barranca De Upia',],
                    ['El Hijoa','Km 36 Via Cabuyaro - Ie Fco Walter','1','12','138','1','0','0','Meta','Barranca De Upia',],
                    ['Guaicaramo','Km 54 - Ie Fco Walter','1','12','76','1','0','0','Meta','Barranca De Upia',],
                    ['San Ignacio (Carutal)','Cra 4 No 4-42 - Ie Fco Walter','1','12','266','1','0','0','Meta','Barranca De Upia',],
                    ['Puesto Cabecera Municipal','Carrera 5 No 12 - 30','11','13','3849','1','0','0','Meta','Cabuyaro',],
                    ['Guayabal','I.E. El Yarico Sede Guayabal','1','13','245','1','0','0','Meta','Cabuyaro',],
                    ['Los Mangos','I.E. El Yarico Sede Naguayas','1','13','376','1','0','0','Meta','Cabuyaro',],
                    ['Viso De Upia','I.E.El Yarico Sede Viso De Upia','2','13','579','1','0','0','Meta','Cabuyaro',],
                    ['Puesto Cabecera Municipal','Inst. Educ Henry Daniels. Cr 8 No. 4-45  Centro','23','14','8798','1','0','0','Meta','Castilla La Nueva',],
                    ['San Lorenzo','Institucion Educativa San Lorenzo','6','14','1926','1','0','0','Meta','Castilla La Nueva',],
                    ['Arenales','Cent Educ Rur Castilla La Nueva. Sede El Progreso','2','14','557','1','0','0','Meta','Castilla La Nueva',],
                    ['El Toro','I.E.Castilla La Nueva Sede El Toro','1','14','223','1','0','0','Meta','Castilla La Nueva',],
                    ['Puesto Cabecera Municipal','Cra 5 A  No 10 - 50','14','15','5144','1','0','0','Meta','Cubarral',],
                    ['Puesto Cabecera Municipal','Cr 20 No 8 - 40 Sede Jose Maria Cordoba','38','15','14456','1','0','0','Meta','Cumaral',],
                    ['Caney Medio','Centro Educativo Rural Jose Maria Guioth','1','15','336','1','0','0','Meta','Cumaral',],
                    ['El Caibe','Cent Educ Rur Jose Maria Guioth Sede La Esperanza','1','15','96','1','0','0','Meta','Cumaral',],
                    ['Guacavia','Inst Educativa Instituto Agricola De Guacavia','2','15','566','1','0','0','Meta','Cumaral',],
                    ['Montebello','Institucion Educativa San Isidro Sede Rancherias','1','15','66','1','0','0','Meta','Cumaral',],
                    ['Presentado','Ie Teniente Cruz Paredes Sede Presentado','1','15','218','1','0','0','Meta','Cumaral',],
                    ['Puerto Pecuca (Veracruz)','Institucion Educativa San Isidro - Veracruz','3','15','917','1','0','0','Meta','Cumaral',],
                    ['San Nicolas','Cent Educ Rur Jose Maria Guioth Sede Santa Lucia','1','15','298','1','0','0','Meta','Cumaral',],
                    ['Varsovia','Caseta Comunal','1','15','58','1','0','0','Meta','Cumaral',],
                    ['Puesto Cabecera Municipal','I.E. Juan Bautista Arnaud','2','17','743','1','0','0','Meta','El Calvario',],
                    ['Montfort','Centro De Salud Montfort','1','17','294','1','0','0','Meta','El Calvario',],
                    ['San Francisco','Col Simon Bolivar','2','17','477','1','0','0','Meta','El Calvario',],
                    ['San Pedro','Esc. Nueva San Pedro','1','17','48','1','0','0','Meta','El Calvario',],
                    ['San Rafael','Esc. San Rafael','1','17','67','1','0','0','Meta','El Calvario',],
                    ['Puesto Cabecera Municipal','I.E Ovidio Decroly','11','18','3996','1','0','0','Meta','El Castillo',],
                    ['Medellin Del Ariari','Ie Ovidio Decroly Sd Jorge E Gaitan','4','18','1440','1','0','0','Meta','El Castillo',],
                    ['Miravalles','I.E El Encanto','1','18','229','1','0','0','Meta','El Castillo',],
                    ['Puesto Cabecera Municipal','Cll 2 Cr 7 Y 8','6','19','2284','1','0','0','Meta','El Dorado',],
                    ['Pueblo Sanchez','Centro Poblado','1','19','242','1','0','0','Meta','El Dorado',],
                    ['San Isidro','Centro','2','19','570','1','0','0','Meta','El Dorado',],
                    ['Santa Rosa Alta','Vereda Santa Rosa','1','19','9','1','0','0','Meta','El Dorado',],
                    ['Puesto Cabecera Municipal','Cr 14 No 13 - 17','20','20','7417','1','0','0','Meta','Fuente De Oro',],
                    ['Union Del Ariari (Caño Blanco','Ie Unión Del Ariari','1','20','381','1','0','0','Meta','Fuente De Oro',],
                    ['Puerto Aljure','Salon Centro De Salud','1','20','242','1','0','0','Meta','Fuente De Oro',],
                    ['Puerto Limon','Unidad Educativa San Juan Bosco','2','20','420','1','0','0','Meta','Fuente De Oro',],
                    ['Puerto Santander','Ie Puerto Santander','2','20','473','1','0','0','Meta','Fuente De Oro',],
                    ['La Cooperativa','Unidad Educativa La Cooperativa','2','20','625','1','0','0','Meta','Fuente De Oro',],
                    ['Institucion Educativa Valentin','Cr 13 Cll 15 Esquina','26','21','9915','1','0','0','Meta','Granada',],
                    ['Escuela Club De Leones','Cr 14 Bis Cll 7 Esquina','9','21','3235','1','0','0','Meta','Granada',],
                    ['Institucion Educativa General','Cr 12 Cll 16 Esquina','17','21','6263','1','0','0','Meta','Granada',],
                    ['Complejo Deportivo Villa Olimpica','Calle 15 No. 6-102 Barrio Villa Olimpica','9','21','3177','1','0','0','Meta','Granada',],
                    ['Escuela El Bosque','Entre Mz 14 Y Mz 29','3','21','1005','1','0','0','Meta','Granada',],
                    ['I.E. Escuela Normal Superior Maria Auxil','Calle 23 Con Cra. 13 Esquina','12','21','4444','1','0','0','Meta','Granada',],
                    ['Escuela Alfonso Montoya Pava','Cll 24 Cr 9 Esquina','18','21','6725','1','0','0','Meta','Granada',],
                    ['Institucion Educativa Luis Car','Cr 9 No 32 - 05 Barrio El Porvenir','14','21','5058','1','0','0','Meta','Granada',],
                    ['Colegio Camilo Torres','Cll 15 No 9 - 185','8','21','6184','1','0','0','Meta','Granada',],
                    ['Carcel Municipal','Cr 14 No 14 - 25 Barrio Centro','1','21','140','1','0','0','Meta','Granada',],
                    ['Aguas Claras','I.E. Jose Antonio Galan','4','21','1206','1','0','0','Meta','Granada',],
                    ['Canaguaro','Esc. Luis Lopez De Mesa','5','21','1707','1','0','0','Meta','Granada',],
                    ['Dos Quebradas','I.E Tecnico Insdustrial Dos Quebradas','3','21','890','1','0','0','Meta','Granada',],
                    ['La Playa','Esc. Juan Xxiii','3','21','812','1','0','0','Meta','Granada',],
                    ['Puerto Caldas','Inst. Escolar Francisco. Jose De Caldas','4','21','1214','1','0','0','Meta','Granada',],
                    ['Puesto Cabecera Municipal','Calle 13 No. 9- 43 Barrio Los Fundadores','29','22','10953','1','0','0','Meta','Guamal',],
                    ['Humadea','C.E Rural De Guamal - Sede Nicolas De Federman','1','22','247','1','0','0','Meta','Guamal',],
                    ['Orotoy','C.E Rural De Guamal - Sede Orotoy Vereda Orotoy','1','22','229','1','0','0','Meta','Guamal',],
                    ['Puesto Cabecera Municipal','Cl 9 No 5 - 49 Barrio Cristales','18','23','6533','1','0','0','Meta','La Macarena',],
                    ['Yari','I.E. Las Brisas- Poblado El Re','2','23','586','1','0','0','Meta','La Macarena',],
                    ['La Catalina','Esc. La Catalina','1','23','138','1','0','0','Meta','La Macarena',],
                    ['San Juan De Lozada','Esc. San Juan De Lozada','4','23','1187','1','0','0','Meta','La Macarena',],
                    ['Puesto Cabecera Municipal','Cr 22 No 7 - 17 Barrio Modelo','17','25','6326','1','0','0','Meta','Lejanias',],
                    ['Angosturas Del Guape','Sede Educativaangostura Del Guape','1','25','127','1','0','0','Meta','Lejanias',],
                    ['Cacayal','I.E. Gabriela Mistral','4','25','1430','1','0','0','Meta','Lejanias',],
                    ['La Veinticuatro','Sede Educativa Santa Rita','1','25','224','1','0','0','Meta','Lejanias',],
                    ['I.E. Educativa Jorge Eliecer Gaitan','Cra. 10 No. 13D - 33 Barrio Manacacias','12','29','5368','1','0','0','Meta','Puerto Gaitan',],
                    ['I.E. J. Eliecer Gaitan Sd Camilo Torres','Cl 10 No. 15-03 Barrio Villa Ortiz','12','29','5374','1','0','0','Meta','Puerto Gaitan',],
                    ['I.E. Luis Carlos Galan Sarmiento','Cra. 3 No. 17-20 Barrio Popular','13','29','5894','1','0','0','Meta','Puerto Gaitan',],
                    ['I.E. L.C Galan Sarmiento - Sd L. A Perez','Cl 18 No. 7 -30 Barrio Galan','10','29','4513','1','0','0','Meta','Puerto Gaitan',],
                    ['El Porvenir','Esc. La Sabana','2','29','455','1','0','0','Meta','Puerto Gaitan',],
                    ['La Cristalina','Esc. Cristalina','3','29','1139','1','0','0','Meta','Puerto Gaitan',],
                    ['Planas','Esc. Primavera','5','29','1607','1','0','0','Meta','Puerto Gaitan',],
                    ['Puente Arimena','Esc. Gregorio Garavito','1','29','236','1','0','0','Meta','Puerto Gaitan',],
                    ['Puerto Trujillo','Esc. Puerto Trujillo','2','29','493','1','0','0','Meta','Puerto Gaitan',],
                    ['Rubiales Puerto Triunfo','Esc Nueva Rubiales','5','29','1715','1','0','0','Meta','Puerto Gaitan',],
                    ['San Miguel O Pescadero','Esc. San Miguel','2','29','425','1','0','0','Meta','Puerto Gaitan',],
                    ['San Pedro De Arimena','Esc. General Santander','1','29','242','1','0','0','Meta','Puerto Gaitan',],
                    ['Tillava','Esc. Alto Tillava','3','29','1038','1','0','0','Meta','Puerto Gaitan',],
                    ['Puesto Cabecera Municipal','I.E. Los Fundadores','19','27','6941','1','0','0','Meta','Mesetas',],
                    ['Brisas Del Duda','Caseta Comunal','2','27','711','1','0','0','Meta','Mesetas',],
                    ['El Mirador','Esc. El Mirador','1','27','34','1','0','0','Meta','Mesetas',],
                    ['Jardin De Las Peñas','Esc. Jardin De Las Peñas','3','27','851','1','0','0','Meta','Mesetas',],
                    ['Puesto Cabecera Municipal','Cll 7 Cr 9-10 I E Rafael Uribe Uribe','47','31','17787','1','0','0','Meta','Puerto Lopez',],
                    ['Bocas Del Guayuriba','Esc Bocas Del Guayuriba','1','31','306','1','0','0','Meta','Puerto Lopez',],
                    ['Altamira','Esc Antonia Santos','1','31','100','1','0','0','Meta','Puerto Lopez',],
                    ['Chaviva','I E Yaaliakeisy','4','31','1235','1','0','0','Meta','Puerto Lopez',],
                    ['El Tigre','Intern Simon Bolivar','1','31','283','1','0','0','Meta','Puerto Lopez',],
                    ['Puerto Guadalupe (Cumaralito)','I E Puerto Guadalupe','3','31','839','1','0','0','Meta','Puerto Lopez',],
                    ['La Balsa','I E Ponton De Rio Negro','2','31','403','1','0','0','Meta','Puerto Lopez',],
                    ['Melúa','Ce Serrania Del Melua','2','31','403','1','0','0','Meta','Puerto Lopez',],
                    ['Pachaquiaro','I E Santa Teresa','5','31','1801','1','0','0','Meta','Puerto Lopez',],
                    ['Puerto Porfia','I E Puerto Porfia','2','31','398','1','0','0','Meta','Puerto Lopez',],
                    ['Remolino','I E Remolino','3','31','818','1','0','0','Meta','Puerto Lopez',],
                    ['Puesto Cabecera Municipal','Esc.Jorge Eliecer Gaitan-La Lo','9','26','3171','1','0','0','Meta','Mapiripan',],
                    ['Buenos Aires','Caseta Comunal','1','26','174','1','0','0','Meta','Mapiripan',],
                    ['El Anzuelo','Caseta Comunal','1','26','113','1','0','0','Meta','Mapiripan',],
                    ['El Mielon','Caseta Comunal-El Melon','2','26','391','1','0','0','Meta','Mapiripan',],
                    ['La Cooperativa','Esc. El Rocio-La Cooperativa','1','26','354','1','0','0','Meta','Mapiripan',],
                    ['Las Guacamayas','Esc. La Independencia-Guacamay','1','26','225','1','0','0','Meta','Mapiripan',],
                    ['Puerto Alvira','Col Divino Niño Jesus-Puerto A','2','26','714','1','0','0','Meta','Mapiripan',],
                    ['Puerto Siare','Caseta Comunal','1','26','151','1','0','0','Meta','Mapiripan',],
                    ['Sardinata','Esc. Simon Bolivar-Sardinata','1','26','40','1','0','0','Meta','Mapiripan',],
                    ['Puesto Cabecera Municipal','I.E. Nueva Esperanza Sede Primaria','15','28','5525','1','0','0','Meta','Puerto Concordia',],
                    ['Cruce De Pororio','Esc. La Vallenata','2','28','410','1','0','0','Meta','Puerto Concordia',],
                    ['La Tigra','Esc. Nueva Esperanza','1','28','1','1','0','0','Meta','Puerto Concordia',],
                    ['Lindenal','Esc. Lindenal','2','28','451','1','0','0','Meta','Puerto Concordia',],
                    ['Puesto Cabecera Municipal','Colegio Inema Cl 7 Kra 12 Br.G','13','30','4677','1','0','0','Meta','Puerto Lleras',],
                    ['Casibare','C.E.San Fernado Ctro Poblado','2','30','443','1','0','0','Meta','Puerto Lleras',],
                    ['La Union','La Union Caseta Comunal Ctro P','2','30','607','1','0','0','Meta','Puerto Lleras',],
                    ['Tierra Grata','Ctro Educativo Tierra Grata Ct','1','30','381','1','0','0','Meta','Puerto Lleras',],
                    ['Villa De La Paz','Esc. Ntra Sra De La Paz Ctro P','2','30','389','1','0','0','Meta','Puerto Lleras',],
                    ['Puesto Cabecera Municipal','Unidad Educativa Urbana','20','32','7481','1','0','0','Meta','Puerto Rico',],
                    ['Barranco Colorado','Caseta Comunal','2','32','416','1','0','0','Meta','Puerto Rico',],
                    ['Lindosa','Caseta Comunal','1','32','66','1','0','0','Meta','Puerto Rico',],
                    ['Puerto Chispas','Caseta Comunal','1','32','236','1','0','0','Meta','Puerto Rico',],
                    ['Puerto Toledo','Caseta Comunal','2','32','511','1','0','0','Meta','Puerto Rico',],
                    ['Puesto Cabecera Municipal','Carrera 8 No. 8  50 Centro','43','33','16393','1','0','0','Meta','Restrepo',],
                    ['Puesto Cabecera Municipal','Cll 5 Cr 4 Esq.I.E Gabriel Garcia Marquez Sec. Bto','14','34','5224','1','0','0','Meta','San Carlos De Guaroa',],
                    ['Rincon De Pajure','Esc. Divino Niño','1','34','358','1','0','0','Meta','San Carlos De Guaroa',],
                    ['La Palmera','I.E. Simón Bólivar','5','34','1811','1','0','0','Meta','San Carlos De Guaroa',],
                    ['Surimena','I.E. Isabel La Catolica','3','34','1123','1','0','0','Meta','San Carlos De Guaroa',],
                    ['Puesto Cabecera Municipal','Tr 12 No 10-12','13','35','4777','1','0','0','Meta','San Juan De Arama',],
                    ['Cerritos','Esc. Nueva Cerritos','1','35','274','1','0','0','Meta','San Juan De Arama',],
                    ['Costa Rica','Vereda Nuevo Progreso Caseta Comunal','1','35','272','1','0','0','Meta','San Juan De Arama',],
                    ['El Vergel','Esc. Francisco De Paula Santander','1','35','291','1','0','0','Meta','San Juan De Arama',],
                    ['Mesa De Fernandez','I.E. Mesa De Fernandez','1','35','185','1','0','0','Meta','San Juan De Arama',],
                    ['Miraflores','Col Dptal Agropecuario Miraflores','1','35','171','1','0','0','Meta','San Juan De Arama',],
                    ['Peñas Blancas','Caseta Comunal Peñas Blancas','1','35','142','1','0','0','Meta','San Juan De Arama',],
                    ['Puesto Cabecera Municipal','Col.Jhon F. Kennedy Cr 3 No. 11 - 01 Barrio Centro','3','36','1039','1','0','0','Meta','San Juanito',],
                    ['La Candelaria','Escuela Candelaria','1','36','110','1','0','0','Meta','San Juanito',],
                    ['San Jose','Escuela  San Jose','1','36','111','1','0','0','Meta','San Juanito',],
                    ['San Luis Del Plan','Escuela San Luis Del Plan','1','36','74','1','0','0','Meta','San Juanito',],
                    ['Puesto Cabecera Municipal','Esc. Raul De Oliveira','46','37','17663','1','0','0','Meta','San Martin De Los Llanos',],
                    ['Alto De Iraca','Esc. El Bambu','1','37','115','1','0','0','Meta','San Martin De Los Llanos',],
                    ['Bajo Camoa','Esc. Gabriel Garcia Marquez','1','37','141','1','0','0','Meta','San Martin De Los Llanos',],
                    ['Brisas Del Manacacias','Caseta Comunal','1','37','34','1','0','0','Meta','San Martin De Los Llanos',],
                    ['El Merey','Esc. Simon Bolivar','1','37','266','1','0','0','Meta','San Martin De Los Llanos',],
                    ['Rincon De Bolivar','Esc. Cristobal Colon','1','37','22','1','0','0','Meta','San Martin De Los Llanos',],
                    ['Puesto Cabecera Municipal','Cr 7 No. 7-18 Barrio La Esperanza','8','24','2724','1','0','0','Meta','Uribe',],
                    ['Diviso','Caseta Comunal','2','24','556','1','0','0','Meta','Uribe',],
                    ['La Julia','Ie La Julia','5','24','1810','1','0','0','Meta','Uribe',],
                    ['Puesto Cabecera Municipal','Kr14#8 Eq Br.Esc.Vencedores D','23','38','8611','1','0','0','Meta','Vista Hermosa',],
                    ['Campoalegre','Esc. Campo Alegre','1','38','211','1','0','0','Meta','Vista Hermosa',],
                    ['Caño Amarillo','Esc. Caño Amarillo','3','38','974','1','0','0','Meta','Vista Hermosa',],
                    ['Maracaibo','Esc. Internado Maracaibo','2','38','400','1','0','0','Meta','Vista Hermosa',],
                    ['Piñalito','Inst. Educt. Gabriela Mistral','5','38','1653','1','0','0','Meta','Vista Hermosa',],
                    ['Puerto Esperanza','Esc. Puerto Esperanza','1','38','160','1','0','0','Meta','Vista Hermosa',],
                    ['Puerto Lucas','Esc. Puerto Lucas','2','38','660','1','0','0','Meta','Vista Hermosa',],
                ];
                break;
            }

            case mb_strtolower('LocationsAcaciasTableSeeder'):
            {
                // https://www.registraduria.gov.co/?page=divipol_delegaciones&delegacion=meta&id_rubrique=592&seccion=85&cod=52
                // name, address, tables, registraduria_location_id, electoral_potential, location_id, latitude, longitude, department, city
                $pollingStations = [
                    ['CONC. ESCOLAR GABRIELA MISTRAL', 'CALLE 13 NO 14-55 BARRIO CENTRO', '14', '1', '5084', '1', '3.985378', '-73.758022', 'Meta', 'Acacias', ],
                    ['I.E. ENRIQUE DANIELS', 'CARRERA 16 NO 18-50 BARRIO SAN CRISTOBAL', '11', '1', '3953', '1', '3.991363', '-73.758521', 'Meta', 'Acacias', ],
                    ['COL MPAL LUIS CARLOS GALAN SARMIENTO', 'CARRERA 12 NO 13-44 BARRIO JUAN MELLAO', '15', '1', '5580', '1', '3.985987', '-73.755662', 'Meta', 'Acacias', ],
                    ['COLEGIO MARIA MONTESORI', 'CARRERA 17A NO 9-49 BARRIO CIUDAD JARDIN', '11', '1', '3943', '1', '3.983293', '-73.761502', 'Meta', 'Acacias', ],
                    ['I. E. PABLO SEXTO', 'CRA. 6 # 16-11 BARRIO POPULAR', '14', '1', '5200', '1', '3.988649', '-73.751790', 'Meta', 'Acacias', ],
                    ['CONC ESC RAFAEL POMBO', 'CALLE 23 CARRERA 24 Y 25 BARRIO LA TIZA', '3', '1', '1003', '1', '3.998432', '-73.763303', 'Meta', 'Acacias', ],
                    ['IE JUAN HUMBERTO BAQUERO SOLER', 'CALLE 16A CARRERA 29 ESQUINA BARRIO EL BOSQUE', '22', '1', '8384', '1', '3.990234', '-73.770863', 'Meta', 'Acacias', ],
                    ['IE 20 DE JULIO', 'CALLE 11 NO 36B 11 BARRIO INDEPENDENCIA', '23', '1', '8486', '1', '3.985757', '-73.778874', 'Meta', 'Acacias', ],
                    ['IE LILIA CASTRO DE PARRADO', 'CALLE 14 CARRERA 44 Y 45 BARRIO LAS COLINAS', '12', '1', '4594', '1', '3.991753', '-73.781659', 'Meta', 'Acacias', ],
                    ['COLEGIO COFREM', 'CALLE 11 NO 24 44 BARRIO EL DORADO', '10', '1', '3675', '1', '3.984652', '-73.767434', 'Meta', 'Acacias', ],
                    ['ESC NORMAL SUP DE ACACIAS', 'AVENIDA 23 NO 41 50 VEREDA COLA DE PATO', '6', '1', '1939', '1', '3.987217', '-73.764662', 'Meta', 'Acacias', ],
                    ['GIMNASIO INTEG. JUAN PABLO II', 'CLL 15 No 22-57 B. COOPERATIVO', '0', '1', '0', '1', '3.988152', '-73.765538', 'Meta', 'Acacias', ],
                    ['COL. NACIONALIZADO PABLO EMILIO RIVEROS', 'CRA. 23 # 8 - 00', '0', '1', '0', '1', '3.981244', '-73.766641', 'Meta', 'Acacias', ],
                    ['COLISEO ANTIGUA NORMAL SUPERIOR', 'CL 14 # 21-32 B. COOPERATIVO', '8', '1', '6344', '1', '3.987126', '-73.764581', 'Meta', 'Acacias', ],
                    ['CARCEL', 'KM. 4 VIA VILLAVICENCIO', '1', '1', '193', '1', 'latitud', 'longitud', 'Meta', 'Acacias', ],
                    ['DINAMARCA', 'INS.EDUC. INSPECCION DINAMARCA', '4', '1', '1420', '1', '3.992553', '-73.748809', 'Meta', 'Acacias', ],
                    ['LA LOMA DE SAN JUAN', 'ESC. RURAL', '1', '1', '69', '1', '3.993358', '-73.575845', 'Meta', 'Acacias', ],
                    ['MANZANARES', 'ESC. RURAL', '1', '1', '87', '1', '4.140804', '-73.808977', 'Meta', 'Acacias', ],
                    ['SAN CRISTOBAL', 'ESC. RURAL', '1', '1', '37', '1', 'latitud', 'longitud', 'Meta', 'Acacias', ],
                    ['SAN ISIDRO DE CHICHIMENE', 'INS.EDUC. SAN ISIDRO DE CHICHIMENE', '4', '1', '1436', '1', '3.916644', '-73.687618', 'Meta', 'Acacias', ],
                    ['SAN JOSE DE LAS PALOMAS', 'ESC. SAN JOSE DE LAS PALOMAS', '1', '1', '301', '1', '3.952659', '-73.434052', 'Meta', 'Acacias', ],
                ];
                break;
            }

            case mb_strtolower('LocationsVillavicencioTableSeeder'):
            {
                // https://www.registraduria.gov.co/?page=divipol_delegaciones&delegacion=meta&id_rubrique=592&seccion=85&cod=52
                $pollingStations = [
                    ['vanguardia','Vanguardia','3',1,'2851',1,'4.170263','-73.621283','Meta','Villavicencio',],
                    ['inst. educativa 12 de octubre','Cl 46 # 50 Br.santa Josefa','9',1,'8603',1,'4.1588012','-73.6516658','Meta','Villavicencio',],
                    ['escuela la ceiba','Calle 30 Cra 15 Esquina','12',1,'11136',1,'4.144046','-73.616141','Meta','Villavicencio',],
                    ['esc nueva arnulfo briceño','Barrio San Antonio','5',1,'4029',1,'4.12339','-73.563155','Meta','Villavicencio',],
                    ['unidad educativa playa rica','Kr 54 # 21a-09','4',1,'3284',1,'4.107946','-73.661408','Meta','Villavicencio',],
                    ['carcel','Transv 26c No 22a 14 Antonio Ricaurt','1',1,'429',1,'4.145674','-73.62528','Meta','Villavicencio',],
                    ['col nuestra señora de la sabid','Calle 40 No 31 42 Centro','5',1,'4469',1,'4.143536','-73.640942','Meta','Villavicencio',],
                    ['terminal de transportes','Carrera 1 No 15 05 Anillo Vial','2',1,'1957',1,'4.131805','-73.604326','Meta','Villavicencio',],
                    ['col. gilberto alzate avendaño','Kr 30a Con Cl 5a','8',1,'7041',1,'4.122752','-73.627846','Meta','Villavicencio',],
                    ['puerto colombia','Puerto Colombia','1',1,'280',1,'0','0','Meta','Villavicencio',],
                    ['colegio santa ines','Calle 37a No 24 04 Barrio Santa Ines','5',1,'4777',1,'4.151695','-73.629204','Meta','Villavicencio',],
                    ['escuela marco antonio pinilla','Via Catama Br Marco Antonio Pinilla','5',1,'4434',1,'4.128362','-73.588303','Meta','Villavicencio',],
                    ['colegio catumare','Kr 46 Con Cl 18 Sur','5',1,'4639',1,'4.108779','-73.652856','Meta','Villavicencio',],
                    ['col luis carlos galan sarmient','Carrera 40 No 51 - 10 Sur Porfia','11',1,'10919',1,'4.083684','-73.666906','Meta','Villavicencio',],
                    ['unillanos sede san antonio','Cl 37 # 41-02 Sede San Antonio','2',1,'1154',1,'4.14566','-73.642491','Meta','Villavicencio',],
                    ['esc. isaac tacha la reliquia','Km 8 Via Catama','1',1,'614',1,'4.132986','-73.552376','Meta','Villavicencio',],
                    ['col. dptal la esperanza','Kr 45 # 13b-25','9',1,'8554',1,'4.131582','-73.634599','Meta','Villavicencio',],
                    ['santa maria la baja','Santa Maria La Baja','1',1,'470',1,'0','0','Meta','Villavicencio',],
                    ['escuela antonio ricaurte','Cr 24c No 26c 09 Barrio 20 De Julio','6',1,'5453',1,'4.145581','-73.627595','Meta','Villavicencio',],
                    ['villa olimpica','Carrera 19 Entre Calle 25 Y 22','1',1,'861',1,'4.138625','-73.619444','Meta','Villavicencio',],
                    ['escuela la rochela','Cl 20 Sur # 44-15 Br.rochela','7',1,'6265',1,'4.10251','-73.650217','Meta','Villavicencio',],
                    ['apiay','Apiay','3',1,'2735',1,'4.092177','-73.556342','Meta','Villavicencio',],
                    ['colegio antonio nariño','Cl L 45 # 39-40 B. Esmerald','7',1,'6109',1,'4.160174','-73.647816','Meta','Villavicencio',],
                    ['esc. atanasio girardot','Cl 40a # 17-28 E Br.covisan','5',1,'4271',1,'4.143817','-73.585543','Meta','Villavicencio',],
                    ['col miguel angel martin','Cr 25 No 15 - 60 Br Nuevo Ricaurte','5',1,'4903',1,'4.132114','-73.627662','Meta','Villavicencio',],
                    ['escuela las palmas porfia','Manzana 35 Lote 15 Y 16 Las Palmas','10',1,'9273',1,'4.073347','-73.670986','Meta','Villavicencio',],
                    ['pipiral','Pipiral','1',1,'450',1,'0','0','Meta','Villavicencio',],
                    ['ie siete de agosto','Calle 7 No 38 57 Siete De Agosto','6',1,'5511',1,'4.15','-73.633333','Meta','Villavicencio',],
                    ['colegio eduardo carranza','Calle 25 No 12a 175 Barrio Popular','7',1,'6199',1,'4.140086','-73.614381','Meta','Villavicencio',],
                    ['escuela los centauros','Kr 27 Y 28 Cl 9 La Rosita','3',1,'2269',1,'4.113574','-73.613173','Meta','Villavicencio',],
                    ['santa teresa','Santa Teresa','1',1,'310',1,'0','0','Meta','Villavicencio',],
                    ['colegio general santander','Transversal 25 # 39d-46','6',1,'5225',1,'4.1566617','-73.633698','Meta','Villavicencio',],
                    ['col. narciso matus torres','Kr 14 # 40b-02 Br.bastilla','11',1,'10501',1,'4.151328','-73.613145','Meta','Villavicencio',],
                    ['col. antony phiipps','Kr 37 Cl 22a Bis','7',1,'6181',1,'4.135612','-73.633322','Meta','Villavicencio',],
                    ['col juan b. caballero medina','Km 1 Nueva Via Bogota','4',1,'3863',1,'4.123378','-73.649553','Meta','Villavicencio',],
                    ['la concepcion','La Concepcion','3',1,'2004',1,'4.051362','-73.740981','Meta','Villavicencio',],
                    ['polideportivo villaflorez azotea','Polideportivo Azotea','2',1,'1455',1,'4.149461','-73.656289','Meta','Villavicencio',],
                    ['col. alberto lleras camargo','Cl 5a # 10a-21 Br.estero','9',1,'8084',1,'4.130084','-73.611949','Meta','Villavicencio',],
                    ['col. san francisco de asis','Cl 11 # 25-15 Br.cooperativo','1',1,'348',1,'4.126439','-73.626769','Meta','Villavicencio',],
                    ['rincon de pompeya','Rincon De Pompeya','2',1,'1501',1,'0','0','Meta','Villavicencio',],
                    ['colegio cofrem','Av.circunvalar19-55 Via Catama','1',1,'239',1,'4.14738','-73.620441','Meta','Villavicencio',],
                    ['escuela las gaviotas','Calle 23 Sur No 10 - 76 Las Gaviotas','8',1,'7111',1,'0','0','Meta','Villavicencio',],
                    ['escuela san jorge','Carrera 41 No. 20a Sur 03','4',1,'3204',1,'4.104516','-73.64948','Meta','Villavicencio',],
                    ['col de bachillerato femenino','Carrera 33 No 18 A 43','21',1,'20821',1,'4.13873','-73.629688','Meta','Villavicencio',],
                    ['escuela francisco miranda','Cl 35 # 29-09','7',1,'6129',1,'4.149406','-73.635321','Meta','Villavicencio',],
                    ['megacolegio rodolfo llinas','Barrio La Reliquia','3',1,'2391',1,'4.123113','-73.54172','Meta','Villavicencio',],
                    ['col. german arciniegas','Kr 47b # 7b-20','7',1,'6955',1,'4.127491','-73.637374','Meta','Villavicencio',],
                    ['alto de pompeya','Alto De Pompeya','2',1,'1477',1,'0','0','Meta','Villavicencio',],
                    ['hogar infantil 20 de julio','Cl 27 # 22c-39 Br.20 De Julio','5',1,'4416',1,'4.146918','-73.626091','Meta','Villavicencio',],
                    ['escuela san carlos','Mz B Casa 1 Br.san Carlos','6',1,'5615',1,'0','0','Meta','Villavicencio',],
                    ['academia militar jose antonio páez','Calle 24a No 40b - 27 Montecarlo','6',1,'5272',1,'4.101081','-73.650502','Meta','Villavicencio',],
                    ['megacolegio porfia','Calle 67 Sur Cra 47 Barrio Porfia','3',1,'2376',1,'4.076425','-73.673078','Meta','Villavicencio',],
                    ['santa rosa de rionegro','Santa Rosa De Rio Negro','1',1,'496',1,'0','0','Meta','Villavicencio',],
                    ['colegio la salle','Calle 39 No. 34 56 Centro','2',1,'1053',1,'4.150654','-73.641074','Meta','Villavicencio',],
                    ['colegio jorge eliecer gaitan','Calle 36 A No 17 - 0 Barrio Morichal','11',1,'10201',1,'4.138204','-73.586094','Meta','Villavicencio',],
                    ['i.e. la alborada','Kr 25 Cl 4B-08 Br.alborada','7',1,'6678',1,'4.122133','-73.621316','Meta','Villavicencio',],
                    ['servita','Servita','1',1,'353',1,'0','0','Meta','Villavicencio',],
                    ['colegio don bosco','Barrio El Buque Sector Buganbiles','8',1,'7504',1,'4.135257','-73.642514','Meta','Villavicencio',],
                    ['escuela las camelias','Cl 19 # 8-62 Br.camelias','6',1,'5698',1,'4.137158','-73.609234','Meta','Villavicencio',],
                    ['multifamiliares centauros','Calle 4 Sur No 35 - 94 Bloque C','2',1,'1848',1,'0','0','Meta','Villavicencio',],
                    ['cocuy','Cocuy','2',1,'1096',1,'0','0','Meta','Villavicencio',],
                    ['escuela policarpa salavarrieta','Cl 25 No 42 22 Barrio La Grama','5',1,'4968',1,'4.1585','-73.637851','Meta','Villavicencio',],
                    ['unidad basica 6 de abril','Cl 32 Con Kr 6','2',1,'1464',1,'4.144098','-73.607401','Meta','Villavicencio',],
                    ['col. abrahan lincoln','Cl 25 # 23-150','10',1,'9604',1,'4.142331','-73.625574','Meta','Villavicencio',],
                    ['bemposta','Frente Al Br.la Nora Via Acacias','3',1,'2283',1,'4.079599','-73.696598','Meta','Villavicencio',],
                    ['buenavista','Buenavista','1',1,'982',1,'0','0','Meta','Villavicencio',],
                    ['polideportivo la sabiduria','Calle 33 B No. 40-77, Barzal Alto','1',1,'481',1,'4.143744','-73.641063','Meta','Villavicencio',],
                    ['col. manuela beltran','Cl 25 # 6-115 Br.popular','11',1,'10263',1,'4.139708','-73.612241','Meta','Villavicencio',],
                    ['esc jhon f. kennedy','Cr 46 No 11 - 28 Esperanza','6',1,'5104',1,'4.129338','-73.635546','Meta','Villavicencio',],
                ];
                break;
            }
            
            case mb_strtolower('LocationsBarrancaDeUpiaTableSeeder'):
            {
                // https://www.registraduria.gov.co/?page=divipol_delegaciones&delegacion=meta&id_rubrique=592&seccion=85&cod=52
                $pollingStations = [
                    ['Puesto Cabecera Municipal','Cll 12 No 2-46 B. Centro Ie Fco Walter Sd Primaria','11','1','4210','1','0','0','Meta','Barranca De Upia',],
                    ['El Hijoa','Km 36 Via Cabuyaro - Ie Fco Walter','1','1','138','1','0','0','Meta','Barranca De Upia',],
                    ['Guaicaramo','Km 54 - Ie Fco Walter','1','1','76','1','0','0','Meta','Barranca De Upia',],
                    ['San Ignacio (Carutal)','Cra 4 No 4-42 - Ie Fco Walter','1','1','266','1','0','0','Meta','Barranca De Upia',],
                ];
                break;
            }

            case mb_strtolower('LocationsCabuyaroTableSeeder'):
            {
                // https://www.registraduria.gov.co/?page=divipol_delegaciones&delegacion=meta&id_rubrique=592&seccion=85&cod=52
                $pollingStations = [
                    ['Puesto Cabecera Municipal','Carrera 5 No 12 - 30','11','1','3849','1','0','0','Meta','Cabuyaro',],
                    ['Guayabal','I.E. El Yarico Sede Guayabal','1','1','245','1','0','0','Meta','Cabuyaro',],
                    ['Los Mangos','I.E. El Yarico Sede Naguayas','1','1','376','1','0','0','Meta','Cabuyaro',],
                    ['Viso De Upia','I.E.El Yarico Sede Viso De Upia','2','1','579','1','0','0','Meta','Cabuyaro',],                    
                ];
                break;
            }

            case mb_strtolower('LocationsCastillaLaNuevaTableSeeder'):
            {
                // https://www.registraduria.gov.co/?page=divipol_delegaciones&delegacion=meta&id_rubrique=592&seccion=85&cod=52
                $pollingStations = [
                    ['Puesto Cabecera Municipal','Inst. Educ Henry Daniels. Cr 8 No. 4-45  Centro','23','1','8798','1','0','0','Meta','Castilla La Nueva',],
                    ['San Lorenzo','Institucion Educativa San Lorenzo','6','1','1926','1','0','0','Meta','Castilla La Nueva',],
                    ['Arenales','Cent Educ Rur Castilla La Nueva. Sede El Progreso','2','1','557','1','0','0','Meta','Castilla La Nueva',],
                    ['El Toro','I.E.Castilla La Nueva Sede El Toro','1','1','223','1','0','0','Meta','Castilla La Nueva',],
                ];
                break;
            }

            case mb_strtolower('LocationsSanLuisDeCubarralTableSeeder'):
            {
                // https://www.registraduria.gov.co/?page=divipol_delegaciones&delegacion=meta&id_rubrique=592&seccion=85&cod=52
                $pollingStations = [
                    ['Puesto Cabecera Municipal','Cra 5 A  No 10 - 50','14','1','5144','1','0','0','Meta','Cubarral',],
                ];
                break;
            }

            case mb_strtolower('LocationsCumaralTableSeeder'):
            {
                // https://www.registraduria.gov.co/?page=divipol_delegaciones&delegacion=meta&id_rubrique=592&seccion=85&cod=52
                $pollingStations = [
                    ['Puesto Cabecera Municipal','Cr 20 No 8 - 40 Sede Jose Maria Cordoba','38','1','14456','1','0','0','Meta','Cumaral',],
                    ['Caney Medio','Centro Educativo Rural Jose Maria Guioth','1','1','336','1','0','0','Meta','Cumaral',],
                    ['El Caibe','Cent Educ Rur Jose Maria Guioth Sede La Esperanza','1','1','96','1','0','0','Meta','Cumaral',],
                    ['Guacavia','Inst Educativa Instituto Agricola De Guacavia','2','1','566','1','0','0','Meta','Cumaral',],
                    ['Montebello','Institucion Educativa San Isidro Sede Rancherias','1','1','66','1','0','0','Meta','Cumaral',],
                    ['Presentado','Ie Teniente Cruz Paredes Sede Presentado','1','1','218','1','0','0','Meta','Cumaral',],
                    ['Puerto Pecuca (Veracruz)','Institucion Educativa San Isidro - Veracruz','3','1','917','1','0','0','Meta','Cumaral',],
                    ['San Nicolas','Cent Educ Rur Jose Maria Guioth Sede Santa Lucia','1','1','298','1','0','0','Meta','Cumaral',],
                    ['Varsovia','Caseta Comunal','1','1','58','1','0','0','Meta','Cumaral',],                    
                ];
                break;
            }

            case mb_strtolower('LocationsElCalvarioTableSeeder'):
            {
                // https://www.registraduria.gov.co/?page=divipol_delegaciones&delegacion=meta&id_rubrique=592&seccion=85&cod=52
                $pollingStations = [
                    ['Puesto Cabecera Municipal','I.E. Juan Bautista Arnaud','2','1','743','1','0','0','Meta','El Calvario',],
                    ['Montfort','Centro De Salud Montfort','1','1','294','1','0','0','Meta','El Calvario',],
                    ['San Francisco','Col Simon Bolivar','2','1','477','1','0','0','Meta','El Calvario',],
                    ['San Pedro','Esc. Nueva San Pedro','1','1','48','1','0','0','Meta','El Calvario',],
                    ['San Rafael','Esc. San Rafael','1','1','67','1','0','0','Meta','El Calvario',],                    
                ];
                break;
            }

            case mb_strtolower('LocationsElCastilloTableSeeder'):
            {
                // https://www.registraduria.gov.co/?page=divipol_delegaciones&delegacion=meta&id_rubrique=592&seccion=85&cod=52
                $pollingStations = [
                    ['Puesto Cabecera Municipal','I.E Ovidio Decroly','11','1','3996','1','0','0','Meta','El Castillo',],
                    ['Medellin Del Ariari','Ie Ovidio Decroly Sd Jorge E Gaitan','4','1','1440','1','0','0','Meta','El Castillo',],
                    ['Miravalles','I.E El Encanto','1','1','229','1','0','0','Meta','El Castillo',],
                ];
                break;
            }

            case mb_strtolower('LocationsElDoradoTableSeeder'):
            {
                // https://www.registraduria.gov.co/?page=divipol_delegaciones&delegacion=meta&id_rubrique=592&seccion=85&cod=52
                $pollingStations = [
                    ['Puesto Cabecera Municipal','Cll 2 Cr 7 Y 8','6','1','2284','1','0','0','Meta','El Dorado',],
                    ['Pueblo Sanchez','Centro Poblado','1','1','242','1','0','0','Meta','El Dorado',],
                    ['San Isidro','Centro','2','1','570','1','0','0','Meta','El Dorado',],
                    ['Santa Rosa Alta','Vereda Santa Rosa','1','1','9','1','0','0','Meta','El Dorado',],                    
                ];
                break;
            }

            case mb_strtolower('LocationsFuenteDeOroTableSeeder'):
            {
                // https://www.registraduria.gov.co/?page=divipol_delegaciones&delegacion=meta&id_rubrique=592&seccion=85&cod=52
                $pollingStations = [
                    ['Puesto Cabecera Municipal','Cr 14 No 13 - 17','20','1','7417','1','0','0','Meta','Fuente De Oro',],
                    ['Union Del Ariari (Caño Blanco','Ie Unión Del Ariari','1','1','381','1','0','0','Meta','Fuente De Oro',],
                    ['Puerto Aljure','Salon Centro De Salud','1','1','242','1','0','0','Meta','Fuente De Oro',],
                    ['Puerto Limon','Unidad Educativa San Juan Bosco','2','1','420','1','0','0','Meta','Fuente De Oro',],
                    ['Puerto Santander','Ie Puerto Santander','2','1','473','1','0','0','Meta','Fuente De Oro',],
                    ['La Cooperativa','Unidad Educativa La Cooperativa','2','1','625','1','0','0','Meta','Fuente De Oro',],                    
                ];
                break;
            }

            case mb_strtolower('LocationsGranadaTableSeeder'):
            {
                // https://www.registraduria.gov.co/?page=divipol_delegaciones&delegacion=meta&id_rubrique=592&seccion=85&cod=52
                $pollingStations = [
                    ['Institucion Educativa Valentin','Cr 13 Cll 15 Esquina','26','1','9915','1','0','0','Meta','Granada',],
                    ['Escuela Club De Leones','Cr 14 Bis Cll 7 Esquina','9','1','3235','1','0','0','Meta','Granada',],
                    ['Institucion Educativa General','Cr 12 Cll 16 Esquina','17','1','6263','1','0','0','Meta','Granada',],
                    ['Complejo Deportivo Villa Olimpica','Calle 15 No. 6-102 Barrio Villa Olimpica','9','1','3177','1','0','0','Meta','Granada',],
                    ['Escuela El Bosque','Entre Mz 14 Y Mz 29','3','1','1005','1','0','0','Meta','Granada',],
                    ['I.E. Escuela Normal Superior Maria Auxil','Calle 23 Con Cra. 13 Esquina','12','1','4444','1','0','0','Meta','Granada',],
                    ['Escuela Alfonso Montoya Pava','Cll 24 Cr 9 Esquina','18','1','6725','1','0','0','Meta','Granada',],
                    ['Institucion Educativa Luis Car','Cr 9 No 32 - 05 Barrio El Porvenir','14','1','5058','1','0','0','Meta','Granada',],
                    ['Colegio Camilo Torres','Cll 15 No 9 - 185','8','1','6184','1','0','0','Meta','Granada',],
                    ['Carcel Municipal','Cr 14 No 14 - 25 Barrio Centro','1','1','140','1','0','0','Meta','Granada',],
                    ['Aguas Claras','I.E. Jose Antonio Galan','4','1','1206','1','0','0','Meta','Granada',],
                    ['Canaguaro','Esc. Luis Lopez De Mesa','5','1','1707','1','0','0','Meta','Granada',],
                    ['Dos Quebradas','I.E Tecnico Insdustrial Dos Quebradas','3','1','890','1','0','0','Meta','Granada',],
                    ['La Playa','Esc. Juan Xxiii','3','1','812','1','0','0','Meta','Granada',],
                    ['Puerto Caldas','Inst. Escolar Francisco. Jose De Caldas','4','1','1214','1','0','0','Meta','Granada',],                    
                ];
                break;
            }

            case mb_strtolower('LocationsGuamalTableSeeder'):
            {
                // https://www.registraduria.gov.co/?page=divipol_delegaciones&delegacion=meta&id_rubrique=592&seccion=85&cod=52
                $pollingStations = [
                    ['Puesto Cabecera Municipal','Calle 13 No. 9- 43 Barrio Los Fundadores','29','1','10953','1','0','0','Meta','Guamal',],
                    ['Humadea','C.E Rural De Guamal - Sede Nicolas De Federman','1','1','247','1','0','0','Meta','Guamal',],
                    ['Orotoy','C.E Rural De Guamal - Sede Orotoy Vereda Orotoy','1','1','229','1','0','0','Meta','Guamal',],                    
                ];
                break;
            }

            case mb_strtolower('LocationsLaMacarenaTableSeeder'):
            {
                // https://www.registraduria.gov.co/?page=divipol_delegaciones&delegacion=meta&id_rubrique=592&seccion=85&cod=52
                $pollingStations = [
                    ['Puesto Cabecera Municipal','Cl 9 No 5 - 49 Barrio Cristales','18','1','6533','1','0','0','Meta','La Macarena',],
                    ['Yari','I.E. Las Brisas- Poblado El Re','2','1','586','1','0','0','Meta','La Macarena',],
                    ['La Catalina','Esc. La Catalina','1','1','138','1','0','0','Meta','La Macarena',],
                    ['San Juan De Lozada','Esc. San Juan De Lozada','4','1','1187','1','0','0','Meta','La Macarena',],
                ];
                break;
            }

            case mb_strtolower('LocationsLejaniasTableSeeder'):
            {
                // https://www.registraduria.gov.co/?page=divipol_delegaciones&delegacion=meta&id_rubrique=592&seccion=85&cod=52
                $pollingStations = [
                    ['Puesto Cabecera Municipal','Cr 22 No 7 - 17 Barrio Modelo','17','1','6326','1','0','0','Meta','Lejanias',],
                    ['Angosturas Del Guape','Sede Educativaangostura Del Guape','1','1','127','1','0','0','Meta','Lejanias',],
                    ['Cacayal','I.E. Gabriela Mistral','4','1','1430','1','0','0','Meta','Lejanias',],
                    ['La Veinticuatro','Sede Educativa Santa Rita','1','1','224','1','0','0','Meta','Lejanias',],
                ];
                break;
            }

            case mb_strtolower('LocationsMapiripanTableSeeder'):
            {
                // https://www.registraduria.gov.co/?page=divipol_delegaciones&delegacion=meta&id_rubrique=592&seccion=85&cod=52
                $pollingStations = [
                    ['Puesto Cabecera Municipal','Esc.Jorge Eliecer Gaitan-La Lo','9','1','3171','1','0','0','Meta','Mapiripan',],
                    ['Buenos Aires','Caseta Comunal','1','1','174','1','0','0','Meta','Mapiripan',],
                    ['El Anzuelo','Caseta Comunal','1','1','113','1','0','0','Meta','Mapiripan',],
                    ['El Mielon','Caseta Comunal-El Melon','2','1','391','1','0','0','Meta','Mapiripan',],
                    ['La Cooperativa','Esc. El Rocio-La Cooperativa','1','1','354','1','0','0','Meta','Mapiripan',],
                    ['Las Guacamayas','Esc. La Independencia-Guacamay','1','1','225','1','0','0','Meta','Mapiripan',],
                    ['Puerto Alvira','Col Divino Niño Jesus-Puerto A','2','1','714','1','0','0','Meta','Mapiripan',],
                    ['Puerto Siare','Caseta Comunal','1','1','151','1','0','0','Meta','Mapiripan',],
                    ['Sardinata','Esc. Simon Bolivar-Sardinata','1','1','40','1','0','0','Meta','Mapiripan',],
                ];
                break;
            }

            case mb_strtolower('LocationsMesetasTableSeeder'):
            {
                // https://www.registraduria.gov.co/?page=divipol_delegaciones&delegacion=meta&id_rubrique=592&seccion=85&cod=52
                $pollingStations = [
                    ['Puesto Cabecera Municipal','I.E. Los Fundadores','19','1','6941','1','0','0','Meta','Mesetas',],
                    ['Brisas Del Duda','Caseta Comunal','2','1','711','1','0','0','Meta','Mesetas',],
                    ['El Mirador','Esc. El Mirador','1','1','34','1','0','0','Meta','Mesetas',],
                    ['Jardin De Las Peñas','Esc. Jardin De Las Peñas','3','1','851','1','0','0','Meta','Mesetas',],
                ];
                break;
            }

            case mb_strtolower('LocationsPuertoConcordiaTableSeeder'):
            {
                // https://www.registraduria.gov.co/?page=divipol_delegaciones&delegacion=meta&id_rubrique=592&seccion=85&cod=52
                $pollingStations = [
                    ['Puesto Cabecera Municipal','I.E. Nueva Esperanza Sede Primaria','15','1','5525','1','0','0','Meta','Puerto Concordia',],
                    ['Cruce De Pororio','Esc. La Vallenata','2','1','410','1','0','0','Meta','Puerto Concordia',],
                    ['La Tigra','Esc. Nueva Esperanza','1','1','1','1','0','0','Meta','Puerto Concordia',],
                    ['Lindenal','Esc. Lindenal','2','1','451','1','0','0','Meta','Puerto Concordia',],                    
                ];
                break;
            }

            case mb_strtolower('LocationsPuertoGaitanTableSeeder'):
            {
                // https://www.registraduria.gov.co/?page=divipol_delegaciones&delegacion=meta&id_rubrique=592&seccion=85&cod=52
                $pollingStations = [
                    ['I.E. Educativa Jorge Eliecer Gaitan','Cra. 10 No. 13D - 33 Barrio Manacacias','12','1','5368','1','0','0','Meta','Puerto Gaitan',],
                    ['I.E. J. Eliecer Gaitan Sd Camilo Torres','Cl 10 No. 15-03 Barrio Villa Ortiz','12','1','5374','1','0','0','Meta','Puerto Gaitan',],
                    ['I.E. Luis Carlos Galan Sarmiento','Cra. 3 No. 17-20 Barrio Popular','13','1','5894','1','0','0','Meta','Puerto Gaitan',],
                    ['I.E. L.C Galan Sarmiento - Sd L. A Perez','Cl 18 No. 7 -30 Barrio Galan','10','1','4513','1','0','0','Meta','Puerto Gaitan',],
                    ['El Porvenir','Esc. La Sabana','2','1','455','1','0','0','Meta','Puerto Gaitan',],
                    ['La Cristalina','Esc. Cristalina','3','1','1139','1','0','0','Meta','Puerto Gaitan',],
                    ['Planas','Esc. Primavera','5','1','1607','1','0','0','Meta','Puerto Gaitan',],
                    ['Puente Arimena','Esc. Gregorio Garavito','1','1','236','1','0','0','Meta','Puerto Gaitan',],
                    ['Puerto Trujillo','Esc. Puerto Trujillo','2','1','493','1','0','0','Meta','Puerto Gaitan',],
                    ['Rubiales Puerto Triunfo','Esc Nueva Rubiales','5','1','1715','1','0','0','Meta','Puerto Gaitan',],
                    ['San Miguel O Pescadero','Esc. San Miguel','2','1','425','1','0','0','Meta','Puerto Gaitan',],
                    ['San Pedro De Arimena','Esc. General Santander','1','1','242','1','0','0','Meta','Puerto Gaitan',],
                    ['Tillava','Esc. Alto Tillava','3','1','1038','1','0','0','Meta','Puerto Gaitan',],                    
                ];
                break;
            }

            case mb_strtolower('LocationsPuertoLlerasTableSeeder'):
            {
                // https://www.registraduria.gov.co/?page=divipol_delegaciones&delegacion=meta&id_rubrique=592&seccion=85&cod=52
                $pollingStations = [
                    ['Puesto Cabecera Municipal','Colegio Inema Cl 7 Kra 12 Br.G','13','1','4677','1','0','0','Meta','Puerto Lleras',],
                    ['Casibare','C.E.San Fernado Ctro Poblado','2','1','443','1','0','0','Meta','Puerto Lleras',],
                    ['La Union','La Union Caseta Comunal Ctro P','2','1','607','1','0','0','Meta','Puerto Lleras',],
                    ['Tierra Grata','Ctro Educativo Tierra Grata Ct','1','1','381','1','0','0','Meta','Puerto Lleras',],
                    ['Villa De La Paz','Esc. Ntra Sra De La Paz Ctro P','2','1','389','1','0','0','Meta','Puerto Lleras',],                    
                ];
                break;
            }

            case mb_strtolower('LocationsPuertoLopezTableSeeder'):
            {
                // https://www.registraduria.gov.co/?page=divipol_delegaciones&delegacion=meta&id_rubrique=592&seccion=85&cod=52
                $pollingStations = [
                    ['Puesto Cabecera Municipal','Cll 7 Cr 9-10 I E Rafael Uribe Uribe','47','1','17787','1','0','0','Meta','Puerto Lopez',],
                    ['Bocas Del Guayuriba','Esc Bocas Del Guayuriba','1','1','306','1','0','0','Meta','Puerto Lopez',],
                    ['Altamira','Esc Antonia Santos','1','1','100','1','0','0','Meta','Puerto Lopez',],
                    ['Chaviva','I E Yaaliakeisy','4','1','1235','1','0','0','Meta','Puerto Lopez',],
                    ['El Tigre','Intern Simon Bolivar','1','1','283','1','0','0','Meta','Puerto Lopez',],
                    ['Puerto Guadalupe (Cumaralito)','I E Puerto Guadalupe','3','1','839','1','0','0','Meta','Puerto Lopez',],
                    ['La Balsa','I E Ponton De Rio Negro','2','1','403','1','0','0','Meta','Puerto Lopez',],
                    ['Melúa','Ce Serrania Del Melua','2','1','403','1','0','0','Meta','Puerto Lopez',],
                    ['Pachaquiaro','I E Santa Teresa','5','1','1801','1','0','0','Meta','Puerto Lopez',],
                    ['Puerto Porfia','I E Puerto Porfia','2','1','398','1','0','0','Meta','Puerto Lopez',],
                    ['Remolino','I E Remolino','3','1','818','1','0','0','Meta','Puerto Lopez',],                    
                ];
                break;
            }

            case mb_strtolower('LocationsPuertoRicoTableSeeder'):
            {
                // https://www.registraduria.gov.co/?page=divipol_delegaciones&delegacion=meta&id_rubrique=592&seccion=85&cod=52
                $pollingStations = [
                    ['Puesto Cabecera Municipal','Unidad Educativa Urbana','20','1','7481','1','0','0','Meta','Puerto Rico',],
                    ['Barranco Colorado','Caseta Comunal','2','1','416','1','0','0','Meta','Puerto Rico',],
                    ['Lindosa','Caseta Comunal','1','1','66','1','0','0','Meta','Puerto Rico',],
                    ['Puerto Chispas','Caseta Comunal','1','1','236','1','0','0','Meta','Puerto Rico',],
                    ['Puerto Toledo','Caseta Comunal','2','1','511','1','0','0','Meta','Puerto Rico',],                    
                ];
                break;
            }

            case mb_strtolower('LocationsRestrepoTableSeeder'):
            {
                // https://www.registraduria.gov.co/?page=divipol_delegaciones&delegacion=meta&id_rubrique=592&seccion=85&cod=52
                $pollingStations = [
                    ['Puesto Cabecera Municipal','Carrera 8 No. 8  50 Centro','43','1','16393','1','0','0','Meta','Restrepo',],
                ];
                break;
            }

            case mb_strtolower('LocationsSanCarlosDeGuaroaTableSeeder'):
            {
                // https://www.registraduria.gov.co/?page=divipol_delegaciones&delegacion=meta&id_rubrique=592&seccion=85&cod=52
                $pollingStations = [
                    ['Puesto Cabecera Municipal','Cll 5 Cr 4 Esq.I.E Gabriel Garcia Marquez Sec. Bto','14','1','5224','1','0','0','Meta','San Carlos De Guaroa',],
                    ['Rincon De Pajure','Esc. Divino Niño','1','1','358','1','0','0','Meta','San Carlos De Guaroa',],
                    ['La Palmera','I.E. Simón Bólivar','5','1','1811','1','0','0','Meta','San Carlos De Guaroa',],
                    ['Surimena','I.E. Isabel La Catolica','3','1','1123','1','0','0','Meta','San Carlos De Guaroa',],                    
                ];
                break;
            }

            case mb_strtolower('LocationsSanJuanDeAramaTableSeeder'):
            {
                // https://www.registraduria.gov.co/?page=divipol_delegaciones&delegacion=meta&id_rubrique=592&seccion=85&cod=52
                $pollingStations = [
                    ['Puesto Cabecera Municipal','Tr 12 No 10-12','13','1','4777','1','0','0','Meta','San Juan De Arama',],
                    ['Cerritos','Esc. Nueva Cerritos','1','1','274','1','0','0','Meta','San Juan De Arama',],
                    ['Costa Rica','Vereda Nuevo Progreso Caseta Comunal','1','1','272','1','0','0','Meta','San Juan De Arama',],
                    ['El Vergel','Esc. Francisco De Paula Santander','1','1','291','1','0','0','Meta','San Juan De Arama',],
                    ['Mesa De Fernandez','I.E. Mesa De Fernandez','1','1','185','1','0','0','Meta','San Juan De Arama',],
                    ['Miraflores','Col Dptal Agropecuario Miraflores','1','1','171','1','0','0','Meta','San Juan De Arama',],
                    ['Peñas Blancas','Caseta Comunal Peñas Blancas','1','1','142','1','0','0','Meta','San Juan De Arama',],                    
                ];
                break;
            }

            case mb_strtolower('LocationsSanJuanitoTableSeeder'):
            {
                // https://www.registraduria.gov.co/?page=divipol_delegaciones&delegacion=meta&id_rubrique=592&seccion=85&cod=52
                $pollingStations = [
                    ['Puesto Cabecera Municipal','Col.Jhon F. Kennedy Cr 3 No. 11 - 01 Barrio Centro','3','1','1039','1','0','0','Meta','San Juanito',],
                    ['La Candelaria','Escuela Candelaria','1','1','110','1','0','0','Meta','San Juanito',],
                    ['San Jose','Escuela  San Jose','1','1','111','1','0','0','Meta','San Juanito',],
                    ['San Luis Del Plan','Escuela San Luis Del Plan','1','1','74','1','0','0','Meta','San Juanito',],                    
                ];
                break;
            }

            case mb_strtolower('LocationsSanMartinTableSeeder'):
            {
                // https://www.registraduria.gov.co/?page=divipol_delegaciones&delegacion=meta&id_rubrique=592&seccion=85&cod=52
                $pollingStations = [
                    ['Puesto Cabecera Municipal','Esc. Raul De Oliveira','46','1','17663','1','0','0','Meta','San Martin De Los Llanos',],
                    ['Alto De Iraca','Esc. El Bambu','1','1','115','1','0','0','Meta','San Martin De Los Llanos',],
                    ['Bajo Camoa','Esc. Gabriel Garcia Marquez','1','1','141','1','0','0','Meta','San Martin De Los Llanos',],
                    ['Brisas Del Manacacias','Caseta Comunal','1','1','34','1','0','0','Meta','San Martin De Los Llanos',],
                    ['El Merey','Esc. Simon Bolivar','1','1','266','1','0','0','Meta','San Martin De Los Llanos',],
                    ['Rincon De Bolivar','Esc. Cristobal Colon','1','1','22','1','0','0','Meta','San Martin De Los Llanos',],                    
                ];
                break;
            }

            case mb_strtolower('LocationsUribeTableSeeder'):
            {
                // https://www.registraduria.gov.co/?page=divipol_delegaciones&delegacion=meta&id_rubrique=592&seccion=85&cod=52
                $pollingStations = [
                    ['Puesto Cabecera Municipal','Cr 7 No. 7-18 Barrio La Esperanza','8','1','2724','1','0','0','Meta','Uribe',],
                    ['Diviso','Caseta Comunal','2','1','556','1','0','0','Meta','Uribe',],
                    ['La Julia','Ie La Julia','5','1','1810','1','0','0','Meta','Uribe',],                    
                ];
                break;
            }

            case mb_strtolower('LocationsVistaHermosaTableSeeder'):
            {
                // https://www.registraduria.gov.co/?page=divipol_delegaciones&delegacion=meta&id_rubrique=592&seccion=85&cod=52
                $pollingStations = [
                    ['Puesto Cabecera Municipal','Kr14#8 Eq Br.Esc.Vencedores D','23','1','8611','1','0','0','Meta','Vista Hermosa',],
                    ['Campoalegre','Esc. Campo Alegre','1','1','211','1','0','0','Meta','Vista Hermosa',],
                    ['Caño Amarillo','Esc. Caño Amarillo','3','1','974','1','0','0','Meta','Vista Hermosa',],
                    ['Maracaibo','Esc. Internado Maracaibo','2','1','400','1','0','0','Meta','Vista Hermosa',],
                    ['Piñalito','Inst. Educt. Gabriela Mistral','5','1','1653','1','0','0','Meta','Vista Hermosa',],
                    ['Puerto Esperanza','Esc. Puerto Esperanza','1','1','160','1','0','0','Meta','Vista Hermosa',],
                    ['Puerto Lucas','Esc. Puerto Lucas','2','1','660','1','0','0','Meta','Vista Hermosa',],                    
                ];
                break;
            }

            default:
            {
                break;
            }
        };
        $text = isset($pollingStations)?sizeof($pollingStations):0;
        echo "- " . mb_strtolower($location) . " = " . $text . " elementos.\r\n"; 
        return $pollingStations;
    }

    public function run()
    {
        $location = Campaing::getLocationsSeeder();
        $pollingStations = (array)null;

        if (mb_strtolower($location) == "locationsmetatableseeder")
        {
            $cities = [
                'Cabuyaro',
                'Cumaral',
                'Granada',
                'Guamal',
                'Mesetas',
                'Restrepo',
                'PuertoGaitan',
                'BarrancadeUpia',
                'Lejanias',
                'SanMartin',
                'Uribe',
                'Villavicencio',
                'Vistahermosa',
                'PuertoConcordia',
                'CastillalaNueva',
                'LaMacarena',
                'Acacias',
                'Mapiripan',
                'SanJuanito',
                'PuertoRico',
                'SanLuisdeCubarral',
                'PuertoLleras',
                'FuentedeOro',
                'SanJuandeArama',
                'SanCarlosdeGuaroa',
                'ElDorado',
                'ElCalvario',
                'ElCastillo',
                'PuertoLopez',
            ];
            foreach ($cities as $city)
            {
                $currentCity = 'locations' . mb_strtolower($city) . 'tableseeder';
                $pollingStations = array_merge($pollingStations, $this->getPollingStations($currentCity));
            }
        }
        else {
            $pollingStations = $this->getPollingStations($location);
        }
        $this->addPollingStation($pollingStations);
    }

    public function addPollingStation($pollingStations)
    {
        foreach($pollingStations as $pollingStation)
        {
            PollingStation::create([
                'name' => $pollingStation[0],
                'address' => $pollingStation[1],
                'tables' => $pollingStation[2],
                'registraduria_location_id' => $pollingStation[3],
                'electoral_potential' => $pollingStation[4],
                'location_id' => $pollingStation[5],
                'latitude' => $pollingStation[6],
                'longitude' => $pollingStation[7],
				'department' => $pollingStation[8],
				'city' => $pollingStation[9],
            ]);
        }
    }
}

?>