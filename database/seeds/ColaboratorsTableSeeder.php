<?php 

use \Illuminate\Database\Seeder;
use App\Entities\Voter;
use App\Libraries\Campaing;

class ColaboratorsTableSeeder extends Seeder
{
    public function run()
    {
        Voter::create([
        	'doc'               => Campaing::getCandidateDoc(),
            'name' 		        => Campaing::getCandidateName(),
            'location_id'       => 1,
            'colaborator'       => true,
            'sex'               => 'M',
            'delegate'          => true,
            'created_at'        => new DateTime,
            'updated_at'        => new DateTime 
        ]);
    }
}

?>