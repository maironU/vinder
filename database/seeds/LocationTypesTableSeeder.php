<?php 

use \Illuminate\Database\Seeder;
use App\Entities\LocationType;

class LocationTypesTableSeeder extends Seeder
{
    public function run()
    {
        // id, name, superior
        $locationTypes = [
            ['1', 'departamento', null],
            ['2', 'municipio', 1],
            ['3', 'comuna', 2],
            ['4', 'barrio', 3],
            ['5', 'corregimiento', 2],
            ['6', 'vereda', 2],
            ['7', 'caserío', 2],
            ['8', 'inspección de policía', 2],
        ];

        foreach($locationTypes as $locationType)
        {
            LocationType::create([
                'id' => $locationType[0],
                'name' => $locationType[1],
                'description' => $locationType[1],
                'superior' => $locationType[2],
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime 
            ]);    
        }
    }
}

?>