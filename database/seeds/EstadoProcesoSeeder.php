<?php

use App\EstadoProceso;
use Illuminate\Database\Seeder;

class EstadoProcesoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EstadoProceso::create([
            "id" => 1,
            "nombre" => "En ejecución",
            "slug" => "en-ejecucion",
        ]);
        EstadoProceso::create([
            "id" => 2,
            "nombre" => "En proceso",
            "slug" => "en-proceso",
        ]);
        EstadoProceso::create([
            "id" => 3,
            "nombre" => "Finalizado",
            "slug" => "finalizado",
        ]);
        EstadoProceso::create([
            "id" => 4,
            "nombre" => "Ocurrió error",
            "slug" => "ocurrio-error",
        ]);
    }
}
