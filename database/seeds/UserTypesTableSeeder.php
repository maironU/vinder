<?php 

use \Illuminate\Database\Seeder;
use App\Entities\UserType;

class UserTypesTableSeeder extends Seeder
{
    public function run()
    {
    	UserType::create([
    		'name'    => 'Súper Administrador',
            'system'  => 1 
    	]);

    	UserType::create([
    		'name' => 'Administrador de Campaña'
    	]);

    	UserType::create([
    		'name' => 'Digitador'
    	]);

        UserType::create([
            'name' => 'Invitado'
		]);
		
		UserType::create([
            'name' => 'Testigo electoral'
        ]);

		UserType::create([
            'name' => 'Lider'
        ]);

		UserType::create([
            'name' => 'Resultados día D'
        ]);

		UserType::create([
            'name' => 'Coordinador de puestos'
        ]);

        UserType::create([
            'name' => 'Coordinador de zona'
        ]);
    }
}

?>