<?php 

use \Illuminate\Database\Seeder;
use App\Entities\Location;

class LocationsPuertoGaitanTableSeeder extends Seeder
{
    public function run()
    {
        // id, name, type_id, superior, electoral_potential
        $locations = [
            [1, 'Puerto Gaitán', 2, null, 51288],
        ];

        foreach($locations as $location)
        {
            Location::create([
                'id'                    => $location[0],
                'name'                  => $location[1],
                'type_id'               => $location[2],
                'superior'              => $location[3],
                'electoral_potential'   => $location[4],
                'created_at'            => new DateTime,
                'updated_at'            => new DateTime 
            ]);
        };
    }
}

?>