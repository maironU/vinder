<?php

use \Illuminate\Database\Seeder;
use App\Entities\Location;

class LocationsPuertoLopezTableSeeder extends Seeder
{
    public static $locations = [
        ['name' => 'Meta', 'type' => 1, 'superior' => null],
        ['name' => 'Puerto López', 'type' => 2, 'superior' => 1],
        ['name' => 'La balsa', 'type' => 8, 'superior' => 2],
        ['name' => 'Pachaquiaro', 'type' => 8, 'superior' => 2],
        ['name' => 'Altamira', 'type' => 8, 'superior' => 2],
        ['name' => 'Puerto Guadalupe', 'type' => 8, 'superior' => 2],
        ['name' => 'Puerto Porfía', 'type' => 8, 'superior' => 2],
        ['name' => 'Remolino', 'type' => 8, 'superior' => 2],
        ['name' => 'Bocas del Guayuriba', 'type' => 7, 'superior' => 2],
        ['name' => 'Güichiral', 'type' => 7, 'superior' => 2],
        ['name' => 'Chaviva', 'type' => 8, 'superior' => 2],
        ['name' => 'El tigre', 'type' => 8, 'superior' => 2],
    ];

    public function run()
    {
        $locations = self::$locations;

        foreach ($locations as $location) {
            Location::create([
                'name'      => $location['name'],
                'type_id'   => $location['type'],
                'superior'  => $location['superior'],
                'electoral_potential'   => 0,
                'created_at'    => new DateTime,
                'updated_at'    => new DateTime
            ]);
        }
    }
}

?>