<?php 

use \Illuminate\Database\Seeder;
use App\Entities\User;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        User::create([
            'name'          => 'Súper Administrador',
            'username'      => 'superadmin',
            'email'         => 'superadmin@vinder.info',
            'password'      => 'demo',
            'type_id'       => 1,
            'created_at'    => new DateTime,
            'updated_at'    => new DateTime 
        ]);

        User::create([
            'name'          => 'Administrador de Campaña',
            'username'      => 'admin',
            'email'         => 'admin@vinder.info',
            'password'      => 'demo',
            'type_id'       => 2,
            'created_at'    => new DateTime,
            'updated_at'    => new DateTime 
        ]);

        User::create([
            'name'          => 'Digitador',
            'username'      => 'digitador',
            'email'         => 'digitador@vinder.info',
            'password'      => 'demo',
            'type_id'       => 3,
            'created_at'    => new DateTime,
            'updated_at'    => new DateTime 
        ]);

        User::create([
            'name'          => 'Invitado',
            'username'      => 'invitado',
            'email'         => 'invitado@vinder.info',
            'password'      => 'demo',
            'type_id'       => 4,
            'created_at'    => new DateTime,
            'updated_at'    => new DateTime 
        ]);

        User::create([
            'name'          => 'Testigo',
            'username'      => 'testigo',
            'email'         => 'testigo@vinder.info',
            'password'      => 'demo',
            'type_id'       => 5,
            'created_at'    => new DateTime,
            'updated_at'    => new DateTime 
        ]);
    }
}

?>