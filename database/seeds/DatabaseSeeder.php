<?php

use \Illuminate\Database\Seeder;
use App\Libraries\Campaing;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		
		Eloquent::unguard();

        $this->call('LocationTypesTableSeeder'); // Static
		$this->call('LocationsVillavicencioTableSeeder');
		/*$locations = Campaing::getLocationsSeeder() ? explode(',', Campaing::getLocationsSeeder()) : [];
		
		foreach($locations as $location){
			if(class_exists($location)){
				$this->call($location); // Dinamic
			}
		}*/
        //$this->call(Campaing::getLocationsSeeder()); // Dinamic

        $this->call('OccupationsTableSeeder'); // Static
		$this->call('UserTypesTableSeeder'); // Static
		$this->call('ModulesTableSeeder'); // Static 
		$this->call('UserTypesHasModulesTableSeeder'); // Static
		$this->call('UsersTableSeeder'); // Static
		//$this->call('ColaboratorsTableSeeder'); //Static
		$this->call('RolesTableSeeder'); // Static
		$this->call('PollingStationsTableSeeder');
		$this->call('ProcesoSeeder');
		$this->call('EstadoProcesoSeeder');


		if(Campaing::isDemo()) // Only Demo
		{
	        $this->call('CommunitiesTableSeeder'); // Only Demo
			//$this->call('VotersTableSeeder'); // Only Demo
		}
		
	}

}

/**
* 
*/
