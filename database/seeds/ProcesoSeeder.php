<?php

use App\Proceso;
use Illuminate\Database\Seeder;

class ProcesoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Proceso::create([
            "id" => 1,
            "name" => "Actualizar Votantes",
            "slug" => "votantes",
            "activo" => 1,
            "descripcion" => "Crea o actualiza los votantes, es necesario primero actualizar coordinadores y orientadores"
        ]);
        Proceso::create([
            "id" => 2,
            "name" => "Actualizar Coordinadores",
            "slug" => "coordinadores",
            "activo" => 1,
            "descripcion" => "Crea o actualiza los coordinadores"
        ]);
        Proceso::create([
            "id" => 3,
            "name" => "Actualizar Orientadores",
            "slug" => "orientadores",
            "activo" => 1,
            "descripcion" => "Crea o actualiza los orientadores"
        ]);
        Proceso::create([
            "id" => 4,
            "name" => "Actualizar Testigos Electorales",
            "slug" => "testigos",
            "activo" => 1,
            "descripcion" => "Crea o actualiza los testigos electorales"
        ]);
        Proceso::create([
            "id" => 5,
            "name" => "Actualizar Mesas de Votación",
            "slug" => "mesas",
            "activo" => 1,
            "descripcion" => "Crea o actualiza las mesas de votación"
        ]);
        Proceso::create([
            "id" => 6,
            "name" => "Actualizar Ciudades",
            "slug" => "ciudades",
            "activo" => 1,
            "descripcion" => "Crea o actualiza las ciudades y sus municipios"
        ]);
        Proceso::create([
            "id" => 7,
            "name" => "Testigos Solo Cédula Y Celular",
            "slug" => "testigoscedula",
            "activo" => 1,
            "descripcion" => "Crea o actualiza las los testigos respecto a la tabla votersnew"
        ]);
    }
}
