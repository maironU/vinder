<?php 

use \Illuminate\Database\Seeder;
use App\Entities\Voter;

class VotersTableSeeder extends Seeder
{
    public function run()
    {
    	$faker = Faker\Factory::create('es_ES');
 		
 		/* Team */ 		
		for ($i = 0; $i < 30; $i++)
		{
			$tel = $faker->phoneNumber(false);
			$tel2 = ($faker->numberBetween(0,1) == 1 ? $faker->phoneNumber(false) : '');

			Voter::create([
	        	'doc'	=> 1121895026 + $i,
	            'name' => $faker->name,
	            'telephone'	=> $tel,
	            'email'	=> $faker->email,
	            'address'	=> $faker->streetName,
	            'location_id'	=> $faker->numberBetween(2, 38),
	            'sex'	=> $faker->randomElement(['M', 'F']),
	            'colaborator'	=> true,
	            'superior'	=> $faker->numberBetween(1, $i + 1),
	            'polling_station_id'	=> $faker->numberBetween(1, 15),
	            'created_by'	=> 2,
	            'created_at'    => new DateTime,
	            'updated_at'    => new DateTime,
				'whatsapp' 	=> $tel,
				'facebook_profile'	=> $faker->userName,
				'instagram_profile'	=> $faker->userName,
				'date_of_birth' => $faker->dateTimeBetween($startDate = '-70 years', $endDate = '-18 years', $timezone = null),
				'table_number' => $faker->numberBetween(1,10),
				'description' => $faker->realText($maxNbChars = 200, $indexSize = 2),
				'telephone_alternative' => $tel2,
				'occupation' => $faker->numberBetween(1,56),
				'semaphore' => $faker->randomElement([0,1,2,3]),
	        ]);
	    }
	    
	    /* Voters */
		for ($i = 0; $i < 200; $i++)
		{
			$tel = $faker->phoneNumber();
			$tel2 = ($faker->numberBetween(0,1) == 1 ? $faker->phoneNumber() : '');

	        Voter::create([
	        	'doc'	=> 1121895126 + $i,
	            'name' => $faker->name,
	            'telephone'	=> $tel,
	            'email'	=> $faker->email,
	            'address'	=> $faker->streetName,
	            'location_id'	=> $faker->numberBetween(2, 38),
	            'sex'	=> $faker->randomElement(['M', 'F']),
	            'colaborator'	=> false,
	            'superior'	=> $faker->numberBetween(2, 29),
	            'polling_station_id'	=> $faker->numberBetween(1, 15),
	            'created_by'	=> 3,
	            'created_at'    => new DateTime,
	            'updated_at'    => new DateTime,
				'whatsapp' 	=> $tel,
				'facebook_profile'	=> $faker->userName,
				'instagram_profile'	=> $faker->userName,
				'date_of_birth' => $faker->dateTimeBetween($startDate = '-70 years', $endDate = '-18 years', $timezone = null),
				'table_number' => $faker->numberBetween(1,10),
				'description' => $faker->realText($maxNbChars = 200, $indexSize = 2),
				'telephone_alternative' => $tel2,
				'occupation' => $faker->numberBetween(1,56),
	        ]);
	    }
	}
	



}

?>