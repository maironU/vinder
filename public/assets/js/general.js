function filter(){
    let url = location.protocol + '//' + location.host + location.pathname + "?"

    let search = $("#search").val()
    let polling_station = $("#polling_station").val()
    let testigo = $("#testigo").val()
    let bandera = $("#bandera").val()
    let zone = $("#zone").val()

    if(search){
        url+= "search=" + search + "&"
    }

    if(bandera){
        url+= "bandera=" + bandera + "&"
    }

    if(polling_station){
        url+= "polling_station=" + polling_station + "&"
    }

    if(testigo){
        url+= "testigo=" + testigo + "&"
    }

    if(Array.isArray(zone)){
        url+= "zone=" + JSON.stringify(zone)
    }
    location.href=url
}

function emptyFilter(field){
    let url = location.protocol + '//' + location.host + location.pathname + "?"
    let search = $("#search").val()
    let zone = null;
    let polling_station = null;
    let testigo = null;
    let bandera = null

    if(field != "polling_station"){
        polling_station = $("#polling_station").val();
        if(polling_station){
            url+= "polling_station=" + polling_station + "&"
        }
    }

    if(field != "bandera"){
        bandera = $("#bandera").val()
        if(bandera){
            url+= "bandera=" + bandera + "&"
        }
    }


    if(field != "testigo"){
        testigo = $("#testigo").val()
        if(testigo){
            url+= "testigo=" + testigo + "&"
        }
    }

    if(field != "zone"){
        zone = $("#zone").val();

        if(Array.isArray(zone)){
            url+= "zone=" + JSON.stringify(zone) + "&"
        }
    }

    if(search){
        url+= "search=" + search + "&"
    }
    location.href=url
}

function refresh(){
    location.reload()
}

const check = (input) => {
    if (!input.validity.valid) input.value = null;
    if (+input.value < 0) input.value = null;
};
  