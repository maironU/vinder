let order = null;

const getCellValue = (tr, idx) => tr.children[idx].innerText || tr.children[idx].textContent;

const comparer = (idx, asc) => (a, b) => ((v1, v2) => 
    v1 !== '' && v2 !== '' && !isNaN(v1) && !isNaN(v2) ? v1 - v2 : v1.toString().localeCompare(v2)
    )(getCellValue(asc ? a : b, idx), getCellValue(asc ? b : a, idx));


function orderTable(elem){
    changeImageOrder(elem)
    let th = elem
    const tbody = document.getElementById('tbody');
    console.log(tbody.querySelectorAll('tr'))
    Array.from(tbody.querySelectorAll('tr'))
        .sort(comparer(Array.from(th.parentNode.children).indexOf(th), this.asc = !this.asc))
        .forEach(tr => tbody.appendChild(tr) );
}

function changeImageOrder(elem){
    document.querySelectorAll('.parent-images-order').forEach(parent => {
        parent.children[0].src = '/images/arrow-top-black.png'
        parent.children[0].style.width = "20px"

        parent.children[1].src = '/images/arrow-down-black.png'
        parent.children[1].style.width = "20px"
    })

    let parent_images = elem.children[0].children[1];
    if(!order){
        parent_images.children[0].src = "/images/arrow-top-blue.png";
        parent_images.children[0].style.width = "25px"

        parent_images.children[1].src = "/images/arrow-down-black.png";
        parent_images.children[1].style.width = "20px"
        order = "asc"
    }else if(order == "asc"){
        parent_images.children[1].src = "/images/arrow-down-blue.png";
        parent_images.children[1].style.width = "25px"

        parent_images.children[0].src = "/images/arrow-top-black.png";
        parent_images.children[0].style.width = "20px"
        order = "desc"
    }else{
        parent_images.children[0].src = "/images/arrow-top-blue.png";
        parent_images.children[0].style.width = "25px"

        parent_images.children[1].src = "/images/arrow-down-black.png";
        parent_images.children[1].style.width = "20px"
        order = "asc"
    }
}