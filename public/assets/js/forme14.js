let fileLogo = null
let fileForm = null
let fileFormSecondary = null
let position_candidate = 9999999;
let updated_upload_form = false;
let updated_upload_form_secondary = false;

let load_image1 = false;
let load_image2 = false;

function saveFormE14() {
    let forme14_info = getInfo()
    let forme14_candidates = getInfoVote()
    let name = $("#name-form").val()
    let total_votes = $("#total-votes").val()

    let data = {
        forme14_info,
        forme14_candidates,
        file: fileLogo,
        fileForm: fileForm,
        fileFormSecondary: fileFormSecondary,
        name,
        total_votes
    }

    if (name == "" || forme14_info.length == 0 || data.forme14_candidates.length == 0) {
        return alert("Por favor llene los campos")
    }
    if(!someCheckCandidateCreate()){
        return alert("Por favor marca el candidato")
    }
    fetchFormE14(data)
}

function checkCandidate(id){
    let checks = $(".check-hijos")

    checks.each(function(){
        $(this).prop('checked', false)
    })

    $("#check-"+id).prop("checked", true)
}

function checkCandidateCreate(e){
    let checks = $(".check-hijos-create")

    checks.each(function(){
        $(this).prop('checked', false)
    })

    e.checked = true
}

function someCheckCandidate(){
    let checks = $(".check-hijos")
    let check = false
    checks.each(function(){
        if($(this).is(':checked')){
            check = true
        }
    })
    return check;
}
function someCheckCandidateCreate(){
    let segments = $("#forme14-votes .segment-info")
    let check = false
    segments.each(function() {
        let childrens = $(this).find("input")
        if (rol != 5) {
            if (childrens[1].value != "") {
                if(childrens[2].checked){
                    check = true
                }
            }
        }
    })
    return check
}

function validateFiledClaimEncrutador(){
    let filed_claim_encrutador = "no"

    if ($('#filed_claim_encrutador').is(":checked")){
        let filed_claim_we = $("#filed_claim_encrutador_we").val()
        if(filed_claim_we){
            if(filed_claim_we == "otro_candidato"){
                filed_claim_encrutador = filed_claim_we;
            }else{
                let filed_claim_we_other_candidate = $("#filed_claim_encrutador_we_other_candidate").val()
                if(filed_claim_we_other_candidate){
                    filed_claim_encrutador = filed_claim_we_other_candidate; 
                }else{
                    return false;
                }
            }
        }else{
            return false;
        }
    }

    return filed_claim_encrutador
}

document.getElementById('form-update-form').addEventListener('click', function() {
    console.log("update")
    var formData = new FormData();

    if (rol == 5) { 
        let forme14_candidates = getInfoVote()
        let total_votes = $("#total-votes").val()
        let filed_claim = "no"
        let forme14_info = getInfo(true)

        if ($('#filed_claim').is(":checked")){
            let filed_claim_we = $("#filed_claim_we").val()
            if(filed_claim_we){
                if(filed_claim_we == "otro_candidato"){
                    filed_claim = filed_claim_we;
                }else{
                    let filed_claim_we_other_candidate = $("#filed_claim_we_other_candidate").val()
                    if(filed_claim_we_other_candidate){
                        filed_claim = filed_claim_we_other_candidate; 
                    }else{
                        return alert("Por favor selecciona si hubo reclamación")
                    }
                }
            }else{
                return alert("Por favor selecciona si hubo reclamación")
            }
        }

        if(rol_auth == 9 || rol_auth == 1){
            let forme14_candidates_encrutador = getInfoVoteEscrutador()
            let total_votes_encrutador = $("#total-votes-escrutador").val()

            if(validateFiledClaimEncrutador() === false){
                return alert("Por favor selecciona si hubo reclamación")
            }else{
                filed_claim_encrutador = validateFiledClaimEncrutador()
            }    
            
            formData.append("forme14_candidates", JSON.stringify(forme14_candidates));
            formData.append("forme14_candidates_encrutador", JSON.stringify(forme14_candidates_encrutador));
            formData.append("total_votes", total_votes);
            formData.append("total_votes_encrutador", total_votes_encrutador);
            formData.append("filed_claim", filed_claim);
            formData.append("forme14_info", JSON.stringify(forme14_info));
            formData.append("filed_claim_encrutador", filed_claim_encrutador);
            formData.append("load_image1", load_image1);
            formData.append("load_image2", load_image2);


            /*data = {
                forme14_candidates,
                forme14_candidates_encrutador,
                fileForm: fileForm,
                fileFormSecondary: fileFormSecondary,
                total_votes,
                total_votes_encrutador,
                filed_claim,
                forme14_info,
                filed_claim_encrutador
            }*/
        }else{

            formData.append("forme14_candidates", JSON.stringify(forme14_candidates));
            formData.append("total_votes", total_votes);
            formData.append("forme14_info", JSON.stringify(forme14_info));
            formData.append("filed_claim", filed_claim);
            formData.append("load_image1", load_image1);
            formData.append("load_image2", load_image2);

            /*data = {
                forme14_candidates,
                fileForm: fileForm,
                fileFormSecondary: fileFormSecondary,
                total_votes,
                forme14_info,
                filed_claim
            }*/
        }        

        if (total_votes == "" || Object.keys(forme14_candidates).length == 0 || fileForm == "") {
            return alert("Por favor llene los campos")
        }
        console.log(formData)
        fetchUpdateFormE142(formData)
    } else {
        let forme14_info = getInfo()
        let forme14_candidates = getInfoVote()
        let name = $("#name-form").val()
        let total_votes = $("#total-votes").val()

        let fileLogoInput = document.getElementById("upload-logo-part")
        let logoInput = fileLogoInput.files[0];

        formData.append("forme14_candidates", JSON.stringify(forme14_candidates));
        formData.append("forme14_info", JSON.stringify(forme14_info));
        formData.append("file", logoInput);
        formData.append("load_image1", load_image1);
        formData.append("name", name);
        formData.append("total_votes", total_votes);


        /*let data = {
            forme14_info,
            forme14_candidates,
            file: fileLogo,
            fileForm: fileForm,
            name,
            total_votes
        }*/

        if (name == "" || forme14_info.length == 0 || data.forme14_candidates.length == 0) {
            return alert("Por favor llene los campos")
        }

        if(!someCheckCandidate()){
            return alert("Por favor marca marca el candidato")
        }
        console.log(formData)

        fetchUpdateFormE142(formData)
    }
});


function updateFormE14() {
    console.log("rol", rol)
    if (rol == 5) { 
    console.log("siii")

        let forme14_candidates = getInfoVote()
        let total_votes = $("#total-votes").val()
        let filed_claim = "no"
        let forme14_info = getInfo(true)

        if ($('#filed_claim').is(":checked")){
            let filed_claim_we = $("#filed_claim_we").val()
            if(filed_claim_we){
                if(filed_claim_we == "otro_candidato"){
                    filed_claim = filed_claim_we;
                }else{
                    let filed_claim_we_other_candidate = $("#filed_claim_we_other_candidate").val()
                    if(filed_claim_we_other_candidate){
                        filed_claim = filed_claim_we_other_candidate; 
                    }else{
                        return alert("Por favor selecciona si hubo reclamación")
                    }
                }
            }else{
                return alert("Por favor selecciona si hubo reclamación")
            }
        }

        let data = null;

        if(rol_auth == 9 || rol_auth == 1){
            let forme14_candidates_encrutador = getInfoVoteEscrutador()
            let total_votes_encrutador = $("#total-votes-escrutador").val()

            if(validateFiledClaimEncrutador() === false){
                return alert("Por favor selecciona si hubo reclamación")
            }else{
                filed_claim_encrutador = validateFiledClaimEncrutador()
            }    
            
            data = {
                forme14_candidates,
                forme14_candidates_encrutador,
                fileForm: fileForm,
                fileFormSecondary: fileFormSecondary,
                total_votes,
                total_votes_encrutador,
                filed_claim,
                forme14_info,
                filed_claim_encrutador
            }
        }else{
            data = {
                forme14_candidates,
                fileForm: fileForm,
                fileFormSecondary: fileFormSecondary,
                total_votes,
                forme14_info,
                filed_claim
            }
        }        
        console.log("sii1i")

        if (total_votes == "" || Object.keys(forme14_candidates).length == 0 || fileForm == "") {
            return alert("Por favor llene los campos")
        }
        console.log(data)
        fetchUpdateFormE14(data)
    } else {
        console.log("sii12")

        let forme14_info = getInfo()
        let forme14_candidates = getInfoVote()
        let name = $("#name-form").val()
        let total_votes = $("#total-votes").val()
console.log(forme14_info)
console.log(forme14_candidates)

        let data = {
            forme14_info,
            forme14_candidates,
            file: fileLogo,
            fileForm: fileForm,
            name,
            total_votes
        }

        if (name == "" || forme14_info.length == 0 || data.forme14_candidates.length == 0) {
            return alert("Por favor llene los campos")
        }

        if(!someCheckCandidate()){
            return alert("Por favor marca marca el candidato")
        }
        console.log(data)

        fetchUpdateFormE14(data)
    }

}

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
})

function fetchFormE14(data) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $("#token").val()
        }
    })
    $("#spinner").css({'display': 'block'})
    $.post(`${url_home}/witnesses/createform/e14`, data).then(response => {
        if (response.success) {
            window.location.href = "/witnesses/createform"
        }else{
            $("#spinner").css({'display': 'none'})
        }
    }, err => {
        $("#spinner").css({'display': 'none'})
    });
}

function pruebaUploadImageTemp(primary = true){

    const formData = new FormData();
    let file = null;
    if(primary){
        file = document.getElementById('forme14-save-upload');
    }else{
        file = document.getElementById('forme14-save-upload-secondary');
    }

    const fileData = file ? file.files[0] : null

    if(fileData){
        formData.append("fileData", fileData);
        formData.append('primary', primary)

        if(primary){
            $("#uploadE141").css({'display': 'none'})
            $("#dot-spinner").css({'display': 'flex'})
        }else{
            $("#uploadE142").css({'display': 'none'})
            $("#dot-spinner2").css({'display': 'flex'})
        }
        $("#form-update-form").attr('disabled', true)
       
        var regex = /^[/]+|[./]+$/g;
        let endpoint = `/witnesses/e14/uploadImageTemp/${forme14_id}/${table_number}${testigo_id && testigo_id != "" ? "/" + testigo_id : ''}`
        endpoint = endpoint.replace(regex, '');

        $.ajax({
            url: "/" + endpoint,
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false,
            success: function (response) {
                let data = response.data
                $("#form-update-form").attr('disabled', false)
                if(primary){
                    load_image1 = true
                    $("#dot-spinner").css({'display': 'none'})
                    $("#uploadE141").css({'display': 'block'})
                    $("#image-uploaded").css({'display': 'block'})
                    file.value = ""
                    if(response.success){
                        $("#name-forme14-save-upload").text(data.fileName)
                    }
                }else{
                    load_image2 = true
                    $("#dot-spinner2").css({'display': 'none'})
                    $("#uploadE142").css({'display': 'block'})
                    $("#image-uploaded2").css({'display': 'block'})
                    file.value = ""
                    if(response.success){
                        $("#name-forme14-save-upload-secondary").text(data.fileName)
                    }
                }
            },
            error: function (error) {
                $("#dot-spinner").css({'display': 'none'})
                $("#uploadE141").css({'display': 'block'})
                $("#form-update-form").attr('disabled', false)
                file.value = ""
                alert("No se pudo cargar la imagen")
            }
        });
    }
    
}

function fetchUpdateFormE142(data) {
    console.log(data)
    console.log("testigo_id", testigo_id)
    console.log(testigo_id == "")
    console.log(testigo_id == null)
    var regex = /^[/]+|[./]+$/g;
    let endpoint = `/witnesses/editform/e14/${forme14_id}/${table_number}${testigo_id && testigo_id != "" ? "/" + testigo_id : ''}`
    endpoint = endpoint.replace(regex, '');

    $("#spinner").css({'display': 'block'})

    $.ajax({
        url: "/" + endpoint,
        type: 'POST',
        data: data,
        
        processData: false,
        contentType: false,
        success: function(response){
            load_image1 = false;
            load_image2 = false;

            $("#image-uploaded").css({'display': 'none'})
            $("#image-uploaded2").css({'display': 'none'})
            if (response.success) {
                /*if(updated_upload_form){
                    $("#name-forme14-save-upload").html("")
                }
                if(updated_upload_form_secondary){
                    $("#name-forme14-save-upload-secondary").html("")
                }*/
    
                //updated_upload_form = false
                /*if(fileForm){
                    fileForm.value = ""
                    fileForm = null
                }*/
                /*updated_upload_form_secondary = false
                if(fileFormSecondary){
                    fileFormSecondary.value = ""
                    fileFormSecondary = null
                }*/
                
    
                console.log("rol_auth" + rol_auth)
                if(rol_auth == 8){
                    sessionStorage.setItem('showMsg', 1)
                    window.location.href = "/witnesses/createform"  
                }else if(rol_auth == 9){
                    sessionStorage.setItem('showMsg', 1)
                    window.location.href = "/witnesses/coordinator"  
                }else if(rol_auth == 1){
                    sessionStorage.setItem('showMsg', 1)
                    window.history.back();
                    $("#alert").removeClass('display-none')
                    $("#spinner").css({'display': 'none'})
                    //window.scrollTo(0, 0)
                    Swal.fire(
                        'Muy bien!',
                        'El formulario ha sido actualizado correctamente',
                        'success'
                    )
                }else{
                    $("#alert").removeClass('display-none')
                    $("#spinner").css({'display': 'none'})
                    //window.scrollTo(0, 0)
                    Swal.fire(
                        'Muy bien!',
                        'El formulario ha sido actualizado correctamente',
                        'success'
                      )
                }
            }else{
                $("#spinner").css({'display': 'none'})
            }
        }  
    })

    /*$.post("/" + endpoint, data).then(response => {
        console.log(response)
        if (response.success) {
            if(updated_upload_form){
                $("#name-forme14-save-upload").html("")
            }
            if(updated_upload_form_secondary){
                $("#name-forme14-save-upload-secondary").html("")
            }

            updated_upload_form = false
            fileForm = null
            updated_upload_form_secondary = false
            fileFormSecondary = null

            console.log("rol_auth" + rol_auth)
            if(rol_auth == 8){
                sessionStorage.setItem('showMsg', 1)
                window.location.href = "/witnesses/createform"  
            }else if(rol_auth == 9){
                sessionStorage.setItem('showMsg', 1)
                window.location.href = "/witnesses/coordinator"  
            }else if(rol_auth == 1){
                sessionStorage.setItem('showMsg', 1)
                window.history.back();
                $("#alert").removeClass('display-none')
                $("#spinner").css({'display': 'none'})
                //window.scrollTo(0, 0)
                Swal.fire(
                    'Muy bien!',
                    'El formulario ha sido actualizado correctamente',
                    'success'
                )
            }else{
                $("#alert").removeClass('display-none')
                $("#spinner").css({'display': 'none'})
                //window.scrollTo(0, 0)
                Swal.fire(
                    'Muy bien!',
                    'El formulario ha sido actualizado correctamente',
                    'success'
                  )
            }
        }else{
            $("#spinner").css({'display': 'none'})
        }
    }, err => {
        $("#spinner").css({'display': 'none'})
    });*/
}

function fetchUpdateFormE14(data) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $("#token").val()
        }
    })
    
    console.log(data)
    console.log("testigo_id", testigo_id)
    console.log(testigo_id == "")
    console.log(testigo_id == null)
    var regex = /^[/]+|[./]+$/g;
    let endpoint = `/witnesses/editform/e14/${forme14_id}/${table_number}${testigo_id && testigo != "" ? "/" + testigo_id : ''}`
console.log("antes:", endpoint)
    endpoint = endpoint.replace(regex, '');

console.log("despuess:", endpoint)

    $("#spinner").css({'display': 'block'})
    $.post("/" + endpoint, data).then(response => {
        if (response.success) {
            if(updated_upload_form){
                $("#name-forme14-save-upload").html("")
            }
            if(updated_upload_form_secondary){
                $("#name-forme14-save-upload-secondary").html("")
            }

            updated_upload_form = false
            fileForm = null
            updated_upload_form_secondary = false
            fileFormSecondary = null

            console.log("rol_auth" + rol_auth)
            if(rol_auth == 8){
                sessionStorage.setItem('showMsg', 1)
                window.location.href = "/witnesses/createform"  
            }else if(rol_auth == 9){
                sessionStorage.setItem('showMsg', 1)
                window.location.href = "/witnesses/coordinator"  
            }else if(rol_auth == 1){
                sessionStorage.setItem('showMsg', 1)
                window.history.back();
                $("#alert").removeClass('display-none')
                $("#spinner").css({'display': 'none'})
                //window.scrollTo(0, 0)
                Swal.fire(
                    'Muy bien!',
                    'El formulario ha sido actualizado correctamente',
                    'success'
                )
            }else{
                $("#alert").removeClass('display-none')
                $("#spinner").css({'display': 'none'})
                //window.scrollTo(0, 0)
                Swal.fire(
                    'Muy bien!',
                    'El formulario ha sido actualizado correctamente',
                    'success'
                  )
            }
        }else{
            $("#spinner").css({'display': 'none'})
        }
    }, err => {
        $("#spinner").css({'display': 'none'})
    });
}
$("#close").on('click', function(){
    $("#alert").addClass('display-none')
})

$("#filed_claim").on('change', function(){
    if($(this).prop('checked')){
        $("#div_filed_claim_we").css({display: 'block'})
    }else{
        $("#div_filed_claim_we").css({display: 'none'})
        $("#filed_claim_we").val("")
    }
})


$("#filed_claim_we").on('change', function(){
    if($(this).val() == "nosotros"){
        $("#div_filed_claim_we_other_candidate").css({display: 'block'})
    }else{
        $("#div_filed_claim_we_other_candidate").css({display: 'none'})
        $("#filed_claim_we_other_candidate").val("")
    }
})

$("#filed_claim_encrutador").on('change', function(){
    if($(this).prop('checked')){
        $("#div_filed_claim_encrutador_we").css({display: 'block'})
    }else{
        $("#div_filed_claim_encrutador_we").css({display: 'none'})
        $("#filed_claim_encrutador_we").val("")
    }
})

$("#filed_claim_encrutador_we").on('change', function(){
    if($(this).val() == "nosotros"){
        $("#div_filed_claim_encrutador_we_other_candidate").css({display: 'block'})
    }else{
        $("#div_filed_claim_encrutador_we_other_candidate").css({display: 'none'})
        $("#filed_claim_encrutador_we_other_candidate").val("")
    }
})

function cambiarFoco(event, siguienteInputId){
    if (event.key === "Enter") {
        event.preventDefault();

        const siguienteInput = document.getElementById(siguienteInputId);

        if (siguienteInput) {
            // Establece el foco en el siguiente input
            siguienteInput.focus();
        }
    }
}

function getInfo(only_edit = false) {
    let segments = $("#forme14-info .segment-info")
    let forme14_info = []
    segments.each(function() {
        let childrens = $(this).children("input")
        if(only_edit){
            if (childrens[0].value != "" && childrens[1].getAttribute('data-can')) {
                let objForme14_info = {
                    [childrens[0].value]: childrens[1].value,
                    forminfo_id: $(this).data("id")
                }
                forme14_info.push(objForme14_info)
            }
        }else{
            if (childrens[0].value != "") {
                let objForme14_info = {
                    [childrens[0].value]: childrens[1].value,
                    forminfo_id: $(this).data("id")
                }
                forme14_info.push(objForme14_info)
            }
        }        
    })

    return forme14_info;
}

function getInfoVote() {
    let segments = $("#forme14-votes .segment-info")
    let forme14_vote = []

    segments.each(function() {
        let childrens = $(this).find("input")
        if (rol != 5) {
            let images = $(this).find("img")
            let image = null
            if(images.length > 0){
                image = images[0].src
            }
            if (childrens[1].value != "") {
                let objForme14_vote = {
                    [childrens[1].value]: null,
                    file: image,
                    check: childrens[2].checked,
                    formcandidate_id: $(this).data("id")
                }
                forme14_vote.push(objForme14_vote)
            }
        } else {
            let objForme14_vote = {
                [childrens[0].value]: childrens[1].value,
                formcandidate_id: $(this).data("id")
            }
            forme14_vote.push(objForme14_vote)
        }

    })
    return forme14_vote
}

function getInfoVoteEscrutador() {
    let segments = $("#forme14-votes .segment-info")
    let forme14_vote_escrutador = []

    segments.each(function() {
        let childrens = $(this).find("input")
        if (rol != 5) {
            if (childrens[0].value != "") {
                let objForme14_vote_escrutador = {
                    [childrens[0].value]: null,
                    check: childrens[2].checked,
                    formcandidate_id: $(this).data("id")
                }
                forme14_vote_escrutador.push(objForme14_vote_escrutador)
            }
        } else {
            let objForme14_vote_escrutador = {
                [childrens[0].value]: childrens[2].value,
                formcandidate_id: $(this).data("id")
            }
            forme14_vote_escrutador.push(objForme14_vote_escrutador)
        }

    })
    return forme14_vote_escrutador
}
/*function getInfoVote() {
    let segments = $(".forme14-votes-cand table .body-vote tr")
    let forme14_vote = []
    console.log("segments", segments)

    segments.each(function() {
        let childrens = $(this).find("input")
        console.log("childrens", childrens)
        if (rol != 5) {
            if (childrens[0].value != "") {
                let objForme14_vote = {
                    [childrens[0].value]: null,
                    check: childrens[1].checked,
                    formcandidate_id: $(this).data("id")
                }
                forme14_vote.push(objForme14_vote)
            }
        } else {
            if (childrens[1].value != "") {
                let objForme14_vote = {
                    [childrens[0].value]: childrens[1].value,
                    formcandidate_id: $(this).data("id")
                }
                forme14_vote.push(objForme14_vote)
            }
        }

    })
    return forme14_vote
}*/

function uploadLogoPart() {
    let file = document.getElementById("upload-logo-part")
    if (file.files && file.files[0]) {
        var reader = new FileReader()
        reader.onload = function(e) {
            $("#preview-logo-part").html(`
                <img src="${e.target.result}" width="70px" id="image-logo-part" style="margin: 5px 32px"/>
            `)
            fileLogo = e.target.result
        }
        reader.readAsDataURL(file.files[0])
    }
}

function uploadFileCandidate(file){
    let parent = file.parentNode
    if (file.files && file.files[0]) {
        var reader = new FileReader()
        reader.onload = function(e) {
            parent.className = 'content-avatar-candidate'

            const newNode = document.createElement("img");
            newNode.src = e.target.result;
            newNode.style.width="100%"
            newNode.style.height="100%"

            let child_img = parent.querySelector('img')
            if(child_img){
                child_img.remove()
            }
            parent.insertBefore(newNode,parent.firstChild)
        }
        reader.readAsDataURL(file.files[0])
    }
}
    
function uploadForm() {
    updated_upload_form = true
    $("#uploading_file").html("")
    let file = document.getElementById("forme14-save-upload")
    fileForm = file
    if (file.files && file.files[0]) {
        var reader = new FileReader()
        reader.onload = function(e) {
            $("#name-forme14-save-upload").text(file.files[0].name)
        }
        reader.readAsDataURL(file.files[0])
    }
}

function uploadFormSecondary() {
    updated_upload_form_secondary = true
    $("#uploading_file_secondary").html("")

    let file = document.getElementById("forme14-save-upload-secondary")
    fileFormSecondary = file
    if (file.files && file.files[0]) {
        var reader = new FileReader()
        reader.onload = function(e) {
            $("#name-forme14-save-upload-secondary").text(file.files[0].name)
        }
        reader.readAsDataURL(file.files[0])
    }
}


function removeSegment(elem) {
    let grandpa = document.getElementById("forme14-info")
    let parent = elem.target.parentNode
    grandpa.removeChild(parent)
}

function removeTr(elem) {
    let tagrandpa = document.getElementById("body-vote")
    let parent = elem.target.parentNode
    let grandpa = parent.parentNode

    tagrandpa.removeChild(grandpa)
}

function removeColumnVote(elem) {
    let grandpa = document.getElementById("forme14-votes")
    let parent = elem.target.parentNode
    grandpa.removeChild(parent)
}

function removeLogoPart() {
    $("#upload-logo-part").val("")
    $("#preview-logo-part").html("")
    fileLogo = null
}

function handleNumberVoters(e) {
    let total = 0;
    let forme14_candidates = getInfoVote()
    forme14_candidates.forEach(function(elem) {
        let elem_value = Object.values(elem)[0]
        if(elem_value && elem_value != ""){
            total += parseInt(elem_value)
        }
    })

    $("#total-votes").val(total)
}

function handleNumberVotersEscrutador(e) {
    let total = 0;
    let forme14_candidates = getInfoVoteEscrutador()
    forme14_candidates.forEach(function(elem) {
        let elem_value = Object.values(elem)[0]
        if(elem_value && elem_value != ""){
            total += parseInt(elem_value)
        }
    })

    $("#total-votes-escrutador").val(total)
}


function newFormE14Info() {
    $("#forme14-info").append(`
        <div class="segment">
            <div class="segment-info" data-id="">
                <input type="text" class="form-control" value="">
                <input type="text" class="form-control">
            </div>
            <span style="color: red" onclick="removeSegment(event)">X</span>
        </div>
    `)
}

function newTr() {
    if (rol != 5) {
        $("#body-vote").append(`
            <tr data-id="">
                <td>
                    <input style="background: #C7E8CA!important; width: 130px" type="text" class="form-control">
                    <input class="check-hijos-create" id="" type="checkbox" style="float: right; margin-top: 7px" onchange="checkCandidateCreate(this)">
                </td>
                <td>
                    <span class="delete-td" onclick="removeTr(event)">X</span>
                </td>
            </tr>
        `)
    } else {
        $("#body-vote").append(`
            <tr>
                <td>
                    <input type="text" class="form-control">
                </td>
                <td>
                    <input type="number" class="form-control number-vote">
                    <span class="delete-td" onclick="removeTr(event)">X</span>
                </td>
            </tr>
        `)
    }

}

function newColumnVote() {
    if (rol != 5) {
        position_candidate = position_candidate + 1
        $("#forme14-votes").append(`
            <div class="segment">
                <div class="segment-info" data-id="">
                    <div class="flex align-items-center" style="min-width: 50%">
                        <div class="content-avatar-candidate-no-photo">
                            <label for="file_candidate_${position_candidate}">
                                <i class="fa fa-pencil display-none cursor-"></i>
                            </label>
                            <input class="file_candidate" type="file" style="display: none" name="file_candidate" id="file_candidate_${position_candidate}" onchange="uploadFileCandidate(this)">
                        </div>
                        <input type="text" class="form-control" value="">
                        <input class="check-hijos-create m-0" id="" type="checkbox" style="float: right" onchange="checkCandidateCreate(this)">
                    </div>
                </div>
                <span style="color: red" onclick="removeColumnVote(event)">X</span>
            </div>
        `)
    }else{
        $("#forme14-votes").append(`
            <div class="segment">
                <div class="segment-info" data-id="">
                    <div class="flex align-items-center" style="min-width: 50%">
                        <input type="text" class="form-control" value="">
                        <input class="check-hijos-create m-0" id="" type="checkbox" style="float: right" onchange="checkCandidateCreate(this)">
                    </div>
                    <input type="text" class="form-control text-center">
                </div>
                <span style="color: red" onclick="removeColumnVote(event)">X</span>
            </div>
        `)
        /*$("#forme14-votes").append(`
            <div class="segment">
                <div class="segment-info" data-id="">
                    <input type="text" class="form-control" value="">
                    <input type="text" class="form-control">
                </div>
                <span style="color: red" onclick="removeSegment(event)">X</span>
            </div>
        `)*/
    }
}


let auto_child = 0;
function newColumnVoteUpdate() {
    if (rol != 5) {
        position_candidate = position_candidate + 1
        auto_child--
        $("#forme14-votes").append(`
            <div class="segment">
                <div class="segment-info" data-id="">
                    <div class="flex align-items-center" style="min-width: 50%">
                        <div class="content-avatar-candidate-no-photo">
                            <label for="file_candidate_${position_candidate}">
                                <i class="fa fa-pencil display-none cursor-"></i>
                            </label>
                            <input class="file_candidate" type="file" style="display: none" name="file_candidate" id="file_candidate_${position_candidate}" onchange="uploadFileCandidate(this)">
                        </div>
                        <input type="text" class="form-control" value="">
                        <input class="check-hijos m-0" id="check-${auto_child}" type="checkbox" style="float: right" onchange="checkCandidate(${auto_child})">
                    </div>
                </div>
                <span style="color: red" onclick="removeColumnVote(event)">X</span>
            </div>
        `)
    }else{
        $("#forme14-votes").append(`
            <div class="segment">
                <div class="segment-info" data-id="">
                    <div class="flex align-items-center" style="min-width: 50%">
                        <input type="text" class="form-control" value="">
                        <input class="check-hijos m-0" id="" type="checkbox" style="float: right" onchange="checkCandidate(this)">
                    </div>
                    <input type="text" class="form-control text-center">
                </div>
                <span style="color: red" onclick="removeColumnVote(event)">X</span>
            </div>
        `)
        /*$("#forme14-votes").append(`
            <div class="segment">
                <div class="segment-info" data-id="">
                    <input type="text" class="form-control" value="">
                    <input type="text" class="form-control">
                </div>
                <span style="color: red" onclick="removeSegment(event)">X</span>
            </div>
        `)*/
    }
}

function getFormE14(form_id, table_number, user_id){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    backForm()

    $.get(`/witnesses/forme14/all/${form_id}/${table_number}/${user_id}`).then(response => {
        let data = response.data
        if(response.success){
            showNameForm(data.formE14.name) 
            showFiledClaim(data.filed_claim) 
            showInfo(data.formE14.info, data.user, table_number, data.zone) 
            //showVoteLogo(data.formE14.match_logo) 
            showTableVote(data.formE14.candidates, data.formE14.total_vote_without_user, data.formE14.candidate_check,data.formE14.total_vote_without_user_escrutador )
            showUploadForm(data.formE14.file_without_user) 
        }
    })
}

function showNameForm(name){
    $("#name-form").val(name)
}

function showFiledClaim(filed_claim){
    $("#mensaje-filed_claims").text("");
    $("#mensaje-filed_claims_escrutador").text("");

    let obj_filed_claims = {
        no: "No hubo reclamación",
        otro_candidato: "Otro candidato",
        excedio: "Excedio el número de votos de la mesa",
        error_aritmetico: "Error aritmético",
        error_nombre: "Error en el nombre del candidato E14",
        firmas_incompletas: "Firmas incompletas",
        "1": "SI",
        "0": "No hubo reclamación",
    }
    
    if(filed_claim){
        $("#filed_claim").prop('checked', filed_claim.filed_claim)
        $("#mensaje-filed_claims").text("Reclamación: "+ obj_filed_claims[filed_claim.filed_claim])
    }

    if(filed_claim && (rol_auth == 9 || rol_auth == 1)){
        $("#filed_claim_encrutador").prop('checked', filed_claim.filed_claim_encrutador)
        $("#mensaje-filed_claims_escrutador").text("Reclamación: "+ obj_filed_claims[filed_claim.filed_claim_encrutador])
    }
}

function showUploadForm(file){
    if(!file){
        $("#name-forme14-save-upload").text('No ha subido el formulario')
    }else{
        $("#name-forme14-save-upload").html(`<span onclick="previewUploadForm('${file.file}', '${file.file_secondary}')" style="text-decoration: underline">Ver fotos</span>`)
    }
}

function previewUploadForm(file, file_secondary){
    $("#forme14-info").css({display: "none"})
    $("#div-total-votes").css({display: "none"})
    $("#forme14-votes").css({display: "none"})
    $("#content-filed_claim").css({display: "none"})
    $("#content-filed_claim_escrutador").css({display: "none"})
    $("#preview-form-upload").html("")

    if(file){
        $("#preview-form-upload").append(`
            <img style="margin-top: 20px" width="90%" src="/files_forme14/${file}" />
        `)
    }
    
    if(file_secondary){
        $("#preview-form-upload").append(`
            <img style="margin-top: 20px" width="90%" src="/files_forme14/${file_secondary}" />
        `)
    }

    $("#name-forme14-save-upload").html(`<span onclick="backForm('${file}', '${file_secondary}')" style="text-decoration: underline">Volver</span>`)
}

function backForm(file, file_secondary){
    $("#preview-form-upload").html("")
    $("#forme14-info").css({display: "block"})
    $("#forme14-votes").css({display: "block"})
    $("#div-total-votes").css({display: "block"})
    $("#content-filed_claim").css({display: "block"})
    $("#content-filed_claim_escrutador").css({display: "block"})
    $("#name-forme14-save-upload").html(`<span onclick="previewUploadForm('${file}', '${file_secondary}')" style="text-decoration: underline">Ver fotos</span>`)
}

function showVoteLogo(file){
    $("#preview-logo-part").html("")
    if(file){
        $("#preview-logo-part").append(`
            <img src="/logos_part/${file}" width="70px" id="image-logo-part" style="margin: 5px 32px"/>
        `)
    }
}

function showTableVote(candidates, total_vote, candidate_check = null, total_vote_escrutador = null){
    $("#forme14-votes").html("")
    $("#total-votes").val("")
    $("#total-votes-escrutador").val("")

    $("#forme14-votes").append(`
        <div class="flex justify-content-between">
            <div class="flex grow">
                <span class="grow">NOMBRE CANDIDATO</span>
                <span class="grow" style="${rol_auth == 9 || rol_auth == 1 ? 'text-align: end; margin-right: 10px' : 'margin-left: 64px'}">VOTOS</span>
                ${rol_auth == 9 || rol_auth == 1 ? `
                    <span class="grow" style="text-align: center">ESCRUTINIO</span>
                ` : ''}    
            </div>
        </div>
    `)
    candidates.forEach(candidate => {
        $("#forme14-votes").append(`
            <div class="segment">
                <div class="segment-info" data-id="${candidate.id}">
                    <div class="flex align-items-center w-100">
                        <div class="flex align-items-center" style="position: relative; min-width: 50%">
                            <div class="${candidate.file ? 'content-avatar-candidate' : 'content-avatar-candidate-no-photo'}" style="margin-left: 7px">
                                ${candidate.file ?
                                    `<img src="/files_candidates/${candidate.file}" alt="" width="100%" height="100%">`
                                    :
                                    ''
                                }    
                            </div>
                            <input disabled type="text" class="form-control" value="${candidate.candidate_number}">
                            ${candidate.id == candidate_check ?
                                '<img src="/images/check.png" alt="" width="15px" style="position: absolute; left: -12px">'
                                :
                                ''
                            }
                        </div>
                        <input type="number" class="form-control number-vote text-center" value="${candidate.vote_without_user ? candidate.vote_without_user.votes_number : ''}" disabled>

                        ${rol_auth == 9 || rol_auth == 1 ? `
                            <input type="number" class="form-control number-vote text-center" value="${candidate.vote_without_user_escrutador ? candidate.vote_without_user_escrutador.votes_number : ''}" disabled>
                        ` : ''}   
                    </div>
                </div>
            </div>  
        `)
    })
    $("#total-votes").val(total_vote ? total_vote.total_votes : null)
    if((rol_auth == 9 || rol_auth == 1) && total_vote_escrutador){
        $("#total-votes-escrutador").val(total_vote_escrutador ? total_vote_escrutador.total_votes : null)
    }
}

function showInfo(info, user, table_number, zone = null){
    $("#forme14-info").html("")
    info.forEach(elem => {
        if(elem.name.toUpperCase() == "MESA"){
                showSegment(elem, table_number)
        }else if(elem.name.toUpperCase() == "PUESTO DE VOTACIÓN"){
                let polling_station = user.polling_station ? user.polling_station.name : null 
                showSegment(elem, polling_station)
        }else if(elem.name.toUpperCase() == "ZONA"){
                showSegment(elem, zone)
        }else{
            if(elem.value){
                showSegment(elem, elem.value)
            }else{
                let info_testigo = getInfotTestigo(user.info_testigo, elem.id)
                if(info_testigo){
                    showSegment(elem, info_testigo.value)
                }else{
                    showSegment(elem, "")
                }
            }
            
        }
    }) 
}

function getInfotTestigo(infos, info_id){
    return infos.find(elem => elem.info_id == info_id);
}

function showSegment(elem, value){
    $("#forme14-info").append(`
        <div class="segment">
            <div class="segment-info" data-id="${elem.id}">
                <input disabled type="text" class="form-control" value="${elem.name}">
                <input disabled type="text" class="form-control" value="${value}">
            </div>
        </div>
    `)
}
/*$(".number-vote").on('change', function() {
    let total_votes = $("#total-votes").val()

    $("#total-votes").val(parseInt(total_votes) + parseInt($(this).val()))
})*/