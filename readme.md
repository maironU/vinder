## Clonación y despliegue de Vinder

Descargar y acceder a los archivos del repositorio:
```
git clone https://gitlab.com/neohitokiri/vinder.git
cd vinder
```

En Linux:
```
cp .env.example .env
```

En Windows:
```
copy .\.env.example .\.env
```

Instalar PHP y sus módulos:

```
sudo add-apt-repository ppa:ondrej/php
sudo apt update
sudo apt install php7.2-{cli,bcmath,bz2,intl,gd,mbstring,mysql,zip,pdo,xml,fpm,ldap,curl}
```

Instalar Composer:
```
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('sha384', 'composer-setup.php') === '756890a4488ce9024fc62c56153228907f1545c228516cbf63f885e036d37e9a59d27d63f46af1d4d07ee0f76181c7d3') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php');"
sudo cp composer.phar /usr/bin/composer
```

Instalar los paquetes de la aplicación:
```
composer install
```

Antes de continuar hay que ingresar a MySQL para crear una base de datos vacía:
```sql
create database u480659193_vinderdemo;
create user u480659193_vinderuser@localhost identified by 'Q!4ldPtU1gi';
grant all privileges on u480659193_vinderdemo.* to u480659193_vinderuser@localhost;
flush privileges;
```

Y luego editar las credenciales de acceso en la sección correspondiente del archivo `.env`:
```ruby
DB_HOST=localhost
DB_DATABASE=u480659193_vinderdemo
DB_USERNAME=u480659193_vinderuser
DB_PASSWORD=Q!4ldPtU1gi
```

Crear la estructura de la base de datos e insertar la información base:
```
php artisan migrate
php artisan db:seed
```

Ejecutar el servidor:
```
php artisan serve
```

Los usuarios disponibles para iniciar sesión en los despliegues demostrativos son:

| Nombre | Contraseña | Rol |
| --- | --- | --- |
| superadmin | demo | Súper Administrador |
| admin | demo | Administrador |
| digitador | demo | Digitador |
| invitado | demo | Invitado |
| testivo | demo | Testigo electoral |

