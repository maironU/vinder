<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ColaProceso extends Model
{
    protected $table = "cola_procesos";

    protected $fillable = [
        "proceso_id",
        "estado_proceso_id",
        "user_id",
        "bloqueado",
        "email",
        "parametros"
    ];

    public function estado_proceso()
    {
        return $this->belongsTo('App\EstadoProceso');
    }

    public function proceso()
    {
        return $this->belongsTo('App\Proceso');
    }
}
