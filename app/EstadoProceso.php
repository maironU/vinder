<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstadoProceso extends Model
{
    protected $table = "estado_procesos";
}
