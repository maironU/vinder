<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class VoterNew extends Model
{
    protected $table = "votersnew";
	public $fillable = ['nuip', 'first_name', 'last_name', 'second_name', 'surname', 'second_surname', 'department', 'municipality', 'polling_station', 'table'];

}
