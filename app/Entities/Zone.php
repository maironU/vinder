<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Zone extends Model
{
    protected $table = 'zones';
    public $timestamp = true;

    public function pollingStations()
    {
        return $this->belongsToMany('App\Entities\PollingStation', 'zone_polling_stations')->withTimestamps();
    }

    public function userZone(){
        return $this->hasOne('App\Entities\UserZone');
    }
}
