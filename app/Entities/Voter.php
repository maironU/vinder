<?php namespace App\Entities;

use App\Http\Services\WasApi;
use Illuminate\Database\Eloquent\Model;  
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;

use App\Libraries\Campaing;
use App\Libraries\WebScraping;
use Session, DB, Auth;
use Carbon\Carbon;

/**
* 
*/
class Voter extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

	protected $table = 'voters';
	public $timestamp = true;
	
    protected $fillable = ['doc', 'name', 'is_lider', 'leader_id', 'is_coordinator', 'latitude', 'longitude', 'email', 'telephone', 'sex', 'superior', 'occupation', 'description',
        'date_of_birth', 'location_id', 'address', 'table_number', 'delegate', 'polling_station_day_d', 
        'telephone_alternative', 'facebook_profile', 'instagram_profile', 'whatsapp', 'semaphore', 'house_check'];
    
    public $result_scraping = null;

    /* Scopes */
    public function scopeIsTeam($query, $isTeam = true)
    {
        return $query->whereColaborator($isTeam);
    }

    public function scopeIsTeamNew($query, $isTeam = true)
    {
        return $query->whereHas('roles', function($q){
            $q->where('voters_has_roles.rol_id', '!=', 12);
        })->whereColaborator($isTeam);
    }

    public function scopeIsDelegate($query, $isDelegate = true)
    {
        return $query->whereDelegate($isDelegate);
    }

    public function scopeIsSex($query, $sex = array('F', 'M'))
    {
        return $query->whereIn('sex', $sex);
    }

    public function scopeWithAll($query)
    {
        return $query->with('location', 'roles', 'occupations', 'communities', 'pollingStation', 'pollingStationDay', 
                        'superiorVoter', 'diaries', 'organizedDiaries', 'delegatedDiaries', 'voters', 'user', 'created_by');
    }

    public function isNew()
    {
        if($this->created_at == $this->updated_at)
        {
            return true;
        }

        return false;
    } 

    
	/* Querys */
    public static function votersBirthDay()
    {
        return self::whereNotNull('date_of_birth')
            ->whereNotNull('telephone')
            ->get();
    }

    public static function withBirthdayCurrentMonth()
    {
        return self::whereNotNull('date_of_birth')->orderBy('date_of_birth')->get()->filter(function($voter)
        {
            return $voter->isBirthdayCurrentMonth();
        });
    }

    public static function whereNullPollingStation()
    {
        return self::whereNull('polling_station_id')
            ->with('superior')
            ->get();
    }

    public static function withSex($sex = null)
    {
        return self::isSex($sex)->get();
    }
     
    public function user(){
        return $this->belongsTo('App\Entities\User', 'leader_id', 'id');
    }

    public static function votersPaginate($number_pages = -1, $isColaborator = false, $order = null, $request = null)
    {
        if ($number_pages < 0)
            $number_pages = Campaing::getResultsPerPage();
        if(Auth::user()->type->can_view_all){
            $voters = self::withAll()->whereColaborator($isColaborator);
        }else{
            $voters = Auth::user()->voters()->with('location', 'roles')->whereColaborator($isColaborator);
        }

        if( ! is_null($order)) {
            $voters = $voters->where(function ($query) use($order) {
                $query->where('name', 'like', mb_strtolower($order) . '%')
                    ->orWhere('name', 'like', ucwords($order) . '%');
            }); 

            if(Auth::user()->type->id != 2 && Auth::user()->type->id != 1 && Auth::user()->type->id != 3){
                $voters->where('created_by', Auth::user()->id);
            }
        }
        if(isset($request->orientador)){
            $voters->where('is_lider', 1)
                    ->where('leader_id', $request->orientador);
        }

        if(isset($request->coordinador)){
            $voters->where('superior', $request->coordinador);
        }

        if(isset($request->municipio)){
            $voters->where('location_id', $request->municipio);
        }

        if(isset($request->comuna)){
            $voters->where('location_id', $request->comuna);
        }

        if(isset($request->barrio)){
            $voters->where('location_id', $request->barrio);
        }
        if(!isset($request->orientador) && !isset($request->coordinador) && !isset($request->comuna) && !isset($request->barrio) && !isset($request->municipio)){
            if(Auth::user()->type->id != 2 && Auth::user()->type->id != 1 && Auth::user()->type->id != 3){
                return $voters->orderBy('name', 'asc')->where(function($query){
                    $query->where('created_by', Auth::user()->id);
                    if(Auth::user()->type->id == 6){
                        $query->orWhere('is_lider', 1)
                            ->where('leader_id', Auth::user()->id);
                    }
                })->paginate($number_pages);
            }
        }else{
            if(Auth::user()->type->id == 6){
                $voters->where('is_lider', 1)
                    ->where('leader_id', Auth::user()->id);
            }
        }
        return $voters->orderBy('name', 'asc')->paginate($number_pages);

    }

    public static function teamPaginate($number_pages = -1, $request = null)
    {
        $is_colaborator = true;

        if ($number_pages < 0)
            $number_pages = Campaing::getResultsPerPage();

        if(isset($request->coordinador)){
            $is_colaborator = false;
        }
        return self::votersPaginate($number_pages, $is_colaborator, $request->get('order'), $request);
    }

    public static function allTeam()
    {
        return self::isTeam()->with('roles')->get()->lists('name_with_roles', 'id')->all();
    }

    public static function allTeamOrientator()
    {
        $voters_colaborators = self::isTeamNew()->with('roles')->get()->lists('name_with_roles', 'id')->all();
        return $voters_colaborators;
    }

    public static function allOrientatorWithVoters()
    {
        $orientators = \App\Entities\User::where('type_id', 6)->get()->lists('name_with_role', 'id')->all();
        return $orientators;
    }

    public static function allDelegates()
    {
        return self::isDelegate()->lists('name', 'id')->all();
    }

    public static function allVoterOfColaborator($colaborator_id)
    {
        return self::whereSuperior($colaborator_id)->isTeam(false)->get();
    }

    public static function allVotersOfTeam($team_ids = [])
    {
        return self::whereIn('superior', $team_ids)->isTeam(false)->get();
    }

    public static function getTeamWithVoters($team_ids = null)
    {
        $team = self::isTeam()->with(['voters' => function($query){
                $query->whereColaborator(false);
            }, 'location']);

        if(!is_null($team_ids))
        {
            $team = $team->whereIn('id', $team_ids);
        }
        
        return $team->get();
    }

    public function getAllDiariesAttribute()
    {
        return $this->diaries->merge($this->organizedDiaries->merge($this->delegatedDiaries));
    }

    public static function numberVoters()
    {
        return self::count();
    }

    public static function getSemaphores()
    {
        $semaphores = [
            ['Indefinido', self::where('semaphore','=',0)->count(), 'silver',],
            ['Voto fijo', self::where('semaphore','=',1)->count(), 'green',],
            ['Indeciso', self::where('semaphore','=',2)->count(), 'orange',],
            ['Voto duro', self::where('semaphore','=',3)->count(), 'red',],
        ];
        return $semaphores;
    }

    public static function getGoalPercentage()
    {
        return (self::numberVoters() / Campaing::getTargetNumber()) * 100;
    }

	/* Relations */
	public function location()
    {
        return $this->belongsTo('App\Entities\Location', 'location_id', 'id');
    }

    public function created_by()
    {
        return $this->belongsTo('App\Entities\User', 'created_by', 'id');
    }

    public function pollingStation()
    {
        return $this->belongsTo('App\Entities\PollingStation', 'polling_station_id');
    }

    public function pollingStationDay()
    {
        return $this->belongsTo('App\Entities\PollingStation', 'polling_station_day_d');
    }

    public function superior()
    {
        return $this->belongsTo('App\Entities\Voter', 'superior', 'id');
    }

    public function superiorVoter()
    {
        return $this->belongsTo('App\Entities\Voter', 'superior', 'id');
    }

    public function voters()
    {
        return $this->hasMany('App\Entities\Voter', 'superior', 'id');
    }

    public function organizedDiaries()
    {
        return $this->hasMany('App\Entities\Diary', 'organizer_id', 'id');
    }

    public function delegatedDiaries()
    {
        return $this->hasMany('App\Entities\Diary', 'delegate_id', 'id');
    }

    public function communities()
    {
        return $this->belongsToMany('App\Entities\Community', 'voters_has_communities', 'voter_id', 'community_id');
    }

    public function occupations()
    {
        return $this->belongsTo('App\Entities\Occupation', 'occupation', 'id');
    }

    public function roles()
    {
        return $this->belongsToMany('App\Entities\Rol', 'voters_has_roles', 'voter_id', 'rol_id');
    }

    public function diaries()
    {
        return $this->belongsToMany('App\Entities\Diary');
    }

    /* Mutator Attribures */

    public function getNameAttribute($value)
    {
        return ucwords(mb_strtolower($value));
    }

    public function getAddressAttribute($value)
    {
        return ucwords(mb_strtolower($value))   ;
    }

    public function getPollingStationNameAttribute()
    {
        if($this->pollingStation)
        {
            return ucwords($this->pollingStation->name);
        }

        return ' ';
    }

    public function getPollingStationDescriptionAttribute()
    {
        if($this->pollingStation)
        {
            return $this->pollingStation->description;
        }

        return ' ';
    }

    public function getOccupationNameAttribute()
    {
        if($this->occupations)
        {
            return $this->occupations->name;
        }

        return ' ';
    }

    public function getFirstNameAttribute()
    {
        return explode(' ', $this->name)[0];
    }

    public function getHappyBirthdayAttribute()
    {
        return 'Hola ' . $this->first_name . ', Feliz Cumpleaños. Muchas felicidades en este dia tan especial. ' 
        . Campaing::getCandidateName();
    }

    public function getWelcomeMessageAttribute()
    {
        return 'Hola ' . $this->first_name . ', Gracias por unirte a nuestro equipo. Unidos lograremos nuestro Proposito. ' 
        . Campaing::getCandidateName();
    }

    public function getNumberVotersAttribute()
    {
        return $this->voters->count();
    }

    public function getLocationNameAttribute()
    {
        if($this->location)
        {
            return ucwords($this->location->name);
        }
        
        return 'Sin Ubicación';
    }

    public function getComunaNameAttribute()
    {
        if($this->location)
        {
            $comuna = $this->location->superiorLocation()->where('name', 'LIKE', 'comuna%')->first();
            return $comuna->name ?? null;
        }
        
        return null;
    }

    public function getLocationRecursiveNameAttribute()
    {
        if($this->location)
        {
            return ucwords($this->location->name_recursive);
        }
        
        return 'Sin Ubicación';
    }

    public function getTitleReportAttribute()
    {
        return '(' . $this->superior_name . '), ' . $this->name_with_roles;
    }

    public function getSuperiorNameAttribute()
    {
        if($this->superiorVoter)
        {
            return $this->superiorVoter->name;
        }

        return 'Sin Referido';
    }

    public function getSuperiorNameWithRolesAttribute()
    {
        if($this->superiorVoter)
        {
            return $this->superiorVoter->name_with_roles;
        }

        return 'Sin Referido';
    }

    public function getCommunitiesListAttribute()
    {
        return $this->communities()->lists('id')->all();
    }

    public function getRolesListAttribute()
    {
        return $this->roles()->lists('id')->all();
    }

    public function getNameWithRolesAttribute()
    {
        $name_with_roles = $this->name;

        foreach ($this->roles as $count => $rol) 
        {
            if($count == 0)
            {
               $name_with_roles .= ' >> '; 
            }
            else
            {
                $name_with_roles .= ', '; 
            }

            $name_with_roles .= $rol->name ;
        }

        return $name_with_roles;
    }

    public function getNameWithRolesAndCommunitiesAttribute()
    {
        $name_with_roles_and_communities = $this->name_with_roles;

        foreach ($this->communities as $count => $community) 
        {
            if($count == 0)
            {
               $name_with_roles_and_communities .= ' ('; 
            }
            else
            {
                $name_with_roles_and_communities .= ', '; 
            }

            $name_with_roles_and_communities .= $community->name;
        }

        if($this->communities->count() > 0)
        {
            $name_with_roles_and_communities .= ')';
        }

        return $name_with_roles_and_communities;
    }

    public function getAllRolesAttribute()
    {
        if($this->colaborator)
        {
            $allRoles = '';
            
            foreach ($this->roles as $count => $rol) 
            {
                if($count > 0)
                {
                    $allRoles .= ', '; 
                }

                $allRoles .= $rol->name ;
            }
            
        }
        else
        {
            $allRoles = 'Votante';
        }

        return $allRoles;
    }

    public function getUpdatedAtHummansAttribute()
    {
        Carbon::setLocale('es');
        return ucfirst(Carbon::createFromFormat('Y-m-d H:i:s', $this->updated_at)->diffForHumans());
    }

    public function getTelTenNumbersAttribute()
    {
       return substr(trim($this->telephone), 0, 10);
    }

    public function getBirthDayMonthAttribute()
    {
        if($this->date_of_birth)
        {
            return date('d-M', strtotime($this->date_of_birth));
        }
    }

    /* Functions */
    public static function setTeamSession($voter_id)
    {
        Session::put('colaborator', $voter_id);
    }

    public static function getTeamSession()
    {
        return Session::get('colaborator', null);
    }

    public static function setOrientatorSession($orientator_id)
    {
        Session::put('orientator', $orientator_id);
    }

    public static function getOrientatorSession()
    {
        return Session::get('orientator');
    }

    public function getTeamSessionAttribute()
    {
        if($this->exists)
        {
            return $this->superior;
        }
        
        return Voter::getTeamSession();
    }

    public function getOrientadorSessionAttribute()
    {
        if($this->exists)
        {
            return $this->leader_id;
        }
        
        return Voter::getOrientatorSession();
    }

    public function isBirthdayCurrentMonth()
    {
        if(date('m', strtotime($this->date_of_birth)) == date('m'))
        {
            return true;
        }

        return false;
    }

    public static function searchLike($text, $number_page = 15)
    {
        $search = '%'. $text . '%';

        return self::withAll()
            ->where(function($query) use ($search) {
                $query->where('doc', 'like', $search)
                    ->orWhere('name', 'like', $search)
                    ->orWhere('email', 'like', $search)
                    ->orWhere('telephone', 'like', $search);
            })->paginate($number_page);
    }

    public static function isVoter($doc)
    {
        $voter = self::withTrashed()->whereDoc($doc)->first();
        //$voter = self::whereDoc($doc)->first();

        if(! $voter)
        {
            $voter = new Voter;
            $voter->doc = $doc;
            return $voter;
        }

        if($voter->trashed()){
            throw new \App\Exceptions\DeleteException(json_encode(["message" => "El votante ha sido eliminado", "voter" => $voter]));
        }else{
            throw new \App\Exceptions\ExistException("El votante ya existe");
        }

    }

    public static function findwithTrashedOrNew($doc)
    {
        $voter = self::withTrashed()->whereDoc($doc)->first();

        if(! $voter)
        {
            $find_voters_new = VoterNew::where('nuip', $doc)->first();

            $voter = new Voter;
            $voter->doc = $doc;

            if($find_voters_new){
                if($find_voters_new->first_name){
                    $voter->name.=$find_voters_new->first_name." ";
                }

                if($find_voters_new->second_name){
                    $voter->name.=$find_voters_new->second_name. " ";
                }

                if($find_voters_new->surname){
                    $voter->name.=$find_voters_new->surname. " ";
                }

                if($find_voters_new->second_surname){
                    $voter->name.=$find_voters_new->second_surname;
                }

                $voter->name = trim($voter->name);

                if($find_voters_new->municipality){
                    $location = Location::where('name', trim(strtolower($find_voters_new->municipality)))->first();
                    $voter->location_id = $location ? $location->id : null; 
                }

                if($find_voters_new->polling_station){
                    $polling = PollingStation::where('name', trim(strtolower($find_voters_new->polling_station)))->first();
                    $voter->polling_station_id = $polling ? $polling->id : null; 
                }

                if($find_voters_new->table){
                    $voter->table_number = $find_voters_new->table;
                }
            }
        }

        return $voter;
    }

    public function getTextMessage($message)
    {
        $message = str_replace('[NOMBRE]', $this->first_name, $message);
        
        if($this->pollingStation)
        {
            $message = str_replace('[PUESTO]', $this->pollingStation->description, $message);
        }
        else
        {
            $message = str_replace('[PUESTO]', '      ', $message);
        }
        
        $message = str_replace('[MESA]', $this->table_number, $message);

        return $message;
    }

    public static function getFileSms($voters, $message)
    {
        $fileArray = [];

        foreach ($voters as $voter) 
        {
            $sms = [$voter->telephone, $voter->getTextMessage($message)];
            array_push($fileArray, $sms);
        }

        return $fileArray;
    } 

    public function hasPollingStation($refresh = false)
    {
        if(! is_null($this->pollingStation) && !$refresh)
        {   
            $this->pollingStation->load('location');
            $result['status'] = true;
            $result['polling_station'] = $this->pollingStation;
            $result['table_number'] = $this->table_number;
        }
        else
        {
            $result['status'] = false;
            $webScraper = new WebScraping;
            $content = $webScraper->run($this->doc);

            if($content['status'])
            {
                if($location = Location::findByName($content['location']))
                {
                    $pollingStation = PollingStation::firstOrNew([
                        'name'                          => $content['place'],
                        'registraduria_location_id'     => $location->id
                    ]);

                    if(! $pollingStation->exists)
                    {
                        $pollingStation->location_id    = $location->id;
                        $pollingStation->description    = $content['place'];
                    }

                    $pollingStation->address = $content['place_address'];
                    $pollingStation->save();

                    $result['status'] = true;
                    $pollingStation->load('location');
                    
                    $result['polling_station']  = $pollingStation;
                    $result['table_number']     = $content['table_number'];
                }
                else
                {
                    $result['message'] = 'La persona no vota dentro del lugar objetivo (' . $content['location'] . ')';    
                }
            }
            else if(array_key_exists('message', $content))
            {
                $result['message'] = $content['message'];
            }
            else
            {
                $result['message'] = 'Lo sentimos, la registraduria no responde';
            }            
        }

        return $result;
    }

    public function saveAndSync($data, $colaborator = false)
    {
        $this->fill($data);
        $this->colaborator = $colaborator;

        if( !empty($data['polling_station_id']) )
        {
            $this->polling_station_id = $data['polling_station_id'];
        }

        if( empty($data['polling_station_day_d']) )
        {
            $this->polling_station_day_d = $data['polling_station_id'];

            if( empty($data['polling_station_id']) ) 
            {
                $this->polling_station_day_d = null;
            }
        }
        
        if(empty($data['date_of_birth']))
        {
            $this->date_of_birth = null;
        }

        if(isset($data["new-occupation"])){
            if(empty($data['occupation']) || !empty($data['new-occupation']))
            {
                $this->syncNewOccupation($data['new-occupation']);
            }
        }

        if(isset($data['house_check']) && $data['house_check'] == "on"){
            $this->house_check = true;
        }

        if(!$this->exists)
        {
            $this->created_by = Auth::user()->id;    
        }

        if(array_key_exists('delegate', $data))
        {
            $this->delegate = 1;
        }
        else
        {
            $this->delegate = 0;
        }

        $this->save();

        if(array_key_exists('communities', $data))
        {
            $this->communities()->sync($data['communities']);
        }
        else
        {
            $this->communities()->sync([]);
        }

        if(isset($data["new-communities"])){
            $this->syncNewCommunities($data['new-communities']);
        }

        if(array_key_exists('roles', $data))
        {
            $this->roles()->sync($data['roles']);
        }
        else
        {
            $this->roles()->sync([]);
        }

        if($this->trashed())
        {
            $this->restore();
        }

        return true;
    }

    public function sendWelcomeMessage(){
        $was_api = new WasApi();
        $telephone = $this->telephone && $this->telephone != "" ? $this->telephone : $this->telephone_alternative;
        $name = $this->name && $this->name != "" ? $this->name : "";

        if($telephone){
            $was_api->sendMessageWithTemplate($telephone, $name);
        }
    }

    public function syncNewCommunities($communities)
    {
        if(!is_null($communities) && !empty($communities))
        {
            $communities = explode(",", $communities);
            foreach ($communities as $name) 
            {
                $newCommunity = Community::firstOrCreate(['name' => trim($name)]);
                $this->communities()->attach($newCommunity);
            }
        }
    }

    public function syncNewOccupation($occupation)
    {
        if(!is_null($occupation) && !empty($occupation))
        {
            $newOccupation = Occupation::firstOrCreate(['name' => $occupation]);
            $this->occupation = $newOccupation->id;
        }
        else
        {
            $this->occupation = null;
        }
    }

    public static function getFilterVoters($data)
    {
        if( !array_key_exists('locations', $data) || (count($data['locations']) == 1 && reset($data['locations']) == 1)) 
        {
            $voters = self::whereNotNull('location_id');                
        }
        else
        {
            $voters = self::whereIn('location_id', Location::getAllOrderIds($data['locations']));
        }

        if(array_key_exists('sex', $data))
        {
            $voters = $voters->isSex($data['sex']);
        }

        if(array_key_exists('polling_stations', $data))
        {
            $voters = $voters->whereIn('polling_station_id', $data['polling_stations']);
        }

        if(array_key_exists('communities', $data))
        {
            $voters = $voters->join('voters_has_communities', 'voters.id', '=', 'voters_has_communities.voter_id')
                ->whereIn('voters_has_communities.community_id', $data['communities']);
        }

        if(array_key_exists('roles', $data))
        {
            $voters = $voters->join('voters_has_roles', 'voters.id', '=', 'voters_has_roles.voter_id')
                ->whereIn('voters_has_roles.rol_id', $data['roles']);
        }

        if(array_key_exists('occupations', $data))
        {
            $voters = $voters->whereIn('occupation', $data['occupations']);
        }
        
        return $voters;
    }

    public static function getTelephones($data)
    {
        $telephones = self::getFilterVoters($data)
            ->where(DB::raw('CHAR_LENGTH(telephone)'), '=', '10')
            ->lists('telephone')->all();

        return $telephones;
    }

    public static function getVotersWithTelephones($data)
    {
        $telephones = self::getFilterVoters($data)
            ->where(DB::raw('CHAR_LENGTH(telephone)'), '=', '10')
            ->get();

        return $telephones;
    }

    public static function getVoterSex($sex)
    {
        $voters = self::isSex($sex)->get();

        return $voters;
    }

	public static function getVotersPerSemaphoreColor($semaphore)
	{
        $voters = Voter::whereIn('semaphore',$semaphore)->get();
		return($voters);
	}
   
    public function scopeByTable($query, $table){
        return $query->where(function($q) use($table){
            $q->where('table_number', $table)
            ->orWhere('table_number', 'Mesa '.$table);
        });
    }

}