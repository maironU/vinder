<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class FormE14TotalVoteEscrutador extends Model
{
    protected $table = 'forme14_total_vote_escrutadors';
    public $timestamp = true;
}
