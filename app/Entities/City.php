<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = 'cities';

    protected $fillable = ['name', 'status', 'longitude', 'latitude', 'department_id'];

    public function getNameAttribute($value){
        return ucfirst(strtolower($value));
    }

    public function setNameAttribute($value){
        $this->attributes['name'] = ucfirst(strtolower($value));
    }
    
}
