<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class FormE14VoteEscrutador extends Model
{
    protected $table = 'forme14_vote_escrutador';
    public $timestamp = true;

    public function candidate(){
        return $this->belongsTo("App\Entities\FormE14Candidate", 'candidate_id', 'id');
    }
}
