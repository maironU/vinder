<?php namespace App\Entities;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Hash;
use App\Libraries\Campaing;


class WitnessPollingStation extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'witnesses_polling_stations';
    public $timestamp = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['witness_id', 'polling_station_id', 'table'];

}
