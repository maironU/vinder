<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class FormE14File extends Model
{
    protected $table = 'formse14_files';
    public $timestamp = true;
}
