<?php namespace App\Entities;

use App\Http\Services\WasApi;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Hash;
use App\Libraries\Campaing;


class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user';
    public $timestamp = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'username', 'address', 'longitude', 'latitude', 'location_id', 'polling_station_id', 'num_table', 'type_id', 'email','phone', 'password', 'coordinator_id', 'house_check', 'message_incident'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /* Querys */

    public static function allPaginate($number_pages = -1)
    {
        if ($number_pages < 0)
            $number_pages = Campaing::getResultsPerPage();

        return self::with('type')->orderBy('updated_at')->paginate($number_pages);
    }
    
    /* Mutators */
	public function setPasswordAttribute($value)
    {
        if(!empty($value)){
            $this->attributes['password'] = Hash::make(trim($value));
        }
    }

    public function sendWelcomeMessage(){
        $was_api = new WasApi('bienvenida');
        $telephone = $this->phone && $this->phone != "" ? $this->phone : null;
        $name = $this->name && $this->name != "" ? $this->name : "";

        $variables = [$name];

        if($telephone){
            $was_api->sendMessageWithTemplate($telephone, $variables);
        }
    }

    public function getNameWithRoleAttribute()
    {
        $name_with_role = $this->name. ' >> Orientador';

        return $name_with_role;
    }

    public function getTypeNameAttribute()
    {
        return $this->type->name;
    }

    public function getCountVotersAttribute()
    {
        return $this->voters->count();
    }

    public function getCountPollsAttribute()
    {
        return $this->voterPolls->count();
    }

    public function coordinator(){
        return $this->belongsTo('App\Entities\Voter', 'coordinator_id', 'id');
    }

    /* Functions */

    public static function getUserByType($type)
    {
        return self::where('type_id', '=', $type)->orderBy('id', 'desc')->get();
    }

    public function colors(){
        return $this->hasMany("App\Entities\FormE14Color", 'user_id', 'id');
    }

    public function getMenuModules()
    {
        return $this->type->getMenuModules();
    }

    public function getModuleNames()
    {
        return $this->type->getModuleNames();
    }

    public function hasModule($module)
    {
        return $this->type->hasModule($module);
    }

    public function voterPollsRealized($poll_id)
    {
        return $this->voterPolls()->realized()->wherePollId($poll_id)->count();
    }

    public function voterPollsAnswered($poll_id)
    {
        return $this->voterPolls()->whereResult('answer')->wherePollId($poll_id)->count();
    }

     public function voterPollsRealizedToday($poll_id)
    {
        return $this->voterPolls()->realized()->wherePollId($poll_id)->today()->count();
    }

    public function voterPollsAnsweredToday($poll_id)
    {
        return $this->voterPolls()->whereResult('answer')->wherePollId($poll_id)->today()->count();
    }

    public function zone(){
        return $this->hasOne('App\Entities\UserZone', 'user_id', 'id');
    }

    public function tables_numbers(){
        return $this->hasMany('App\Entities\TableNumberUser', 'user_id', 'id');
    }

    /* Relations */
    public function location()
    {
        return $this->belongsTo('App\Entities\Location', 'location_id', 'id');
    }
    
    public function type()
    {
        return $this->belongsTo('App\Entities\UserType', 'type_id');
    }

    public function total_votes(){
        return $this->hasMany('App\Entities\FormE14TotalVote', 'user_id', 'id');
    }

    public function pollingStation()
    {
        return $this->belongsTo('App\Entities\PollingStation', 'polling_station_id');
    }

    public function voters()
    {
        return $this->hasMany('App\Entities\Voter', 'created_by', 'id');
    }

    public function voters_leaders()
    {
        return $this->hasMany('App\Entities\Voter', 'leader_id', 'id');
    }

    public function voterPolls()
    {
        return $this->hasMany('App\Entities\VoterPoll');
    }

    public function votes(){
        return $this->hasMany('App\Entities\FormE14Vote', 'user_id', 'id');
    }

    public function info_testigo(){
        return $this->hasMany('App\Entities\FormE14InfoTestigo', 'user_id', 'id');
    }

    public function files(){
        return $this->hasMany('App\Entities\FormE14File', 'user_id', 'id');
    }
}
