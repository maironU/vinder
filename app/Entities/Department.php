<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $table = 'departments';
    public $increments = true;

    protected $fillable = ['name', 'status'];
}
