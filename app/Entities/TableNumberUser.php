<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class TableNumberUser extends Model
{
    protected $table = 'table_numbers_user';
    protected $fillable = ['user_id', 'table_number'];
}
