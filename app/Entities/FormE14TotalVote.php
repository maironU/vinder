<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class FormE14TotalVote extends Model
{
    protected $table = 'forme14_total_votes';
    public $timestamp = true;
}
