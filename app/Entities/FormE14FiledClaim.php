<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class FormE14FiledClaim extends Model
{
    protected $table = 'forme14_filed_claims';
    public $timestamp = true;
}
