<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class FormE14Color extends Model
{
    protected $table = 'forme14_color';
    protected $fillable = ['forme14_id', 'user_id', 'table_number', 'color'];

    public $timestamp = true;

    public function scopeColor($query, $testigo_id, $table_number){
        return $query->where('user_id', $testigo_id)
        ->where("table_number", $table_number);
    }
}
