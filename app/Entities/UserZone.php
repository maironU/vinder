<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class UserZone extends Model
{
    protected $table = 'user_zone';
    protected $fillable = ['id', 'user_id', 'zone_id'];
    public $timestamp = true;

    public function user(){
        return $this->belongsTo('App\Entities\User');
    }

    public function zone(){
        return $this->belongsTo('App\Entities\Zone');
    }
}
