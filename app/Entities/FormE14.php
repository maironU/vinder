<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class FormE14 extends Model
{
    protected $table = 'formse14';
    public $timestamp = true;


    public function info(){
        return $this->hasMany("App\Entities\FormE14Info", 'forme14_id', 'id');
    }

    public function candidates(){
        return $this->hasMany("App\Entities\FormE14Candidate", 'forme14_id', 'id');
    }

    public function total_vote(){
        return $this->hasOne("App\Entities\FormE14TotalVote", 'forme14_id', 'id')
        ->where('user_id', Auth::user()->id);
    }

    public function total_vote_without_user(){
        return $this->hasOne("App\Entities\FormE14TotalVote", 'forme14_id', 'id');
    }
    public function colors(){
        return $this->hasMany("App\Entities\FormE14Color", 'forme14_id', 'id');
    }

    public function total_vote_without_user_escrutador(){
        return $this->hasOne("App\Entities\FormE14TotalVoteEscrutador", 'forme14_id', 'id');
    }


    public function file(){
        return $this->hasOne("App\Entities\FormE14File", 'forme14_id', 'id')
        ->where('user_id', Auth::user()->id);
    }

    public function fileWithoutUser(){
        return $this->hasOne("App\Entities\FormE14File", 'forme14_id', 'id');
    }

    public function files(){
        return $this->hasMany("App\Entities\FormE14File", 'forme14_id', 'id');
    }

    public function candidate_check(){
        return $this->hasOne("App\Entities\FormE14Candidate", 'id', 'candidate_check');
    }

    //scopes

    public function scopeActive($query){
        return $query->where('state', 1);
    }
}
