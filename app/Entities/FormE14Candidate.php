<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class FormE14Candidate extends Model
{
    protected $table = 'forme14_candidates';
    public $timestamp = true;

    public function vote(){
        return $this->hasOne("App\Entities\FormE14Vote", 'candidate_id', 'id')
        ->where('user_id', Auth::user()->id);
    }

    public function voteWithoutUser(){
        return $this->hasOne("App\Entities\FormE14Vote", 'candidate_id', 'id');
    }

    public function voteWithoutUserEscrutador(){
        return $this->hasOne("App\Entities\FormE14VoteEscrutador", 'candidate_id', 'id');
    }
    
}
