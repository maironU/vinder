<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class FormE14InfoTestigo extends Model
{
    protected $table = 'forme14_info_testigos';
    protected $fillable = ['info_id', 'user_id', 'table_number', 'value'];

}
