<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class FormE14Vote extends Model
{
    protected $table = 'forme14_votes';
    public $timestamp = true;

    public function candidate(){
        return $this->belongsTo("App\Entities\FormE14Candidate", 'candidate_id', 'id');
    }
}
