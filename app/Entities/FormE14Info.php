<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class FormE14Info extends Model
{
    protected $table = 'forme14_info';
    public $timestamp = true;
}
