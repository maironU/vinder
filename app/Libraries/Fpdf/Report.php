<?php namespace App\Libraries\Fpdf;

use Fpdf;
use App\Libraries\Campaing;

class Report extends Fpdf
{
	public $candidate;
	public $title;
	public $withFooter;
	public $withHabeas;

	function init($candidate, $title, $withFooter = true) 
	{
		$this->candidate = $candidate;
		$this->title = $title;
		$this->withFooter = $withFooter;
		$this->withHabeas = ['',''];
		$this->AliasNbPages();
		$this->addPage();
	}

	public function getWidth()
	{
		return $this->w - $this->lMargin - $this->rMargin -10;
	}

	public function getHeight()
	{
		return $this->h - $this->lMargin - $this->rMargin;
	}

	function Header()
	{	
		if(file_exists(public_path('images/reportes.jpg'))){
			$this->Image(public_path('images/reportes.jpg'), 10, 5, 20);
		}
	    $this->SetFont('Arial', 'B', 19);
	    $this->setY(13);
	    $this->Cell(0, 0, utf8_decode($this->candidate), 0, 0, 'C');

	    $this->setY(23);
	    $this->SetFont('Arial', 'B', 17);
	    $this->SetTextColor(21, 75, 179);
	    $this->Cell(0, 0, utf8_decode($this->title), 0, 0, 'C');
		$this->Image(Campaing::getPoliticalLogo(), $this->getWidth() - 30, 6, 40 , 20);
        $this->Line(10, 30, $this->w - $this->lMargin, 30);

        $this->setY(37);
	}


	function Footer()
	{
		if($this->withFooter)
		{
	    	$this->SetY(-15);
	        $this->SetFont('Arial', 'B', 10);
	        $this->Cell(0, 10, utf8_decode('Fecha de consulta '.date("d-m-Y", time())), 0, 0, 'L');
			$this->Cell(0, 10, utf8_decode($this->PageNo().' de {nb}'), 0, 0, 'R');
		}
		$this->SetFont('Arial', 'I', 8);
		$this->SetY(-15);
		if(isset($this->withHabeas[0])){
			$this->Cell(0, 10, utf8_decode($this->withHabeas[0]), 0, 0, 'C');
		}
		$this->SetY(-10);
		if(isset($this->withHabeas[1])){
			$this->Cell(0, 10, utf8_decode($this->withHabeas[1]), 0, 0, 'C');
		}
	}
}

?>