<?php namespace App\Libraries;

class Campaing {
	private static $political_logo = null;

	//region Número de votantes esperados
	private static $resultsPerPage = null;

	static public function getResultsPerPage()
	{
		if (self::$target_number == null) {
			self::$target_number = env('RESULTS_PER_PAGE', 10);
		}
		return self::$target_number;
	}

	static public function setResultsPerPage($value)
	{
		self::$target_number = $value;
	}
	//endregion

	//region Número de votantes esperados
	private static $target_number = null;

	static public function getTargetNumber()
	{
		if (self::$target_number == null) {
			self::$target_number = env('TARGET_NUMBER', 10000);
		}
		return self::$target_number;
	}

	static public function setTargetNumber($value)
	{
		self::$target_number = $value;
	}
	//endregion

	//region Documento de identificación del candidato
	private static $candidate_doc = null;

	static public function getCandidateDoc()
	{
		if (self::$candidate_doc == null) {
			self::$candidate_doc = env('DOC', 0011100);
		}
		return self::$candidate_doc;
	}

	static public function setCandidateDoc($value)
	{
		self::$candidate_doc = $value;
	}
	//endregion

	//region Nombre del candidato
	private static $candidate_name = null;

	static public function getCandidateName()
	{
		if (self::$candidate_name == null) {
			self::$candidate_name = env('NAME', 'Candidato Vinder');
		}
		return self::$candidate_name;
	}

	static public function setCandidateName($value)
	{
		self::$candidate_name = $value;
	}
	//endregion

	static public function getPoliticalLogo()
	{
		return public_path(env('POLITICAL_LOGO', '/images/partido.jpg'));
	}

	static public function getLocationsSeeder()
	{
		return env('LOCATIONS_SEEDER', 'LocationsMetaTableSeeder');
	}

	public static function getStatisticRolNames()
	{
		if(env('STATISTICS_ROL_NAMES'))
		{
			return array_map('ucfirst', explode(',', env('STATISTICS_ROL_NAMES')));
		}

		return [];
	}

	public static function getStatisticRolIds()
	{
		if(env('STATISTICS_ROL_IDS'))
		{
			return explode(',', env('STATISTICS_ROL_IDS'));
		}
		
		return [];
	}

	public static function getReports()
	{
		return explode(',', env('REPORTS', 'people'));
	}

	public static function getStatistics()
	{
		return explode(',', env('STATISTICS'));
	}

	public static function getSms()
	{
		return explode(',', env('SMS'));
	}

	public static function getVoterTitles()
	{
		return explode(',', env('VOTER_LIST_TITLE'));
	}

	public static function getTeamTitles()
	{
		return explode(',', env('TEAM_LIST_TITLE'));
	}

	public static function getVoterAttributes()
	{
		return explode(',', env('VOTER_LIST_ATTRIBUTE'));
	}

	public static function getTeamAttributes()
	{
		return explode(',', env('TEAM_LIST_ATTRIBUTE'));
	}

	public static function getVoterClass()
	{
		return explode(',', env('VOTER_LIST_CLASS'));
	}

	public static function getTeamClass()
	{
		return explode(',', env('TEAM_LIST_CLASS'));
	}

	static public function isDemo()
	{
		return env('IS_DEMO', true);
	}

	public static function getPhoto()
	{
		return url(env('PHOTO', '/images/placeholders/photos/photo.png'));
	}

	public static function getElibomUsername()
	{
		return env('ELIBOM_USERNAME', 'andresmaopinzon@gmail.com');
	}

	public static function getElibomCode()
	{
		return env('ELIBOM_CODE', 'o6Gi4M04no');
	}

	public static function getMailchimpApiKey()
	{
		return env('MAILCHIMP_APIKEY', '684e040793fa2af676448301627d2482');
	}

	public static function getMailchimpIdList()
	{
		return env('MAILCHIMP_IDLIST', '8e3b136caf');
	}

	public static function getTemplateCss()
	{
		return env('TEMPLATE', 'flat');
	}

	public static function getRegistraduriaUrl()
	{
		return env('REGISTRADURIA_URL', 'https://wsp.registraduria.gov.co/censo/_censoResultado.php?nCedula=');	
	}

	private static $appVersion = null;

    public static function getAppVersion()
    {
		return '1.0';
		/*try
		{
			if (self::$appVersion == null) {
				self::$appVersion = shell_exec('git describe --always');
			}
			return self::$appVersion;
		}
		catch (Exception $e)
		{
			return '1.0';
		}*/
    }

}
