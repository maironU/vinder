<?php namespace App\Libraries\Reports;

use App\Entities\Voter;
use App\Entities\Rol;
use App\Entities\Location;
use App\Entities\PollingStation;
use App\Entities\Diary;
use App\Entities\Community;
use App\Entities\Occupation;
use App\Entities\Poll;

use App\Libraries\Campaing;
use App\Libraries\Fpdf\Report;
use App\Libraries\Fpdf\ReportTable;
use App\Entities\User;

use PDF;
use Excel;

class ReportsUtilities
{
	public static $pageSizeOffice = array(216,330);

	public static function getPeople($sex, $xls = false)
	{
		$voters = Voter::withSex($sex);
    
        $pdf = new ReportTable('L', 'mm', self::$pageSizeOffice);
    
        if(!$xls){
            $pdf->init(Campaing::getCandidateName(), 'Todos los Votantes');	
            $pdf->voters($voters);
            
            return $pdf;
        }else{
			return self::downloadExcelNormalSheet("Todos los Votantes", $voters, $pdf, 'voters');
        }
		
	}

	public static function getUsers($xls = false)
	{
		$users = User::all();
        $pdf = new ReportTable('L', 'mm', self::$pageSizeOffice);

		if(!$xls){
			$pdf->init(Campaing::getCandidateName(), 'Todos los Usuarios', 10);
			$pdf->users($users);
			return $pdf;
		}else{
			return self::downloadExcelNormalSheet("Todos los Usuarios", $users, $pdf, 'users');
		}
		
		return;
	}

	public static function getVoterPolls($poll_id)
	{
		$poll = Poll::findOrFail($poll_id);
		$voterPolls = $poll->voterPolls()->realized()->with('voter', 'answers')->get();
		
		$pdf = new ReportTable('L', 'mm', self::$pageSizeOffice);
		$pdf->init(Campaing::getCandidateName(), 'Sondeo: ' . $poll->name, 9);
		$pdf->voterPolls($poll, $voterPolls);
		
		return $pdf;
	}

	public static function getDelegates($xls = false)
	{
		$voters = Voter::where('delegate', '=', 1)->get();
		
		$pdf = new ReportTable('L', 'mm', self::$pageSizeOffice);
        if(!$xls){
            $pdf->init(Campaing::getCandidateName(), 'Delegados de Campaña');
            $pdf->voters($voters);
            
            return $pdf;
        }else{
			return self::downloadExcelNormalSheet("Delegados de Campaña", $voters, $pdf, 'voters');
        }
		
	}

	public static function getPeopleWithBirthdayCurrentMonth($xls = false)
	{
		$voters = Voter::withBirthdayCurrentMonth();

		$pdf = new ReportTable('L', 'mm', self::$pageSizeOffice);
        if(!$xls){
            $pdf->init(Campaing::getCandidateName(), 'Personas con cumpleaños '.date('m-Y'));
            $pdf->voters($voters);
            
            return $pdf;
        }else{
            $title = 'Personas con cumpleaños '.date('m-Y');
            return self::downloadExcelNormalSheet($title, $voters, $pdf, 'voters');
        }
		
	}

	public static function getPeopleWithoutPollingStation($xls = false)
	{
		$voters = Voter::whereNullPollingStation();

		$pdf = new ReportTable('L', 'mm', self::$pageSizeOffice);
        if(!$xls){
            $pdf->init(Campaing::getCandidateName(), 'Personas sin puesto de votación');
            $pdf->votersWithSuperior($voters);
            
            return $pdf;
        }else{
			return self::downloadExcelNormalSheet("Personas sin puesto de votación", $voters, $pdf, 'votersWithSuperior');
        }
        
	}

	public static function getPeopleOfLocations($locations_id = [], $xls = false)
	{
		$locations = Location::withVoters($locations_id);
		$pdf = new ReportTable('L', 'mm', self::$pageSizeOffice);

        if(!$xls){
            self::getPeopleOfLocationsReport($locations, $pdf);

            return $pdf;
        }else{
            $data = [];
            self::getPeopleOfLocationsReportExcel($locations, $pdf, $data);

			$method = "voters";
			return self::downloadExcelMultipleSheet('Votantes por Ubicaciones', $data, $pdf, $method);
        }
		
	}

	public static function getPeopleOfLocationsReport($locations, $pdf)
	{
		foreach($locations as $location)
		{
			if($location->voters->count() > 0)
			{
				$pdf->init(Campaing::getCandidateName(), 'Votantes por Ubicaciones - ' . $location->name);
				$pdf->voters($location->voters);
			}
			
			self::getPeopleOfLocationsReport($location->childrenLocations, $pdf);
		}
	}

    public static function getPeopleOfLocationsReportExcel($locations, $pdf, &$data)
	{
		foreach($locations as $location)
		{
			if($location->voters->count() > 0)
			{
                $data[$location->name] = $location->voters;
			}
			
			self::getPeopleOfLocationsReportExcel($location->childrenLocations, $pdf, $data);
		}
	}

	public static function getPeopleOfPollingStations($polling_station_ids = [], $rol_ids = [], $colaborator = null, $xls = false)
	{
		$pollingStations = PollingStation::withVoters($polling_station_ids, $rol_ids, $colaborator);
		$roles = Rol::whereIn('id', $rol_ids)->get()->implode('name', ', ');

		if(! is_null($colaborator) && $colaborator == '1')
		{
			$pre_title = 'Equipo';
		}
		else if($colaborator == '0')
		{
			$pre_title = 'Solo Votantes';
		}
		else
		{
			$pre_title = 'Votantes';
		}

		$pdf = new ReportTable('L', 'mm', self::$pageSizeOffice);

        if(!$xls){
            foreach($pollingStations as $key => $pollingStation)
            {
                $title = $pre_title . ' por Puesto de Votación' . ' - ' . $pollingStation->description;
                if($roles)
                {
                    $title .= ' - Cargos (' . $roles . ')';
                }
    
                $pdf->init(Campaing::getCandidateName(), $title);
                $pdf->votersPollingStation($pollingStation->voters->sortBy('superior_name'));
    
                $pdf->Ln();
                $pdf->SetFont('Arial', 'B', 13);
                $pdf->MultiCell(0, 7, utf8_decode('Ubicaciones'), 0, 'L');
    
                $votersCollection = $pollingStation->voters->groupBy('location_recursive_name');
                $locations = [];
    
                foreach ($votersCollection as $location_name => $voters)
                {
                    $locations[$location_name]['name'] = $location_name;
                    $locations[$location_name]['number_voters'] = $voters->count();
                }
                
                $pdf->locations(collect($locations)->sortByDesc('number_voters'));
            }
            
            return $pdf;
        }else{
            $data = [];
            foreach($pollingStations as $key => $pollingStation)
            {
                $data[$pollingStation->description] = $pollingStation->voters->sortBy('superior_name');
            }
			$method = "votersPollingStation";
            $title = $pre_title . ' por Puesto de Votación';

			return self::downloadExcelMultipleSheet($title, $data, $pdf, $method);
        }
		
	}

	public static function getPeopleOfPollingStationsDayD($polling_station_ids = [], $rol_ids = [], $colaborator = null)
	{
		$pollingStations = PollingStation::withVotersDayD($polling_station_ids, $rol_ids, $colaborator);
		
		$roles = Rol::whereIn('id', $rol_ids)->get()->implode('name', ', ');

		if(! is_null($colaborator) && $colaborator == '1')
		{
			$pre_title = 'Equipo';
		}
		else if($colaborator == '0')
		{
			$pre_title = 'Solo Votantes';
		}
		else
		{
			$pre_title = 'Votantes';
		}

		$pdf = new ReportTable('L', 'mm', self::$pageSizeOffice);

		foreach($pollingStations as $key => $pollingStation)
		{
			$title = $pre_title . ' UBICADOS por Puesto de Votación' . ' - ' . $pollingStation->description;
			if($roles)
			{
				$title .= ' - Cargos (' . $roles . ')';
			}

			$pdf->init(Campaing::getCandidateName(), $title);
			$pdf->votersPollingStation($pollingStation->votersDayD->sortBy('superior_name'));

			$pdf->Ln();
			$pdf->SetFont('Arial', 'B', 13);
			$pdf->MultiCell(0, 7, utf8_decode('Ubicaciones'), 0, 'L');

			$votersCollection = $pollingStation->votersDayD->groupBy('location_recursive_name');
			$locations = [];

			foreach ($votersCollection as $location_name => $voters)
			{
				$locations[$location_name]['name'] = $location_name;
				$locations[$location_name]['number_voters'] = $voters->count();
			}
			
			$pdf->locations(collect($locations)->sortByDesc('number_voters'));
		}
		
		return $pdf;
	}

	public static function getPeopleOfOccupations($occupations_id = [], $xls = false)
	{
		$occupations = Occupation::withVoters($occupations_id);
		$pdf = new ReportTable('L', 'mm', self::$pageSizeOffice);

        if(!$xls){
            foreach($occupations as $occupation)
            {
                $title = 'Votantes por Ocupación' . ' - ' . $occupation->name;
                $pdf->init(Campaing::getCandidateName(), $title);
                $pdf->voters($occupation->voters);
            }
            
            return $pdf;
        }else{
            $data = [];
            foreach($occupations as $occupation)
            {
                $data[$occupation->name] = $occupation->voters;
            }
			$method = "voters";
			return self::downloadExcelMultipleSheet('Votantes por Ocupación', $data, $pdf, $method);
        }
		
	}

	public static function getPeopleOfCommunities($communities_id = [], $xls = false)
	{
		$communities = Community::withVoters($communities_id);
		$pdf = new ReportTable('L', 'mm', self::$pageSizeOffice);

        if(!$xls){
            foreach($communities as $community)
            {
                $title = 'Votantes por Comunidades' . ' - ' . $community->name;
                $pdf->init(Campaing::getCandidateName(), $title);
                $pdf->voters($community->voters);
            }
            
            return $pdf;
        }else{
            $data = [];
            foreach($communities as $community)
            {
                $data[$community->name] = $community->voters;
            }
			$method = "voters";
			return self::downloadExcelMultipleSheet('Votantes por Comunidades', $data, $pdf, $method);
        }
	}

	public static function getTeam($xls = false)
	{
		$team = Voter::getTeamWithVoters()->sortByDesc('number_voters');

		$pdf = new ReportTable('L', 'mm', self::$pageSizeOffice);
        if(!$xls){
            $pdf->init(Campaing::getCandidateName(), 'Equipo de Campaña');
            $pdf->team($team);
            
            return $pdf;
        }else{
            return self::downloadExcelNormalSheet("Equipo de campaña", $team, $pdf, 'team');
        }
        
	}

	public static function getTeamWithVoters($team_ids = [], $xls = false)
	{
		$team = Voter::getTeamWithVoters($team_ids);
		$pdf = new ReportTable('L', 'mm', self::$pageSizeOffice);

        if(!$xls){
            foreach($team as $colaborator)
            {
                $pdf->init(Campaing::getCandidateName(), $colaborator->name . ', teléfono: '.$colaborator->telephone);
                $pdf->voters($colaborator->voters);
            }
            
            return $pdf;
        }else{
            $data = [];
            foreach($team as $colaborator)
            {
                $data[$colaborator->name] = $colaborator->voters;
            }
			$method = "voters";
			return self::downloadExcelMultipleSheet('Votantes del equipo', $data, $pdf, $method);
        }
		
	}

	public static function getRecursiveTeam($team_ids = [], $xls = false)
	{
		$team = Voter::with(['voters' => function($query){
			$query->isTeam()
			->orderBy('name', 'asc');
		},'voters.voters' => function($query){
			$query->isTeam()
			->orderBy('name', 'asc');
		},'voters.voters.voters' => function($query){
			$query->isTeam()
			->orderBy('name', 'asc');
		},'voters.voters.voters.voters' => function($query){
			$query->isTeam()
			->orderBy('name', 'asc');
		},'voters.voters.voters.voters.voters' => function($query){
			$query->isTeam()
			->orderBy('name', 'asc');
		}])
		->isTeam()
		->whereIn('id', $team_ids)->get();
    
		$pdf = new ReportTable('L', 'mm', self::$pageSizeOffice);

		if(!$xls){
			foreach($team as $colaborator)
			{
				$pdf->init(Campaing::getCandidateName(), 'Equipo de Campaña - ' . $colaborator->name, 10);
				$pdf->recursiveTeam($colaborator->voters->sortByDesc('number_voters'));
	
				self::recursiveReportTeam($colaborator, $pdf);
			}
			return $pdf;
		}else{
			$data = [];
			foreach($team as $colaborator)
			{
				$data[$colaborator->name] = $colaborator->voters->sortByDesc('number_voters');
                self::recursiveReportTeamExcel($colaborator, $pdf, $data);
			}
			$method = "recursiveTeam";
			return self::downloadExcelMultipleSheet('Equipo de campaña', $data, $pdf, $method);
		}
	}

	public static function downloadExcelMultipleSheet($title, $data, $pdf, $method){
		$excel = Excel::create($title, function($excel) use ($data, $pdf, $method) {
            foreach($data as $name => $sub_data){
				$cosa = $pdf->{$method}($sub_data, 'xlsx');
				$excel->sheet($name, function($sheet) use ($cosa) {
					$sheet->setOrientation('landscape');
					$sheet->setGenerateHeadingByIndices = false;
	//					$sheet->fromArray($titles);
					$sheet->fromArray($cosa);
				});
			}
			
		});
		$excel->export('xlsx');
	}


    public static function downloadExcelNormalSheet($title, $data, $pdf, $method){
		$excel = Excel::create($title, function($excel) use ($data, $pdf, $method) {
            $cosa = $pdf->{$method}($data, 'xlsx');
            $excel->sheet('Hoja1', function($sheet) use ($cosa) {
                $sheet->setOrientation('landscape');
                $sheet->setGenerateHeadingByIndices = false;
//					$sheet->fromArray($titles);
                $sheet->fromArray($cosa);
            });
		});
		$excel->export('xlsx');
	}
	public static function getRecursiveTeamVoters($team_ids = [], $xls = false)
	{
		$team = Voter::with(['voters.voters.voters.voters.voters.voters' => function($query){
			$query->orderBy('colaborator', 'desc')
				->orderBy('name', 'asc');
		}])
		->whereIn('id', $team_ids)->get();

		$pdf = new ReportTable('L', 'mm', self::$pageSizeOffice);

        if(!$xls){
            foreach($team as $colaborator)
            {
                $pdf->init(Campaing::getCandidateName(), 'Equipo de Campaña y Votantes - ' . $colaborator->name, 10);
                $pdf->recursiveTeam($colaborator->voters->sortByDesc('number_voters'));
    
                self::recursiveReportTeam($colaborator, $pdf);
            }
            
            return $pdf;
        }else{
            $data = [];
			foreach($team as $colaborator)
			{
				$data[$colaborator->name] = $colaborator->voters->sortByDesc('number_voters');
				self::recursiveReportTeam($colaborator, $pdf, $data);
            }
			$method = "recursiveTeam";
			return self::downloadExcelMultipleSheet('Equipo de Campaña y Votantes', $data, $pdf, $method);
        }
		
	}

	public static function getRecursiveOrientatorVoters($team_ids = [], $xls = false)
	{
		$team = \App\Entities\User::whereIn('id', $team_ids)
		->get();
		$pdf = new ReportTable('L', 'mm', self::$pageSizeOffice);

        if(!$xls){
            foreach($team as $colaborator)
            {
                $pdf->init(Campaing::getCandidateName(), 'Orientadores y Votantes - ' . $colaborator->name, 10);
                $pdf->recursiveTeam($colaborator->voters_leaders);
            }
            return $pdf;
        }else{
            $data = [];
			foreach($team as $colaborator)
			{
				$data[$colaborator->name] = $colaborator->voters_leaders;
			}
			$method = "recursiveTeam";
			return self::downloadExcelMultipleSheet('Orientadores y Votantes', $data, $pdf, $method);
        }
		
	}

	private static function recursiveReportTeam($colaborator, $pdf)
	{
		foreach ($colaborator->voters->sortByDesc('number_voters') as $voter)
		{
			if($voter->voters->count() >= 1)
			{
				$pdf->init(Campaing::getCandidateName(), $voter->title_report, 10);
				$pdf->recursiveTeam($voter->voters->sortByDesc('number_voters'));

				self::recursiveReportTeam($voter, $pdf);
			}
		}
	}

    private static function recursiveReportTeamExcel($colaborator, $pdf, &$data)
	{
		foreach ($colaborator->voters->sortByDesc('number_voters') as $voter)
		{
			if($voter->voters->count() >= 1)
			{
                $data[$voter->title_report] = $voter->voters->sortByDesc('number_voters');
				self::recursiveReportTeam($voter, $pdf, $data);
			}
		}
	}

	public static function getTeamOfRoles($rol_ids = [], $xls = false)
	{
		$roles = Rol::withTeam($rol_ids)->get();
		$pdf = new ReportTable('L', 'mm', self::$pageSizeOffice);
            if(!$xls){
            foreach($roles as $rol){
                if($rol->name == "Orientador"){
                    $pdf->init(Campaing::getCandidateName(), $rol->name, 8);

                    $users = \App\Entities\User::select('user.name', 'l.name as location_name', 'user.address')
                                ->where('user.type_id', '=', 6)
                                ->orderBy('name')
                                ->join('locations as l', 'user.location_id', '=', 'l.id')
                                ->get();
                    $pdf->orientators($users);
                }else{
                    $pdf->init(Campaing::getCandidateName(), $rol->name, 8);
                    $pdf->teamOfRoles($rol->voters->sortBy('superior_name_with_roles'));
                }
            }
            
            return $pdf;
        }else{
            $dataOrientador = [];
            $dataTeams = [];
            foreach($roles as $rol){
                if($rol->name == "Orientador"){
                    $users = \App\Entities\User::select('user.name', 'l.name as location_name', 'user.address')
                        ->where('user.type_id', '=', 6)
                        ->orderBy('name')
                        ->join('locations as l', 'user.location_id', '=', 'l.id')
                        ->get();
                    $dataOrientador[$rol->name] = $users; 
                }else{
                    $dataTeams[$rol->name] = $rol->voters->sortBy('superior_name_with_roles'); 
                }
            }

            $excel = Excel::create("Equipo por cargos", function($excel) use ($dataOrientador, $dataTeams, $pdf) {
                foreach($dataOrientador as $name => $sub_data){
                    $cosa = $pdf->orientators($sub_data, 'xlsx');
                    $excel->sheet($name, function($sheet) use ($cosa) {
                        $sheet->setOrientation('landscape');
                        $sheet->setGenerateHeadingByIndices = false;
        //					$sheet->fromArray($titles);
                        $sheet->fromArray($cosa);
                    });
                }

                foreach($dataTeams as $name => $sub_data){
                    $cosa = $pdf->teamOfRoles($sub_data, 'xlsx');
                    $excel->sheet($name, function($sheet) use ($cosa) {
                        $sheet->setOrientation('landscape');
                        $sheet->setGenerateHeadingByIndices = false;
        //					$sheet->fromArray($titles);
                        $sheet->fromArray($cosa);
                    });
                }
                
            });
		    $excel->export('xlsx');
            return;
        }
		
	}

	public static function getTemplatesOfTeam($team_ids = [])
	{
		$team = Voter::getTeam($team_ids);
		$pdf = new ReportTable('L', 'mm', self::$pageSizeOffice);
        
		foreach($team as $colaborator)
		{
			$pdf->init(Campaing::getCandidateName(), $colaborator->name . ', teléfono: '.$colaborator->telephone);
			$pdf->blank();
		}
		
		$pdf->Output();
	}

	public static function printDiary($date = null){
		if(is_null($date))
		{
			$date = date('d-m-Y');
		}

		$title = 'Agenda de '. $date;
		$events = Diary::where('date', '=', $date)->orderBy('time', 'ASC')->get();

		$pdf = new Report('P', 'mm', self::$pageSizeOffice);
		$pdf->init(Campaing::getCandidateName(), $title);

		foreach($events as $item => $event) {
			$pdf->SetFont('Arial','B',14);
			$pdf->MultiCell(0,10, utf8_decode($event->name .' por '. $event->organizer->name), 'LRT', 1);
			$pdf->SetFont('Arial','',12);
			$pdf->MultiCell(0,8, utf8_decode('Hora: ' . $event->time_to_endtime), 'LR', 1);
			$pdf->MultiCell(0,8, utf8_decode('Lugar: ' . $event->location->name . ', ' . $event->place), 'LR', 1);
			$pdf->MultiCell(0,8, utf8_decode('Organizador: ' . $event->organizer->name . ', ' . $event->organizer->telephone), 'LR', 1);
			$pdf->MultiCell(0,8, utf8_decode('Delegado: ' . $event->delegate->name . ', ' . $event->delegate->telephone), 'LR', 1);
			$pdf->MultiCell(0,8, utf8_decode('Cantidad de personas: ' . $event->people), 'LR', 1);

			if(!empty($event->logistic)) {
				$pdf->MultiCell(0,8, utf8_decode('Logistica: ' . $event->logistic), 'LR', 1);
			}
			if(!empty($event->advertising)) {
				$pdf->MultiCell(0,8, utf8_decode('Publicidad: ' . $event->advertising), 'LR', 1);
			}
			$pdf->MultiCell(0,8, utf8_decode('Notas: ' . $event->description), 'LRB', 1);
			$pdf->Ln();
		}

		return $pdf;
	}

	// Reportes > Plan de campaña
	public static function getPlans($plan_ids)
	{
		$pdf = new ReportTable('L', 'mm', self::$pageSizeOffice);

		$habeas = ['La presente planilla se ajusta a la ley 1581 de 2012 y al Decreto 1377 de 2013 los cuales nos rigen a solicitar su autorización para la recolección de datos personales;',
					'por lo que autorizo la recolección y divulgacion de mis datos personales consignados en la presente, de mi puño y los cuales quedan consignados y a su disposicion.'];
		

		foreach ($plan_ids as $plan_id)
		{
			$plan = ReportTable::$plans[$plan_id];
			$pdf->init(Campaing::getCandidateName(), 'Plan 1x'.$plan, false);
			$pdf->withHabeas = $habeas;
			$pdf->planBlank($plan);
		}
		
		return $pdf;
	}

	// Reportes > Plan de campaña de equipo
	public static function getPlansTeam($team_ids, $xls = false)
	{
		$team = Voter::getTeamWithVoters($team_ids);

		if (!$xls) {
			$pdf = new ReportTable('L', 'mm', self::$pageSizeOffice);

			$habeas = ['La presente planilla se ajusta a la ley 1581 de 2012 y al Decreto 1377 de 2013 los cuales nos rigen a solicitar su autorización para la recolección de datos personales;',
						'por lo que autorizo la recolección y divulgacion de mis datos personales consignados en la presente, de mi puño y los cuales quedan consignados y a su disposicion.'];

			foreach($team as $colaborator)
			{
				//TODO: Hacer método para buscar nombre de ocupación enviando el id "$colaborator->occupation"
				$plan = ReportTable::findPlan($colaborator->voters->count());
				$pdf->init(Campaing::getCandidateName(), 'Plan 1x' . $plan);
				$pdf->withHabeas = $habeas;
				$pdf->planTeam($colaborator, $plan);
				$pdf->Output();
			}
		}
		else {
			foreach($team as $colaborator)
			{
				ReportsUtilities::downloadExcelReport($team, "Plan de Campaña de equipos");
			}
		}

		return;
	}

	// Exporta a excel (Sin títulos ni formatos)
	public static function downloadExcelReport($data, $bookName = "Reporte Excel", $sheetName = "Hoja1")
	{
		Excel::create($bookName, function($excel) use ($data, $sheetName) {
			$excel->sheet($sheetName, function($sheet) use ($data) {
				$sheet->fromArray($data);
			});
		})->export('xlsx');
	}

	// Exporta nombres y números whatsapp a csv
	public static function getWhatsapp($sex)
	{ 
		$colSplit = ';';
		$newLine = "\r\n";
		$voters = Voter::getVoterSex($sex);

		$fileContent = ReportsUtilities::convertToIso8859_1(
			"Nombre" . $colSplit .
			"Whatsapp" . $colSplit .
			"Teléfono" . $colSplit .
			"Teléfono alternativo\r\n");

		foreach($voters as $voter)
		{
			$fileContent = $fileContent . 
				ReportsUtilities::convertToIso8859_1(
					ucwords(mb_strtolower($voter->name)) . $colSplit . 
					$voter->whatsapp . $colSplit . 
					$voter->telephone . $colSplit . 
					$voter->telephone_alternative . $newLine
				);
		}
		return $fileContent;
	}

	public static function convertToIso8859_1($input)
	{
		return iconv("UTF-8", "ISO-8859-1", $input);
	}

	public static function getSemaphore($semaphore)
	{
		$title = '';
		$colors = ["Indefinido", "Voto Fijo", "Indeciso", "Voto duro"];
		$voters = Voter::getVotersPerSemaphoreColor($semaphore);
		foreach($semaphore as $color)
		{
			$title = $title . $colors[$color] . ' - ';
		}
		$title = 'Intención de voto ('. $title .')';

		$pdf = new ReportTable('L', 'mm', self::$pageSizeOffice);
		$pdf->init(Campaing::getCandidateName(), $title );
		$pdf->semaphore($voters);

		return $pdf;
	}

}
?>