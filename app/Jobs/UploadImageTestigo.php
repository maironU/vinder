<?php

namespace App\Jobs;

use App\Entities\FormE14File;
use App\Jobs\Job;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class UploadImageTestigo extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    public $file;
    public $forme14_file;
    public $primary;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($file, $forme14_file, $primary = true)
    {
        $this->file = $file;
        $this->forme14_file = $forme14_file;
        $this->primary = $primary;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if($this->forme14_file){
            $this->forme14_file->save();
            log::info($this->forme14_file);
            $forme14_file = FormE14File::find($this->forme14_file->id);
            $this->updateState("SUBIENDO", $forme14_file);

            $fileName = null;
            try{
                $fileName = $this->saveFileForm($this->file);
            }catch(\Exception $e){
                $this->updateState("ERROR", $forme14_file);
                Log::error(["error" => true, "message" => "Hubo un error al guardar el archivo", "emessage" => $e->getMessage()]);
            }
            
            if($fileName){
                $file = $this->primary ? $forme14_file->file : $forme14_file->file_secondary;

                if($file){
                    $path = public_path("files_forme14/{$file}");
    
                    if(file_exists($path) && $file){
                        unlink($path);
                    }
                }
    
                if($this->primary){
                    $forme14_file->file = $fileName;
                }else{
                    $forme14_file->file_secondary = $fileName;
                }
                $forme14_file->save(); 
                $this->updateState("SUBIDA", $forme14_file);
            }else{
                $this->updateState("ERROR", $forme14_file);
                Log::error("No se guardó la imagen");
            }
        }
    }

    public function updateState($state, $forme14_file){
        if($this->primary){
            $forme14_file->state_file = $state;
        }else{
            $forme14_file->state_file_secondary = $state;
        }
        $forme14_file = $forme14_file->save();
    }

    public function saveFileForm($file){
        preg_match("/data:application\/(.*?);/",$file,$file_extension);

        if(count($file_extension) > 0) {
            $file = preg_replace('/data:application\/(.*?);base64,/','',$file);
        }else{
            preg_match("/data:image\/(.*?);/",$file,$file_extension);
            $file = preg_replace('/data:image\/(.*?);base64,/','',$file);
        }
        $file = str_replace(' ', '+', $file);

        $numbers = rand(1000000000,9999999999);

        $fileName = 'file_' . $numbers   . '.' . $file_extension[1];
        Storage::disk('files_forme14')->put($fileName, base64_decode($file));

        return $fileName;
    }
}
