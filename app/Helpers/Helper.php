<?php
namespace App\Helpers;

use App\Entities\PollingStation;
use App\Entities\User;
use Illuminate\Support\Facades\DB;
use GuzzleHttp\Client;

class Helper {
    protected static $url = "https://maps.googleapis.com/maps/api/geocode/json";
    protected static $apiKey = "AIzaSyDNy9BfpOnvvIKlRkyXIboKbfEbtmNmkFU";

    public static function getCoordinates($address, $location = "") {
        $url = self::$url."?address={$address} {$location}, Villavicencio, Meta&key=".self::$apiKey;

        $url = self::replaceSpecialCharacters($url);
        $client = new Client;
        try{
            $request = $client->get($url);
            return $request->json();
        }catch(\Exception $e){
            return false;
        }

    }

    public static function replaceSpecialCharacters($url){
        $url_replace = str_replace(" ", "+", $url);
        $url_replace = str_replace("#", "+", $url);
        return $url_replace;
    }

    public static function isAvailableTable($polling_station_id, $table){
        $user = User::where('type_id', 5)
            ->where('polling_station_id', $polling_station_id)
            ->whereHas('tables_numbers', function($subquery) use($table){
                $subquery->where('table_number', $table)
                ->orWhere('table_number', 'Mesa '.$table);
            })
            ->first();

        if(!$user) return true;

        return false;
    }

    public static function generateEmail($first_name = null, $surname = null){
        $concat = $first_name. " ".$surname;
        if(!$first_name && !$surname){
            $concat = self::generateString();
        }

        $slug = str_slug($concat, "_");
        $email = $slug."@gmail.com";

        $user = User::where('email', $email)->first();
        if($user){
            self::generateEmail($first_name, $surname);
        }
        return $email;
    }

    public static function generateString(){
        $caracteres_permitidos = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $longitud = 12;
        return substr(str_shuffle($caracteres_permitidos), 0, $longitud);
    }

    public static function getPollingStationByVoterNew($polling_name = null){
        if(!$polling_name) return;

        $polling_station = PollingStation::where('name', $polling_name)->first();
        if(!$polling_station) return;

        return $polling_station->id;
    }
}
