<?php

namespace App\Console\Commands;

use App\ColaProceso;
use App\Http\Services\Coordinador;
use App\Http\Services\Orientador;
use App\Http\Services\TestigoElectoral;
use App\Http\Services\Votante;
use App\Http\Services\Cities;
use App\Http\Services\PollingStation;
use App\Http\Services\TestigoElectoralCedula;
use Carbon\Carbon;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class ColaProcesoCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:cola_procesos';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cron para actualizar las actualizaciones';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try{
            Log::info("Inicia syncColaProcesos ");
            $cola_procesos = ColaProceso::where('estado_proceso_id', 2)
                                ->where('bloqueado', 0)
                                ->where('created_at', 'like', Carbon::now()->format('Y-m-d')."%")
                                ->get();

            foreach($cola_procesos as $cola_proceso){
                $cola_proceso->estado_proceso_id = 1;
                $cola_proceso->save();
            }
            foreach($cola_procesos as $cola_proceso){
                try{
                    if(!$cola_proceso->parametros) continue;
                    $parametros = json_decode($cola_proceso->parametros);
                    $path = $parametros->file;
                    switch($cola_proceso->proceso_id){
                        case 1:
                            $instance = new Votante($path);
                            break;
                        case 2:
                            $instance = new Coordinador($path);
                            break;
                        case 3:
                            $instance = new Orientador($path);
                            break;  
                        case 4:
                            $instance = new TestigoElectoral($path);
                            break;  
                        case 5:
                            $instance = new PollingStation($path);
                            break;  
                        case 6:
                            $instance = new Cities($path);
                            break;
                        case 7:
                            $instance = new TestigoElectoralCedula($path);
                            break; 
                    }
                    $instance->sync();

                    $cola_proceso->estado_proceso_id = 3;
                    $cola_proceso->save();
                }catch(Exception $e){
                    $cola_proceso->estado_proceso_id = 3;
                    $cola_proceso->bloqueado = 1;
                    $cola_proceso->save();
                    Log::error(["Error en syncColaProcesos " . $e->getMessage() . " linea " . $e->getLine(), "proceso_id" => $cola_proceso->id]);
                }
            }
            Log::info("Termina syncColaProcesos ");
        }catch(Exception $e){
            Log::error("Error en syncColaProcesos " . $e->getMessage() . " linea " . $e->getLine());
        }
    }
}
