<?php

namespace App\Console;

use App\Jobs\ImportFileJob;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\Inspire::class,
        \App\Console\Commands\Birthday::class,
        \App\Console\Commands\UpdatePollingStations::class,
        \App\Console\Commands\ColaProcesoCommand::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('inspire')
                 ->hourly();

        $schedule->command('command:birthday')
            ->dailyAt('17:00');

        $schedule->command('command:birthday')
            ->dailyAt('09:30');

        $schedule->command('update:cola_procesos')
            ->everyMinute();

    }
}
