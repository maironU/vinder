<?php

use App\Libraries\Reports\Report;
use App\Libraries\Campaing;

use App\Entities\Location;
use App\Libraries\Sms\SendSMS;
use App\Entities\Voter;
use Maatwebsite\Excel\Facades\Excel;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/***** Auth *****/
Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

Route::get('update/polling_station/voters/{min}/{max}', function($min, $max){
	ini_set('MAX_EXECUTION_TIME', '-1');
	$voters = \App\Entities\Voter::offset($min)->take($max)->get();;
	$no_update = [];
	foreach($voters as $voter){
		$voternew = \DB::table('votersnew')->where('nuip', $voter->doc)->first();
		if($voternew){
			$polling_bdd = \DB::table('polling_stations')->where('name', $voternew->polling_station)->first();
			if($polling_bdd){
					$voter->polling_station_id = $polling_bdd->id;
					$voter->table_number = $voternew->table;
					$voter->save();
			}else{
				array_push($no_update, $voter);
			}
		}
	}
	ob_end_clean();
	ob_start();
	Excel::create('votantes_no_actualizados desde '.$min."-".$max, function($excel) use($no_update) {
		$excel->setTitle('Votantes no actualizados');
		$excel->sheet('Nueva hoja', function($sheet) use($no_update){
			$sheet->setOrientation('landscape');
			$sheet->loadView('exports.votersnoupdate', ["voters" => $no_update]);
		});
	
	})->export('xls');
});

Route::get('save/coordinates/voters', function(){
	set_time_limit(500);
	$voters = Voter::all();
	foreach($voters as $voter){
		if($voter->address && $voter->latitude == 0 && $voter->longitude == 0){
			$location_name = "";
			if($voter->location_id){
				$location_collect = Location::where('id', $voter->location_id)->first();
				if($location_collect){
					$location_name = $location_collect->name;
				}
			}
			$data = \Helper::getCoordinates($voter->address, $location_name);
			if($data){
				$latitude = 0;
				$longitude = 0;
		
				if($data["status"] == "OK"){
					$results = $data["results"];
					$latitute = $results[0]["geometry"]["location"]["lat"];
					$longitude = $results[0]["geometry"]["location"]["lng"];

					$voter->latitude = $latitute;
					$voter->longitude = $longitude;
					$voter->save();
				}
			}
		}
	}

	return redirect()->back();
});

/** App **/
Route::group(['middleware' => ['auth']], function()
{
	// /
	Route::get('/', ['as' => 'modules', 'uses' => 'DashboardController@index']);

	// /system/users
	Route::resource('info', 'MapsController');


	// /maps
	Route::group(['prefix' => 'maps', 'middleware' => 'auth'], function() 
	{
		// /maps/
		Route::get('/', ['as' => 'maps.index', 'uses' => 'Secondary\MapsController@index']);
		Route::post('/filter', ['as' => 'maps.index', 'uses' => 'Secondary\MapsController@filter']);
//		Route::get('/polling-stations', ['as' => 'maps.pollingStations', 'uses' => 'Secondary\MapsController@getMapPollingStations']);
	});

	// /maps
	Route::group(['prefix' => 'witnesses', 'middleware' => 'auth'], function() 
	{
		// /witnesses/
		Route::get('/', ['as' => 'witnesses.index', 'uses' => 'WitnessesController@index']);
		Route::get('/createform', ['as' => 'witnesses.createform', 'uses' => 'WitnessesController@createForm' ]);
		Route::get('/createform/e14', ['as' => 'witnesses.createforme14', 'uses' => 'WitnessesController@createFormE14' ]);
		Route::post('/createform/e14', ['as' => 'witnesses.createpostforme14', 'uses' => 'WitnessesController@createPostFormE14' ]);
		Route::delete('/deleteForm/e14/{forme14_id}', ['as' => 'witnesses.destroy', 'uses' => 'WitnessesController@destroyFormE14']);

		Route::get('/editform/e14/{forme14_id}/{table_number?}/{testigo_id?}', ['as' => 'witnesses.editforme14', 'uses' => 'WitnessesController@editFormE14' ]);
		Route::get('/list_tables', ['as' => 'witnesses.list_tables', 'uses' => 'WitnessesController@lisTables' ]);

		//obtener toda la infor del formulario e14 para el modal
		Route::get('/forme14/all/{forme14_id}/{table_number}/{user_id}', ['as' => 'witnesses.forme14all', 'uses' => 'WitnessesController@getAllFormE14' ]);

		Route::post('/editform/e14/{forme14_id}/{table_number?}/{testigo_id?}', ['as' => 'witnesses.editpostforme14', 'uses' => 'WitnessesController@editPostFormE14' ]);
		Route::post('/e14/uploadImageTemp/{forme14_id}/{table_number}/{testigo_id?}', ['as' => 'witnesses.uploadimagetemp', 'uses' => 'WitnessesController@uploadImageTempFormE14' ]);
		
		Route::post('/forms/e14/{forme14_id}', ['as' => 'witnesses.editpostforme14', 'uses' => 'WitnessesController@editPostFormE14' ]);

		Route::get('/forme14/validate_state_file/{forme14_id}/{table_number?}/{user_id?}', ['as' => 'witnesses.forme14validatestate', 'uses' => 'WitnessesController@getStateFile' ]);

		Route::group(['middleware' => 'permission:1'], function(){
			Route::get('/assign/create/{cedula?}', ['as' => 'witnesses.create', 'uses' => 'WitnessesController@create']);
			Route::get('/results', ['as' => 'witnesses.resultsaccordingwitnesses', 'uses' => 'WitnessesController@resultsaccordingwitnesses' ]);
		});

		Route::group(['prefix' => '/', 'middleware' => 'role:Coordinador de zona,Súper Administrador'], function() {
			Route::get('/coordinator/{coordinator_zone_id?}', ['as' => 'coordinator.index', 'uses' => 'CoordinatorPositionController@index' ])->where('coordinator_zone_id', '[0-9]+');
			Route::get('/coordinator/{id}/edit', ['as' => 'coordinator.edit', 'uses' => 'CoordinatorPositionController@edit']);
			Route::put('/coordinator/{id}/edit', ['as' => 'coordinator.update', 'uses' => 'CoordinatorPositionController@update']);
		});


		Route::group(['prefix' => '/', 'middleware' => 'role:Súper Administrador'], function() {
			Route::get('/assign', ['as' => 'witnesses.assignwitnesses', 'uses' => 'WitnessesController@assignwitnesses' ]);
			Route::get('/assign_cedula', ['as' => 'witnesses.assignwitnesses.cedula', 'uses' => 'WitnessesController@assignwitnessesCedula' ]);
			Route::get('/assign/{id}/edit', ['as' => 'witnesses.edit', 'uses' => 'WitnessesController@edit']);
			Route::post('/assign/create', ['as' => 'witnesses.store', 'uses' => 'WitnessesController@store']);
			Route::put('/assign/{id}/edit', ['as' => 'witnesses.update', 'uses' => 'WitnessesController@update']);

			// zonas
			Route::get('/zone', ['as' => 'zone.index', 'uses' => 'ZoneController@index' ]);
			Route::get('/zone/create', ['as' => 'zone.create', 'uses' => 'ZoneController@create']);
			Route::get('/zone/{id}/edit', ['as' => 'zone.edit', 'uses' => 'ZoneController@edit']);
			Route::post('/zone/create', ['as' => 'zone.store', 'uses' => 'ZoneController@store']);
			Route::put('/zone/{id}/edit', ['as' => 'zone.update', 'uses' => 'ZoneController@update']);

			//coordinador de puestos
			Route::get('/coordinator/create', ['as' => 'coordinator.create', 'uses' => 'CoordinatorPositionController@create']);
			Route::post('/coordinator/create', ['as' => 'coordinator.store', 'uses' => 'CoordinatorPositionController@store']);

			//coordinador de zonas
			Route::get('/coordinator_zone', ['as' => 'coordinator_zone.index', 'uses' => 'CoordinatorZoneController@index' ]);
			Route::get('/coordinator_zone/create/{cedula?}', ['as' => 'coordinator_zone.create', 'uses' => 'CoordinatorZoneController@create']);
			Route::get('/coordinator_zone/{id}/edit', ['as' => 'coordinator_zone.edit', 'uses' => 'CoordinatorZoneController@edit']);
			Route::post('/coordinator_zone/create', ['as' => 'coordinator_zone.store', 'uses' => 'CoordinatorZoneController@store']);
			Route::put('/coordinator_zone/{id}/edit', ['as' => 'coordinator_zone.update', 'uses' => 'CoordinatorZoneController@update']);

			//lideres
			Route::get('/leaders', ['as' => 'witnesses.leaders', 'uses' => 'WitnessesController@indexLeaders']);
			Route::get('/leaders/create', ['as' => 'witnesses.leaders.create', 'uses' => 'WitnessesController@createLeaders']);
			Route::post('/leaders/create', ['as' => 'witnesses.leaders.store', 'uses' => 'WitnessesController@storeLeaders']);
			Route::get('/leaders/{id}/edit', ['as' => 'witnesses.leaders.edit', 'uses' => 'WitnessesController@editLeaders']);
			Route::put('/leaders/{id}/edit', ['as' => 'witnesses.leaders.update', 'uses' => 'WitnessesController@updateLeaders']);

			// /database/incidents
			Route::get('incidents', ['as' => 'witnesses.incidents.index', 'uses' => 'IncidentsController@index']);
			Route::get('incidents/api', ['as' => 'witnesses.incidents.indexApi', 'uses' => 'IncidentsController@indexApi']);

			// /database/incidents
			Route::get('polling_stations', ['as' => 'witnesses.polling_stations.index', 'uses' => 'WitnessesController@pollingStations']);
			Route::post('polling_stations/assign_testigo', ['as' => 'witnesses.polling_stations_assign.index', 'uses' => 'WitnessesController@pollingStationsAssignTestigo']);
		});

		Route::delete('/infoForm/delete/{user_id}/{table_number}/{form_id}', ['as' => 'infoForm.delete', 'uses' => 'WitnessesController@emptyInfoForm' ]);
		Route::get('/results/statistic/{forme14_id?}/{candidate_id?}', ['as' => 'witnesses.resultstatistic', 'uses' => 'ResultController@index'])->where([
			'forme14_id' => '[0-9]+',
			'candidate_id' => '[0-9]+'
		]);
		Route::get('/results/statistic/table/{polling_station_id?}/{candidate_id?}/{forme14_id?}', ['as' => 'witnesses.resultstatistictable', 'uses' => 'ResultController@indexTable']);

		Route::get('/update/results/statistic/{forme14_id?}/{candidate_id?}', ['as' => 'witnesses.update.resultstatistic', 'uses' => 'ResultController@getStatisticUpdate'])->where([
			'forme14_id' => '[0-9]+',
			'candidate_id' => '[0-9]+'
		]);

		Route::get('/update/results/statistic/table/{polling_station_id?}/{candidate_id?}/{forme14_id?}', ['as' => 'witnesses.update.resultstatistictable', 'uses' => 'ResultController@getStatisticTableUpdate'])->where([
			'forme14_id' => '[0-9]+',
			'candidate_id' => '[0-9]+'
		]);
		
		//resultados estadísticos
		//Route::get('/results/statistic/polling_station/{forme14_id?}/{candidate_id?}/', ['as' => 'witnesses.resultstatisticpollingstation', 'uses' => 'ResultController@resultPollingStation']);
		Route::get('/candidates/{form_id}', ['as' => 'witnesses.candidatesforform', 'uses' => 'ResultController@getCandidatesForForm']);


		//rutas de la api mentionilitycs
		Route::get('/mentionlytics', ['as' => 'witnesses.mentionlytics', 'uses' => 'WitnessesController@mentionlytics' ]);
	});

	Route::group(['namespace' => 'System'], function() {
		Route::get('polling_stations/{id}', ['as' => 'polling-stations.show', 'uses' => 'PollingStationsController@show']);
	});

	Route::group(['prefix' => 'system','middleware' => 'module:voters_polling', 'namespace' => 'System'], function() {
		Route::get('voters_polling', ['as' => 'voters_polling.index', 'uses' => 'VoterPollingController@index']);
	});


	/******* SYSTEM ******/
	Route::group(['prefix' => 'system', 'middleware' => 'module:system', 'namespace' => 'System'], function() 
	{
		// /system/
		Route::get('/', ['as' => 'system', 'uses' => 'SystemController@getindex']);
		// /system/logs
		Route::get('logs', ['as' => 'system.logs', 'uses' => '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index']);
		// /system/users
		Route::resource('users', 'UsersController');
		// /system/user-types
		Route::resource('user-types', 'UserTypesController');
		// /system/modules
		Route::resource('modules', 'ModulesController');

		// /system/locations/{id_usuario}/destroy
		Route::get('locations/{id_usuario}/destroy', [
			'uses' => 'LocationsController@destroy',
			'as'   => 'system.locations.destroy'
		]);

		// /system/locations
		Route::resource('locations', 'LocationsController', ['except' => ['destroy']]);

		// /system/polling-stations/{id_usuario}/destroy
		Route::get('polling-stations/{id_usuario}/destroy', [
			'uses' => 'PollingStationsController@destroy',
			'as'   => 'system.polling-stations.destroy'
		]);

		// /system/polling-stations
		Route::resource('polling-stations', 'PollingStationsController', ['except' => ['destroy']]); 
	});
	

	Route::group(['prefix' => 'procesos', 'middleware' => 'module:database'], function() {
		//rutas para actualizaciones
		Route::get('/', ['as' => 'procesos.index', 'uses' => 'ColaProcesoController@index']);
		Route::get('/api', ['as' => 'procesos.indexApi', 'uses' => 'ColaProcesoController@indexApi']);
		Route::get('/create', ['as' => 'procesos.create', 'uses' => 'ColaProcesoController@create']);
		Route::post('/store', ['as' => 'procesos.store', 'uses' => 'ColaProcesoController@store']);
	});

	Route::group(['prefix' => 'wasapi', 'middleware' => 'module:database'], function() {
		//rutas para actualizaciones
		Route::get('/', ['as' => 'whatsapi.index', 'uses' => 'WhatsApiController@index']);
		Route::post('/store', ['as' => 'whatsapi.store', 'uses' => 'WhatsApiController@store']);
	});
	/****** DATABASE ******/
	Route::group(['prefix' => 'database', 'namespace' => 'Database', 'middleware' => 'module:database'], function() 
	{
		// /database/
		Route::get('/', ['as' => 'database', 'uses' => 'DataBaseController@index']);

		Route::group(['middleware' => 'module:delete-voters'], function() {
			// /database/voters/delete/{id}
			Route::post('voters/delete/{id}', ['as' => 'database.voters.destroy', 'uses' => 'VotersController@destroy']);
		});

		Route::group(['middleware' => 'module:voters'], function() {
			// /database/voters
			Route::get('voters', ['as' => 'database.voters.index', 'uses' => 'VotersController@index']);

			// /database/redirect
			Route::post('voters/redirect', ['as' => 'database.voters.redirect', 'uses' => 'VotersController@redirect']);
			// /database/search

			//restaurar votante
			Route::get('voters/restore/{voter_id}', ['as' => 'database.voters.restore', 'uses' => 'VotersController@restoreVoter']);

			Route::get('voters/search', ['as' => 'database.voters.search', 'uses' => 'VotersController@search']);
			// /database/find-name/{doc}
			Route::get('voters/find-name/{doc}', ['as' => 'database.voters.find-name', 'uses' => 'VotersController@findName']);
			// /database/voters/find-polling-station/{doc}
			Route::get('voters/find-polling-station/{doc}', ['as' => 'database.voters.find-polling-station', 'uses' => 'VotersController@findPollingStation']);
			// /database/voters/diaries/{doc}
			Route::get('voters/diaries/{doc}', ['as' => 'database.voters.diaries', 'uses' => 'VotersController@diaries']);
			
			// /database/voters/{id}/add-to-team/{diary?}
			Route::get('voters/{id}/add-to-team/{diary?}', ['as' => 'database.voters.add-to-team', 'uses' => 'VotersController@addToTeam', 'middleware' => 'module:add-to-team-voters']);
			// /database/voters/{doc}/{diary?}
			Route::get('voters/{doc}/{diary?}', ['as' => 'database.voters.create', 'uses' => 'VotersController@create']);
			// /database/voters/{doc}/{diary?}
			Route::post('voters/{doc}/{diary?}', ['as' => 'database.voters.store', 'uses' => 'VotersController@store']);
		});

		Route::group(['middleware' => 'module:team'], function() {
			// /database/team
			Route::get('team', ['as' => 'database.team.index', 'uses' => 'ColaboratorsController@index']);
			
			//restaurar votante
			Route::get('team/restore/{voter_id}', ['as' => 'database.team.restore', 'uses' => 'ColaboratorsController@restoreVoter']);
			// /database/team/redirect
			Route::post('team/redirect', ['as' => 'database.team.redirect', 'uses' => 'ColaboratorsController@redirectColaborator']);
			// /database/team/{doc}/remove/{diary?}
			Route::get('team/{doc}/remove/{diary?}', ['as' => 'database.team.remove', 'uses' => 'ColaboratorsController@remove']);				
			// /database/team/{doc}/{diary?}
			Route::get('team/{doc}/{diary?}', ['as' => 'database.team.create', 'uses' => 'ColaboratorsController@create']);
			// /database/team/{doc}/{diary?}
			Route::post('team/{doc}/{diary?}', ['as' => 'database.team.store', 'uses' => 'ColaboratorsController@store']);
		});

		Route::group(['middleware' => 'module:roles'], function() {
			// /database/roles
			Route::resource('roles', 'RolesController',  ['only' => ['index', 'edit', 'update', 'store', 'destroy']]);
		});
	});


	/****** SECONDARY ******/
	Route::group(['namespace' => 'Secondary'], function()
	{
		Route::group(['middleware' => 'module:reports'], function()
		{
			// /reports
			Route::controller('reports', 'ReportsController', [
				'getIndex' 								=> 'reports.index',

				'getPeople' 							=> 'reports.people',
				'getDelegates'							=> 'reports.delegates',
				'getPeopleWithBirthdayCurrentMonth' 	=> 'reports.people-with-birthday-current-month',
				'getPeopleWithoutPollingStation' 		=> 'reports.people-without-polling-station',

				'getPeopleOfLocations' 					=> 'reports.people-of-locations',
				'getPeopleOfPollingStations' 			=> 'reports.people-of-polling-stations',
				'getPeopleOfPollingStationsDayD' 		=> 'reports.people-of-polling-stations-day-d',
				'getPeopleOfCommunities' 				=> 'reports.people-of-communities',
				'getPeopleOfOccupations' 				=> 'reports.people-of-occupations',

				'getTeamWithVoters' 					=> 'reports.team-with-voters',
				'getRecursiveTeam' 						=> 'reports.recursive-team',
				'getRecursiveTeamVoters' 				=> 'reports.recursive-team-voters',
				'getRecursiveOrientatorVoters' 			=> 'reports.recursive-orientator-voters',
				'getTeam' 								=> 'reports.team',

				'getTeamOfRoles'						=> 'reports.team-of-roles',

				'getPlans' 								=> 'reports.plans',
				'getPlansTeam' 							=> 'reports.plans-team',
				
				'getBlankTemplate' 						=> 'reports.blank-template',
				
				'getUsers'								=> 'reports.users',
				'getVoterPolls'							=> 'reports.voter-polls',

				'getWhatsapp'							=> 'reports.whatsapp',
				'getSemaphore'							=> 'reports.semaphore',
			]);
		});
			
		Route::group(['prefix' => 'statistics', 'middleware' => 'module:statistics'], function() 
		{
			// /statistics/
			Route::controller('/', 'StatisticsController', [
				'getIndex'							=> 'statistics.index',
				'getBars'							=> 'statistics.bars',
				'getVotersOfPollingStations'		=> 'statistics.voters-of-polling-stations',
				'getVotersOfPollingStationsDayD'	=> 'statistics.voters-of-polling-stations-day-d',
				'getVotersOfLocations' 				=> 'statistics.voters-of-locations'
			]);		
		});
		
		Route::group(['middleware' => 'module:diary'], function() 
		{
			// /diary-json
			Route::get('diary-json', ['as' => 'diary.json', 'uses' => 'DiaryController@json']);
			// /diary-print
			Route::get('diary-print', ['as' => 'diary.print', 'uses' => 'DiaryController@printDiary']);
			// /diary
			// /diary/{id_usuario}/destroy
			Route::delete('diary/{diary}/destroy', [
				'uses' => 'DiaryController@destroy',
				'as'   => 'diary.destroy'
			]);

			Route::resource('diary', 'DiaryController', ['except' => ['destroy']]);


			
			// /diary/{diary}/people
			Route::get('diary/{diary}/people', ['as' => 'diary.people.index', 'uses' => 'DiaryPeopleController@index']);
			// /diary/{diary}/people/add-masive
			Route::post('diary/{diary}/people/add-masive', ['as' => 'diary.people.add-masive', 'uses' => 'DiaryPeopleController@addMasive']);

			// /diary/{diary}/people/{id}/remove
			Route::post('diary/{diary}/people/{id}/remove', ['as' => 'diary.people.remove', 'uses' => 'DiaryPeopleController@remove']);
		});

		// /logistic
		Route::group(['prefix' => 'logistic', 'middleware' => 'module:logistic'], function(){
			// /logistic/
			Route::get('/', ['as' => 'logistic', 'uses' => 'LogisticController@getindex']);
		});

		// /advertising
		Route::group(['prefix' => 'advertising', 'middleware' => 'module:advertising'], function(){
			// advertising/
			Route::get('/', ['as' => 'advertising', 'uses' => 'AdvertisingController@getindex']);
		});
	});
	

	/****** EXTRA ******/
	Route::group(['namespace' => 'Extra'], function() {
		
		/* Envío masivo de Mensajes de texto */
		Route::group(['middleware' => 'module:sms'], function() {
			// /sms
			Route::controller('sms', 'SmsController', [
				'getIndex' 				=> 'sms.index',
				'postSend' 				=> 'sms.send',		
			]);
		});
		/* Fin Módulo Envío masivo de Mensajes de texto */

		Route::group(['middleware' => 'module:polls'], function() {
			// /polls
			Route::resource('polls', 'PollsController');
			// /polls/questions
			Route::resource('polls.questions', 'PollsQuestionsController');	
			// /polls/{polls}/stats
			Route::get('polls/{polls}/stats', ['as' => 'polls.stats', 'uses' => 'PollsController@stats']);
			// /polls/{polls}/stats/json
			Route::get('polls/{polls}/stats/json', ['as' => 'polls.stats.json', 'uses' => 'PollsController@statsJson']);
			// /answers/{id}/delete
			Route::delete('answers/{id}/delete', ['as' => 'answers.destroy', 'uses' => 'PollsQuestionsController@answersDestroy']);

			// /polls/{polls}/voters/options
			Route::get('polls/{polls}/voters/options', ['as' => 'polls.voters.options', 'uses' => 'PollsVotersController@options']);
			// /polls/{polls}/voters/options
			Route::post('polls/{polls}/voters/options', ['as' => 'polls.voters.options.store', 'uses' => 'PollsVotersController@postOptions']);
			// /polls/voters
			Route::resource('polls.voters', 'PollsVotersController');
		});
	});
	
});