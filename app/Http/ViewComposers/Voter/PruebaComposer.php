<?php namespace App\Http\ViewComposers\Voter;

use Illuminate\Contracts\View\View;

use App\Entities\Voter;
use App\Entities\Location;
use App\Entities\Community;
use App\Entities\Occupation;
use App\Entities\Rol;
use App\Entities\PollingStation;

class PruebaComposer {
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $team               = Voter::allTeam();
        $locations          = Location::getAllOrder(1);
        $communities        = Community::allLists();
        $occupations        = Occupation::allLists();
        $roles              = Rol::allLists();
        //$polling_stations   = PollingStation::allLists();
        $polling_stations   = PollingStation::pollingWithMunicipaly();

        $view->with([
            'team'          => $team,
            'locations'     => $locations, 
            'communities'   => $communities, 
            'occupations'   => $occupations,
            'roles'         => $roles,
            'polling_stations'  => $polling_stations
        ]);
    }
}


