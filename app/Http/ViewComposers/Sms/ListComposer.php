<?php namespace App\Http\ViewComposers\Sms;

use Illuminate\Contracts\View\View;
use Auth;

use App\Libraries\Sms\SendSMS;
use App\Entities\Location;
use App\Entities\PollingStation;
use App\Entities\Community;
use App\Entities\Occupation;
use App\Entities\Rol;

class ListComposer {
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $sms = new SendSMS();

        $credits 	= $sms->credits();
		

        return $view->with([
            'credits'           => $credits,
            'locations'         => Location::getAllOrder(),
            'polling_stations'  => PollingStation::allLists(),
            'communities'       => Community::allLists(),
            'roles'             => Rol::allLists(),
            'occupations'       => Occupation::allLists(),
            'sex'               => ['F' => 'Femenino', 'M' => 'Masculino']            
        ]);
    }
}


