<?php namespace App\Http\ViewComposers\Statistics;

use Illuminate\Contracts\View\View;

use App\Entities\Voter;
use App\Libraries\Campaing;
use App\Libraries\Reports\Report;

class ListComposer {
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $statistics 		= Report::getAllGraphicActive();
		
        $goal_percentage 	= Voter::getGoalPercentage();	
		$number_voters 		= Voter::numberVoters();
		$target_number 		= Campaing::getTargetNumber();

        return $view->with([
            'statistics'    	=> $statistics,
            'goal_percentage'   => $goal_percentage,
            'number_voters'   	=> $number_voters,
            'target_number'   	=> $target_number
        ]);
    }
}


