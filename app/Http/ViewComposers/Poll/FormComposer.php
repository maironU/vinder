<?php namespace App\Http\ViewComposers\Voter;

use Illuminate\Contracts\View\View;
use Auth;

use App\Entities\Voter;
use App\Entities\Location;
use App\Entities\Community;
use App\Entities\Occupation;
use App\Entities\Rol;

class FormComposer {
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
		$locations 			= Location::getAllOrder(1);
        $communities        = Community::allLists();

		$view->with([
            'locations'     => $locations
        ]);
    }
}


