<?php namespace App\Http\Requests\Voter;

use App\Http\Requests\Request;
use App\Entities\Voter;
use Illuminate\Routing\Route;
use Auth;

class NewRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		if(Auth::user()->type->id != 6){
			$rules = [
				'colaborator'	=> 'required|exists:voters,id',
				'doc' 			=> 'required|integer'
			];
	
			return $rules;
		}
		return [];
	}
}