<?php

namespace App\Http\Controllers;

use App\Entities\FormE14;
use App\Entities\PollingStation;
use App\Entities\User;
use App\Entities\Zone;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CoordinatorPositionController extends Controller
{
	private $user;  

    public function __construct() 
	{
		$this->beforeFilter('@newUser', ['only' => ['create', 'store']]);
		$this->beforeFilter('@findUser', ['only' => ['show', 'edit', 'update', 'destroy']]);
		$this->middleware('logs', ['only' => ['store', 'update', 'destroy']]);
	}

    public function newUser()
	{
		$this->user = new User;
	}

	public function findUser(Route $route)
	{
	 	$this->user = User::where('id', $route->getParameter('id'))->where('type_id', 8)->first();
	}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $coordinator_zone_id = null)
    {
        $Users = User::where('type_id', '=', 8)
                    ->select('user.*')
                    ->join('polling_stations as ps', 'ps.id', '=', 'polling_station_id')
                    ->orderBy('ps.name', 'ASC');

        $pollingStationsZone = PollingStation::query();
        if(Auth::user()->type->id == 9){
            $pollingStations = Auth::user()->zone->zone->pollingStations->pluck('id') ?? [];
            $Users->whereIn('polling_station_id', $pollingStations);
            $pollingStationsZone = PollingStation::whereIn('id', $pollingStations);
        }

        if($coordinator_zone_id && Auth::user()->type->id == 1){
            $user = User::find($coordinator_zone_id);
            if($user){
                $pollingStations = $user->zone->zone->pollingStations->pluck('id') ?? [];
                $Users->whereIn('polling_station_id', $pollingStations);
                $pollingStationsZone = PollingStation::whereIn('id', $pollingStations);
            }
        }
        if(isset($request->search) && $request->search){
            if(Auth::user()->type->id == 9 || (Auth::user()->type->id == 1 && $coordinator_zone_id)){
                $pollingStationsZone->where(function($q) use($request){
                    $q->where('name', 'LIKE', '%'.$request->search.'%');
                });
            }else{
                $Users->where(function($q) use($request){
                    $q->where('user.username', 'LIKE', '%'.$request->search.'%');
                    $q->orWhere('user.name', 'LIKE', '%'.$request->search.'%');
                    $q->orWhere('user.email', 'LIKE', '%'.$request->search.'%');
                    $q->orWhere('user.phone', 'LIKE', '%'.$request->search.'%');
                });
            }
            
        }

        if(isset($request->polling_station) && $request->polling_station){
            $Users->where(function($q) use($request){
                $q->where('polling_station_id', '=', $request->polling_station);
            });

            if(Auth::user()->type->id == 9){
                $pollingStations = Auth::user()->zone->zone->pollingStations->pluck('id')->toArray() ?? [];
                if(in_array($request->polling_station, $pollingStations)){
                    $pollingStationsZone = PollingStation::whereIn('id', [$request->polling_station]);
                }
            }

            if(Auth::user()->type->id == 1 && $coordinator_zone_id){
                $user = User::find($coordinator_zone_id);
                $pollingStations = [];
                if($user){
                    $pollingStations = $user->zone->zone->pollingStations->pluck('id')->toArray() ?? [];
                }
                if(in_array($request->polling_station, $pollingStations)){
                    $pollingStationsZone = PollingStation::whereIn('id', [$request->polling_station]);
                }
            }
        }

        if(isset($request->zone)){
            $zones = json_decode($request->zone);
            if(count($zones) > 0){
                $zones_db = Zone::whereIn('id', $zones)->get();
                $pollingStations = [];
                foreach($zones_db as $zone){
                    $pollingStations = array_merge($pollingStations, $zone->pollingStations->pluck('id')->toArray() ?? []);
                }
    
                $pollingStationsZone->whereIn('id', $pollingStations);
                $Users->whereIn('polling_station_id', $pollingStations);
            }
        }

        $form = FormE14::first();

        $pollingStationsZone = $pollingStationsZone->get();

//        dd($Users);
        $pollingStations = DB::table('polling_stations')->distinct('location_id')->count();
        //$coordinators = DB::table('user')->where('type_id', 8)->get();

//        $witnesses = User::with('type_id');
//        dd($witnesses);
        return view('dashboard.pages.witnesses.coordinator.index',['users'=>$Users->paginate(5), 'pollingStations'=> $pollingStations, 'form' => $form, 'request' => $request, 'coordinator_zone_id' => $coordinator_zone_id, 'pollingStationsZone' => $pollingStationsZone]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $form_data = ['route' => 'coordinator.store', 'method' => 'POST'];
        $locations = \App\Entities\PollingStation::pollingWithMunicipaly();
        return view('dashboard.pages.witnesses.coordinator.form', compact('form_data', 'locations'))
        ->with('user', $this->user);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!$request->polling_station_id) return redirect()->route('coordinator.create')->with('exist', 'El puesto de votación no puede estar vacío')->withInput($request->input());
        $user = User::where('username', $request->username)->first();
        if($user) return redirect()->route('coordinator.create')->with('exist', 'El nombre de usuario ya existe')->withInput($request->input());
        
        $user = User::where('email', $request->email)->first();
        if($user) return redirect()->route('coordinator.create')->with('exist', 'El correo electrónico ya existe')->withInput($request->input());
        
        $data = $request->all();
        $data['type_id'] = 8;

        $user_exist = User::where('type_id', '=', 8)
                        ->where('polling_station_id', $data['polling_station_id'])
                        ->exists();

        if(!$user_exist){
            $this->user->fill($data);
            $this->user->save();
        }else{
            return redirect()->route('coordinator.create')->with('exist', 'Ya existe un coordinador de puesto con esa ubicación')->withInput($request->input());
        }
        
		return redirect()->route('coordinator.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!$this->user){
            return redirect()->route('coordinator.index');
        }
        $form_data = ['route' => ['coordinator.update',  $this->user->id], 'method' => 'PUT'];
        $locations = \App\Entities\PollingStation::pollingWithMunicipaly();

        return view('dashboard.pages.witnesses.coordinator.form', compact('form_data', 'locations'))
        ->with('user', $this->user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user_coordinator = User::find($id);
        if(!$request->polling_station_id) return redirect()->route('coordinator.edit', $id)->with('exist', 'El puesto de votación no puede estar vacío')->withInput($request->input());
        if($user_coordinator->username != $request->username){
            $user = User::where('username', $request->username)->first();
            if($user) return redirect()->route('coordinator.edit', $id)->with('exist', 'El nombre de usuario ya existe')->withInput($request->input());
        }

        if($user_coordinator->email != $request->email){
            $user = User::where('email', $request->email)->first();
            if($user) return redirect()->route('coordinator.edit', $id)->with('exist', 'El correo electrónico ya existe')->withInput($request->input());
        }
        
        $data = $request->all();

        if($user_coordinator->polling_station_id != $data['polling_station_id']){
            $user_exist = User::where('type_id', '=', 8)
            ->where('polling_station_id', $data['polling_station_id'])
            ->exists();

            if(!$user_exist){
                $user_coordinator->update($data);
            }else{
                return redirect()->route('coordinator.edit', $id)->with('exist', 'Ya existe un coordinador de puesto con esa ubicación')->withInput($request->input());
            }
        }else{
            $user_coordinator->update($data);
        }        
		return redirect()->route('coordinator.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
