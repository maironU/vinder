<?php

namespace App\Http\Controllers;

use App\Entities\FormE14;
use App\Entities\PollingStation;
use Illuminate\Http\Request;
use App\Entities\User;
use App\Entities\UserZone;
use App\Entities\VoterNew;
use App\Entities\Zone;
use App\Helpers\Helper;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\DB;

class CoordinatorZoneController extends Controller
{
    private $user;  

    public function __construct() 
	{
		$this->beforeFilter('@newUser', ['only' => ['create', 'store']]);
		$this->beforeFilter('@findUser', ['only' => ['show', 'edit', 'update', 'destroy']]);
		$this->middleware('logs', ['only' => ['store', 'update', 'destroy']]);
	}

    public function newUser()
	{
		$this->user = new User;
	}

	public function findUser(Route $route)
	{
	 	$this->user = User::findOrFail($route->getParameter('id'));
	}
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $Users = User::where('type_id', '=', 9)
                    ->has('zone')
                    ->orderBy('name','desc');
        if(isset($request->search) && $request->search){
            $Users->where(function($q) use($request){
                $q->where('username', 'LIKE', '%'.$request->search.'%');
                $q->orWhere('name', 'LIKE', '%'.$request->search.'%');
                $q->orWhere('email', 'LIKE', '%'.$request->search.'%');
                $q->orWhere('phone', 'LIKE', '%'.$request->search.'%');
            });
        }

        if(isset($request->zone)){
            $zones = json_decode($request->zone);
            if(count($zones) > 0){
                $Users->whereHas('zone', function($q) use($zones){
                    $q->whereIn('zone_id', $zones);
                });
            }
        }
        
        return view('dashboard.pages.witnesses.coordinator_zone.index',['users'=>$Users->paginate(30), 'request' => $request]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($cedula = null)
    {
        $fromFindAndcreate = false;
        $tables = [];
        if($cedula){
            $user = User::where('username', $cedula)->first();
            if($user) return redirect()->route('coordinator_zone.index')->with('message', 'El usuario ya existe');

            $find_testigo = VoterNew::select('*', DB::raw("CONCAT(first_name, ' ', second_name, ' ', surname, ' ', second_surname) as full_name"))
                ->where('nuip', $cedula)->first();
            if($find_testigo){
                $this->user->username = $find_testigo->nuip;
                $this->user->name = $find_testigo->full_name;
                $this->user->email = Helper::generateEmail($find_testigo->first_name, $find_testigo->surname);
            }else{
                return redirect()->route('coordinator_zone.index')->with('message', 'No existe ningún usuario con esa cédula en la base de datos');
            }
            $fromFindAndcreate = true;
        }

        $form_data = ['route' => 'coordinator_zone.store', 'method' => 'POST'];
        $zones = \App\Entities\Zone::wheredoesnthave('userZone')->get();
        return view('dashboard.pages.witnesses.coordinator_zone.form', compact('form_data', 'zones', 'fromFindAndcreate'))
        ->with('user', $this->user);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!$request->zone_id) return redirect()->route('coordinator_zone.create')->with('exist', 'La zona no puede estar vacía')->withInput($request->input());
        $user = User::where('username', $request->username)->first();
        if($user) return redirect()->route('coordinator_zone.create')->with('exist', 'El nombre de usuario ya existe')->withInput($request->input());
        
        $user = User::where('email', $request->email)->first();
        if($user) return redirect()->route('coordinator_zone.create')->with('exist', 'El correo electrónico ya existe')->withInput($request->input());
        
        $data = $request->all();
        $data['type_id'] = 9;

        $zone_id = $request->zone_id;
        $user_exist = User::where('type_id', '=', 9)
                        ->whereHas('zone', function($q) use($zone_id){
                            $q->where('zone_id', $zone_id);
                        })
                        ->exists();

        if(!$user_exist){
            $this->user->fill($data);
            $this->user->save();
        }else{
            return redirect()->route('coordinator_zone.create')->with('exist', 'Ya existe un coordinador de zona con esa zona')->withInput($request->input());
        }

        if(isset($this->user->id)){
            UserZone::create([
                "user_id" => $this->user->id,
                "zone_id" => $request->zone_id,
            ]);
        }
        
		return redirect()->route('coordinator_zone.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $fromFindAndcreate = false;
        $form_data = ['route' => ['coordinator_zone.update',  $this->user->id], 'method' => 'PUT'];

        $user_id = $id;

        $zones = \App\Entities\Zone::wheredoesnthave('userZone')
        ->orWhereHas('userZone', function($q) use($user_id){
            $q->where('user_id', $user_id);
        })->get();
        return view('dashboard.pages.witnesses.coordinator_zone.form', compact('form_data', 'zones', 'fromFindAndcreate'))
        ->with('user', $this->user);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        if(!$request->zone_id) return redirect()->route('coordinator_zone.edit', $id)->with('exist', 'La zona no puede estar vacía')->withInput($request->input());
        
        $data = $request->all();
        $user->update($data);

        $user_zone = $user->zone;
        $user_zone->zone_id = $request->zone_id;
        $user_zone->save();

		return redirect()->route('coordinator_zone.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }
}
