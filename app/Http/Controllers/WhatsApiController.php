<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Services\WasApi;
use Illuminate\Support\Facades\DB;

class WhatsApiController extends Controller
{
    private $whatsApiService;

    public function __construct()
    {
        $this->whatsApiService = new WasApi;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $templates = $this->whatsApiService->getTemplates();
        $phones = $this->whatsApiService->getPhones();
        
        $config_welcome = DB::table('config_wasapi')->where('title', 'bienvenida')->first();
        $config_incidents = DB::table('config_wasapi')->where('title', 'incidencias')->first();

        return view('dashboard.pages.whatsapi.index', compact('templates', 'phones', 'config_welcome', 'config_incidents'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $config = DB::table('config_wasapi')->where('title', $request->title)->first();
        if($config){
            DB::table('config_wasapi')->where('id', $config->id)->update($request->except('_token'));
        }else{
            DB::table('config_wasapi')->insert($request->except('_token'));
        }

        return redirect()->route('whatsapi.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
