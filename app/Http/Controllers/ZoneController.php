<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Entities\User;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\DB;
use App\Entities\FormE14;
use App\Entities\Zone;

class ZoneController extends Controller
{
    private $zone;

    public function __construct() 
	{
		$this->beforeFilter('@newZone', ['only' => ['create', 'store']]);
		$this->beforeFilter('@findZone', ['only' => ['show', 'edit', 'editLeaders', 'update', 'destroy']]);
		$this->middleware('logs', ['only' => ['store', 'update', 'destroy']]);
	}

    public function newZone()
	{
		$this->zone = new Zone;
	}

	public function findZone(Route $route)
	{
	 	$this->zone = Zone::findOrFail($route->getParameter('id'));
	}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $zones = Zone::orderBy('name','desc');

        if(isset($request->search) && $request->search){
            $zones->where(function($q) use($request){
                $q->orWhere('name', 'LIKE', '%'.$request->search.'%');
            });
        }

        if(isset($request->polling_station) && $request->polling_station){
            $polling_id = $request->polling_station;
            $zones->whereHas('pollingStations', function($q) use($polling_id){
                $q->where('zone_polling_stations.polling_station_id', '=', $polling_id);
            });
        }
        
        $pollingStations = DB::table('polling_stations')->distinct('location_id')->count();
        $form = FormE14::first();
        return view('dashboard.pages.witnesses.zone.index',['zones'=>$zones->get(), 'pollingStations'=> $pollingStations, 'form' => $form, 'request' => $request]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $form_data = ['route' => 'zone.store', 'method' => 'POST'];
        $locations = \App\Entities\PollingStation::pollingWithMunicipalyNotUsedZone();
        return view('dashboard.pages.witnesses.zone.form', compact('form_data', 'locations'))
        ->with('zone', $this->zone);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!$request->name) return redirect()->route('zone.create')->with('exist', 'Ingrese el nombre de la zona')->withInput($request->input());
        if(!$request->polling_stations) return redirect()->route('zone.create')->with('exist', 'Seleccione por lo menos un pueto de votación')->withInput($request->input());
        
        $this->zone->name = $request->name;
        $this->zone->save();

        if(isset($this->zone->id)){
            $this->zone->pollingStations()->sync($request->polling_stations);
        }
        
		return redirect()->route('zone.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $form_data = ['route' => ['zone.update',  $this->zone->id], 'method' => 'PUT'];
        $locations = \App\Entities\PollingStation::pollingWithMunicipalyNotUsedZone($id);
        return view('dashboard.pages.witnesses.zone.form', compact('form_data', 'locations'))
        ->with('zone', $this->zone);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(!$request->name) return redirect()->route('zone.edit', $id)->with('exist', 'Ingrese el nombre de la zona')->withInput($request->input());
        if(!$request->polling_stations) return redirect()->route('zone.edit', $id)->with('exist', 'Seleccione por lo menos un pueto de votación')->withInput($request->input());
        
        $zone = Zone::find($id);

        $zone->name = $request->name;
        $zone->save();

        $zone->pollingStations()->sync($request->polling_stations);
        
		return redirect()->route('zone.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
