<?php namespace App\Http\Controllers\Secondary;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Libraries\Reports\ReportsUtilities;
use App\Libraries\Reports\Report;

use Excel;

class ReportsController extends Controller {

	function __construct() {
		$this->middleware('logs');
	}
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		$reports = Report::getAllPlainActive();
		return view('dashboard.pages.reports.lists', compact('reports'));
	}

	public function getPeople(Request $request)
	{
		$xls = ($request->get('xls') === "true");
		$sex = $request->get('select');
		$pdf = ReportsUtilities::getPeople($sex, $xls);
		$pdf->Output();

		return exit;
	}

	public function getUsers(Request $request)
	{
		$xls = ($request->get('xls') === "true");
		$pdf = ReportsUtilities::getUsers($xls);
		$pdf->Output();

		return exit;
	}

	public function getVoterPolls(Request $request, $poll_id = null)
	{
		$pdf = ReportsUtilities::getVoterPolls($poll_id);
		$pdf->Output();

		return exit;
	}

	public function getDelegates(Request $request)
	{
		$xls = ($request->get('xls') === "true");
		$pdf = ReportsUtilities::getDelegates($xls);
		$pdf->Output();

		return exit;
	}

	public function getPeopleWithBirthdayCurrentMonth(Request $request)
	{
		$xls = ($request->get('xls') === "true");
		$pdf = ReportsUtilities::getPeopleWithBirthdayCurrentMonth($xls);
		$pdf->Output();

		return exit;
	}

	public function getPeopleWithoutPollingStation(Request $request)
	{
		$xls = ($request->get('xls') === "true");
		$pdf = ReportsUtilities::getPeopleWithoutPollingStation($xls);
		$pdf->Output();

		return exit;
	}

	public function getPeopleOfLocations(Request $request)
	{
		$xls = ($request->get('xls') === "true");
		$location_ids = $request->get('select');
		$pdf = ReportsUtilities::getPeopleOfLocations($location_ids, $xls);
		$pdf->Output();

		return exit;
	}

	public function getPeopleOfPollingStations(Request $request, $colaborator = null)
	{
		set_time_limit(300);
		$xls = ($request->get('xls') === "true");
		$polling_station_ids = $request->get('select');
		$rol_ids = $request->get('roles');
		$pdf = ReportsUtilities::getPeopleOfPollingStations($polling_station_ids, $rol_ids, $colaborator, $xls);
		$pdf->Output();

		return exit;
	}	

	public function getPeopleOfPollingStationsDayD(Request $request, $colaborator = null)
	{
		$polling_station_ids = $request->get('select');
		$rol_ids = $request->get('roles');

		$pdf = ReportsUtilities::getPeopleOfPollingStationsDayD($polling_station_ids, $rol_ids, $colaborator);
		$pdf->Output();

		return exit;
	}

	public function getPeopleOfCommunities(Request $request)
	{
		$xls = ($request->get('xls') === "true");
		$community_ids = $request->get('select');
		$pdf = ReportsUtilities::getPeopleOfCommunities($community_ids, $xls);
		$pdf->Output();

		return exit;
	}

	public function getPeopleOfOccupations(Request $request)
	{
		$xls = ($request->get('xls') === "true");
		$occupation_ids = $request->get('select');
		$pdf = ReportsUtilities::getPeopleOfOccupations($occupation_ids, $xls);
		$pdf->Output();

		return exit;
	}

	public function getTeamWithVoters(Request $request)
	{
		$xls = ($request->get('xls') === "true");
		$team_ids = $request->get('select');
		$pdf = ReportsUtilities::getTeamWithVoters($team_ids, $xls);
		$pdf->Output();

		return exit;
	}

	public function getRecursiveTeam(Request $request)
	{
		$xls = ($request->get('xls') === "true");
		$team_ids = $request->get('select');
		$pdf = ReportsUtilities::getRecursiveTeam($team_ids, $xls);
		$pdf->Output();

		return exit;
	}

	public function getRecursiveTeamVoters(Request $request)
	{
		$xls = ($request->get('xls') === "true");
		$team_ids = $request->get('select');
		$pdf = ReportsUtilities::getRecursiveTeamVoters($team_ids, $xls); 
		$pdf->Output();

		return exit;
	}

	public function getRecursiveOrientatorVoters(Request $request)
	{
		$xls = ($request->get('xls') === "true");
		$team_ids = $request->get('select');
		$pdf = ReportsUtilities::getRecursiveOrientatorVoters($team_ids, $xls); 
		$pdf->Output();

		return exit;
	}

	public function getTeam(Request $request)
	{
		$xls = ($request->get('xls') === "true");
		$pdf = ReportsUtilities::getTeam($xls);
		$pdf->Output();

		return exit;
	}

	public function getTeamOfRoles(Request $request)
	{
		$xls = ($request->get('xls') === "true");
		$rol_ids = $request->get('select');
		$pdf = ReportsUtilities::getTeamOfRoles($rol_ids, $xls);
		$pdf->Output();

		return exit;
	}

	public function getBlankTemplate(Request $request)
	{
		$team_ids = $request->get('select');
		$pdf = ReportsUtilities::reportStandard($team_ids);
		
		return exit;
	} 

	// Plan de Campaña
	public function getPlans(Request $request, bool $excel = false)
	{
		$plan_ids = $request->get('select');
		if (!$excel) # PDF
		{
			$pdf = ReportsUtilities::getPlans($plan_ids);
			$pdf->Output();
		}
		else # EXCEL
		{
			dd($plan_ids);
		}
	
		return exit;
	}

	// Plan de Campaña de Equipo
	public function getPlansTeam(Request $request)
	{
		$xls = ($request->get('xls') === "true");
		$team_ids = $request->get('select');

		ReportsUtilities::getPlansTeam($team_ids, $xls);
		return exit;
	}

	public function getWhatsapp(Request $request)
	{
		$fileName = "Listado de Votantes con Whatsapp - Vinder";
		$votersWhatsapp = ReportsUtilities::getWhatsapp($request->get('select'));

		$headers = [
			'MIME-Version' => '1.0',
			'Content-type' => 'text/csv',
			'Content-Disposition' => 'attachment; filename="'. $fileName . '.csv"',
			'charset'=>'UTF-8',
		];

		return \Response::make($votersWhatsapp, 200, $headers);
	}

	public function getSemaphore(Request $request)
	{
		$semaphore = $request->get('select');
		$pdf = ReportsUtilities::getSemaphore($semaphore);

		$pdf->Output();
	}
}