<?php namespace App\Http\Controllers\Secondary;

use App\Entities\City;
use Illuminate\Routing\Route;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Libraries\Reports\Report;
use App\Libraries\Reports\Statitics;
use App\Libraries\Campaing;

use App\Entities\PollingStation;
use App\Entities\Location;
use App\Entities\Voter;
use App\Entities\Module;
use App\Entities\User;

use Illuminate\Database\QueryException;
use Response;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use Session;
use Event;

class MapsController extends Controller {
	private $campaing;
	private $map;
	private $polling_stations;
	private $points;
	private $mainModules = null;
	private $extraModules;

	public function __construct()
	{
		$this->middleware('logs', ['only' => ['store', 'update', 'destroy']]);
		$userId = Auth::id();

//		$this->$mainModules[] = [];


	}
	
	public function getHeatMapPollingStations()
	{
		getMapPollingStations($color);
	}

    public function getDataForPollingStationsMarkers()
    {
        $polling_stations = DB::table('polling_stations')->get();

        $puntos=[];
        foreach($polling_stations as $polling_station)
        {
            $punto = [
                'total' => $polling_station->{'Total'},
                'cero' => $polling_station->{'cero'},
                'uno' => $polling_station->{'uno'},
                'dos' => $polling_station->{'dos'},
                'tres' => $polling_station->{'tres'},
                'puesto' => $polling_station->{'Puesto de votación'}, 
                'ciudad' => $polling_station->{'Ciudad'},
                'latitud' => $polling_station->{'Latitud'},
                'longitud' => $polling_station->{'Longitud'}
            ];
            array_push ($puntos, $punto);
        }
        return($puntos);
    }

	public function getMapPollingStations($color = null)
	{
		$_color = .3512;


		$userId = Auth::id();
//		$this->ObtainModulesFromUser(Auth::id());

		$mainModules = DB::table('modules')
			->join('user_types_has_modules','modules.id','=','user_types_has_modules.module_id')
			->join('user','user_types_has_modules.user_type_id','=','user.type_id')
			->where('user_types_has_modules.user_type_id','=',$userId)
			->where('type','=','main')
			->get();

		$extraModules = DB::table('modules')
			->join('user_types_has_modules','modules.id','=','user_types_has_modules.module_id')
			->join('user','user_types_has_modules.user_type_id','=','user.type_id')
			->where('user_types_has_modules.user_type_id','=',$userId)
			->where('type','=','extra')
			->get();


		$polling_stations = PollingStation::all();
		$points[] = [];
		foreach($polling_stations as $polling_station)
		{
			// Nombre, Latitud, Longitud, Zoom, Título, Descripción
			array_push($points, [
				$polling_station->name,
				$polling_station->latitude,
				$polling_station->longitude,
				$polling_station->name,
				$polling_station->description,
				$polling_station->address,
				$polling_station->men,
				$polling_station->women,
				$polling_station->electoral_potential,
				$polling_station->tables,
				'COLOR:'.$_color,
			]);
		}

		$centerIn = $this->getInitialCoordinates();

		$campaing = array(
			"template" => Campaing::gettemplateCss(),
			"title" => "sdasdsadsa",
			"mainModules" => $mainModules,
			"extraModules" => $extraModules,
			"latitude" => $centerIn[0],
			"longitude" => $centerIn[1],
			"zoom" => 15,
			"points" => $points,
		);

		return view('dashboard.pages.maps.polling-stations', $campaing);
	}

/*
	public function getMapVoters()
	{
		$userId = Auth::id();
//		$this->ObtainModulesFromUser(Auth::id());

		$mainModules = DB::table('modules')
			->join('user_types_has_modules','modules.id','=','user_types_has_modules.module_id')
			->join('user','user_types_has_modules.user_type_id','=','user.type_id')
			->where('user_types_has_modules.user_type_id','=',$userId)
			->where('type','=','main')
			->get();

		$extraModules = DB::table('modules')
			->join('user_types_has_modules','modules.id','=','user_types_has_modules.module_id')
			->join('user','user_types_has_modules.user_type_id','=','user.type_id')
			->where('user_types_has_modules.user_type_id','=',$userId)
			->where('type','=','extra')
			->get();

		$voters = Voter::all();

		$polling_stations = PollingStation::all();
		$points[] = [];
		foreach($polling_stations as $polling_station)
		{
			// Nombre, Latitud, Longitud, Zoom, Título, Descripción
			array_push($points,
				array(	$polling_station->name,
						$polling_station->latitude,
						$polling_station->longitude,
						$polling_station->name,
						$polling_station->description,
						$polling_station->address,
						$polling_station->men,
						$polling_station->women,
						$polling_station->electoral_potential,
						$polling_station->tables,
					)
				);
		}

		$campaing = array(
			"template" => Campaing::gettemplateCss(),
			"title" => "sdasdsadsa",
			"mainModules" => $mainModules,
			"extraModules" => $extraModules,
			"latitude" => 4.1445678,
			"longitude" => -73.6376905,
			"zoom" => 15,
			"points" => json_encode($points),
		);

		return view('dashboard.pages.maps.polling-stations', $campaing);
	}
*/

	/* +-------------------------------------------------------+
	*  |
	*  |
	*  |
	*  +-------------------------------------------------------+
	*/
	public function Index()
	{
		$polling_stations = PollingStation::all();
		$points[] = [];
		
//		dd($polling_stations);
		
		foreach($polling_stations as $polling_station)
		{
			$color = 1;
			// Nombre, Latitud, Longitud, Zoom, Título, Descripción, Dirección, Hombres, Mujeres, Potencial electoral, Mesas
			array_push($points,
				array(	$polling_station->name,
						$polling_station->latitude,
						$polling_station->longitude,
						$polling_station->name,
						$polling_station->description,
						$polling_station->address,
						$polling_station->men,
						$polling_station->women,
						$polling_station->electoral_potential,
						$polling_station->tables,	
						$color,				
					)
				);
		}

//		dd($points);

		$centerIn = $this->getInitialCoordinates();

		$campaing = array(
			"template" => Campaing::gettemplateCss(),
			"mainModules" => [ new Module],
			"extraModules" => [ new Module],
			"latitude" => $centerIn[0],
			"longitude" => $centerIn[1],
			"zoom" => 15,
			"points" => json_encode($points),
			'polling_stations' => $polling_stations
		);
		return view('dashboard.pages.maps.polling-stations', $campaing);
	}

	public function filter(Request $request){
		$usersMarkers = [];
		$votersMarkers = [];

		if($request->coordinador){
			$usersMarkers = DB::table('user')
			->where('house_check', 1)
			->where('coordinator_id', $request->coordinador)
			->select('name', 'address', 'latitude', 'longitude', 'phone', 'polling_station_id', 'num_table as mesa')
			->get();

			foreach($usersMarkers as $orientator){
				$voters = DB::table('voters')
				->where('house_check', 1)
				->where('is_lider', 1)
				->where('leader_id', $orientator->id)
                ->select('name', 'address', 'latitude', 'longitude', 'telephone as phone', 'polling_station_id', 'table_number as mesa')
				->get();

				foreach($voters as $voter){
					array_push($votersMarkers, $voter);
				}
			}
		}else if($request->orientador){
			$votersMarkers = DB::table('voters')
			->where('house_check', 1)
			->where('is_lider', 1)
			->where('leader_id', $request->orientador)
			->select('name', 'address', 'latitude', 'longitude', 'telephone as phone', 'polling_station_id', 'table_number as mesa')
			->get();
		}else{
			$usersMarkers = DB::table('user')
			->where('house_check', 1)
			->select('name', 'address', 'latitude', 'longitude', 'phone', 'polling_station_id', 'num_table as mesa')
			->get();

			$votersMarkers = DB::table('voters')
			->where('house_check', 1)
			->select('name', 'address', 'latitude', 'longitude', 'telephone as phone', 'polling_station_id', 'table_number as mesa')
			->get();


			$users = array_merge($usersMarkers, $votersMarkers);
			return $users;
		}
	
		$users = array_merge($usersMarkers, $votersMarkers);
		return $users;
	}

	public function getInitialCoordinates()
	{
		$centerIn = [4.1445678, -73.6376905];
		$locationSeeder = ucfirst(mb_strtolower(campaing::getLocationsSeeder()));

		$city = City::where('name', $locationSeeder)->first();
		if($city){
			$centerIn = [$city->latitude, $city->longitude];
		}

		/*switch(mb_strtolower(campaing::getLocationsSeeder()))
		{
			case mb_strtolower('LocationsAcaciasTableSeeder'): $centerIn = [3.9888888888889, -73.764722222222]; break;
			case mb_strtolower('LocationsVillavicencioTableSeeder'): $centerIn = [4.1445678, -73.6376905]; break;
			case mb_strtolower('LocationsBarrancaDeUpiaTableSeeder'): $centerIn = [4.5647222222222, -72.9625]; break;
			case mb_strtolower('LocationsCabuyaroTableSeeder'): $centerIn = [4.2858333333333, -72.791388888889]; break;
			case mb_strtolower('LocationsCastillaLaNuevaTableSeeder'): $centerIn = [3.8258333333333, -73.687222222222]; break;
			case mb_strtolower('LocationsSanLuisDeCubarralTableSeeder'): $centerIn = [3.7927777777778, -73.8375]; break;
			case mb_strtolower('LocationsCumaralTableSeeder'): $centerIn = [4.2694444444444, -73.486388888889]; break;
			case mb_strtolower('LocationsElCalvarioTableSeeder'): $centerIn = [4.3516666666667, -73.713333333333]; break;
			case mb_strtolower('LocationsElCastilloTableSeeder'): $centerIn = [3.5647222222222, -73.794444444444]; break;
			case mb_strtolower('LocationsElDoradoTableSeeder'): $centerIn = [3.7391666666667, -73.835277777778]; break;
			case mb_strtolower('LocationsFuenteDeOroTableSeeder'): $centerIn = [3.4625, -73.619166666667]; break;
			case mb_strtolower('LocationsGranadaTableSeeder'): $centerIn = [3.5472222222222, -73.708611111111]; break;
			case mb_strtolower('LocationsGuamalTableSeeder'): $centerIn = [3.8777777777778, -73.768611111111]; break;
			case mb_strtolower('LocationsLaMacarenaTableSeeder'): $centerIn = [2.1827777777778, -73.784722222222]; break;
			case mb_strtolower('LocationsUribeTableSeeder'): $centerIn = [3.2408333333333, -74.353611111111]; break;
			case mb_strtolower('LocationsLejaniasTableSeeder'): $centerIn = [3.526806, -74.023222]; break;
			case mb_strtolower('LocationsMapiripanTableSeeder'): $centerIn = [2.8919444444444, -72.133611111111]; break;
			case mb_strtolower('LocationsMesetasTableSeeder'): $centerIn = [3.3841666666667, -74.045277777778]; break;
			case mb_strtolower('LocationsPuertoConcordiaTableSeeder'): $centerIn = [2.6233333333333, -72.759166666667]; break;
			case mb_strtolower('LocationsPuertoGaitanTableSeeder'): $centerIn = [4.3141694444444, -72.0825]; break;
			case mb_strtolower('LocationsPuertoLlerasTableSeeder'): $centerIn = [3.2691666666667, -73.376111111111]; break;
			case mb_strtolower('LocationsPuertoLopezTableSeeder'): $centerIn = [4.0897222222222, -72.961944444444]; break;
			case mb_strtolower('LocationsPuertoRicoTableSeeder'): $centerIn = [2.9383333333333, -73.208333333333]; break;
			case mb_strtolower('LocationsRestrepoTableSeeder'): $centerIn = [4.2616666666667, -73.564166666667]; break;
			case mb_strtolower('LocationsSanCarlosDeGuaroaTableSeeder'): $centerIn = [3.7111111111111, -73.2425]; break;
			case mb_strtolower('LocationsSanJuanDeAramaTableSeeder'): $centerIn = [3.3736111111111, -73.876666666667]; break;
			case mb_strtolower('LocationsSanJuanitoTableSeeder'): $centerIn = [4.4586111111111, -73.675555555556]; break;
			case mb_strtolower('LocationsSanMartinTableSeeder'): $centerIn = [3.6969444444444, -73.698611111111]; break;
			case mb_strtolower('LocationsVistaHermosaTableSeeder'): $centerIn = [3.1238888888889, -73.751388888889]; break;
			case mb_strtolower('LocationsMetaTableSeeder'): $centerIn = [4.15, -73.633333333333]; break;
			default: break;
		};*/
		return $centerIn;
	}


}