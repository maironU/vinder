<?php

namespace App\Http\Controllers\System;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Entities\VoterNew;

class VoterPollingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index(Request $request)
    {
        $voters = [];
        if(isset($request->search) && $request->search){
            $voters = VoterNew::where('nuip', 'LIKE', '%'.$request->search.'%')
            ->orWhere('first_name', 'LIKE', '%'.$request->search.'%')
            ->orWhere('second_name', 'LIKE', '%'.$request->search.'%')
            ->orWhere('surname', 'LIKE', '%'.$request->search.'%')
            ->orWhere('second_surname', 'LIKE', '%'.$request->search.'%')
            ->orWhere('department', 'LIKE', '%'.$request->search.'%')
            ->paginate(30);
        }
        return view('dashboard.pages.system.voters_polling.index', compact('polling_stations'), compact('request', 'voters'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
