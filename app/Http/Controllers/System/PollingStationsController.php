<?php namespace App\Http\Controllers\System;

use Illuminate\Routing\Route;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Entities\PollingStation;
use App\Helpers\Helper;
use App\Libraries\ReportsUtilities;

use App\Http\Requests\PollingStation\CreateRequest;
use App\Http\Requests\PollingStation\EditRequest;

use Laracasts\Flash\Flash;
use Illuminate\Support\Facades\DB;
use App\Libraries\Campaing;

class PollingStationsController extends Controller {

	private $polling_station;

	public function __construct() 
	{
		$this->beforeFilter('@newPollingStation', ['only' => ['create', 'store']]);
		$this->beforeFilter('@findPollingStation', ['only' => ['edit', 'update']]);
	}

	/**
	 * A new instance of polling_station
	 *
	 */
	public function newPollingStation()
	{
		$this->polling_station = new PollingStation;
	}

	/**
	 * Find a specific polling_station
	 *
	 */
	public function findPollingStation(Route $route)
	{
		$this->polling_station = PollingStation::findOrFail($route->getParameter('polling_stations'));
	}

	/**
	 * Destroy the the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$form_data = ['route' => ['system.polling-stations.destroy', $id], 'method' => 'DELETE'];
		$polling_station = PollingStation::find($id);
		try {
			$polling_station->delete();
		}
		catch(\Exception $e)
		{
//			Flash::error($e->getMessage());
			Flash::error("No se ha podido eliminar el puesto de votación, por favor asegúrese de que no está en uso.");
		}
		
		return redirect()->route('system.polling-stations.index'); 
	}



	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$polling_stations = PollingStation::with('location')->paginate(Campaing::getResultsPerPage());
		return view('dashboard.pages.system.polling_stations.lists', compact('polling_stations'));
	}

	public function show($id){
		try{
			$location = PollingStation::where('id', $id)->first();
			$num_tables = $location->tables;
			$tables = [];
			
			for($i = 1; $i <= $num_tables; $i++){
				if(Helper::isAvailableTable($location->id, $i)){
					array_push($tables, "Mesa ".$i);
				}
			}

			return response()->json(compact('location', 'tables'));
		}catch(\Exception $e){
			return $e->getMessage();
		}

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$locations_ = '';
		$registraduria_ = '';
		$form_data = ['route' => 'system.polling-stations.store', 'method' => 'POST'];
		
		return view('dashboard.pages.system.polling_stations.form', compact('form_data'))
			->with('polling_station', $this->polling_station)
			->with('locationId', $locations_ )
			->with('registraduriaId', $registraduria_);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(CreateRequest $request)
	{
        $this->polling_station->fill($request->all());
        $this->polling_station->save();

		return redirect()->route('system.polling-stations.index');
	}

	public static function getLocation($id)
	{
		$locationName = DB::table('locations')->select('name')
		->where('id','=',$id)
		->get();

		$retornar = ucwords(mb_strtolower($locationName[0]->name));
		return $retornar;
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$locations_ = $this->getLocation($this->polling_station->location_id);
		$registraduria_ = $this->getLocation($this->polling_station->registraduria_location_id);
		$form_data = ['route' => ['system.polling-stations.update', $this->polling_station->id], 'method' => 'PUT'];
		return view('dashboard.pages.system.polling_stations.form', compact('form_data') )
			->with('polling_station', $this->polling_station)
			->with('locationId', $locations_ )
			->with('registraduriaId', $registraduria_);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, EditRequest $request)
	{
		$this->polling_station->fill($request->all());
		$this->polling_station->save();

		return redirect()->route('system.polling-stations.index');
	}
}
