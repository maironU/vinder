<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Entities\User;
use App\Entities\PollingStation;
use App\Entities\FormE14TotalVote;
use App\Entities\FormE14Vote;
use App\Entities\FormE14;
use App\Entities\FormE14Candidate;
use Illuminate\Support\Facades\Log;

class ResultController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = $this->getResultPollingAndGeneral($request);
        $formse14 = FormE14::active()->get();
        return view('dashboard.pages.witnesses.results', compact('formse14', 'data', 'request'));
    }

    public function getResultPollingAndGeneral($request){
        $data_total = [];
        $data_general = [];
        $percent_general = 0;
        if($request->forme14_id){
                $data_general = $this->resultPollingStationGeneral($request->forme14_id);
                if($request->candidate_id){
                    $data_total = $this->resultPollingStation($request->forme14_id, $request->candidate_id);

                }
        }
        $data = count($data_total) > 0 ? $data_total["data"] : [];
        $total_votos = count($data_total) > 0 ? $data_total["total_votos"] : 0;
        //general
        $data_general_table =  count($data_general) > 0 ? $data_general["data"] : [];
        $data_series_table =  count($data_general) > 0 ? $data_general["data_series"] : [];
        $percent_general = count($data_general) > 0 ? $data_general["percent_general"] : 0;

        $data_send = array(
            "data" => $data,
            "total_votos" => $total_votos,
            "data_general_table" => $data_general_table,
            "data_series_table" => $data_series_table,
            "percent_general" => $percent_general,
        );

        return $data_send;
    }

    public function getStatisticUpdate(Request $request){
        $data = $this->getResultPollingAndGeneral($request);
        return response()->json(["success" => true, "data" => $data]);  
    }

    public function getStatisticTableUpdate(Request $request){
        $data = $this->getResultPollingTable($request);

        return response()->json(["success" => true, "data" => $data]);  
    }

    public function indexTable(Request $request)
    {
        $data = $this->getResultPollingTable($request);
        $polling_stations = PollingStation::all();
        $formse14 = FormE14::active()->get();

        return view('dashboard.pages.witnesses.resultstable', compact('polling_stations', 'formse14', 'data', 'request'));
    }

    
    public function getResultPollingTable($request){
        $data_total = [];
        if($request->polling_station_id && $request->candidate_id && $request->forme14_id){
            $data_total = $this->resultPollingStationTable($request->polling_station_id, $request->candidate_id, $request->forme14_id);
        }
        $data = count($data_total) > 0 ? $data_total["data"] : [];
        $total_votos = count($data_total) > 0 ? $data_total["total_votos"] : 0;

        $data_send = array(
            "data" => $data,
            "total_votos" => $total_votos,
        );
        return $data_send;
    }

    public function getTotalGeneralVote($forme14_id, $candidate_id){
        $sum = 0;
        $polling_stations = PollingStation::all();
        foreach ($polling_stations as $polling_station){
            for($i = 1; $i <= $polling_station->tables; $i++){
                $users = User::where('polling_station_id', $polling_station->id)->get();
                foreach($users as $user){
                    $user_table = User::where('polling_station_id', $polling_station->id)
                    ->whereHas('tables_numbers', function($subquery) use($i){
                        $subquery->where('table_number', $i)
                        ->orWhere('table_number', 'Mesa '.$i);
                    })
                    ->first();

                    if($user_table){
                        $total = FormE14Vote::where('user_id', $user->id)
                        ->where('candidate_id', $candidate_id)
                        ->where('table_number', $i)
                        ->first();
                        $sum += $total ? $total->votes_number : 0;
                    }
                }
            }
        }

        return $sum;
    }

    public function resultPollingStation($forme14_id, $candidate_id){
        $polling_stations = PollingStation::all();
        $data = [];
        /*$names = [];
        $barColor = [];*/
        $total_general = $this->getTotalGeneralVote($forme14_id, $candidate_id);
        foreach ($polling_stations as $polling_station){
            $sum = 0;
            $total = null;
            for($i = 1; $i <= $polling_station->tables; $i++){
                $users = User::where('polling_station_id', $polling_station->id)->get();
                foreach($users as $user){
                    $user_table = User::where('polling_station_id', $polling_station->id)
                    ->whereHas('tables_numbers', function($subquery) use($i){
                        $subquery->where('table_number', $i)
                        ->orWhere('table_number', 'Mesa '.$i);
                    })
                    ->first();

                    if($user_table){
                        $total = FormE14Vote::where('user_id', $user->id)
                        ->where('candidate_id', $candidate_id)
                        ->where('table_number', $i)
                        ->first();
                        $sum += $total ? $total->votes_number : 0;
                    }
                    
                }
            }

            $objData = array(
                "name" => $polling_station->name,
                "y" =>  $sum > 0 ? ($sum/$total_general)*100 : 0,
                "total_votes" => $sum
            );

            array_push($data, $objData);   
        }

        return ["data" => $data, "total_votos" => $total_general];
    }

    public function resultPollingStationGeneral($forme14_id){
        $polling_stations = PollingStation::all();
        $forme14 = FormE14::where('id', $forme14_id)->first();
        $data = [];
        $data_series = [];

        $percent_general = $this->resultPollingStationGeneralVote($forme14_id);
        /*$names = [];
        $barColor = [];*/
        //$total_general = $this->getTotalGeneralVote($forme14_id, $candidate_id);

        foreach ($polling_stations as $polling){
            $num_tables = $polling->tables;
            $users_voters = 0;
            $sw = true;
            $dataTables = [];

            for($i = 1; $i <= $num_tables; $i++){
                $user = User::where('type_id', '=', 5)->where('polling_station_id', $polling->id)
                ->whereHas('tables_numbers', function($subquery) use($i){
                    $subquery->where('table_number', $i)
                    ->orWhere('table_number', 'Mesa '.$i);
                })
                ->first();

                if($user){
                    $votes = $user->votes()->where('table_number', $i)->get();
                    if(count($votes) > 0){
                        $candidates = $forme14->candidates;
                        foreach($votes as $vote){
                            if(!$candidates->where('id', $vote->candidate_id)->first() && $vote->candidate->forme14_id == $forme14_id) {
                                $sw = false;
                            }
                        }
                        if($sw){
                            $users_voters += 1;
                            $objDataTable = ["Mesa {$i}", 100];
                            array_push($dataTables, $objDataTable);
                        }else{
                            $objDataTable = ["Mesa {$i}", 0];
                            array_push($dataTables, $objDataTable);
                        }
                    }else{
                        $objDataTable = ["Mesa {$i}", 0];
                        array_push($dataTables, $objDataTable);
                    }

                }else{
                    $objDataTable = ["Mesa {$i}", 0];
                    array_push($dataTables, $objDataTable);
                }
            }
            $objData = array(
                "name" => $polling->name,
                "y" =>  $users_voters > 0 ? ($users_voters/$num_tables)*100 : 0,
                "drilldown" => "relation".$polling->id,
            );

            $objDataSerie = array(
                "name" => $polling->name,
                "id" => "relation".$polling->id,
                "data" => $dataTables
            );

            array_push($data, $objData);
            array_push($data_series, $objDataSerie);
        }

        return ["data" => $data, "percent_general" => $percent_general, "data_series" => $data_series];
    }

    public function resultPollingStationGeneralVote($forme14_id){
        $polling_stations = PollingStation::all();
        $forme14 = FormE14::where('id', $forme14_id)->first();
        $data = [];
        $total_tables = 0;
        $users_voters = 0;
        $percent = 0;
        /*$names = [];
        $barColor = [];*/
        //$total_general = $this->getTotalGeneralVote($forme14_id, $candidate_id);

        foreach ($polling_stations as $polling){
            $num_tables = $polling->tables;
            $sw = true;
            $total_tables += $polling->tables;

            for($i = 1; $i <= $num_tables; $i++){
                $user = User::where('type_id', '=', 5)->where('polling_station_id', $polling->id)
                ->whereHas('tables_numbers', function($subquery) use($i){
                    $subquery->where('table_number', $i)
                    ->orWhere('table_number', 'Mesa '.$i);
                })
                ->first();

                if($user){
                    $votes = $user->votes()->where('table_number', $i)->get();
                    if(count($votes) > 0){
                        $candidates = $forme14->candidates;
                        foreach($votes as $vote){
                            if(!$candidates->where('id', $vote->candidate_id)->first() && $vote->candidate->forme14_id == $forme14_id) {
                                $sw = false;
                            }
                        }
                        if($sw){
                            $users_voters += 1;
                        }
                    }

                }
            }
        }
        $percent = $total_tables > 0 ? ($users_voters/$total_tables)*100 : 0;
        return $percent;
    }

    public function getTotalTableVote($polling_station_id, $candidate_id, $forme14_id){
        $sum = 0;
        $polling_station = PollingStation::where('id', $polling_station_id)->first();

        for($i = 1; $i <= $polling_station->tables; $i++){
            $total = null;
            $users = User::where('polling_station_id', $polling_station->id)->get();
            foreach($users as $user){
                $user_table = User::where('polling_station_id', $polling_station_id)
                ->whereHas('tables_numbers', function($subquery) use($i){
                    $subquery->where('table_number', $i)
                    ->orWhere('table_number', 'Mesa '.$i);
                })
                ->first();
                if($user_table){
                    $total = FormE14Vote::where('user_id', $user->id)
                    ->where('candidate_id', $candidate_id)
                    ->where('table_number', $i)
                    ->first();
                    $sum += $total ? $total->votes_number : 0;
                }
                
            }
        }
        return $sum;
    }

    public function resultPollingStationTable($polling_station_id, $candidate_id, $forme14_id){
        $polling_station = PollingStation::where('id', $polling_station_id)->first();
        $data = [];
        $total_general = $this->getTotalTableVote($polling_station_id, $candidate_id, $forme14_id);

        for($i = 1; $i <= $polling_station->tables; $i++){
            $sum = 0;
            $total = null;
            $users = User::where('polling_station_id', $polling_station->id)->get();
            foreach($users as $user){
                $user_table = User::where('polling_station_id', $polling_station_id)
                ->whereHas('tables_numbers', function($subquery) use($i){
                    $subquery->where('table_number', $i)
                    ->orWhere('table_number', 'Mesa '.$i);
                })
                ->first();
               
                if($user_table){
                    $total = FormE14Vote::where('user_id', $user->id)
                    ->where('candidate_id', $candidate_id)
                    ->where('table_number', $i)
                    ->first();
                    $sum += $total ? $total->votes_number : 0;
                }
            }

            $objData = array(
                "name" => "Mesa {$i}",
                "y" =>  $sum > 0 ? ($sum/$total_general)*100 : 0,
                "total_votes" => $sum
            );
            array_push($data, $objData);
        }
        return ["data" => $data, "total_votos" => $total_general];
    }

    public function getCandidatesForForm($form_id){
        $candidates = FormE14Candidate::where('forme14_id', $form_id)->get();

        return response()->json(["success" => true, "data" => $candidates, "message" => ""]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
