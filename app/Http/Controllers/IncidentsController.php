<?php

namespace App\Http\Controllers;

use App\Entities\Location;
use App\Entities\PollingStation;
use App\Entities\User;
use App\Entities\Voter;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Services\WasApi;
use App\Libraries\Campaing;
use Illuminate\Support\Facades\Log;

class IncidentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $number_voters 			= Voter::numberVoters();
        Log::info(["number_voters" => $number_voters]);
        $polling_station_id = $request->polling_station;
        $polling_stations = [];
        if($polling_station_id){
            $polling_stations = [$polling_station_id];
        }

        $zones = null;
        if($request->zone){
            $zones = $request->zone;
        }

        if($request->bandera){
            $polling = count($polling_stations) > 0 ? $polling_stations : null;
            
            $pollingStationsNew = PollingStation::withVotersNewZone($polling, $zones, null, null, false);
        }else{
            if(count($polling_stations) > 0){
                $pollingStationsNew = PollingStation::withVotersNewZone($polling_stations, $zones, null, true);
            }else{
                $pollingStationsNew = PollingStation::withVotersNewZone(null, $zones, null, true);
            }
        }
        $pollingStations = collect();
        foreach($pollingStationsNew as $pollingStation){
            if($pollingStation->tables > 0){
                for ($i=1; $i <= $pollingStation->tables; $i++) {
                    $pollingNew = clone $pollingStation;
                    $testigoElectoral = User::where('type_id', '=', 5)
                                                ->where(function($q) use($i){
                                                    $q->where('num_table', $i)
                                                        ->orWhere('num_table', 'Mesa '.$i);
                                                })
                                                ->where('polling_station_id', $pollingStation->id)
                                                ->with(['votes' => function($q){
                                                    $q->join('forme14_candidates as fc', 'fc.id', '=', 'forme14_votes.candidate_id');
                                                    $q->join('formse14 as fe', 'fe.candidate_check', '=', 'fc.id');
                                                }])
                                                ->first();
    
                    $pollingNew->testigo = $testigoElectoral;

                    $meet_condition = true;
                    // si tiene filtro quitamos los que no cumplan
                    if($request->bandera && !$this->meetConditions($pollingNew, $request->bandera)){
                        $meet_condition = false;
                    }

                    if($meet_condition){
                        $pollingNew->number_table = $i;
                            
                        if($i > 1){
                            $pollingNew->oculto = true;
                        }else{
                            $pollingNew->oculto = false;
                        }
    
                        if($i == $pollingStation->tables){
                            $pollingNew->last = true;
                        }else{
                            $pollingNew->last = false;
                        }
                        $pollingStations->push($pollingNew);
                    } 
                }
            }
        }
        Log::info(["pollingStations" => $pollingStations]);
		return view('dashboard.pages.witnesses.incidents.index', compact(
			'pollingStations', 'pollingStationsNew', 'number_voters', 'request'
		));
    }

    public function indexApi(){
		$pollingStationsNew 		= PollingStation::withVoters();
        $pollingStations = collect();

        foreach($pollingStationsNew as $pollingStation){
            if($pollingStation->tables > 1){
                for ($i=1; $i <= $pollingStation->tables; $i++) {
                    $pollingNew = clone $pollingStation;
                    $testigoElectoral = User::where('type_id', '=', 5)
                                                ->where(function($q) use($i){
                                                    $q->where('num_table', $i)
                                                        ->orWhere('num_table', 'Mesa '.$i);
                                                })
                                                ->where('polling_station_id', $pollingStation->id)
                                                ->with('files')
                                                ->with(['votes' => function($q){
                                                    $q->join('forme14_candidates as fc', 'fc.id', '=', 'forme14_votes.candidate_id');
                                                    $q->join('formse14 as fe', 'fe.candidate_check', '=', 'fc.id');
                                                }])
                                                ->first();
                    $pollingNew->testigo = $testigoElectoral;
                    $total = $pollingNew->number_voters;
                    $percent = 0;
                    if($pollingNew->testigo){
                        if($pollingNew->testigo->files->count() > 0){
                            $percent = $total > 0 ? (($pollingNew->testigo->votes[0]->votes_number*100)/$total) : 100;
                            
                            if($percent >=80) continue;
                            if($percent > 50 && $percent < 80) continue;
                            
                            $pollingNew->message = $pollingNew->description. " Mesa ".$i;

                            $pollingStations->push($pollingNew);
                            $message_send = $pollingNew->testigo->message_incident;

                            if($pollingNew->testigo->phone && !$message_send){
                                $was_api = new WasApi('incidencias');
                                $telephone = $pollingNew->testigo->phone;
                                $name = $pollingNew->testigo->name;
                                
                                $polling_station = $pollingNew->testigo->pollingStation ? ucfirst($pollingNew->testigo->pollingStation->name) : null;
                                $mesa_explode = $pollingNew->testigo->num_table ? explode(" ", $pollingNew->testigo->num_table) : [];
                                $mesa = null;

                                if(count($mesa_explode) == 2){
                                    $mesa = $mesa_explode[1];
                                }elseif(count($mesa_explode) == 1){
                                    $mesa = $mesa_explode[0];
                                }

                                if($polling_station && $mesa && $name && $telephone){
                                    $variables = [$name, $polling_station, $mesa];
                                    try{
                                        $was_api->sendMessageWithTemplate($telephone, $variables);
                                        $testigo_update = User::find($pollingNew->testigo->id);
                                        $testigo_update->message_incident = 1;
                                        $testigo_update->save();
                                    }catch(\Exception $e){

                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return response()->json(["success" => true, "data" => $pollingStations]);
    }

    public function meetConditions($pollingStation, $bandera){
        $total = $pollingStation->number_voters_by_table;
        $percent = 0;
        if($pollingStation->testigo && $pollingStation->testigo->votes->count() > 0){
            $percent = $total > 0 ? (($pollingStation->testigo->votes[0]->votes_number*100)/$total) : 100;
            if($bandera == "verde"){
                if($percent >=80){
                    return true;
                }
            }else if($bandera == "amarilla") {
                if($percent > 50 && $percent < 80) {
                    return true;
                }
            }else if($bandera == "roja") {
                if($percent >= 0 && $percent < 50) {
                    return true;
                }
            }
            
        }else{
            if($bandera == "gris"){
                return true;
            }
        }

        return false;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
