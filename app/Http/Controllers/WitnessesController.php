<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Libraries\Campaing;
use App\Entities\User;
use Illuminate\Routing\Route;
use App\Entities\FormE14;
use App\Entities\FormE14Info;
use App\Entities\FormE14Vote;
use App\Entities\FormE14File;
use App\Entities\FormE14TotalVote;
use App\Entities\FormE14Candidate;
use App\Entities\FormE14FiledClaim;
use App\Entities\FormE14InfoTestigo;
use App\Entities\FormE14TotalVoteEscrutador;
use App\Entities\FormE14VoteEscrutador;
use App\Entities\FormE14Color;
use App\Entities\Location;
use App\Entities\PollingStation;
use App\Entities\TableNumberUser;
use App\Entities\Voter;
use App\Entities\VoterNew;
use App\Entities\Zone;
use App\Helpers\Helper;
use App\Jobs\UploadImageTestigo;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Http\UploadedFile;

class WitnessesController extends Controller
{
	private $user;

    public function __construct() 
	{
		$this->beforeFilter('@newUser', ['only' => ['create', 'createLeaders', 'store']]);
		$this->beforeFilter('@findUser', ['only' => ['show', 'edit', 'editLeaders', 'update', 'destroy']]);
		$this->middleware('logs', ['only' => ['store', 'storeLeaders', 'update', 'destroy']]);
	}

    public function newUser()
	{
		$this->user = new User;
	}

	public function findUser(Route $route)
	{
	 	$this->user = User::findOrFail($route->getParameter('id'));
	} 
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $campaing = [];
		return view('dashboard.pages.witnesses.witnesses', $campaing);
    }

    public function indexLeaders(){
        $users = User::getUserByType(6);
		return view('dashboard.pages.witnesses.leaders', ['users' => $users]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($cedula = null)
    {   
        $fromFindAndcreate = false;
        $tables = [];
        if($cedula){
            $user = User::where('username', $cedula)->first();
            if($user) return redirect()->route('witnesses.assignwitnesses')->with('message', 'El usuario ya existe');

            $find_testigo = VoterNew::select('*', DB::raw("CONCAT(first_name, ' ', second_name, ' ', surname, ' ', second_surname) as full_name"))
                ->where('nuip', $cedula)->first();
            if($find_testigo){
                $this->user->username = $find_testigo->nuip;
                $this->user->name = $find_testigo->full_name;
                $this->user->email = Helper::generateEmail($find_testigo->first_name, $find_testigo->surname);
                $this->user->polling_station_id = Helper::getPollingStationByVoterNew($find_testigo->polling_station);
                if($this->user->polling_station_id){
                    $polling = PollingStation::find($this->user->polling_station_id);
                    $num_tables = $polling->tables ?? 0;

                    for($i = 1; $i <= $num_tables; $i++){
                        if(Helper::isAvailableTable($polling->id, $i)){
                            array_push($tables, "Mesa ".$i);
                        }
                    }
                    $this->user->num_table = $find_testigo->table;
                }
            }else{
                return redirect()->route('witnesses.assignwitnesses')->with('message', 'No existe ningún usuario con esa cédula en la base de datos');
            }
            $fromFindAndcreate = true;
        }

        $form_data = ['route' => 'witnesses.store', 'method' => 'POST'];
        $locations = \App\Entities\PollingStation::pollingWithMunicipaly();
        return view('dashboard.pages.witnesses.form', compact('form_data', 'locations', 'tables', 'fromFindAndcreate'))
        ->with('user', $this->user);
    }

    public function createLeaders()
    {
        $team               = Voter::allTeam();
        $form_data = ['route' => 'witnesses.leaders.store', 'method' => 'POST'];
        return view('dashboard.pages.witnesses.form_leaders', compact('form_data', 'team'))
        ->with('user', $this->user);
    }

    /**
    * Muestra la vista para crear formularios.
    *
    * @return \Illuminat\Http\Response
    */
    public function createForm(Request $request)
    {
        // Obtiene los roles asignados al usuario:
        //$userType =  in_array(Auth::user()->type_id, [1,2,5]);
/*
        if ($userType == true) {
            DB::select('select * from user where active = ?', [1]);
            $campaing = ['sd','df','fg'];
        }
*/
        // Muestra la vista web

        if(Auth::user()->type->id == 5){
            return redirect()->route('witnesses.list_tables');
        }
        $formse14 = FormE14::all();
        $user = null;
        return view('dashboard.pages.witnesses.createForms', compact('formse14', 'request', 'user'));    
        
    }

    public function createFormE14(){
        return view('dashboard.pages.witnesses.createFormE14');    
    }

    public function pollingStations(Request $request)
    {
        $number_voters 			= Voter::numberVoters();
        $polling_station_id = $request->polling_station;
        $polling_stations = [];
        if($polling_station_id){
            $polling_stations = [$polling_station_id];
        }

        if($request->testigo && count($polling_stations) > 0){
            $pollingStationsNew = PollingStation::withVotersNew($polling_stations, null, null, false);
        }elseif($request->testigo){
            $pollingStationsNew = PollingStation::withVotersNew(null, null, null, false);
        }elseif(isset($request->zone)){
            $polling = count($polling_stations) > 0 ? $polling_stations : null;
            $pollingStationsNew = PollingStation::withVotersNewZone($polling, $request->zone, null, null, true);
        }elseif(count($polling_stations) > 0){
            $pollingStationsNew = PollingStation::withVotersNew($polling_stations, null, null, true);
        }else{
            $pollingStationsNew = PollingStation::withVotersNew(null, null, null, true);
        }

        $testigosSinMesa = User::where('type_id', '=', 5)
            ->where(function($q){
                $q->doesntHave('tables_numbers')
                ->orWhereNull('polling_station_id');
            })->get();

        $pollingStations = collect();
        foreach($pollingStationsNew as $pollingStation){
            if($pollingStation->tables > 0){
                for ($i=1; $i <= $pollingStation->tables; $i++) {
                    $pollingNew = clone $pollingStation;
                    $testigoElectoral = User::where('type_id', '=', 5)
                                                ->whereHas('tables_numbers', function($subquery) use($i){
                                                    $subquery->where('table_number', $i)
                                                    ->orWhere('table_number', 'Mesa '.$i);
                                                })
                                                ->where('polling_station_id', $pollingStation->id)
                                                ->with(['votes' => function($q){
                                                    $q->join('forme14_candidates as fc', 'fc.id', '=', 'forme14_votes.candidate_id');
                                                    $q->join('formse14 as fe', 'fe.candidate_check', '=', 'fc.id');
                                                }])
                                                ->first();
                    if(($request->testigo && $request->testigo == "without" && !$testigoElectoral)){
                        Log::info(["mesa" => $i, "testigo" => $testigoElectoral]);
                        $pollingNew->testigo = $testigoElectoral;

                        $meet_condition = true;
                        // si tiene filtro quitamos los que no cumplan
                        if($request->bandera && !$this->meetConditions($pollingNew, $request->bandera)){
                            $meet_condition = false;
                        }
    
                        if($meet_condition){
                            $pollingNew->number_table = $i;
                                
                            if($i > 1){
                                $pollingNew->oculto = true;
                            }else{
                                $pollingNew->oculto = false;
                            }
        
                            if($i == $pollingStation->tables){
                                $pollingNew->last = true;
                            }else{
                                $pollingNew->last = false;
                            }
                            $pollingStations->push($pollingNew);
                        } 
                    }elseif($request->testigo && $request->testigo == "with" && $testigoElectoral !== null){
                        Log::info(["mesa" => $i, "testigo" => $testigoElectoral]);

                        $pollingNew->testigo = $testigoElectoral;

                        $meet_condition = true;
                        // si tiene filtro quitamos los que no cumplan
                        if($request->bandera && !$this->meetConditions($pollingNew, $request->bandera)){
                            $meet_condition = false;
                        }
    
                        if($meet_condition){
                            $pollingNew->number_table = $i;
                                
                            if($i > 1){
                                $pollingNew->oculto = true;
                            }else{
                                $pollingNew->oculto = false;
                            }
        
                            if($i == $pollingStation->tables){
                                $pollingNew->last = true;
                            }else{
                                $pollingNew->last = false;
                            }
                            $pollingStations->push($pollingNew);
                        } 
                    }elseif(!$request->testigo){
                        Log::info(["mesa" => $i, "testigo" => $testigoElectoral]);

                        $pollingNew->testigo = $testigoElectoral;

                        $meet_condition = true;
                        // si tiene filtro quitamos los que no cumplan
                        if($request->bandera && !$this->meetConditions($pollingNew, $request->bandera)){
                            $meet_condition = false;
                        }
    
                        if($meet_condition){
                            $pollingNew->number_table = $i;
                                
                            if($i > 1){
                                $pollingNew->oculto = true;
                            }else{
                                $pollingNew->oculto = false;
                            }
        
                            if($i == $pollingStation->tables){
                                $pollingNew->last = true;
                            }else{
                                $pollingNew->last = false;
                            }
                            $pollingStations->push($pollingNew);
                        } 
                    }
                    
                }
            }
        }
		return view('dashboard.pages.witnesses.polling_stations.index', compact(
			'pollingStations', 'pollingStationsNew', 'number_voters', 'request', 'testigosSinMesa'
		));
    }

    public function pollingStationsAssignTestigo(Request $request){
        $testigosSinMesa = User::find($request->testigo_id);
        if($testigosSinMesa){
            $num_table_trim = trim(preg_replace('/Mesa/m',"", $request->mesa));

            $testigosSinMesa->polling_station_id = $request->polling_station_id;
            $testigosSinMesa->num_table = $num_table_trim;
            $testigosSinMesa->save();

            return response()->json(['success' => true, 'message' => 'Testigo asignado correctamente']);
        }

        return response()->json(['success' => false, 'message' => 'no asignado']);
    }

    public function uploadImageTempFormE14(Request $request, $forme14_id, $table_number, $testigo_id = null){
        if($request->hasFile('fileData')){
            $user = Auth::user();
            if($testigo_id){
                $user = User::find($testigo_id);
            }

            $forme14_file = FormE14File::where('forme14_id', $forme14_id)
            ->where('user_id', $user->id)
            ->where('table_number', $table_number)
            ->first();

            $filePrimary = $request->file('fileData');
            $fileIntervention = Image::make($filePrimary);
            $fileIntervention->encode('jpg', 0);
            $newWidth = 628; 
            $newHeight = $newWidth * $fileIntervention->height() / $fileIntervention->width();

            $fileIntervention->resize($newWidth, $newHeight);

            $fileName = time() . '.' . $filePrimary->getClientOriginalExtension();
            Storage::disk('files_forme14')->put($fileName, $fileIntervention->stream());

            if(!$forme14_file){
                $formE14_file = new FormE14File();
                $formE14_file->forme14_id = $forme14_id;
                $formE14_file->user_id = $user->id;
                $formE14_file->table_number = $table_number;
                if($request->primary && $request->primary != "false"){
                    $formE14_file->file_temp = $fileName;
                }else{
                    $formE14_file->file_secondary_temp = $fileName;
                }
                $formE14_file->save();
            }else{
                if($request->primary && $request->primary != "false"){
                    if($forme14_file->file_temp){
                        $path = public_path("files_forme14/{$forme14_file->file_temp}");
        
                        if(file_exists($path) && $forme14_file->file_temp){
                            unlink($path);
                        }
                    }
                }else{
                    if($forme14_file->file_secondary_temp){
                        $path = public_path("files_forme14/{$forme14_file->file_secondary_temp}");
        
                        if(file_exists($path) && $forme14_file->file_secondary_temp){
                            unlink($path);
                        }
                    }
                }
                

                if($request->primary && $request->primary != "false"){
                    $forme14_file->file_temp = $fileName;
                }else{
                    $forme14_file->file_secondary_temp = $fileName;
                }
                $forme14_file->save();
            }

            return response()->json(['success' => true, 'data' => ['fileName' => $fileName], 'message' => 'Imagen cargada']);
        }else{
            return response()->json(['success' => false, 'data' => null, 'message' => 'Imagen vacía']);
        }
    }

    public function editFormE14($form_id, $table_number = null, $testigo_id = null){
        $formE14 = FormE14::find($form_id);

        if(Auth::user()->type_id == 8 && !$testigo_id){
            return redirect()->back();
        }
        $user = Auth::user();
        if($testigo_id){
            $user = User::find($testigo_id);
        }else{
            if(Auth::user()->type_id == 1){
                $user = null;
            }
        }

        if($user){
            $polling_station = $user->pollingStation;
            $tables = $polling_station->tables;
    
            if($tables < $table_number || !$user->tables_numbers()->where('table_number', $table_number)->exists()){
                if(Auth::user()->type->id == 5){
                    return redirect()->to('/');
                }else{
                    return redirect()->back();
                }
            }
        }
        
        $create = false;
        if(Auth::user()->type_id == 1 && !$testigo_id){
            $create = true;
        }
        return view('dashboard.pages.witnesses.editFormE142', compact('formE14', 'user', 'table_number', 'create'));    
    }

    public function lisTables(){
        $forme14 = FormE14::first();
        return view('dashboard.pages.witnesses.listTables', compact('forme14'));    
    }

    public function getAllFormE14($form_id, $table_number, $user_id){
        $formE14 = FormE14::where('id', $form_id)
        ->with(['candidates' => function($q) use($user_id, $table_number){
            $q->with(['voteWithoutUser' => function($q) use($user_id, $table_number){
                $q->where('user_id', $user_id)
                ->where('table_number', $table_number);
            }]);
            if(Auth::user()->type->id == 9 || Auth::user()->type->id == 1){
                $q->with(['voteWithoutUserEscrutador' => function($q) use($user_id, $table_number){
                    $q->where('user_id', $user_id)
                    ->where('table_number', $table_number);
                }]);
            }
        }])
        ->with('info')
        ->with(['fileWithoutUser' => function($q) use($user_id, $table_number){
            $q->where('user_id', $user_id)
            ->where('table_number', $table_number);
        }])
        ->with(['total_vote_without_user' => function($q) use($user_id, $table_number){
            $q->where('user_id', $user_id)
            ->where('table_number', $table_number);
        }]);

        if(Auth::user()->type->id == 9 || Auth::user()->type->id == 1){
            $formE14->with(['total_vote_without_user_escrutador' => function($q) use($user_id, $table_number){
                $q->where('user_id', $user_id)
                ->where('table_number', $table_number);
            }]);
        }

        $formE14 = $formE14->first();

        foreach($formE14->candidates as $candidate){
            $path = public_path("files_candidates/{$candidate->file}");
            if($candidate->file && file_exists($path)){
                $candidate->file = $candidate->file;
            }else{
                $candidate->file = null;
            }
        }

        $user = \App\Entities\User::where('id', $user_id)
        ->with('pollingStation')
        ->with(['info_testigo' => function($query) use($table_number){
            $query->where('table_number', $table_number);
        }])
        ->first();

        $zone_num = null;

        if($user){
            $polling_station_id = $user->polling_station_id;
            $zone_db = Zone::whereHas('pollingStations', function($query) use($polling_station_id){
                $query->where('polling_station_id', $polling_station_id);
            })->first();

            $zone = $zone_db->name ?? null;
            $zone_num = trim(preg_replace('/Zona /m',"", $zone));
        }

        $filed_claim = FormE14FiledClaim::where('forme14_id', $formE14->id)
                                    ->where('user_id', $user_id)
                                    ->where('table_number', $table_number)
                                    ->first();

        $data = [
            "formE14" => $formE14,
            "user" => $user,
            "filed_claim" => $filed_claim,
            "zone" => $zone_num
        ];
        return response()->json(["success" => true, "data" => $data]);
    }

    public function editPostFormE14(Request $request, $forme14_id, $table_number = null, $testigo_id = null){
        try{
            if(Auth::user()->type->id == 5 || Auth::user()->type->id == 8 || Auth::user()->type->id == 9 || (Auth::user()->type->id == 1 && $testigo_id)){
                DB::beginTransaction();
                $this->updateFormE14Witnesses($forme14_id, $table_number, $request, $testigo_id);
                DB::commit();
                return response()->json(["success" => true, "data" => "", "message" => "El formulario ha sido actualizado correctamente"]);
            }else{
                DB::beginTransaction();
                $this->updateFormE14Admin($forme14_id, $request);
                DB::commit();
                return response()->json(["success" => true, "data" => "", "message" => "El formulario {$request->name} ha sido actualizado correctamente"]);
            }
        }catch(\Exception $e){
            return $e->getMessage();
            DB::rollback();
        }
    }

    public function updateFormE14Witnesses($forme14_id, $table_number, $request, $testigo_id = null){
        $forme14_candidates_request = isset($request->forme14_candidates) && $request->forme14_candidates ? json_decode($request->forme14_candidates) : []; 
        $forme14_candidates_escrutador_request = isset($request->forme14_candidates_escrutador) && $request->forme14_candidates_escrutador ? json_decode($request->forme14_candidates_escrutador) : []; 

        $forme14_info_request = isset($request->forme14_info) && $request->forme14_info ? json_decode($request->forme14_info) : []; 
        $forme14 = FormE14::findOrFail($forme14_id);
        $user = Auth::user();
        if($testigo_id){
            $user = User::find($testigo_id);
        }

        if(isset($request->filed_claim)){
            $filed_claim = FormE14FiledClaim::where('forme14_id', $forme14->id)
                                    ->where('user_id', $user->id)
                                    ->where('table_number', $table_number)
                                    ->first();

            if($filed_claim){
                $filed_claim->filed_claim = $request->filed_claim;
                $filed_claim->save();
            }else{
                $filed_claim = new FormE14FiledClaim();
                $filed_claim->forme14_id =  $forme14->id;
                $filed_claim->user_id =  $user->id;
                $filed_claim->table_number =  $table_number;
                $filed_claim->filed_claim =  $request->filed_claim;
                $filed_claim->save();
            }
        }

        if(isset($request->filed_claim_encrutador) && (Auth::user()->type->id == 1 || Auth::user()->type->id == 9)){
            $filed_claim_encrutador = FormE14FiledClaim::where('forme14_id', $forme14->id)
                                    ->where('user_id', $user->id)
                                    ->where('table_number', $table_number)
                                    ->first();

            if($filed_claim_encrutador){
                $filed_claim_encrutador->filed_claim_encrutador = $request->filed_claim_encrutador;
                $filed_claim_encrutador->save();
            }else{
                $filed_claim_encrutador = new FormE14FiledClaim();
                $filed_claim_encrutador->forme14_id =  $forme14->id;
                $filed_claim_encrutador->user_id =  $user->id;
                $filed_claim_encrutador->table_number =  $table_number;
                $filed_claim_encrutador->filed_claim_encrutador =  $request->filed_claim_encrutador;
                $filed_claim->save();
            }
        }
        foreach($forme14_info_request as $info){
            foreach($info as $key => $value){
                if($key != "forminfo_id"){
                    if($this->canEditTestigo($key, $forme14)){
                        $info_id = $info->forminfo_id;
                        $info_testigo = $user->info_testigo()->where('info_id', $info_id)
                            ->where('table_number', $table_number)
                            ->first();
                        if(!$info_testigo){
                            FormE14InfoTestigo::create([
                                'info_id' => $info_id,
                                'user_id' => $user->id,
                                'table_number' => $table_number,
                                'value' => $value
                            ]);
                        }else{
                            if($info_testigo->value != $value){
                                $info_testigo->value = $value;
                                $info_testigo->save();
                            }
                        }
                    }
                }
            } 
        }
        $forme14_file = FormE14File::where('forme14_id', $forme14_id)
            ->where('user_id', $user->id)
            ->where('table_number', $table_number)
            ->first();

        if(isset($request->load_image1) && $request->load_image1){
            if($forme14_file){
                $forme14_file->file = $forme14_file->file_temp ? $forme14_file->file_temp : $forme14_file->file; 
                $forme14_file->file_temp = null;
                $forme14_file->save();
            }
        }

        if(isset($request->load_image2) && $request->load_image2){
            if($forme14_file){
                $forme14_file->file_secondary = $forme14_file->file_secondary_temp ? $forme14_file->file_secondary_temp : $forme14_file->file_secondary;
                $forme14_file->file_secondary_temp = null;
                $forme14_file->save();
            }
        }
        
        foreach($forme14_candidates_request as $candidate){
            $vote = FormE14Vote::where('candidate_id', $candidate->formcandidate_id)
            ->where('user_id', $user->id)
            ->where('table_number', $table_number)
            ->first();
            if($vote){
                foreach($candidate as $key => $value){
                    if($key != "formcandidate_id"){
                        if($value != ""){
                            $vote->votes_number = intval($value);
                            //$vote->check = $candidate['check'] == "true" ? 1 : 0;
                            $vote->save();
                            $this->updateColorForm($user->id, $table_number, $forme14);
                            break;
                        }else{
                            $vote->delete();
                            $this->updateColorForm($user->id, $table_number, $forme14);
                        }
                    }
                    
                }
            }else{
                foreach($candidate as $key => $value){
                    if($key != "formcandidate_id"){
                        if($value != ""){
                            $forme14_vote = new FormE14Vote();
                            $forme14_vote->candidate_id = $candidate->formcandidate_id;
                            $forme14_vote->votes_number = intval($value);
                            //$forme14_vote->check = $candidate['check'] == "true" ? 1 : 0;
                            $forme14_vote->user_id = $user->id;
                            $forme14_vote->table_number = $table_number;
                            $forme14_vote->save();
                            $this->updateColorForm($user->id, $table_number, $forme14);
                            break;
                        } 
                    }
                }
            }
        }

        if(count($forme14_candidates_escrutador_request) > 0 && (Auth::user()->type->id == 1 || Auth::user()->type->id == 9)){
            foreach($forme14_candidates_escrutador_request as $candidate){
                $vote = FormE14VoteEscrutador::where('candidate_id', $candidate->formcandidate_id)
                ->where('user_id', $user->id)
                ->where('table_number', $table_number)  
                ->first();
    
                if($vote){
                    foreach($candidate as $key => $value){
                        if($key != "formcandidate_id"){
                            if($value){
                                $vote->votes_number = intval($value);
                                //$vote->check = $candidate['check'] == "true" ? 1 : 0;
                                $vote->save();
                                break;
                            }else{
                                $vote->delete();
                            }
                        }
                    }
                }else{
                    foreach($candidate as $key => $value){
                        if($key != "formcandidate_id"){
                            if($value){
                                $forme14_vote = new FormE14VoteEscrutador();
                                $forme14_vote->candidate_id = $candidate->formcandidate_id;
                                $forme14_vote->votes_number = intval($value);
                                //$forme14_vote->check = $candidate['check'] == "true" ? 1 : 0;
                                $forme14_vote->user_id = $user->id;
                                $forme14_vote->table_number = $table_number;
                                $forme14_vote->save();
                                break;
                            }
                        }
                    }
                }
            }
        }

        if($request->total_votes){
            $total_votes = FormE14TotalVote::where('forme14_id', $forme14_id)
            ->where('user_id', $user->id)
            ->where('table_number', $table_number)  
            ->first();

            if($total_votes){
                $total_votes->total_votes = $request->total_votes;
                $total_votes->save();
            }else{
                $formE14_file = new FormE14TotalVote();
                $formE14_file->total_votes = $request->total_votes;
                $formE14_file->forme14_id = $forme14_id;
                $formE14_file->user_id = $user->id;
                $formE14_file->table_number = $table_number;
                $formE14_file->save();
            }
        }

        if(isset($request->total_votes_encrutador) && $request->total_votes_encrutador > 0 && (Auth::user()->type->id == 1 || Auth::user()->type->id == 9)){
            $total_votes = FormE14TotalVoteEscrutador::where('forme14_id', $forme14_id)
            ->where('user_id', $user->id)
            ->where('table_number', $table_number)  
            ->first();

            if($total_votes){
                $total_votes->total_votes = $request->total_votes_encrutador;
                $total_votes->save();
            }else{
                $formE14_file = new FormE14TotalVoteEscrutador();
                $formE14_file->total_votes = $request->total_votes_encrutador;
                $formE14_file->forme14_id = $forme14_id;
                $formE14_file->user_id = $user->id;
                $formE14_file->table_number = $table_number;
                $formE14_file->save();
            }
        }
    }

    public function updateColorForm($testigo_id = null, $table_number = null, $form){
        $color = "blanco";
        if($testigo_id && $table_number){
            $votes = FormE14Vote::where('user_id', $testigo_id)
            ->where('table_number', $table_number)
            ->join('forme14_candidates as fc', 'forme14_votes.candidate_id', '=', 'fc.id')
            ->where('fc.forme14_id', $form->id)
            ->get();

            $file = $form
                ->files()
                ->where('user_id', $testigo_id)
                ->where('table_number', $table_number)
                ->first();

            if(count($votes) > 0 && $file && $file->file && $file->file_secondary){
                $color = "verde";
            }elseif((count($votes) > 0 && !$file) || (count($votes) > 0 && $file && $file->file && !$file->file_secondary)){
                $color = "naranja";
            }
        }

        $colorForm = $form->colors()->color($testigo_id, $table_number)->first();
        if($colorForm){
            $colorForm->color = $color;
            $colorForm->save();
        }else{
            FormE14Color::create([
                'user_id' => $testigo_id,
                'table_number' => $table_number,
                'forme14_id' => $form->id,
                'color' => $color,
            ]);
        }
        
    }

    public function getStateFile($form_id, $table_number = null, $user_id = null){
        if($table_number){
            $testigo_id = $user_id;
            if(!$testigo_id){
                $testigo_id = Auth::user()->id;
            }

            $formE14 = FormE14File::where('forme14_id', $form_id)
                    ->where('user_id', $testigo_id)
                    ->where('table_number', $table_number)
                    ->first();

            return response()->json(['success' => true, 'data' => $formE14]);
        }else{
            return response()->json(['success' => true, 'data' => null]);
        }
    }

    public function canEditTestigo($key, $forme14){
        $can = true;
        if($key == "Ciudad / Municipio" || $key == "Zona" || $key == "Puesto de Votación" || $key == "Mesa"){
            $can = false;
        }
        if($forme14->info()->where('name', $key)->whereNotNull('value')->where('value', '!=', '')->first()){
            $can = false;
        }

        return $can;
    }

    public function updateFormE14Admin($forme14_id, $request){
        $forme14 = FormE14::findOrFail($forme14_id);
        $imageName = null;
        
        if($request->file){
            $path = public_path("logos_part/{$forme14->match_logo}");

            if(file_exists($path) && $forme14->match_logo){
                unlink($path);
            }

            $imageName = $this->saveFileLogoPart($request->file);
            $forme14->match_logo = $imageName;
        }

        $forme14->name = $request->name;
        $forme14->save();
        
        if(count($request->forme14_info) > 0){
            $this->saveFormE14Info($request->forme14_info, $forme14_id);
        }
        //Creacion de informacion de los votos del formulario e14
        if(count($request->forme14_candidates) > 0){
            $this->saveFormE14Candidates($request->forme14_candidates, $forme14_id);
        }
    }

    public function saveFileLogoPart($file, $folder="logos_part"){
        $image = $file; // image base64 encoded
        preg_match("/data:image\/(.*?);/",$image,$image_extension);
        $image = preg_replace('/data:image\/(.*?);base64,/','',$image);
        $image = str_replace(' ', '+', $image);

        $numbers = rand(1000000000,9999999999);

        $imageName = 'image_' . $numbers   . '.' . $image_extension[1];

        Storage::disk($folder)->put($imageName, base64_decode($image));

        return $imageName;
    }

    public function saveFileForm($file){
        preg_match("/data:application\/(.*?);/",$file,$file_extension);

        if(count($file_extension) > 0) {
            $file = preg_replace('/data:application\/(.*?);base64,/','',$file);
        }else{
            preg_match("/data:image\/(.*?);/",$file,$file_extension);
            $file = preg_replace('/data:image\/(.*?);base64,/','',$file);
        }
        $file = str_replace(' ', '+', $file);

        $numbers = rand(1000000000,9999999999);

        $fileName = 'file_' . $numbers   . '.' . $file_extension[1];
        Storage::disk('files_forme14')->put($fileName, base64_decode($file));

        return $fileName;
    }

    public function saveFormE14Candidates($forme14_candidates, $forme14_id){
        $form_candidates_actually = FormE14Candidate::where('forme14_id', $forme14_id)->get();
        foreach($forme14_candidates as $candidate){
            $file = null;
            if($candidate['file'] && substr($candidate['file'], 0, 11) === "data:image/"){
                $file = $this->saveFileLogoPart($candidate['file'], 'files_candidates');
            }
            if($candidate["formcandidate_id"] == ""){
                foreach($candidate as $key => $value){
                    $forme14_candidate = new FormE14Candidate();
                    $forme14_candidate->candidate_number = $key;
                    $forme14_candidate->forme14_id = $forme14_id;
                    $forme14_candidate->file = $file;
                    $forme14_candidate->save();
                    break;
                }
            }else{
                $form_candidate = $form_candidates_actually->where("id", intval($candidate["formcandidate_id"]))->first();
                if($form_candidate){
                    foreach($candidate as $key => $value){
                        if($candidate['check'] && $candidate['check'] == 'true'){
                            $forme14 = FormE14::find($forme14_id);
                            $forme14->candidate_check = intval($candidate['formcandidate_id']);
                            $forme14->save();
                        }
                        
                        if($candidate['file'] && substr($candidate['file'], 0, 11) === "data:image/"){
                            $path = public_path("files_candidates/{$form_candidate->file}");
                            if(file_exists($path) && $form_candidate->file){
                                unlink($path);
                            }
                            $form_candidate->file = $file;
                        }

                        $form_candidate->candidate_number = $key;
                        $form_candidate->save();
                        break;
                    }
                }
            }
        }

        foreach($forme14_candidates as $candidate){
            if($candidate["formcandidate_id"] == ""){
                foreach($candidate as $key => $value){
                    if($candidate['check'] && $candidate['check'] == 'true'){
                        $forme14 = FormE14::find($forme14_id);
                        $candidate = $forme14->candidates()->where('candidate_number', $key)->first();

                        if($candidate){
                            $forme14->candidate_check = $candidate->id;
                            $forme14->save();
                        }
                    }
                    break;
                }
            }
        }

        foreach($form_candidates_actually as $form_actually){
            if($this->searchArrayCandidates($forme14_candidates, $form_actually) == false){
                $form_actually->delete();
            }
        }
        
    }

    public function saveFormE14Info($forme14_info_array, $forme14_id){
        $form_info_actually = FormE14Info::where('forme14_id', $forme14_id)->get();
        foreach($forme14_info_array as $info){
            if($info["forminfo_id"] == ""){
                foreach($info as $key => $value){
                    $forme14_info = new FormE14Info();
                    $forme14_info->name = $key;
                    $forme14_info->value = $value;
                    $forme14_info->forme14_id = $forme14_id;
                    $forme14_info->save();
                    break;
                }
            }else{
                $form_info = $form_info_actually->where("id", intval($info["forminfo_id"]))->first();
                if($form_info){
                    foreach($info as $key => $value){
                        if(strtoupper($key) != "Mesa" && strtoupper($key) != "Puesto"){
                            $form_info->name = $key;
                            $form_info->value = $value;
                            $form_info->save();
                        }
                        break;
                    }
                }
            }
        }
        foreach($form_info_actually as $form_actually){
            if($this->searchArray($forme14_info_array, $form_actually) == false){
                $form_actually->delete();
            }
        }

        /*$form_info_actually = FormE14Info::where('forme14_id', $forme14_id)->get();
        foreach($forme14_info as $info){
            foreach($info as $key => $value){
                $forme14_info = new FormE14Info();
                $forme14_info->name = $key;
                $forme14_info->value = $value;
                $forme14_info->forme14_id = $forme14_id;
                $forme14_info->save();
            }
        }*/
    }

    public function searchArray($forme14_info, $form_actually){
        foreach($forme14_info as $info){
            if($info["forminfo_id"] == $form_actually->id){
                return true;
            }
        }

        return false;
    }

    public function searchArrayCandidates($forme14_candidates, $form_actually){
        foreach($forme14_candidates as $info){
            if($info["formcandidate_id"] == $form_actually->id){
                return true;
            }
        }

        return false;
    }

    public function createPostFormE14(Request $request){
        try{
            DB::beginTransaction();
            if($request->file){
                $imageName = $this->saveFileLogoPart($request->file);
                $request->file = $imageName;
            }

            $forme14 = new FormE14();
            $forme14->name = $request->name;
            $forme14->match_logo = $request->file;
            $forme14->save();            

            if(!$forme14) return response()->json(["success" => false, "message" => "Error al crear el formulario"]);


            //Creacion de informacion del formulario e14
            if(count($request->forme14_info) > 0){
                $this->saveFormE14Info($request->forme14_info, $forme14->id);
            }

            //Creacion de informacion de los votos del formulario e14
            if(count($request->forme14_candidates) > 0){
                $this->saveFormE14Candidates($request->forme14_candidates, $forme14->id);
            }
            DB::commit();
            return response()->json(["success" => true, "data" => "", "message" => "El formulario {$request->name} ha sido creado correctamente"]);
        }catch(\Exception $e){
            DB::rollback();
            return $e->getMessage();
        }
    }

    public function mentionlytics(){
        return view('dashboard.pages.witnesses.mentionlytics');
    }

    /**
    * Muestra la vista para asignar testigos electorales a un puesto de votación.
    *
    * @return \Illuminat\Http\Response
    */
    public function assignwitnesses(Request $request)
    {   
        $Users = User::where('type_id', '=', 5)
        ->where(function($query){
            $query->has('tables_numbers')
            ->whereNotNull('polling_station_id')
            ->whereNotNull('name')
            ->whereNotNull('email');
        })
        ->orderBy('id','desc');

        if(isset($request->search) && $request->search){
            $Users->where(function($q) use($request){
                $q->where('username', 'LIKE', '%'.$request->search.'%');
                $q->orWhere('name', 'LIKE', '%'.$request->search.'%');
                $q->orWhere('email', 'LIKE', '%'.$request->search.'%');
                $q->orWhere('phone', 'LIKE', '%'.$request->search.'%');
            });
        }

        if(isset($request->polling_station) && $request->polling_station){
            $Users->where(function($q) use($request){
                $q->where('polling_station_id', '=', $request->polling_station);
            });
        }

        if(isset($request->zone)){
            $zones = json_decode($request->zone);
            if(count($zones) > 0){
                $zones_db = Zone::whereIn('id', $zones)->get();
                $pollingStations = [];
                foreach($zones_db as $zone){
                    $pollingStations = array_merge($pollingStations, $zone->pollingStations->pluck('id')->toArray() ?? []);
                }
    
                $Users->whereIn('polling_station_id', $pollingStations);
            }
        }
        
//        dd($Users);
        $locations = DB::table('polling_stations')->distinct('location_id')->count();
        $witnesses = DB::table('user')->where('type_id', 5)->get();

//        $witnesses = User::with('type_id');

//        dd($witnesses);
        return view('dashboard.pages.witnesses.assignwitnesses',['users'=>$Users->paginate(30), 'locations'=> $locations, 'request' => $request]);
    }

    public function assignwitnessesCedula(Request $request)
    {   
        $Users = User::where('type_id', '=', 5)
        ->orderBy('id','desc');
       

        if($request->alert){
            if($request->alert == "puesto"){
                $Users->whereNotNull('name')
                ->whereNotNull('email')
                ->whereNull('polling_station_id');
            }elseif($request->alert == "mesa"){
                $Users->whereNotNull('name')
                ->whereNotNull('email')
                ->whereNotNull('polling_station_id')
                ->doesntHave('tables_numbers');
            }elseif($request->alert == "bdd"){
                $Users->whereNull('name')
                ->whereNull('email');
            }
        }else{
            $Users->where(function($query){
                $query->doesntHave('tables_numbers')
                ->orWhereNull('polling_station_id')
                ->orWhereNull('name')
                ->orWhereNull('email');
            });
        }
        
        if(isset($request->search) && $request->search){
            $Users->where(function($q) use($request){
                $q->where('username', 'LIKE', '%'.$request->search.'%');
                $q->orWhere('name', 'LIKE', '%'.$request->search.'%');
                $q->orWhere('email', 'LIKE', '%'.$request->search.'%');
                $q->orWhere('phone', 'LIKE', '%'.$request->search.'%');
            });
        }

        if(isset($request->polling_station) && $request->polling_station){
            $Users->where(function($q) use($request){
                $q->where('polling_station_id', '=', $request->polling_station);
            });
        }
        
//        dd($Users);
        $locations = DB::table('polling_stations')->distinct('location_id')->count();
        $witnesses = DB::table('user')->where('type_id', 5)->get();

//        $witnesses = User::with('type_id');

//        dd($witnesses);
        return view('dashboard.pages.witnesses.assignwitnesses_cedula',['users'=>$Users->paginate(30), 'locations'=> $locations, 'request' => $request]);
    }

    /**
    * Muestra la vista para ver los resultados reportados por los testigos electorales.
    *
    * @return \Illuminat\Http\Response
    */
    public function resultsaccordingwitnesses(Request $request)
    {
        //
        //$campaing = [];
        /*$users = User::whereHas('type', function($q){
            $q->where('id', 5);
        })->get();*/

        $users = User::where('type_id', '=', 5);
        if(isset($request->search) && $request->search){
            $users->where(function($q) use($request){
                $q->where('username', 'LIKE', '%'.$request->search.'%');
                $q->orWhere('name', 'LIKE', '%'.$request->search.'%');
                $q->orWhere('phone', 'LIKE', '%'.$request->search.'%');
            });
        }

        if(isset($request->polling_station) && $request->polling_station){
            $users->where(function($q) use($request){
                $q->where('polling_station_id', '=', $request->polling_station);
            });
        }
        if(isset($request->zone)){
            $zones = json_decode($request->zone);
            if(count($zones) > 0){
                $zones_db = Zone::whereIn('id', $zones)->get();
                $pollingStations = [];
                foreach($zones_db as $zone){
                    $pollingStations = array_merge($pollingStations, $zone->pollingStations->pluck('id')->toArray() ?? []);
                }
    
                $users->where(function($q) use($pollingStations){
                    $q->whereIn('polling_station_id', $pollingStations);
                });
            }
            
        }

        $users = $users->paginate(20);

        $formse14 = FormE14::all();

        return view('dashboard.pages.witnesses.resultsaccordingwitnesses', compact('users', 'formse14', 'request'));
    }

    public function emptyInfoForm($user_id, $table_number, $form_id){
        $file = FormE14File::where('forme14_id', $form_id)
        ->where('table_number', $table_number)
        ->where('user_id', $user_id)->first();

        $votes = FormE14Vote::where('user_id', $user_id)
        ->where('table_number', $table_number)
        ->get();

        $color = FormE14Color::where('user_id', $user_id)
        ->where('table_number', $table_number)
        ->where('forme14_id', $form_id)
        ->first();

        $votes_escrutador = FormE14VoteEscrutador::where('user_id', $user_id)
        ->where('table_number', $table_number)
        ->get();

        $total_votes = FormE14TotalVote::where('forme14_id', $form_id)
        ->where('table_number', $table_number)
        ->where('user_id', $user_id)->first();

        $total_votes_escrutador = FormE14TotalVoteEscrutador::where('forme14_id', $form_id)
        ->where('table_number', $table_number)
        ->where('user_id', $user_id)->first();

        $filed_claim = FormE14FiledClaim::where('forme14_id', $form_id)
        ->where('table_number', $table_number)
        ->where('user_id', $user_id)
        ->first();

        if($file){
            $path = public_path("files_forme14/".$file->file);
            if(file_exists($path) && $file->file){
                unlink($path);
            }

            $file->delete();
        }

        if(count($votes) > 0){
            foreach($votes as $vote){
                if($vote->candidate->forme14_id == $form_id){
                    $vote->delete();
                }
            }
        }

        if(count($votes_escrutador) > 0){
            foreach($votes_escrutador as $vote){
                if($vote->candidate->forme14_id == $form_id){
                    $vote->delete();
                }
            }
        }

        if($color){
            $color->delete();
        }

        if($total_votes){
            $total_votes->delete();
        }

        if($total_votes_escrutador){
            $total_votes_escrutador->delete();
        }

        if($filed_claim){
            $filed_claim->delete();
        }

        return redirect()->back();
    }

    /*public function createwitnesses(){
		$form_data = ['route' => 'witnesses.store', 'method' => 'POST'];
        return view('dashboard.pages.witnesses.form', compact('form_data'))
        ->with('user', $this->user);
    }*/
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['type_id'] = 5;

        $user = User::where('username', $data['username'])->first();
        if($user) return redirect()->route('witnesses.create')->with('message', 'El nombre de usuario ya existe');

        $mesas = [];
        foreach($data['table_multiple'] as $table){
            $num_table = trim(preg_replace('/Mesa/m',"", $table));
            array_push($mesas, $num_table);
        } 

        $testigo = User::where('polling_station_id', $data['polling_station_id'])
        ->whereHas('tables_numbers', function($subquery) use($mesas){
            $subquery->whereIn('table_number', $mesas);
        })->first();


        if($testigo){
		    return redirect()->route('witnesses.create')->with('message', 'Ya existe un testigo para ese puesto de votación y esa mesa');
        }

        $this->user->fill($data);
        $this->user->save();

        if($this->user){
            foreach($data['table_multiple'] as $table){
                $num_table = trim(preg_replace('/Mesa/m',"", $table));
                TableNumberUser::create(['user_id' => $this->user->id, 'table_number' => $num_table]);
            }
        }

		return redirect()->route('witnesses.assignwitnesses');
    }

    public function storeLeaders(Request $request){
        $user = User::where('username', $request->username)->first();
        if($user) return redirect()->route('witnesses.leaders.create')->with('exist', 'El nombre de usuario ya existe')->withInput($request->input());
        
        $user = User::where('email', $request->email)->first();
        if($user) return redirect()->route('witnesses.leaders.create')->with('exist', 'El correo electrónico ya existe')->withInput($request->input());

        $this->user = new User;

        $data = $request->all();
        $data['type_id'] = 6;

        if(isset($data['house_check']) && $data['house_check'] == "on"){
            $data['house_check'] = true;
        }else{
            $data['house_check'] = false;
        }

        if($data["address"]){
            $location_name = "";
            if(isset($request["location_id"])){
                $location_collect = Location::where('id', $request["location_id"])->first();
                if($location_collect){
                    $location_name = $location_collect->name;
                }
            }
            $location = \Helper::getCoordinates($data["address"], $location_name);
            
            if($location){
                if($location["status"] == "OK"){
                    $results = $location["results"];
                    $latitute = $results[0]["geometry"]["location"]["lat"];
                    $longitude = $results[0]["geometry"]["location"]["lng"];

                    $data["latitude"] = $latitute;
                    $data["longitude"] = $longitude;
                }
            }
        }
        $data["location_id"] = $data["location_id"] == "" ? NULL : $data["location_id"];
        $this->user->fill($data);
        $this->user->save();
        try{
            $this->user->sendWelcomeMessage();
        }catch(\Exception $e){

        }
        return redirect()->route('witnesses.leaders');   
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $fromFindAndcreate = false;
        $form_data = ['route' => ['witnesses.update', $this->user->id], 'method' => 'PUT'];
        $locations   = \App\Entities\PollingStation::pollingWithMunicipaly();
        $num_tables = $this->user->pollingStation ? $this->user->pollingStation->tables : 0;

        $tables = [];

        for($i = 1; $i <= $num_tables; $i++){
            $has_table = $this->user->tables_numbers()->where('table_number', $i)
            ->orWhere('table_number', "Mesa ".$i)
            ->exists();
            if(Helper::isAvailableTable($this->user->pollingStation->id, $i) || $has_table){
                array_push($tables, "Mesa ".$i);
            }
        };
		return view('dashboard.pages.witnesses.form', compact('form_data', 'locations', 'tables', 'fromFindAndcreate'))
			->with('user', $this->user);
    }

    public function editLeaders($id)
    {
        $team               = Voter::allTeam();
        $form_data = ['route' => ['witnesses.leaders.update', $this->user->id], 'method' => 'PUT'];
		return view('dashboard.pages.witnesses.form_leaders', compact('form_data', 'team'))
			->with('user', $this->user);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();

        $this->user->fill($data);
        $this->user->save();

        if($this->user){
            $this->user->tables_numbers()->delete();

            foreach($data['table_multiple'] as $table){
                $num_table = trim(preg_replace('/Mesa/m',"", $table));
                TableNumberUser::create(['user_id' => $this->user->id, 'table_number' => $num_table]);
            }
        }

		return redirect()->route('witnesses.assignwitnesses');
    }

    public function updateLeaders(Request $request, $id)
    {
        $this->user = User::where('id', $id)->first();
        $data = $request->all();

        if($data["address"]){
			$location_name = "";
			if(isset($request["location_id"])){
				$location_collect = Location::where('id', $request["location_id"])->first();
				if($location_collect){
					$location_name = $location_collect->name;
				}
			}
			$location = \Helper::getCoordinates($data["address"], $location_name);
			
            if($location){
				if($location["status"] == "OK"){
					$results = $location["results"];
					$latitute = $results[0]["geometry"]["location"]["lat"];
					$longitude = $results[0]["geometry"]["location"]["lng"];

					$data["latitude"] = $latitute;
					$data["longitude"] = $longitude;
				}
			}
		}
        $data["location_id"] = $data["location_id"] == "" ? NULL : $data["location_id"];
        if(isset($data['house_check']) && $data['house_check'] == "on"){
            $data['house_check'] = true;
        }else{
            $data['house_check'] = false;
        }
        $this->user->fill($data);
        $this->user->save();
		return redirect()->route('witnesses.leaders');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyFormE14($forme14_id)
    {
        $formE14 = FormE14::where('id', $forme14_id)->first();

        if(count($formE14->files) > 0){
            foreach($formE14->files as $file){
                $path = public_path()."/files_forme14/{$file->file}";
                if(file_exists($path) && $file->file) {
                    unlink($path);
                }
            }
        }
        $path_logo = public_path()."/logos_part/{$formE14->match_logo}";
        if(file_exists($path_logo) && $formE14->match_logo) {
            unlink($path_logo);
        }

        $formE14->delete();

        

        return response()->json(true);
    }

    public function getLocations()
    {
        dd (DB::table('polling_stations'->get()->distinct()));
    }

}
