<?php

namespace App\Http\Controllers;

use App\ColaProceso;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Proceso;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Laracasts\Flash\Flash;

class ColaProcesoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cola_procesos = ColaProceso::all();
        return view('dashboard.pages.procesos.index')->with('cola_procesos', $cola_procesos);
    }

    public function indexApi()
    {
        $cola_procesos = ColaProceso::with(['estado_proceso', 'proceso'])->get();
        return response()->json($cola_procesos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $form_data = ['route' => 'procesos.store', 'method' => 'POST'];
        $procesos = Proceso::where('activo', 1)->get();
        return view('dashboard.pages.procesos.create', compact('procesos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            DB::beginTransaction();
            $data = $request->all();
            $data['user_id'] = Auth::id();
            $data['email'] = Auth::user()->email;
            $data['estado_proceso_id'] = 2;
            $cola = $this->getColaProceso($data['proceso_id']);
            if ($cola) {
                switch ($cola->estado_proceso->slug) {
                    case 'en-proceso':
                        $mensaje = (string)"Ya existe una solicitud pendiente para " . $cola->proceso->nombre;
                        Flash::error($mensaje);
                        break;
                    case 'en-ejecucion':
                        Flash::error('La solicitud para esta actualización está actualmente en ejecución.');
                        break;
                    case 'finalizado':
                        $this->createProceso($request, $data);
                        Flash::success('Una nueva solicitud para actualización fue registrada con éxito.');
                        break;
                }
            } else {
                $this->createProceso($request, $data);
                Flash::success('Una nueva solicitud para actualización fue registrada con éxito.');
            }
            DB::commit();
            return redirect()->route('procesos.index');
        }catch(\Exception $e){
            dd($e->getMessage());
            DB::rollback();
            Flash::error('Parece que hubo un error en la solicitud de actualización');
        }
    }

    public function createProceso($request, $data){
        $file = $request->file('file');
        $nameFull = $file->getClientOriginalName();
        $name = pathinfo($nameFull, PATHINFO_FILENAME)."-".time();
        $extension = pathinfo($nameFull, PATHINFO_EXTENSION);
        $name .= ".$extension";
        
        $path_init = 'voters';

        if($data["proceso_id"] == 2){
            $path_init = 'coordinators';
        }else if($data["proceso_id"] == 3){
            $path_init = 'orientators';
        }else if($data["proceso_id"] == 4){
            $path_init = 'testigos';
        }else if($data["proceso_id"] == 5){
            $path_init = 'mesas';
        }else if($data["proceso_id"] == 6){
            $path_init = 'ciudades';
        }

        $path = $path_init.'/'.$name;
        Storage::disk('local')->put($path, file_get_contents($file));

        $data['parametros'] = json_encode([
            "file" => $path
        ]);

        $cola_proceso = ColaProceso::create($data);
        
        return $cola_proceso;
    }

    public function getColaProceso($proceso_id)
    {
        $colaProcesos = ColaProceso::join('user', 'cola_procesos.user_id', '=', 'user.id');
        $colaProcesos = $colaProcesos
            ->where('proceso_id', '=', $proceso_id)
            ->whereDate('cola_procesos.created_at', '=', date('Y-m-d'))
            ->orderBy('cola_procesos.created_at', 'DESC')
            ->select('cola_procesos.*')
            ->first();

        return $colaProcesos;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
