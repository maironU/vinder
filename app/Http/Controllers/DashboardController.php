<?php namespace App\Http\Controllers;

use App\Entities\FormE14;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
class DashboardController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function index(Request $request)
	{
		if(Auth::user()->type->id == 5){
        	$forme14 = FormE14::first();
			if(!$forme14){
				return redirect()->route('witnesses.list_tables');
			}
			$tables_numbers = Auth::user()->tables_numbers;
			if(count($tables_numbers) > 1){
				return redirect()->route('witnesses.list_tables');
			}else{
				if(count($tables_numbers) == 1){
					return redirect()->route('witnesses.editforme14', ['forme14_id' => $forme14->id, 'table_number' => intval($tables_numbers[0]->table_number)]);
				}else{
					return redirect()->route('witnesses.list_tables');
				}
			}
		}

		if(Auth::user()->type->id == 9){
			return redirect()->route('coordinator.index');
		}
		return view('dashboard.pages.modules', compact('request'));
	}

}
