<?php namespace App\Http\Controllers\Database;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests\Voter\CreateRequest;
use App\Http\Requests\Voter\NewRequest;

use Illuminate\Routing\Route;

use Illuminate\Database\QueryException;

use App\Libraries\WebScraping;

use Session;
use Event;
use Auth;

use App\Events\VoterWasCreated;

use App\Entities\Voter;
use App\Entities\Location;

use Laracasts\Flash\Flash;


class VotersController extends Controller {

	private $nameRoute;
	private $oppositeRoute;
	private $nameView;
	private $colaborator;
	private $voters;
	private $voter;

	function __construct() {
 		$this->nameRoute = 'voters';
 		$this->oppositeRoute = 'team';
		$this->nameView = 'voter';
 		$this->colaborator = 0;

 		$this->beforeFilter('@getList', ['only' => ['index']]);
 		$this->beforeFilter('@findVoter', ['only' => ['diaries']]);
 		$this->middleware('logs', ['only' => ['store', 'destroy']]);
 	}

 	public function setNameRoute($nameRoute)
 	{
 		$this->nameRoute = $nameRoute;
 	}

 	public function getNameRoute()
 	{
 		return $this->nameRoute;
 	}

 	public function setOppositeRoute($oppositeRoute)
 	{
 		$this->oppositeRoute = $oppositeRoute;
 	}

 	public function getOppositeRoute()
 	{
 		return $this->oppositeRoute;
 	}

 	public function setNameView($nameView)
 	{
 		$this->nameView = $nameView;
 	}

 	public function getNameView()
 	{
 		return $this->nameView;
 	}

 	public function setColaborator($colaborator)
 	{
 		$this->colaborator = $colaborator;
 	}

 	public function getColaborator()
 	{
 		return $this->colaborator;
 	}

 	public function setVoters($voters)
 	{
 		$this->voters = $voters;
 	}

 	public function getVoters()
 	{
		if(is_null($this->voters)) {
			return 'NULL';
		}
		return $this->voters;
 	}

	public function getList(Route $route, Request $request)
	{
		//return Voter::votersPaginate(24, false, $request->get('order'), $request);
		$this->setVoters(Voter::votersPaginate(24, false, $request->get('order'), $request));
	}

	public function findVoter(Route $route)
	{
		$this->voter = Voter::withAll()->whereDoc($route->getParameter('doc'))->firstOrFail();
	}

	/**
	 * Display voter list.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
		try {
			$from_colaborators = false;
			$name_view = "voters";

			if($request->ajax()) {
				return [$this->getVoters()->toJson()];
			}

			if($this->getNameView() == "team"){
				$from_colaborators = true;
				$name_view = "team";
			}	

			// Llama a voter.blade.php
			return view('dashboard.pages.database.voters.lists.'.$this->getNameView())
					->with(['voters' => $this->getVoters(), "from_colaborators" => $from_colaborators, "request" => $request, "type" => $name_view]);
		} 
		catch(Exception $ex) {
			Flash::error($ex->getMessage());
		}
	}

	/**
	 * Change the friendly url
	 *
	 * @return Response
	 */


	public function redirect(Request $request, $nameView = null)
	{
		try{ 
			$name = $nameView ? $nameView : $this->getNameRoute();
			$doc = $request->get('doc');
			if(Auth::user()->type->id == 6){
				$voter = Voter::find($request->get('colaborator'));
				$orientator = Voter::find($request->get('orientator'));

				if($voter){
					Voter::setTeamSession($request->get('colaborator'));
				}

				if($orientator){
					Voter::setOrientatorSession($request->get('orientator'));
				}
			}else{
				Voter::setTeamSession($request->get('colaborator'));
				Voter::setOrientatorSession($request->get('orientator'));
			}
			//}

			if ($request->has('diary')) 
			{
				return redirect()->route('database.'.$name.'.create', [$doc, $request->get('diary')]);
			}
			$voter = Voter::isVoter($doc);
			return redirect()->route('database.'.$name.'.create', $doc);

		}catch(\App\Exceptions\ExistException $e){
			return redirect()->back()->with('error', $e->getMessage());
		}catch(\App\Exceptions\DeleteException $e){
			return redirect()->back()->with('error_delete', $e->getMessage())->with(["type" => $name]);
		}

	}

	public function restoreVoter($voter_id){
		$voter = Voter::onlyTrashed()
		->where('id', $voter_id)
		->first();

		$voter->restore();

		return redirect()->back()->with('restore', 'Usuario activado correctamente');
	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($doc, $diary = null)
	{
		$voter = Voter::findwithTrashedOrNew($doc);
		$from_colaborators = false;
		if($voter->exists && $voter->colaborator != $this->getColaborator())
		{
			return redirect()->route('database.'. $this->getOppositeRoute().'.create', [$doc, $diary]);
		}
		$form_data = ['route' => ['database.'.$this->getNameRoute().'.store', $doc, $diary], 'method' => 'POST'];
		if($this->getNameView() == "team"){
			$from_colaborators = true;
		}	
		return view('dashboard.pages.database.voters.forms.'.$this->getNameView(), compact('voter', 'form_data', 'pollingStation', 'from_colaborators'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(CreateRequest $request, $doc, $diary = null)
	{
		$voter = $request->getVoter();
		$data = $request->all();
		$colaborator = false;
		if($data["address"]){
			$location_name = "";
			if(isset($request["location_id"])){
				$location_collect = Location::where('id', $request["location_id"])->first();
				if($location_collect){
					$location_name = $location_collect->name;
				}
			}
			$location = \Helper::getCoordinates($data["address"], $location_name);
			if($location){
				if($location["status"] == "OK"){
					$results = $location["results"];
					$latitute = $results[0]["geometry"]["location"]["lat"];
					$longitude = $results[0]["geometry"]["location"]["lng"];

					$data["latitude"] = $latitute;
					$data["longitude"] = $longitude;
				}
			}
		}
		if($this->getNameView() == "team"){
			$data["is_lider"] = 0;
			$data["leader_id"] = null;
			$data["is_coordinator"] = true;
		}else{
			$data["is_lider"] = 1;
			if(Auth::user()->type->id == 1 || Auth::user()->type->id == 2 || Auth::user()->type->id == 3){
				$data["leader_id"] = $data["orientator"];
			}else{
				$data["leader_id"] = Auth::user()->id;
			}

		}	
		$voter->saveAndSync($data, $this->getColaborator());
		Event::fire(new VoterWasCreated($voter)); 
		$voter->sendWelcomeMessage();

		if(! is_null($diary))
		{
			if(! $voter->diaries()->find($diary))
			{
				$voter->diaries()->attach($diary);
			}
			return redirect()->route('diary.people.index', $diary);	
		}
        return redirect()->route('database.'.$this->getNameRoute().'.index');
	}

	public function diaries(Request $request, $doc)
	{
		return view('dashboard.pages.database.voters.diaries')->with('voter', $this->voter);
	}

	/**
	 * Display a voter lists searched
	 *
	 * @return Response
	 */
	public function search(Request $request)
	{
		$text 	= trim($request->get('text'));
		$voters = Voter::searchLike($text);

		return view('dashboard.pages.database.voters.lists.search', compact('voters', 'text'));
	}

	public function addToTeam($id, $diary = null)
	{
		$voter = Voter::findOrFail($id);
		$voter->colaborator = 1;
		$voter->save();

		return redirect()->route('database.team.create', $voter->doc);
	}

	public function findName($doc)
	{
		//$webScraper = new WebScraping;
        //return response()->json( ['name' => $webScraper->runProcuraduria($doc)] );

        return response()->json();
	}

	public function findPollingStation($doc)
	{
		$voter = Voter::firstOrNew(['doc' => $doc]);
        return response()->json( ['result' => $voter->hasPollingStation()]);
	}

	public function destroy($id, Request $request)
	{
		$voter = Voter::findOrFail($id);
		try {
			$voter->delete();
			$result = array('msg' => $voter->name . ' eliminad@ exitosamente', 'success' => true, 'id' => $voter->id);
		} catch (QueryException $e) {
			$result = ['msg' => 'Solo se puede borrar un Votante que no tenga asignados más voters', 
				'success' => false, 'id' => $voter->id];
		}

        if ($request->ajax())
        {
            return $result;
        }
        else
        {
        	if($result['success'] == true)
        	{
        		return redirect()->route('database.'.$this->getNameRoute().'.index');	
        	}

            return redirect()->route('database.'.$this->getNameRoute().'.index', $voter->id)->withInput()->with('result', $result);
        }
	}
}
