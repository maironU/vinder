<?php namespace App\Http\Controllers\Database;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use App\Entities\Voter;

class ColaboratorsController extends VotersController {

 	function __construct() {
 		$this->setNameRoute('team');
 		$this->setOppositeRoute('voters');
		$this->setNameView('team');
 		$this->setColaborator(1);

 		$this->beforeFilter('@getList', ['only' => ['index']]);
 		$this->middleware('logs', ['only' => ['store', 'destroy']]);
 	}

 	private function redirectToCorrectRoute($doc)
	{
		return redirect()->route('database.voters.create', $doc);
	}

	public function getList(Route $route, Request $request)
	{
		$this->setVoters(Voter::teamPaginate(12, $request));
	}

	public function redirectColaborator(Request $request)
	{
		return $this->redirect($request, "team");
	}

	public function restoreVoter($voter_id){
		$voter = Voter::onlyTrashed()
		->where('id', $voter_id)
		->first();

		$voter->restore();

		return redirect()->back()->with('restore', 'Usuario activado correctamente');
	}

	public function remove($doc, $diary = null)
	{
		$voter = Voter::whereDoc($doc)->first();
		$voter->colaborator = false;
		$voter->save();

		if(! is_null($diary))
		{
			return redirect()->route('diary.people.index', $diary);	
		}

		return redirect()->route('database.team.index');
	}



}
