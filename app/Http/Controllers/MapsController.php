<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class MapsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Devuelve puestos de votación donde existan votantes decididos, no trae gente con semaphore = 0

        $type = $request->input('_type');

        switch ($type) {
            case 0: {
                $semaphores = DB::table('view_semaphores')->get();
                return($this->getDataForMarkers($semaphores));
            }

            case 1: {
                $pollingStations = DB::table('polling_stations')->get();
                return ($this->getPollingStationsMarkers($pollingStations));
            }

            case 2: {
                $voters = DB::table('voters')->get();
                return ($this->getDataVoters($voters));
            }

            case 3: {
                $voters = DB::table('voters')
                ->where('is_coordinator', 1)->get();
                return ($this->getDataVoters($voters));
            }

            case 4: {
                $users = DB::table('user')
                ->where('type_id', 6)->get();
                return ($this->getDataOrientators($users));
            }

            case 5: {
                $usersMarkers = DB::table('user')
                ->where('house_check', 1)
                ->select('name', 'address', 'latitude', 'longitude', 'phone', 'polling_station_id', 'num_table as mesa')
                ->get();

                $votersMarkers = DB::table('voters')
                ->where('house_check', 1)
                ->select('name', 'address', 'latitude', 'longitude', 'telephone as phone', 'polling_station_id', 'table_number as mesa')
                ->get();


                $users = array_merge($usersMarkers, $votersMarkers);
                return $users;
            }

            default: {
                return ("");
            }
        }



        //
    }


    /**
     * Da formato a los puntos de puestos de votación
     */
	private function getPollingStationsMarkers($pollingStations)
	{
//        dd($pollingStations);
        $puntos=[];
        foreach($pollingStations as $pollingStation)
        {
            $punto = [  
                'name' => $pollingStation->{'name'},
                'address' => $pollingStation->{'address'},
                'tables' => $pollingStation->{'tables'},
                'description' => $pollingStation->{'description'},
                'latitude' => $pollingStation->{'latitude'},
                'longitude' => $pollingStation->{'longitude'}, 
                'place' => $pollingStation->{'place'},
                'zone' => $pollingStation->{'zone'},
            ];
            array_push ($puntos, $punto);
        }
        return($puntos);
	}

    private function getDataVoters($voters)
	{
//        dd($pollingStations);
        $puntos=[];
        foreach($voters as $voter)
        {
            $punto = [  
                'name' => $voter->{'name'},
                'address' => $voter->{'address'},
                'phone' => $voter->{'telephone'},
                'polling_station' => $voter->{'polling_station_id'},
                'Mesa' => $voter->{'table_number'},
                'description' => $voter->{'description'},
                'latitude' => $voter->{'latitude'},
                'longitude' => $voter->{'longitude'}, 
                //'place' => $voter->{'place'},
                //'zone' => $voter->{'zone'},
            ];
            array_push ($puntos, $punto);
        }
        return($puntos);
	}

    private function getDataOrientators($orientators)
	{
//        dd($pollingStations);
        $puntos=[];
        foreach($orientators as $orientator)
        {
            $punto = [  
                'name' => $orientator->{'name'},
                'address' => $orientator->{'address'},
                'latitude' => $orientator->{'latitude'},
                'longitude' => $orientator->{'longitude'}, 
            ];
            array_push ($puntos, $punto);
        }
        return($puntos);
	}



    /**
     * Da formato a los marcadores de intención de voto
     */
    private function getDataForMarkers($semaphores)
    {
//        dd($semaphores);
        $puntos=[];
        foreach($semaphores as $semaphore)
        {
            $punto = [  
                'total' => $semaphore->{'Total'},
                'cero' => $semaphore->{'cero'},
                'uno' => $semaphore->{'uno'},
                'dos' => $semaphore->{'dos'},
                'tres' => $semaphore->{'tres'},
                'puesto' => $semaphore->{'Puesto de votación'}, 
                'ciudad' => $semaphore->{'Ciudad'},
                'latitud' => $semaphore->{'Latitud'},
                'longitud' => $semaphore->{'Longitud'}
            ];
            array_push ($puntos, $punto);
        }
        return($puntos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $semaphores = DB::table('view_semaphores')->where('Intención de voto', $id)->get();
        dd($semaphores);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
