<?php

namespace App\Http\Middleware;

use Closure, Auth;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ...$rol)
    {
        
        if(!in_array(Auth::user()->type->name, $rol)){
            return redirect()->to('/');
        }

        return $next($request);
    }
}
