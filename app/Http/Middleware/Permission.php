<?php

namespace App\Http\Middleware;

use Closure, Auth;

class Permission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $permission)
    {        
        if( Auth::user()->type->id != $permission)
        {
            return redirect()->to('/');
        } 
        
        return $next($request);
    }
}
