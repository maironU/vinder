<?php

/* COMANDOS DE CONSOLA
sincronizacion de Canales => php artisan sync:abako distribuidor canales
sincronizacion de Ciudades => php artisan sync:abako distribuidor ciudades
sincronizacion de Vendedores => php artisan sync:abako distribuidor vendedores
sincronizacion de Listas de Precios => php artisan sync:abako distribuidor listasprecios
sincronizacion de Tiendas => php artisan sync:abako distribuidor tiendas
sincronizacion de Rutas de vendedores a tiendas => php artisan sync:abako distribuidor ruteros
sincronizacion de Productos => php artisan sync:abako distribuidor productos
sincronizacion de Productos inactivos => php artisan sync:abako distribuidor productosinactivos
sincronizacion de Productos sin inventario => php artisan sync:abako distribuidor productossininventario
sincronizacion de Precios y Ofertas => php artisan sync:abako distribuidor preciosnew
envio de Pedidos  => php artisan sync:abako distribuidor pedidosconsolidadostiendasvisitadashoy
*/

namespace App\Http\Services;

use App\Entities\PollingStation;
use App\Entities\User;
use App\Entities\VoterNew;
use App\Helpers\Helper as HelperService;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;

ini_set('upload_max_filesize', '50M');
ini_set("memory_limit", "1000M");
set_time_limit(0);


class TestigoElectoralCedula extends Helper{
    private $name;

    public function __construct($name)
    {
        $this->name = $name;
    }

    public function sync(){
        try{
            $this->imprimirLog("Inicia", "syncTestigoElectoralCedula");
            if(file_exists(storage_path('app/'.$this->name))){
                $file = storage_path('app/'.$this->name);
                Excel::load($file, function($reader){
                    $reader->each(function($row){
                        try{
                            if($row->cedula){
                                $cedula = $row->cedula;
                                $find_testigo = VoterNew::select('*', DB::raw("CONCAT(first_name, ' ', second_name, ' ', surname, ' ', second_surname) as full_name"))
                                    ->where('nuip', $row->cedula)->first();
                                $testigoElectoral = User::where('username', $cedula)->first();

                                if($testigoElectoral){
                                    if($find_testigo){
                                        $objTestigoElectoral = $this->updateObjElectoral($find_testigo, $testigoElectoral, $row);
                                        $objTestigoElectoral->save();
                                    }else{
                                        $objTestigoElectoral = $this->createObjElectoral($row, $testigoElectoral);
                                        $objTestigoElectoral->save();
                                    }
                                }else{
                                    if($find_testigo){
                                        $testigoElectoralNew = new User();
                                        $objTestigoElectoral = $this->updateObjElectoral($find_testigo, $testigoElectoralNew, $row);
                                        $objTestigoElectoral->save();
                                    }else{
                                        $testigoElectoralNew = new User();
                                        $objTestigoElectoral = $this->createObjElectoral($row, $testigoElectoralNew);
                                        $objTestigoElectoral->save();
                                    }
                                }
                            }
                        }catch(\Exception $e){
                            Log::info(["error_interno_orientador" => true, "message" => $e->getMessage(), "line" => $e->getLine()]);
                        }
                    }); 
                });
                //unlink(storage_path('app/'.$this->name));
                $this->imprimirLog("Termina", "syncTestigoElectoralCedula");
            }else{
                Log::error("archivo no existe");
                $this->imprimirLog("Termina", "syncTestigoElectoralCedula");
            }
        }catch(\Exception $e){
            $this->imprimirLog("Termina", "syncTestigoElectoralCedula");
            Log::error(["error_general" => true, "import" => "testigo electoral cedula", "message" => $e->getMessage(), "line" => $e->getLine(), "file" => $e->getFile()]);
        }
    }

    public function createObjElectoral($row, $testigoElectoral){
        $testigoElectoral->username = strval($row->cedula);
        $testigoElectoral->phone = $row->celular ?? null;
        $testigoElectoral->type_id = 5;

        return $testigoElectoral;
    }

    public function updateObjElectoral($find_testigo, $testigoElectoral, $row){
        try{
            if(!$testigoElectoral->username){
                $testigoElectoral->username = $find_testigo->nuip;
            }

            if(!$testigoElectoral->password){
                $testigoElectoral->password = $find_testigo->nuip;
            }

            if(!$testigoElectoral->name){
                $testigoElectoral->name = $find_testigo->full_name;
            }else{
                if($find_testigo->full_name){
                    $testigoElectoral->name = $find_testigo->full_name;
                }
            }
    
            if(!$testigoElectoral->email){
                $testigoElectoral->email = HelperService::generateEmail($find_testigo->first_name, $find_testigo->surname);
            }
    
            if(!$testigoElectoral->polling_station_id){
                $testigoElectoral->polling_station_id = HelperService::getPollingStationByVoterNew($find_testigo->polling_station);
            }else{
                $polling_id = HelperService::getPollingStationByVoterNew($find_testigo->polling_station);
                if($polling_id){
                    $testigoElectoral->polling_station_id = $polling_id;
                }
            }
    
            if($testigoElectoral->polling_station_id){
                if(!$testigoElectoral->num_table){
                    if(HelperService::isAvailableTable($testigoElectoral->polling_station_id, $find_testigo->table)){
                        $testigoElectoral->num_table = $find_testigo->table;
                    }
                }else{
                    if($find_testigo->table){
                        if(HelperService::isAvailableTable($testigoElectoral->polling_station_id, $find_testigo->table)){
                            $testigoElectoral->num_table = $find_testigo->table;
                        }
                    }
                }
            }
    
            if(!$testigoElectoral->phone){
                $testigoElectoral->phone = $row->celular;
            }else{
                if($row->celular){
                    $testigoElectoral->phone = $row->celular;
                }
            }
            $testigoElectoral->type_id = 5;

            return $testigoElectoral;
        }catch(\Exception $e){
            Log::error(["Error" => true, "import" => "testigo electoral cedula", "doc" => $row->cedula, "message" => $e->getMessage(), "line" => $e->getLine()]);
        }
        
    }
}