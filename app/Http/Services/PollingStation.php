<?php

/* COMANDOS DE CONSOLA
sincronizacion de Canales => php artisan sync:abako distribuidor canales
sincronizacion de Ciudades => php artisan sync:abako distribuidor ciudades
sincronizacion de Vendedores => php artisan sync:abako distribuidor vendedores
sincronizacion de Listas de Precios => php artisan sync:abako distribuidor listasprecios
sincronizacion de Tiendas => php artisan sync:abako distribuidor tiendas
sincronizacion de Rutas de vendedores a tiendas => php artisan sync:abako distribuidor ruteros
sincronizacion de Productos => php artisan sync:abako distribuidor productos
sincronizacion de Productos inactivos => php artisan sync:abako distribuidor productosinactivos
sincronizacion de Productos sin inventario => php artisan sync:abako distribuidor productossininventario
sincronizacion de Precios y Ofertas => php artisan sync:abako distribuidor preciosnew
envio de Pedidos  => php artisan sync:abako distribuidor pedidosconsolidadostiendasvisitadashoy
*/

namespace App\Http\Services;

use App\Entities\Location;
use App\Entities\LocationType;
use App\Entities\PollingStation as Polling;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;

ini_set('upload_max_filesize', '50M');
ini_set("memory_limit", "1000M");
set_time_limit(0);


class PollingStation extends Helper{
    private $name;

    public function __construct($name)
    {
        $this->name = $name;
    }

    public function sync(){
        try{            
            $this->imprimirLog("Inicia", "syncPuestosDeVotacion");
            if(file_exists(storage_path('app/'.$this->name))){
                $file = storage_path('app/'.$this->name);
                Excel::load($file, function($reader){
                    $reader->each(function($row){
                        try{
                            if($row->puesto){
                                $nombre = $row->puesto;
                                $polling_station = Polling::where('name', $nombre)->first();
    
                                if(!$polling_station){
                                    $polling_station = new Polling();
                                }
    
                                $objPolling = $this->getObjPolling($row, $polling_station);
                                $objPolling->save();
                            }
                        }catch(\Exception $e){
                            Log::info(["error_interno_voter" => true, "message" => $e->getMessage(), "line" => $e->getLine()]);
                        }
                    }); 
                });
                unlink(storage_path('app/'.$this->name));
                $this->imprimirLog("Termina", "syncPuestosDeVotacion");
            }else{
                Log::error("archivo no existe");
                $this->imprimirLog("Termina", "syncPuestosDeVotacion");
            }
        }catch(\Exception $e){
            $this->imprimirLog("Termina", "syncPuestosDeVotacion");
            Log::error(["error_general" => true, "import" => "polling_stations", "message" => $e->getMessage(), "line" => $e->getLine()]);
        }
    }

    public function getObjPolling($row, $polling){
        try{
            $polling->name = $row->puesto ? $row->puesto : '';
            $polling->address = $row->direccion ? $row->direccion : '';
            $polling->tables = $row->mesas ? $row->mesas : 1;
            $polling->electoral_potential = $row->potencial ? $row->potencial : 0;
            $polling->longitude = $row->longitud ? $row->longitud : 0;
            $polling->latitude = $row->latitud ? $row->latitud : 0;
            $polling->zone = "";
            $polling->place = "";
            $polling->city = $row->ciudad ? $row->ciudad : "";
            $polling->department = $row->departamento ? $row->departamento : "";

            $location = null;
            $types = LocationType::all()->pluck('id', 'name');
            if($row->nombre_ubicacion){
                $type_id = null;
                if($row->tipo && isset($types[trim($row->tipo)])){
                    $type_id = $types[trim($row->tipo)];
                }
                $location = Location::where('name', $row->nombre_ubicacion);

                if($type_id){
                    $location->where('type_id', $type_id);
                }
            }

            if($location && $location->first()){
                $polling->location_id = $location->first()->id;
            }

            $location_registraduria = null;
            if($row->sede){
                $location_registraduria = Location::where('name', $row->sede);
            }

            if($location_registraduria && $location_registraduria->first()){
                $polling->registraduria_location_id = $location_registraduria->first()->id;
            }
    
            return $polling;
        }catch(\Exception $e){
            Log::error(["Error" => true, "import" => "voter", "doc" => $row->cedula, "message" => $e->getMessage(), "line" => $e->getLine()]);
        }
    }
}