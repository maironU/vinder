<?php

namespace App\Http\Services;

class Helper {
    public function imprimirLog($tipo, $proceso)
    {
        echo "Fecha: " . date("d-m-Y H:i:s") . " - " . $tipo . " proceso " . $proceso. chr(10) . chr(13);
        return;
    }
}