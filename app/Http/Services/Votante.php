<?php

/* COMANDOS DE CONSOLA
sincronizacion de Canales => php artisan sync:abako distribuidor canales
sincronizacion de Ciudades => php artisan sync:abako distribuidor ciudades
sincronizacion de Vendedores => php artisan sync:abako distribuidor vendedores
sincronizacion de Listas de Precios => php artisan sync:abako distribuidor listasprecios
sincronizacion de Tiendas => php artisan sync:abako distribuidor tiendas
sincronizacion de Rutas de vendedores a tiendas => php artisan sync:abako distribuidor ruteros
sincronizacion de Productos => php artisan sync:abako distribuidor productos
sincronizacion de Productos inactivos => php artisan sync:abako distribuidor productosinactivos
sincronizacion de Productos sin inventario => php artisan sync:abako distribuidor productossininventario
sincronizacion de Precios y Ofertas => php artisan sync:abako distribuidor preciosnew
envio de Pedidos  => php artisan sync:abako distribuidor pedidosconsolidadostiendasvisitadashoy
*/

namespace App\Http\Services;

use App\Entities\Community;
use App\Entities\Location;
use App\Entities\LocationType;
use App\Entities\Occupation;
use App\Entities\PollingStation;
use App\Entities\User;
use App\Entities\Voter;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;

ini_set('upload_max_filesize', '50M');
ini_set("memory_limit", "1000M");
set_time_limit(0);


class Votante extends Helper{
    private $name;

    public function __construct($name)
    {
        $this->name = $name;
    }

    public function sync(){
        try{            
            $this->imprimirLog("Inicia", "syncVotantes");
            if(file_exists(storage_path('app/'.$this->name))){
                $file = storage_path('app/'.$this->name);
                Excel::load($file, function($reader){
                    $reader->each(function($row){
                        try{
                            if($row->cedula){
                                $cedula = $row->cedula;
                                $voter = Voter::where('doc', $cedula)->first();
    
                                if(!$voter){
                                    $voter = new Voter();
                                }
    
                                $objVoter = $this->getObjVoter($row, $voter);
                                $objVoter->save();

                                if($row->comunidad){
                                    $split = explode(",", $row->comunidad);
                                    $comunities_agregar = [];
                                    foreach($split as $comunity){
                                        $find_comunity = Community::where('name', $comunity)->first();
                                        
                                        if(!$find_comunity){
                                            $comunity_create = Community::create(["name" => $comunity]);
                                            array_push($comunities_agregar, $comunity_create->id);
                                        }else{
                                            array_push($comunities_agregar, $find_comunity->id);
                                        }
                                    }

                                    if(count($comunities_agregar) > 0){
                                        $objVoter->communities()->sync($comunities_agregar);
                                    }
                                }
                            }
                        }catch(\Exception $e){
                            Log::info(["error_interno_voter" => true, "message" => $e->getMessage(), "line" => $e->getLine()]);
                        }
                    }); 
                });
                unlink(storage_path('app/'.$this->name));
                $this->imprimirLog("Termina", "syncVotantes");
            }else{
                Log::error("archivo no existe");
                $this->imprimirLog("Termina", "syncVotantes");
            }
        }catch(\Exception $e){
            $this->imprimirLog("Termina", "syncVotantes");
            Log::error(["error_general" => true, "import" => "voter", "message" => $e->getMessage(), "line" => $e->getLine()]);
        }
    }

    public function getObjVoter($row, $voter){
        try{
            $voter->name = $row->nombre_completo ? $row->nombre_completo : '';
            $voter->doc = $row->cedula;
            $voter->house_check = $row->casa_marcada == "si" ? 1 : 0;

            if($row->orientador){
                $orientador = User::where('type_id', 6)
                            ->where('username', $row->orientador)
                            ->first();
    
                if($orientador){
                    $voter->is_lider = 1;
                    $voter->leader_id = $orientador->id;
                }else{
                   $voter->is_lider = 0; 
                   $voter->leader_id = null; 
                }
            }
    
            if($row->coordinador){
                $coordinador = Voter::where('colaborator', 1)
                                    ->where('doc', $row->coordinador)
                                    ->first();
    
                if($coordinador){
                    $voter->superior = $coordinador->id;
                }
            }
            $sex = 'M';
    
            if($row->genero && $row->genero[0] == 'F'){
                $sex = 'F';
            }

            $location = null;
            $types = LocationType::all()->pluck('id', 'name');
            if($row->nombre_ubicacion){
                $type_id = null;
                if($row->tipo && isset($types[trim($row->tipo)])){
                    $type_id = $types[trim($row->tipo)];
                }
                $location = Location::where('name', $row->nombre_ubicacion);

                if($type_id){
                    $location->where('type_id', $type_id);
                }
            }

            if($location && $location->first()){
                $voter->location_id = $location->first()->id;
            }
    
            $voter->sex = $sex;
            if($row->nombre_puesto){
                $polling_station = PollingStation::where('name', trim($row->nombre_puesto))->first();
                if($polling_station){
                    $voter->polling_station_id = $polling_station->id;
                }
            }
            if($row->mesa){
                $voter->table_number = $row->mesa;
            }
            $voter->address = $row->direccion ? $row->direccion : "";
            $voter->telephone = $row->telefono1 ? $row->telefono1 : "";
            $voter->facebook_profile = $row->facebook ? $row->facebook : "";
            $voter->instagram_profile = $row->instagram ? $row->instagram : "";
            $voter->email = $row->correo ? $row->correo : "";
            $voter->date_of_birth = $row->fechanacimiento ? $row->fechanacimiento->format('Y-m-d') : "";
    
            if($row->ocupacion){
                $occupation = Occupation::where('name', 'like', '%'.$row->ocupacion.'%')
                            ->first();
    
                if($occupation){
                    $voter->occupation = $occupation->id;
                }else{
                    $occupation = Occupation::create(["name" => $row->ocupacion]);
                    $voter->occupation = $occupation->id;
                }
            }
    
            if($row->voto){
                $semaphore = array(
                    'por definir' => 0,
                    'fijo' => 1,
                    'indeciso' => 2,
                    'duro' => 3
                );
                if(isset($semaphore[strtolower($row->voto)])){
                    $voter->semaphore = $semaphore[strtolower($row->voto)];
                }else{
                    $voter->semaphore = 0;
                }
            }else{
                $voter->semaphore = 0;
            }

            $voter->description = $row->nota ? $row->nota : "";

            return $voter;
        }catch(\Exception $e){
            Log::error(["Error" => true, "import" => "voter", "doc" => $row->cedula, "message" => $e->getMessage(), "line" => $e->getLine()]);
        }
    }
}