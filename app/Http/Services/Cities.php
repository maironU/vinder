<?php

namespace App\Http\Services;

use App\Entities\City;
use App\Entities\Department;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;

ini_set('upload_max_filesize', '50M');
ini_set("memory_limit", "1000M");
set_time_limit(0);


class Cities extends Helper{
    private $name;

    public function __construct($name)
    {
        $this->name = $name;
    }

    public function sync(){
        try{
            $this->imprimirLog("Inicia", "syncCities");
            if(file_exists(storage_path('app/'.$this->name))){
                $file = storage_path('app/'.$this->name);
                Excel::load($file, function($reader){
                    $reader->each(function($row){
                        try{
                            $city_name = $row->municipio;

                            $city =  City::where('name', $city_name)
                            ->first();

                            if(!$city){
                                $city = new City();
                            }

                            $objCity = $this->getObjCity($row, $city);
                            Log::info(["objCity" => $objCity]);

                            $objCity->save();
                        }catch(\Exception $e){
                            Log::info(["error_interno_cities" => true, "message" => $e->getMessage(), "line" => $e->getLine()]);
                        }
                    }); 
                });
                unlink(storage_path('app/'.$this->name));
                $this->imprimirLog("Termina", "syncCities");
            }else{
                Log::error("archivo no existe");
                $this->imprimirLog("Termina", "syncCities");
            }
        }catch(\Exception $e){
            $this->imprimirLog("Termina", "syncCities");
            Log::error(["error_general" => true, "import" => "cities", "message" => $e->getMessage(), "line" => $e->getLine()]);
        }
    }

    public function getObjCity($row, $city){
        try{
            if($row->departamento){
                $name_department = $this->quitar_acentos($row->departamento);
                $department = Department::where('name', $name_department)->first();
                if(!$department){
                    $department = Department::create(["name" => $name_department]);
                }

                $city->name = $this->quitar_acentos($row->municipio);
                $city->longitude = $row->longitud;
                $city->latitude = $row->latitud;
                $city->department_id = $department->id;
                $city->status = isset($row->status) ? $row->status : 1;
            }
            
            return $city;
        }catch(\Exception $e){
            Log::error(["Error" => true, "import" => "cities", "city" => $row->municipio, "message" => $e->getMessage(), "line" => $e->getLine()]);
        }
    }

    function quitar_acentos($cadena){
        $originales = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿ';
        $modificadas = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyyby';
        $cadena = utf8_decode($cadena);
        $cadena = strtr($cadena, utf8_decode($originales), $modificadas);
        return utf8_encode($cadena);
    }
}