<?php

/* COMANDOS DE CONSOLA
sincronizacion de Canales => php artisan sync:abako distribuidor canales
sincronizacion de Ciudades => php artisan sync:abako distribuidor ciudades
sincronizacion de Vendedores => php artisan sync:abako distribuidor vendedores
sincronizacion de Listas de Precios => php artisan sync:abako distribuidor listasprecios
sincronizacion de Tiendas => php artisan sync:abako distribuidor tiendas
sincronizacion de Rutas de vendedores a tiendas => php artisan sync:abako distribuidor ruteros
sincronizacion de Productos => php artisan sync:abako distribuidor productos
sincronizacion de Productos inactivos => php artisan sync:abako distribuidor productosinactivos
sincronizacion de Productos sin inventario => php artisan sync:abako distribuidor productossininventario
sincronizacion de Precios y Ofertas => php artisan sync:abako distribuidor preciosnew
envio de Pedidos  => php artisan sync:abako distribuidor pedidosconsolidadostiendasvisitadashoy
*/

namespace App\Http\Services;

use App\Entities\Location;
use App\Entities\LocationType;
use App\Entities\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;

ini_set('upload_max_filesize', '50M');
ini_set("memory_limit", "1000M");
set_time_limit(0);


class Orientador extends Helper{
    private $name;

    public function __construct($name)
    {
        $this->name = $name;
    }

    public function sync(){
        try{
            $this->imprimirLog("Inicia", "syncOrientador");
            if(file_exists(storage_path('app/'.$this->name))){
                $file = storage_path('app/'.$this->name);
                Excel::load($file, function($reader){
                    $reader->each(function($row){
                        try{
                            if($row->usuario){
                                $usuario = $row->usuario;
                                $orientador = User::where('username', $usuario)->first();

                                if(!$orientador){
                                    $orientador = User::where('email', $row->email)->first();
                                }

                                if(!$orientador){
                                    $orientador = new User();
                                }
    
                                $objOrientador = $this->getObjOrientator($row, $orientador);
                                $objOrientador->save();
                            }
                        }catch(\Exception $e){
                            Log::info(["error_interno_orientador" => true, "message" => $e->getMessage(), "line" => $e->getLine()]);
                        }
                    }); 
                });
                unlink(storage_path('app/'.$this->name));
                $this->imprimirLog("Termina", "syncOrientador");
            }else{
                Log::error("archivo no existe");
                $this->imprimirLog("Termina", "syncOrientador");
            }
        }catch(\Exception $e){
            $this->imprimirLog("Termina", "syncOrientador");
            Log::error(["error_general" => true, "import" => "orientador", "message" => $e->getMessage(), "line" => $e->getLine()]);
        }
    }

    public function getObjOrientator($row, $orientador){
        try{
            $orientador->name = $row->nombre ? $row->nombre : '';
            $orientador->username = $row->usuario;
            $orientador->address = $row->direccion ? $row->direccion : "";
            $orientador->email = $row->email ? $row->email : "";
            $orientador->password = $row->contrasena ? Hash::make($row->contrasena) : Hash::make($row->cedula);
            $orientador->house_check = $row->casa_marcada == "si" ? 1 : 0;
            $location = null;
            $types = LocationType::all()->pluck('id', 'name');
            if($row->nombre_ubicacion){
                $type_id = null;
                if($row->tipo && isset($types[trim($row->tipo)])){
                    $type_id = $types[trim($row->tipo)];
                }
                $location = Location::where('name', $row->nombre_ubicacion);

                if($type_id){
                    $location->where('type_id', $type_id);
                }
            }

            if($location && $location->first()){
                $orientador->location_id = $location->first()->id;
            }
    
            $orientador->type_id = 6;

            return $orientador;
        }catch(\Exception $e){
            Log::error(["Error" => true, "import" => "orientador", "doc" => $row->cedula, "message" => $e->getMessage(), "line" => $e->getLine()]);
        }
    }
}