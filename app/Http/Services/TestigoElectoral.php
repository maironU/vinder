<?php

/* COMANDOS DE CONSOLA
sincronizacion de Canales => php artisan sync:abako distribuidor canales
sincronizacion de Ciudades => php artisan sync:abako distribuidor ciudades
sincronizacion de Vendedores => php artisan sync:abako distribuidor vendedores
sincronizacion de Listas de Precios => php artisan sync:abako distribuidor listasprecios
sincronizacion de Tiendas => php artisan sync:abako distribuidor tiendas
sincronizacion de Rutas de vendedores a tiendas => php artisan sync:abako distribuidor ruteros
sincronizacion de Productos => php artisan sync:abako distribuidor productos
sincronizacion de Productos inactivos => php artisan sync:abako distribuidor productosinactivos
sincronizacion de Productos sin inventario => php artisan sync:abako distribuidor productossininventario
sincronizacion de Precios y Ofertas => php artisan sync:abako distribuidor preciosnew
envio de Pedidos  => php artisan sync:abako distribuidor pedidosconsolidadostiendasvisitadashoy
*/

namespace App\Http\Services;

use App\Entities\PollingStation;
use App\Entities\TableNumberUser;
use App\Entities\User;
use Exception;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;

ini_set('upload_max_filesize', '50M');
ini_set("memory_limit", "1000M");
set_time_limit(0);


class TestigoElectoral extends Helper{
    private $name;

    public function __construct($name)
    {
        $this->name = $name;
    }

    public function sync(){
        try{
            $this->imprimirLog("Inicia", "syncTestigoElectoral");
            if(file_exists(storage_path('app/'.$this->name))){
                $file = storage_path('app/'.$this->name);
                Excel::load($file, function($reader){
                    $reader->each(function($row){
                        try{
                            if($row->cedula){
                                $cedula = $row->cedula;
                                $testigoElectoral = User::where('username', $cedula)->first();
    
                                if(!$testigoElectoral){
                                    $testigoElectoral = new User();
                                }
    
                                $objTestigoElectoral = $this->getObjTestigoElectoral($row, $testigoElectoral);
                                if($objTestigoElectoral){
                                    $objTestigoElectoral->save();
                                }

                                if($objTestigoElectoral && $objTestigoElectoral->polling_station_id){
                                    $objTestigoElectoral->tables_numbers()->delete();
                                    if($row->mesa == "all"){
                                        $polling_station = PollingStation::where('id', $objTestigoElectoral->polling_station_id)->first();
                                        if($polling_station){
                                            $tables = $polling_station->tables;
                                            
                                            for($mesa=1; $mesa<=$tables; $mesa++){
                                                $testigo = User::where('polling_station_id', $objTestigoElectoral->polling_station_id)
                                                    ->where(function($query) use($mesa){
                                                        $query->whereHas('tables_numbers', function($subquery) use($mesa){
                                                            $subquery->where('table_number', $mesa)
                                                            ->orWhere('table_number', 'Mesa '.$mesa);
                                                        });
                                                    })->first();

                                                if(!$testigo){
                                                    TableNumberUser::create(['user_id' => $objTestigoElectoral->id, 'table_number' => $mesa]);
                                                }else{
                                                    Log::error('Ya existe un testigo para ese puesto y esa mesa');
                                                }
                                            }
                                        }
                                    }else{
                                        $mesas = explode(".", $row->mesa);
                                        if(count($mesas) > 0){
                                            foreach($mesas as $mesa){
                                                $testigo = User::where('polling_station_id', $objTestigoElectoral->polling_station_id)
                                                    ->where(function($query) use($mesa){
                                                        $query->whereHas('tables_numbers', function($subquery) use($mesa){
                                                            $subquery->where('table_number', $mesa)
                                                            ->orWhere('table_number', 'Mesa '.$mesa);
                                                        });
                                                    })->first();

                                                if(!$testigo){
                                                    $num_table = trim(preg_replace('/Mesa/m',"", $mesa));
                                                    TableNumberUser::create(['user_id' => $objTestigoElectoral->id, 'table_number' => $num_table]);
                                                }else{
                                                    Log::error('Ya existe un testigo para ese puesto y esa mesa');
                                                }
                                            }
                                        }else{
                                            $mesas = explode(",", $row->mesa);
                                            if(count($mesas) > 0){
                                                foreach($mesas as $mesa){
                                                    $testigo = User::where('polling_station_id', $objTestigoElectoral->polling_station_id)
                                                        ->where(function($query) use($mesa){
                                                            $query->whereHas('tables_numbers', function($subquery) use($mesa){
                                                                $subquery->where('table_number', $mesa)
                                                                ->orWhere('table_number', 'Mesa '.$mesa);
                                                            });
                                                        })->first();
        
                                                    if(!$testigo){
                                                        $num_table = trim(preg_replace('/Mesa/m',"", $mesa));
                                                        TableNumberUser::create(['user_id' => $objTestigoElectoral->id, 'table_number' => $num_table]);
                                                    }else{
                                                        Log::error('Ya existe un testigo para ese puesto y esa mesa');
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    
                                }     
                            }
                        }catch(\Exception $e){
                            Log::info(["error_interno_orientador" => true, "message" => $e->getMessage(), "line" => $e->getLine()]);
                        }
                    }); 
                });
                unlink(storage_path('app/'.$this->name));
                $this->imprimirLog("Termina", "syncTestigoElectoral");
            }else{
                Log::error("archivo no existe");
                $this->imprimirLog("Termina", "syncTestigoElectoral");
            }
        }catch(\Exception $e){
            $this->imprimirLog("Termina", "syncTestigoElectoral");
            Log::error(["error_general" => true, "import" => "testigo electoral", "message" => $e->getMessage(), "line" => $e->getLine(), "file" => $e->getFile()]);
        }
    }

    public function getObjTestigoElectoral($row, $testigoElectoral){
        try{
            $testigoElectoral->name = $row->nombre ? $row->nombre : '';
            $testigoElectoral->username = strval($row->cedula);
            $testigoElectoral->phone = $row->telefono ? $row->telefono : "";
            $testigoElectoral->email = $row->email ? $row->email : "";
            $testigoElectoral->password = $row->contrasena ? strval($row->contrasena) : strval($row->cedula);
            //$testigoElectoral->num_table = $row->mesa;

            if($row->nombre_puesto){
                $polling_station = PollingStation::where('name', trim($row->nombre_puesto))->first();
                if($polling_station){
                    $testigoElectoral->polling_station_id = $polling_station->id;        
                }
            }

            $testigoElectoral->type_id = 5;

            return $testigoElectoral;
        }catch(\Exception $e){
            Log::error(["Error" => true, "import" => "orientador", "doc" => $row->cedula, "message" => $e->getMessage(), "line" => $e->getLine()]);
        }
    }
}