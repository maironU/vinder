<?php

/* COMANDOS DE CONSOLA
sincronizacion de Canales => php artisan sync:abako distribuidor canales
sincronizacion de Ciudades => php artisan sync:abako distribuidor ciudades
sincronizacion de Vendedores => php artisan sync:abako distribuidor vendedores
sincronizacion de Listas de Precios => php artisan sync:abako distribuidor listasprecios
sincronizacion de Tiendas => php artisan sync:abako distribuidor tiendas
sincronizacion de Rutas de vendedores a tiendas => php artisan sync:abako distribuidor ruteros
sincronizacion de Productos => php artisan sync:abako distribuidor productos
sincronizacion de Productos inactivos => php artisan sync:abako distribuidor productosinactivos
sincronizacion de Productos sin inventario => php artisan sync:abako distribuidor productossininventario
sincronizacion de Precios y Ofertas => php artisan sync:abako distribuidor preciosnew
envio de Pedidos  => php artisan sync:abako distribuidor pedidosconsolidadostiendasvisitadashoy
*/

namespace App\Http\Services;

use App\Entities\Community;
use App\Entities\Location;
use App\Entities\LocationType;
use App\Entities\Occupation;
use App\Entities\PollingStation;
use App\Entities\Rol;
use App\Entities\Voter;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;

ini_set('upload_max_filesize', '50M');
ini_set("memory_limit", "1000M");
set_time_limit(0);


class Coordinador extends Helper{
    private $name;

    public function __construct($name)
    {
        $this->name = $name;
    }

    public function sync(){
        try{
            $this->imprimirLog("Inicia", "syncCoordinador");
            if(file_exists(storage_path('app/'.$this->name))){
                $file = storage_path('app/'.$this->name);
                Excel::load($file, function($reader){
                    $reader->each(function($row){
                        try{
                            if($row->cedula){
                                $cedula = $row->cedula;

                                $coordinador =  Voter::where('colaborator', 1)
                                ->where('doc', $cedula)
                                ->first();
    
                                if(!$coordinador){
                                    $coordinador = new Voter();
                                }
    
                                $objCoordinador = $this->getObjCoordinador($row, $coordinador);
                                $objCoordinador->save();

                                if($row->cargos){
                                    $cargos = explode(",", $row->cargos);
                                    $cargos_agregar = [];
                                    foreach($cargos as $cargo){
                                        $find_cargo = Rol::where('name', $cargo)->first();
                                        
                                        if(!$find_cargo){
                                            $cargo = Rol::create(["name" => $cargo]);
                                            array_push($cargos_agregar, $cargo->id);
                                        }else{
                                            array_push($cargos_agregar, $find_cargo->id);
                                        }
                                    }
                                    $objCoordinador->roles()->sync($cargos_agregar);
                                }

                                if($row->comunidad){
                                    $split = explode(",", $row->comunidad);
                                    $comunities_agregar = [];
                                    foreach($split as $comunity){
                                        $find_comunity = Community::where('name', $comunity)->first();
                                        
                                        if(!$find_comunity){
                                            $comunity_create = Community::create(["name" => $comunity]);
                                            array_push($comunities_agregar, $comunity_create->id);
                                        }else{
                                            array_push($comunities_agregar, $find_comunity->id);
                                        }
                                    }
                                    $objCoordinador->communities()->sync($comunities_agregar);
                                }
                            }
                        }catch(\Exception $e){
                            Log::info(["error_interno_coordinador" => true, "message" => $e->getMessage(), "line" => $e->getLine()]);
                        }
                    }); 
                });
                unlink(storage_path('app/'.$this->name));
                $this->imprimirLog("Termina", "syncCoordinador");
            }else{
                Log::error("archivo no existe");
                $this->imprimirLog("Termina", "syncCoordinador");
            }
        }catch(\Exception $e){
            $this->imprimirLog("Termina", "syncCoordinador");
            Log::error(["error_general" => true, "import" => "voter", "message" => $e->getMessage(), "line" => $e->getLine()]);
        }
    }

    public function getObjCoordinador($row, $coordinador){
        try{
            $coordinador->doc = $row->cedula;
            $coordinador->name = $row->nombre ? $row->nombre : '';
            if($row->delegado && $row->delegado == "si"){
                $coordinador->delegate = 1;
            }else{
                $coordinador->delegate = 0;
            }

            $coordinador->house_check = $row->casa_marcada == "si" ? 1 : 0;

            $sex = 'M';
    
            if($row->genero && $row->genero[0] == 'F'){
                $sex = 'F';
            }
    
            $coordinador->sex = $sex;
            if($row->nombre_puesto){
                $polling_station = PollingStation::where('name', trim($row->nombre_puesto))->first();
                if($polling_station){
                    $coordinador->polling_station_id = $polling_station->id;
                }
            }

            if($row->mesa){
                $coordinador->table_number = $row->mesa;
            }
            $location = null;
            $types = LocationType::all()->pluck('id', 'name');
            if($row->nombre_ubicacion){
                $type_id = null;
                if($row->tipo && isset($types[trim($row->tipo)])){
                    $type_id = $types[trim($row->tipo)];
                }
                $location = Location::where('name', $row->nombre_ubicacion);

                if($type_id){
                    $location->where('type_id', $type_id);
                }
            }

            if($location && $location->first()){
                $coordinador->location_id = $location->first()->id;
            }

            $coordinador->address = $row->direccion ? $row->direccion : "";
            $coordinador->telephone = $row->telefono1 ? $row->telefono1 : "";
            $coordinador->facebook_profile = $row->facebook ? $row->facebook : "";
            $coordinador->instagram_profile = $row->instagram ? $row->instagram : "";
            $coordinador->email = $row->email ? $row->email : "";
            $coordinador->date_of_birth = $row->fecha_nacimiento ? $row->fecha_nacimiento->format('Y-m-d') : "";
            if($row->ocupacion){
                $occupation = Occupation::where('name', 'like', '%'.$row->ocupacion.'%')
                            ->first();
    
                if($occupation){
                    $coordinador->occupation = $occupation->id;
                }else{
                    $occupation = Occupation::create(["name" => $row->ocupacion]);
                    $coordinador->occupation = $occupation->id;
                }
            }
            
            if($row->voto){
                $semaphore = array(
                    'por definir' => 0,
                    'fijo' => 1,
                    'indeciso' => 2,
                    'duro' => 3
                );
                if(isset($semaphore[strtolower($row->voto)])){
                    $coordinador->semaphore = $semaphore[strtolower($row->voto)];
                }else{
                    $coordinador->semaphore = 0;
                }
            }else{
                $coordinador->semaphore = 0;
            }
            $coordinador->description = $row->nota ? $row->nota : "";
            $coordinador->colaborator = 1;    
            $coordinador->is_coordinator = 1;    

            return $coordinador;
        }catch(\Exception $e){
            Log::error(["Error" => true, "import" => "voter", "doc" => $row->cedula, "message" => $e->getMessage(), "line" => $e->getLine()]);
        }
    }
}