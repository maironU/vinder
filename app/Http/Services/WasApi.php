<?php
namespace App\Http\Services;
use GuzzleHttp;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use phpDocumentor\Reflection\DocBlock\Tags\Throws;

class WasApi extends Helper{
    public $api_key;
    public $client;
    public $url_endpoint;
    public $config;

    public function __construct($config_title = 'bienvenida'){
        $this->api_key = env('WASAPI_KEY');
        $this->url_endpoint = 'https://api.wasapi.io/prod/api/v1';
        $this->client = new GuzzleHttp\Client();
        $this->config = DB::table('config_wasapi')->where('title', $config_title)->first();
    }

    public function sendMessageWithTemplate($phone_number, $variables = []){
        try{
            if($this->config){
                $data = array(
                    "recipients" => "+57 $phone_number",
                    "template_id" => $this->config->plantilla_id,
                    "contact_type" => "phone",
                    "from_id" => $this->config->from_id,
                );
                if($this->config->has_variable && $this->config->type_variable == "header"){
                    foreach($variables as $key => $variable){
                        $key = $key + 1;
                        $data['header_var'][] = array(
                            "text" => "{{{$key}}}",
                            "val" => $variable
                        );
                    }
                }else if($this->config->has_variable && $this->config->type_variable == "body"){
                    foreach($variables as $key => $variable){
                        $key = $key + 1;
                        $data['body_vars'][] = array(
                            "text" => "{{{$key}}}",
                            "val" => $variable
                        );
                    }
                }
                $this->post('whatsapp-messages/send-template', $data);
                return true;
            }else{
                Log::error(["error_message" => true, "message" => 'La configuracion de wasapi esta vacía']);
            }
        }catch(\Exception $e){
            Log::error(["error_message" => true, "message" => $e->getMessage(), "sendMessageWithTemplate" => "Error al enviar mensaje al numero $phone_number"]);
        }
    }

    public function getTemplates($template_uuid = null){
        if($template_uuid){
            return $this->get("whatsapp-templates/$template_uuid");
        }else{
            return $this->get("whatsapp-templates");
        }
    }

    public function getPhones(){
        return $this->get("whatsapp-numbers");
    }

    public function get($type){
        try{
            $response = $this->client->get("$this->url_endpoint/$type", [
                'headers' => $this->getHeaders()
            ]);
            Log::error(["peticion" => "get", "response" => $response->json()]);
            if($response->getStatusCode() == 200){
                return $response->json();
            }
            return;
        }catch(\Exception $e){
            Log::error(["error_message" => true, "message" => $e->getMessage()]);
        }
    }

    public function post($type, $data = []){
        try{
            $response = $this->client->post("$this->url_endpoint/$type", [
                'headers' => $this->getHeaders(),
                'json' => $data
            ]);
            Log::error(["peticion" => "post", "response" => $response->json()]);
            if($response->getStatusCode() == 200){
                Log::info(["error_message" => false, "message" => "Mensaje enviado", "response" => $response->json()]);
            }
        }catch(\Exception $e){
            Log::error(["error_message" => true, "message" => $e->getMessage()]);
        }
    }

    public function getHeaders(){
        return [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
                "Authorization" => "Bearer ".$this->api_key
            ];
    }
    
}