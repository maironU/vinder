<?php namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composers([
            'App\Http\ViewComposers\MenuComposer'        => ['auth.login',
                                                                'dashboard.pages.modules',
                                                                'dashboard.pages.database.hello',
                                                                'dashboard.pages.database.voters.diaries',
                                                                'dashboard.pages.database.voters.lists.layout',
                                                                'dashboard.pages.database.voters.forms.layout',
                                                                'dashboard.pages.database.roles.*',
                                                                'dashboard.pages.database.incidents.*',
                                                                'dashboard.pages.reports.*',
                                                                'dashboard.pages.statistics.*',
                                                                'dashboard.pages.diary.*',
                                                                'dashboard.pages.logistic.*',
                                                                'dashboard.pages.advertising.*',
                                                                'dashboard.pages.sms.*',
                                                                'dashboard.pages.polls.*',
                                                                'dashboard.pages.system.*',
                                                                'dashboard.pages.maps.*',
                                                                'dashboard.pages.witnesses.*',
                                                                'dashboard.pages.procesos.*',
                                                                'dashboard.pages.whatsapi.*',
                                                            ],
            'App\Http\ViewComposers\DatabaseComposer'    => 'dashboard.pages.database.hello',

            /* Database */
            'App\Http\ViewComposers\Voter\ListComposer'          => ['dashboard.pages.database.voters.lists.voter', 
                                                                        'dashboard.pages.database.voters.lists.team',
                                                                        'dashboard.pages.diary.people'],
            'App\Http\ViewComposers\Voter\ListVoterComposer'     => ['dashboard.pages.database.voters.lists.voter',
                                                                        'dashboard.pages.database.voters.lists.search',
                                                                        'dashboard.pages.diary.people'],
            'App\Http\ViewComposers\Voter\ListTeamComposer'      => 'dashboard.pages.database.voters.lists.team',
            'App\Http\ViewComposers\Voter\PruebaComposer'          => ['dashboard.pages.database.voters.forms.voter', 
                                                                        'dashboard.pages.database.voters.forms.team'],
            'App\Http\ViewComposers\Rol\FormComposer'            => 'dashboard.pages.database.roles.lists',

            /* System */
            'App\Http\ViewComposers\Location\FormComposer'           => 'dashboard.pages.system.locations.form',
            'App\Http\ViewComposers\PollingStation\FormComposer'     => 'dashboard.pages.system.polling_stations.form',
            'App\Http\ViewComposers\Module\FormComposer'             => 'dashboard.pages.system.modules.form',
            'App\Http\ViewComposers\Sms\FormComposer'                => 'dashboard.pages.system.sms.form',
            'App\Http\ViewComposers\User\FormComposer'               => 'dashboard.pages.system.users.form',
            'App\Http\ViewComposers\UserType\FormComposer'           => 'dashboard.pages.system.user_types.form',
            'App\Http\ViewComposers\ReportStatistics\FormComposer'   => 'dashboard.pages.system.reports.form',

            /* Secundary */
            'App\Http\ViewComposers\Diary\FormComposer'              => 'dashboard.pages.diary.form',
            'App\Http\ViewComposers\Diary\ListComposer'              => ['dashboard.pages.logistic.hello',
                                                                            'dashboard.pages.advertising.hello'],
            'App\Http\ViewComposers\Statistics\ListComposer'         => 'dashboard.pages.statistics.lists',
            'App\Http\ViewComposers\Report\ListComposer'             => ['dashboard.pages.reports.lists',
                                                                            'dashboard.pages.statistics.lists'],

            /* Extra */
            'App\Http\ViewComposers\Sms\ListComposer'                => 'dashboard.pages.sms.sms',
            'App\Http\ViewComposers\Poll\ListComposer'               => 'dashboard.pages.polls.lists',
            'App\Http\ViewComposers\Poll\OptionsComposer'            => 'dashboard.pages.polls.options'
        ]);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
